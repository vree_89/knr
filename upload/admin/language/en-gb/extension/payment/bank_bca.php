<?php
// Heading
$_['heading_title']      = 'Bank BCA';

// Text 
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified bank bca details!';
$_['text_edit']          = 'Edit Bank BCA';
$_['text_bank_bca'] 	 = '<img src="view/image/payment/bca_logo.jpg" alt="Bank BCA" title="Bank BCA" /></a>';

// Entry
$_['entry_bank']         = 'Bank Transfer Instructions';
$_['entry_total']		 = 'Total';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_order_status'] = 'Order Status';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Help
$_['help_total']		 = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment bank bca!';
$_['error_bank']		 = 'Bank Transfer Instructions Required!';