<?php
// Heading
$_['heading_title']    = 'Raja Ongkir';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified flat rate shipping!';
$_['text_edit']        = 'Edit tarif JNE';

// Entry
$_['entry_cost']       = 'Cost';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_key'] = 'Rajaongkir Key';
$_['entry_default_origin'] = 'Default Origin';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify flat rate shipping!';