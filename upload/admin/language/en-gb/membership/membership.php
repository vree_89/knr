<?php
// Heading
$_['heading_title']          = 'Membership';

// Text
$_['text_success']           = 'Success: You have modified memberships!';
$_['text_list']              = 'Memberships List';
$_['text_add']               = 'Add Membership';
$_['text_edit']              = 'Edit Membership';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
// Column
$_['column_name']            = 'Membership Name';
$_['column_price']           = 'Price (IDR)';
$_['column_period']          = 'Period (days)';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Membership Name';
$_['entry_price']            = 'Price (IDR)';
$_['entry_period']           = 'Period (days)';
$_['entry_discount']         = 'Discount (%)';
$_['entry_loyalty_point']    = 'Loyalty Point (%)';
$_['entry_bonus_point']      = 'Bonus Point (%)';
$_['entry_text_color']      = 'Text color in hex code';
// $_['entry_description']      = 'Description';
// $_['entry_meta_title'] 	     = 'Meta Tag Title';
// $_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
// $_['entry_meta_description'] = 'Meta Tag Description';
// $_['entry_intro_text']       = 'Introduction Text';
$_['entry_status']           = 'Status';
$_['entry_featured']         = 'Featured';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Categories';

// Help
$_['help_category']          = '(Autocomplete)';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify memberships!';
$_['error_name']             = 'Membership Name must be greater than 3 and less than 255 characters!';
$_['error_price']            = 'Membership Price must be filled!';
$_['error_period']           = 'Membership Period must be filled!';
$_['error_discount']         = 'Membership Discount must be filled!';
$_['error_loyalty_point']	 = 'Membership Loyalty Point must be filled!';
$_['error_bonus_point']	     = 'Membership Bonus Point must be filled!';
$_['error_bonus_point']	     = 'Text Color must be filled!';
// $_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';

$_['button_setting']         = 'Settings';

