<?php
class ModelMembershipMembership extends Model {

    public function addMembership($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "membership SET name = '" . $this->db->escape($data['name']) . "', price = '" . (int)$data['price'] . "', period = '" . (int)$data['period'] . "', discount = '" . (int)$data['discount'] . "', loyalty_point = '" . (double)$data['loyalty_point'] . "', bonus_point = '" . (double)$data['bonus_point'] . "', text_color = '" . $data['text_color'] . "', featured = '" . (int)$data['featured'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW()");

		$membership_id = $this->db->getLastId();

		$this->cache->delete('membership');

		return $membership_id;
	}

	public function editMembership($membership_id, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "membership SET name = '" . $this->db->escape($data['name']) . "', price = '" . (int)$data['price'] . "', period = '" . (int)$data['period'] . "', discount = '" . (int)$data['discount'] . "', loyalty_point = '" . (double)$data['loyalty_point'] . "', bonus_point = '" . (double)$data['bonus_point'] . "', text_color = '" . $data['text_color'] . "', featured = '" . (int)$data['featured'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE membership_id = '" . (int)$membership_id . "'");

		$this->cache->delete('article');
	}

	public function copyMembership($membership_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "membership WHERE membership_id = '" . (int)$membership_id . "'");

		if ($query->num_rows) {
			$data = array();

			$data = $query->row;
            $data['status'] = '0';

			$this->addMembership($data);
		}
	}

	public function deleteMembership($membership_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "membership WHERE membership_id = '" . (int)$membership_id . "'");

		$this->cache->delete('article');
	}

	public function getMembership($membership_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "membership WHERE membership_id = '" . (int)$membership_id . "'");

		return $query->row;
	}

	public function getMemberships($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "membership where 1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}
		
		$sql .= " GROUP BY membership_id";

        $sql .= " ORDER BY sort_order ASC, date_modified DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql; exit;

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalMemberships($data = array()) {
		$sql = "SELECT COUNT(DISTINCT membership_id) AS total FROM " . DB_PREFIX . "membership where 1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}
		//echo $sql;exit;

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

}