<?php
class ModelSettingSetting extends Model {
	public function getSetting($code, $store_id = 0) {
		$setting_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$setting_data[$result['key']] = $result['value'];
			} else {
				$setting_data[$result['key']] = json_decode($result['value'], true);
			}
		}

		return $setting_data;
	}




	public function editSettingJne($code, $data, $store_id = 0) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");
		foreach ($data as $key => $value) {			
			if (substr($key, 0, strlen($code)) == $code) {				
				$sql = "INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'";
				if (!is_array($value)) {					
					$this->db->query($sql);
				} 
			}
		}

		// //insert tarif array
		// $jne_zone_from = $data['jne_zone_from'];
		// $jne_zone_to = $data['jne_zone_to'];
		// $jne_reguler = $data['jne_reguler'];
		// $jne_yes = $data['jne_yes'];
		// $arr_tarif = array();
		// $i=0;
		// foreach ($jne_zone_from as $key => $value) {
		// 	if($jne_zone_from!="" && $jne_zone_to!="" && $jne_reguler!="" && $jne_reguler!="0" && $jne_yes!="" && $jne_yes!="0"){
		// 		//echo "1<br/>";
		// 		array_push($arr_tarif, $jne_zone_from[$key]."#".$jne_zone_to[$key]."#".$jne_reguler[$key]."#".$jne_yes[$key]);
		// 	}			
		// }
		// $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '".$code."_cost', `value` = '" . $this->db->escape(json_encode($arr_tarif, true)) . "', serialized = '1'");
	

	}

	public function editSetting($code, $data, $store_id = 0) {
		//var_dump($data);exit;
		/*
		array(7) { 
		["bank_bca_bank2"]=> string(3) "asd" 
		["bank_bca_bank1"]=> string(3) "asd" 
		["bank_bca_total"]=> string(1) "1" 
		["bank_bca_order_status_id"]=> string(1) "7" 
		["bank_bca_geo_zone_id"]=> string(1) "0" 
		["bank_bca_status"]=> string(1) "1" 
		["bank_bca_sort_order"]=> string(1) "1" 
		}
		*/
		// var_dump($data);exit;
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");
		foreach ($data as $key => $value) {
			if (substr($key, 0, strlen($code)) == $code) {
					if (!is_array($value)) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
				}
			}
		}
	}

	public function deleteSetting($code, $store_id = 0) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");
	}
	
	public function getSettingValue($key, $store_id = 0) {
		$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

		if ($query->num_rows) {
			return $query->row['value'];
		} else {
			return null;	
		}
	}
	
	public function editSettingValue($code = '', $key = '', $value = '', $store_id = 0) {
		if (!is_array($value)) {
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($value) . "', serialized = '0'  WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1' WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
		}
	}
}
