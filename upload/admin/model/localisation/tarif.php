<?php
class ModelLocalisationTarif extends Model {
	public function addTarif($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "tarif_jne SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', country_id = '" . (int)$data['country_id'] . "'");

		$this->cache->delete('zone');
		
		return $this->db->getLastId();
	}

	public function editTarif($zone_id, $data, $code,$store_id=0) {
		//$this->db->query("DELETE FROM `" . DB_PREFIX . "tarif_jne`");
		// $this->cache->delete('zone');
		$zone_from = $data['zone_from'];
		$zone_to = $data['zone_to'];
		$reguler = $data['reguler'];
		$yes = $data['yes'];
		foreach ($zone_from as $key => $value) {
			$sql = "INSERT INTO `" . DB_PREFIX . "tarif_jne` (zone_from, zone_to, reguler, yes) values('".$zone_from[$key]."','".$zone_to[$key]."','".$reguler[$key]."','".$yes[$key]."')";
			//echo $sql."<br/>";
			$this->db->query($sql);
		}


		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = 'jne'");
		foreach ($data as $key => $value) {
			if (substr($key, 0, strlen($code)) == $code) {
					if (!is_array($value)) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = 'jne', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = 'jne', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
				}
			}
		}


	}

	public function deleteTarif($zone_from,$zone_to,$reguler,$yes) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "tarif_jne WHERE zone_from = '" . $zone_from . "' and zone_to = '" . $zone_to . "' and reguler = '" . $reguler . "' and yes = '" . $yes . "'");

		$this->cache->delete('zone');
	}

	public function getTarif($id_tarif_jne) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "zone WHERE id_tarif_jne = '" . (int)$id_tarif_jne . "'");

		return $query->row;
	}


	public function getTarifs($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "tarif_jne";



			$sql .= " ORDER BY zone_from";

			$sql .= " ASC";


		$query = $this->db->query($sql);

		return $query->rows;
	}



	public function getTotalTarifs() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "tarif_jne");

		return $query->row['total'];
	}


}