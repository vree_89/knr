<?php
class ModelExtensionShippingRajaongkir extends Model {

	function getQuote($address){
		$method_data = array();
		$zone_id = $address['zone_id'] ;
		$country_id = $address['country_id'];
		$weight = round($this->cart->getWeight()*1000);			
		if($weight<1000){
			$weight = 1000; //in gr
		}
		$origin=151;
		$destination = $this->session->data['shipping_address']['id_city'];
		$sql = "SELECT `key`, `value` from " . DB_PREFIX . "setting where `code` = 'jne'";
		$jne = $this->db->query($sql);
		$jne_key = "";
		$jne_default_origin = "";
		foreach ($jne->rows as $result) {
			if($result['key']=="jne_key"){
				$jne_key = $result['value'];
				
			}
			if($result['key']=="jne_default_origin"){
				$jne_default_origin = $result['value'];
				
			}

		}
		//echo $key;


		// exit;
		// var_dump($jne);exit;
		//echo $weight;exit;
		//die($weight);
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  origin* - id kota asal (POST)
		  destination* - id kota tujuan (POST)
		  weight* - berat dalam gram (POST)
		  courier* - kode courer:jne, pos, tiki (POST)
		*/ 

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=jne",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return false;
		} else {
			$jne = json_decode($response,true);
			$jne_services = explode("",$jne);
			$arr_service_jne = array('OKE','REG','CTC','CTCSPS','CTCYES');
			$status = $jne['rajaongkir']['status']['code']; //200 = OK
			//echo $status;exit;
			if($status=="200"){
				$method_data = array(
					'title' => 'JNE',
					'quote' => array(),
					'sort_order' => '1',
					'error' => false,
					);
				$res = $jne['rajaongkir']['results']; //result of type of costs yes, regular, oke
				foreach ($res as $_res) {
					foreach($_res['costs'] as $costs){
						foreach($costs['cost'] as $_cost){
							if(in_array($costs['service'], $arr_service_jne)){
							$method_data['quote'][strtolower($costs['service'])]['code'] = $_res['code'].".".strtolower($costs['service']);
							$method_data['quote'][strtolower($costs['service'])]['title'] = 'JNE '.$costs['service'];
							$method_data['quote'][strtolower($costs['service'])]['cost'] = $_cost['value'];
							$method_data['quote'][strtolower($costs['service'])]['tax_class_id'] = $this->config->get('jne_tax_class_id');
							$method_data['quote'][strtolower($costs['service'])]['text'] = $this->currency->format($this->tax->calculate($_cost['value'], $this->config->get('jne_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']);
							}
						}
					}
				}
			}
			return $method_data;
		  // var_dump($method_data) ;
		  // exit;
		}



	}


	function getCity(){

		$sql = "SELECT `key`, `value` from " . DB_PREFIX . "setting where `code` = 'jne'";
		$jne = $this->db->query($sql);
		$jne_key = "";
		foreach ($jne->rows as $result) {
			if($result['key']=="jne_key"){
				$jne_key = $result['value'];				
			}
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return false;
		} else {
		  return json_decode($response,true);
		}		
	}
	//function getQuote($address) {
	// 	//var_dump($address);
	// 	$this->load->language('shipping/jne');

	// 	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('jne_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

	// 	if (!$this->config->get('jne_geo_zone_id')) {
	// 		$status = true;
	// 	} elseif ($query->num_rows) {
	// 		$status = true;
	// 	} else {
	// 		$status = false;
	// 	}

	// 	$method_data = array();

	// 	if ($status) {
	// 		$jne_cost_reg = 0;
	// 		$jne_cost_yes = 0;
	// 		$weight = $this->cart->getWeight();
	// 		$rates = $this->config->get('jne_cost');
	// 		foreach ($rates as $rate) {

	// 			$data = explode('#', $rate);
	// 			$from = $data[0];
	// 			$to = $data[1];
	// 			$reguler = $data[2];
	// 			$yes = $data[3];
	// 			if ($address['zone'] == $to) {
	// 				$hitung = number_format($weight, 2, '.', ',');
	// 				$berat = substr($hitung, 0, -2);
	// 				$desimal = $weight - $berat;
	// 				if ($weight <= 1.0){
	// 					$jne_cost_reg = $data[2];
	// 					$jne_cost_yes = $data[3];
	// 				}elseif($desimal < 0.3){
	// 					$jne_cost_reg = $berat*$data[2];
	// 					$jne_cost_yes = $berat*$data[3];
	// 				}else{
	// 					$jne_cost_reg = ($berat+1)* $data[2];
	// 					$jne_cost_yes = ($berat+1)* $data[3];
	// 				}
                                       
	// 				break;
	// 			}

	// 		}
		
	// 		$quote_data = array();
	// 		//echo $jne_cost;exit;
	// 		if($jne_cost_reg == 0 || $jne_cost_yes == 0){ // kontrol
	// 			$quote_data['jne'] = array(
	// 				'reg'=>array(
	// 				'code'           => 'jne.reg',
	// 				'title'        => $this->language->get('text_shipping_not_available'),
	// 				'cost'         => $jne_cost_reg,
	// 				'tax_class_id' => $this->config->get('jne_tax_class_id'),
	// 				'text'         => $this->language->get('text_cost_zero')
	// 			),
	// 				'reg'=>array(
	// 				'code'           => 'jne.yes',
	// 				'title'        => $this->language->get('text_shipping_not_available'),
	// 				'cost'         => $jne_cost_yes,
	// 				'tax_class_id' => $this->config->get('jne_tax_class_id'),
	// 				'text'         => $this->language->get('text_cost_zero')
	// 			)
	// 				);

	// 			$method_data = array(
	// 				'code'         => 'jne',
	// 				'title'      => $this->language->get('text_title'),
	// 				'quote'      => $quote_data,
	// 				'sort_order' => $this->config->get('jne_sort_order'),
	// 				'error'      => false
	// 			);
	// 		}
	// 		else{ // kontrol
	// 			// $quote_data['jne'] = array(
	// 			// 	'code'         => 'jne.jne',
	// 			// 	'title'        => $this->language->get('text_description'),
	// 			// 	'cost'         => $jne_cost,
	// 			// 	'tax_class_id' => $this->config->get('jne_tax_class_id'),
	// 			// 	//'text'         => $this->currency->format($this->tax->calculate($jne_cost, $this->config->get('jne_tax_class_id'), $this->config->get('config_tax')))
	// 			// 	'text' => 		$this->currency->format($this->tax->calculate($jne_cost, $this->config->get('jne_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
	// 			// );

	// 			$quote_data['jne'] = array(
	// 				'reg'=> 				array(
	// 					'code'           => 'jne.reg',
	// 					'title'        => 'JNE REG',
	// 					'cost'         => $jne_cost_reg,
	// 					'tax_class_id' => $this->config->get('jne_tax_class_id'),
	// 					'text'         => $this->currency->format($this->tax->calculate($jne_cost_reg, $this->config->get('jne_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
	// 				),
	// 				'yes'=>				array(
	// 					'code'           => 'jne.yes',
	// 					'title'        => 'JNE YES',
	// 					'cost'         => $jne_cost_yes,
	// 					'tax_class_id' => $this->config->get('jne_tax_class_id'),
	// 					'text'         => $this->currency->format($this->tax->calculate($jne_cost_yes, $this->config->get('jne_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
	// 				)

	// 			);				
	// 			$method_data = array(
	//         		'code'         => 'jne',
	//         		'title'      => $this->language->get('text_title'),
	//         		'quote'      => $quote_data,
	// 				'sort_order' => $this->config->get('jne_sort_order'),
	//         		'error'      => FALSE
	//       		);
	// 		}
	// }

	// 	return $method_data;
	//}
}























				// $data = explode(',', $rate);
				// if ($address['country_id'] == $data[0]) {
				// 	$pos = strpos(strtolower($data[1]),strtolower($address['city']));
				// 	if ($pos !== false) {
				// 		$hitung = number_format($weight, 2, '.', ',');
				// 		$berat = substr($hitung, 0, -2);
				// 		$desimal = $weight - $berat;
				// 		if ($weight <= 1.0){
				// 			$jne_cost = $data[2];
				// 		}elseif($desimal < 0.3){
				// 			$jne_cost = $berat*$data[2];
				// 		}else{
				// 			$jne_cost = ($berat+1)* $data[2];
				// 		}
                                           
				// 		break;
				// 	}

				// }