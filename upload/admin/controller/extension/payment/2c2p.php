<?php
class ControllerExtensionPayment2c2p extends Controller {
  private $error = array();
 
  public function index() {
    $this->language->load('extension/payment/2c2p');
    $this->document->setTitle('Custom Payment Method Configuration');
    $this->load->model('setting/setting');
 
    if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
      $this->model_setting_setting->editSetting('2c2p', $this->request->post);
      $this->session->data['success'] = 'Saved.';
      $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
    }
 
    $data['heading_title'] = $this->language->get('heading_title');
    $data['text_edit'] = $this->language->get('text_edit');
    $data['entry_text_config_one'] = $this->language->get('text_config_one');
    $data['entry_text_config_two'] = $this->language->get('text_config_two');
    $data['button_save'] = $this->language->get('text_button_save');
    $data['button_cancel'] = $this->language->get('text_button_cancel');
    $data['entry_order_status'] = $this->language->get('entry_order_status');
    $data['text_enabled'] = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');
    $data['entry_status'] = $this->language->get('entry_status');
 
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_extension'),
      'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('extension/payment/2c2p', 'token=' . $this->session->data['token'], true)
    );



    $data['action'] = $this->url->link('payment/2c2p', 'token=' . $this->session->data['token'], 'SSL');
    $data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
 
    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }


    if (isset($this->request->post['text_config_one'])) {
      $data['text_config_one'] = $this->request->post['text_config_one'];
    } else {
      $data['text_config_one'] = $this->config->get('text_config_one');
    }
        
    if (isset($this->request->post['text_config_two'])) {
      $data['text_config_two'] = $this->request->post['text_config_two'];
    } else {
      $data['text_config_two'] = $this->config->get('text_config_two');
    }
            
    if (isset($this->request->post['status'])) {
      $data['status'] = $this->request->post['status'];
    } else {
      $data['status'] = $this->config->get('status');
    }
        
    if (isset($this->request->post['order_status_id'])) {
      $data['order_status_id'] = $this->request->post['order_status_id'];
    } else {
      $data['order_status_id'] = $this->config->get('order_status_id');
    }
 
    $this->load->model('localisation/order_status');
    $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
    $this->template = 'payment/2c2p.tpl';
            
    // $this->children = array(
    //   'common/header',
    //   'common/footer'
    // );
    //$this->response->setOutput($this->render());
    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');
    $this->response->setOutput($this->load->view('extension/payment/2c2p', $data));

    
  }
}