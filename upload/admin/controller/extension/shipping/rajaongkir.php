<?php
class ControllerExtensionShippingRajaongkir extends Controller {
	private $error = array();
	public function remove(){
		$zone_from =  $this->request->post['zone_from'];
		$zone_to =  $this->request->post['zone_to'];
		$reguler =  $this->request->post['reguler'];
		$yes =  $this->request->post['yes'];

		$this->load->model('localisation/tarif');
		$this->model_localisation_tarif->deleteTarif($zone_from,$zone_to,$reguler,$yes);
		
	}




	public function index() {
		$this->load->language('extension/shipping/rajaongkir');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$this->load->model('localisation/zone');
		$this->load->model('localisation/tarif');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			//$this->model_setting_setting->editSettingrajaongkir('rajaongkir', $this->request->post);
			$this->model_setting_setting->editSetting('rajaongkir', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/shipping/rajaongkir', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['tarifs'] = $this->model_localisation_tarif->getTarifs();
		$data['zones'] = $this->model_localisation_zone->getCities('Indonesia');
		//var_dump($data['zones'] );exit;

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');

		$data['entry_cost'] = $this->language->get('entry_cost');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_key'] = $this->language->get('entry_key');
		$data['entry_default_origin'] = $this->language->get('entry_default_origin');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/shipping/rajaongkir', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/rajaongkir', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('extension/shipping/rajaongkir', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL');

		$this->load->model('extension/shipping/rajaongkir');
		$data['cities'] = $this->model_extension_shipping_rajaongkir->getCity()['rajaongkir']['results'];


		function cmp($a, $b)
		{
		   return strcmp($a["province"], $b["province"]);
		}

		usort($data['cities'], "cmp");		
		//var_dump($data['cities']);exit;

		if (isset($this->request->post['rajaongkir_cost'])) {
			$data['rajaongkir_cost'] = $this->request->post['rajaongkir_cost'];
		} else {
			$data['rajaongkir_cost'] = $this->config->get('rajaongkir_cost');
		}
		// var_dump ($data['rajaongkir_cost']);
		// exit;
		if (isset($this->request->post['rajaongkir_tax_class_id'])) {
			$data['rajaongkir_tax_class_id'] = $this->request->post['rajaongkir_tax_class_id'];
		} else {
			$data['rajaongkir_tax_class_id'] = $this->config->get('rajaongkir_tax_class_id');
		}


		$this->load->model('localisation/tax_class');

		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['rajaongkir_geo_zone_id'])) {
			$data['rajaongkir_geo_zone_id'] = $this->request->post['rajaongkir_geo_zone_id'];
		} else {
			$data['rajaongkir_geo_zone_id'] = $this->config->get('rajaongkir_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['rajaongkir_status'])) {
			$data['rajaongkir_status'] = $this->request->post['rajaongkir_status'];
		} else {
			$data['rajaongkir_status'] = $this->config->get('rajaongkir_status');
		}

		if (isset($this->request->post['rajaongkir_sort_order'])) {
			$data['rajaongkir_sort_order'] = $this->request->post['rajaongkir_sort_order'];
		} else {
			$data['rajaongkir_sort_order'] = $this->config->get('rajaongkir_sort_order');
		}


		if (isset($this->request->post['rajaongkir_key'])) {
			$data['rajaongkir_key'] = $this->request->post['rajaongkir_key'];
		} else {
			$data['rajaongkir_key'] = $this->config->get('rajaongkir_key');
		}

		if (isset($this->request->post['rajaongkir_services'])) {
			$data['rajaongkir_services'] = $this->request->post['rajaongkir_services'];
		} else {
			$data['rajaongkir_services'] = $this->config->get('rajaongkir_services');
		}		


		if (isset($this->request->post['rajaongkir_default_origin'])) {
			$data['rajaongkir_default_origin'] = $this->request->post['rajaongkir_default_origin'];
		} else {
			$data['rajaongkir_default_origin'] = $this->config->get('rajaongkir_default_origin');
		}
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/rajaongkir.tpl', $data));
	}

	protected function validate() {
		//echo $this->request->post['rajaongkir_services'];exit;
		if (!$this->user->hasPermission('modify', 'extension/shipping/rajaongkir')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	public function city(){
		$this->load->model('extension/shipping/rajaongkir');
		$val = $this->request->post['val'];
		$is_kabupaten = strpos(strtolower($val), 'kabupaten');

		$data['cities'] = $this->model_extension_shipping_rajaongkir->getCity()['rajaongkir']['results'];
		echo $val; exit;
	}
}