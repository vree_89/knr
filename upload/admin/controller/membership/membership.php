<?php
class ControllerMembershipMembership extends Controller {
	private $error = array();

   	public function index() {
		$this->load->language('membership/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('membership/membership');

		$this->getList();
	}

	public function add() {
		$this->load->language('membership/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('membership/membership');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_membership_membership->addMembership($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('membership/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('membership/membership');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_membership_membership->editMembership($this->request->get['membership_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('membership/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('membership/membership');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $membership_id) {
				$this->model_membership_membership->deleteMembership($membership_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function copy() {
		$this->load->language('membership/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('membership/membership');

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $membership_id) {
				$this->model_membership_membership->copyMembership($membership_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_modified';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url, true)
		);

        // $data['setting'] = $this->url->link('blog/setting', 'token=' . $this->session->data['token'] . $url, true);
		$data['add'] = $this->url->link('membership/membership/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['copy'] = $this->url->link('membership/membership/copy', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('membership/membership/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['memberships'] = array();

		$filter_data = array(
			'filter_name'	  => $filter_name,
			'filter_status'   => $filter_status,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		//$this->load->model('tool/image');

		$membership_total = $this->model_membership_membership->getTotalMemberships($filter_data);

		$results = $this->model_membership_membership->getMemberships($filter_data);

		foreach ($results as $result) {
			$data['memberships'][] = array(
				'membership_id' => $result['membership_id'],
				'name'       	=> $result['name'],
				'price'       	=> $result['price'],
				'period'       	=> $result['period'],
				'discount'      => $result['discount'],
				'loyalty_point' => $result['loyalty_point'],
				'bonus_point'   => $result['bonus_point'],
				'status'     	=> ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'       	=> $this->url->link('membership/membership/edit', 'token=' . $this->session->data['token'] . '&membership_id=' . $result['membership_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_period'] = $this->language->get('column_period');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_period'] = $this->language->get('entry_period');
		$data['entry_discount'] = $this->language->get('entry_discount');
		$data['entry_loyalty_point'] = $this->language->get('entry_loyalty_point');
		$data['entry_bonus_point'] = $this->language->get('entry_bonus_point');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_copy'] = $this->language->get('button_copy');
		$data['button_add'] = $this->language->get('button_add');
        $data['button_setting'] = $this->language->get('button_setting');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('membership/membership', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_status'] = $this->url->link('membership/membership', 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);
		$data['sort_order'] = $this->url->link('membership/membership', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $membership_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($membership_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($membership_total - $this->config->get('config_limit_admin'))) ? $membership_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $membership_total, ceil($membership_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_status'] = $filter_status;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('membership/membership_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['membership_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_period'] = $this->language->get('entry_period');
		$data['entry_discount'] = $this->language->get('entry_discount');
		$data['entry_loyalty_point'] = $this->language->get('entry_loyalty_point');
		$data['entry_bonus_point'] = $this->language->get('entry_bonus_point');
		$data['entry_text_color'] = $this->language->get('entry_text_color');
		// $data['entry_description'] = $this->language->get('entry_description');
  //       $data['entry_category'] = $this->language->get('entry_category');
		// $data['entry_meta_title'] = $this->language->get('entry_meta_title');
		// $data['entry_meta_description'] = $this->language->get('entry_meta_description');
		// $data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		// $data['entry_intro_text'] = $this->language->get('entry_intro_text');
        $data['entry_featured'] = $this->language->get('entry_featured');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['help_category'] = $this->language->get('help_category');
		$data['help_intro_text'] = $this->language->get('help_intro_text');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_remove'] = $this->language->get('button_remove');

		$data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['price'])) {
			$data['error_price'] = $this->error['price'];
		} else {
			$data['error_price'] = '';
		}

		if (isset($this->error['period'])) {
			$data['error_period'] = $this->error['period'];
		} else {
			$data['error_period'] = '';
		}

		if (isset($this->error['discount'])) {
			$data['error_discount'] = $this->error['discount'];
		} else {
			$data['error_discount'] = '';
		}

		if (isset($this->error['loyalty_point'])) {
			$data['error_loyalty_point'] = $this->error['loyalty_point'];
		} else {
			$data['error_loyalty_point'] = '';
		}

		if (isset($this->error['bonus_point'])) {
			$data['error_bonus_point'] = $this->error['bonus_point'];
		} else {
			$data['error_bonus_point'] = '';
		}

		if (isset($this->error['text_color'])) {
			$data['error_text_color'] = $this->error['text_color'];
		} else {
			$data['error_text_color'] = '';
		}

		// if (isset($this->error['meta_title'])) {
		// 	$data['error_meta_title'] = $this->error['meta_title'];
		// } else {
		// 	$data['error_meta_title'] = array();
		// }

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['membership_id'])) {
			$data['action'] = $this->url->link('membership/membership/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('membership/membership/edit', 'token=' . $this->session->data['token'] . '&membership_id=' . $this->request->get['membership_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('membership/membership', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['membership_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$membership_info = $this->model_membership_membership->getMembership($this->request->get['membership_id']);
		}

		$data['token'] = $this->session->data['token'];

		// $this->load->model('localisation/language');

		// $data['languages'] = $this->model_localisation_language->getLanguages();

		// if (isset($this->request->post['article_description'])) {
		// 	$data['article_description'] = $this->request->post['article_description'];
		// } elseif (isset($this->request->get['article_id'])) {
		// 	$data['article_description'] = $this->model_blog_article->getArticleDescriptions($this->request->get['article_id']);
		// } else {
		// 	$data['article_description'] = array();
		// }

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($membership_info)) {
			$data['name'] = $membership_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['price'])) {
			$data['price'] = $this->request->post['price'];
		} elseif (!empty($membership_info)) {
			$data['price'] = $membership_info['price'];
		} else {
			$data['price'] = '';
		}

		if (isset($this->request->post['period'])) {
			$data['period'] = $this->request->post['period'];
		} elseif (!empty($membership_info)) {
			$data['period'] = $membership_info['period'];
		} else {
			$data['period'] = '';
		}

		if (isset($this->request->post['discount'])) {
			$data['discount'] = $this->request->post['discount'];
		} elseif (!empty($membership_info)) {
			$data['discount'] = $membership_info['discount'];
		} else {
			$data['discount'] = '';
		}

		if (isset($this->request->post['loyalty_point'])) {
			$data['loyalty_point'] = $this->request->post['loyalty_point'];
		} elseif (!empty($membership_info)) {
			$data['loyalty_point'] = $membership_info['loyalty_point'];
		} else {
			$data['loyalty_point'] = '';
		}

		if (isset($this->request->post['bonus_point'])) {
			$data['bonus_point'] = $this->request->post['bonus_point'];
		} elseif (!empty($membership_info)) {
			$data['bonus_point'] = $membership_info['bonus_point'];
		} else {
			$data['bonus_point'] = '';
		}


		if (isset($this->request->post['text_color'])) {
			$data['text_color'] = $this->request->post['text_color'];
		} elseif (!empty($membership_info)) {
			$data['text_color'] = $membership_info['text_color'];
		} else {
			$data['text_color'] = '';
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($membership_info)) {
			$data['sort_order'] = $membership_info['sort_order'];
		} else {
			$data['sort_order'] = 1;
		}

		if (isset($this->request->post['featured'])) {
			$data['featured'] = $this->request->post['featured'];
		} elseif (!empty($membership_info)) {
			$data['featured'] = $membership_info['featured'];
		} else {
			$data['featured'] = true;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($membership_info)) {
			$data['status'] = $membership_info['status'];
		} else {
			$data['status'] = true;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('membership/membership_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'membership/membership')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 255)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (empty($this->request->post['price'])) {
			$this->error['price'] = $this->language->get('error_price');
		}

		if (empty($this->request->post['period'])) {
			$this->error['period'] = $this->language->get('error_period');
		}

		if (empty($this->request->post['discount'])) {
			$this->error['discount'] = $this->language->get('error_discount');
		}

		if (empty($this->request->post['loyalty_point'])) {
			$this->error['loyalty_point'] = $this->language->get('error_loyalty_point');
		}

		if (empty($this->request->post['bonus_point'])) {
			$this->error['bonus_point'] = $this->language->get('error_bonus_point');
		}
		if (empty($this->request->post['text_color'])) {
			$this->error['text_color'] = $this->language->get('error_text_color');
		}

		// foreach ($this->request->post['article_description'] as $language_id => $value) {
		// 	if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
		// 		$this->error['name'][$language_id] = $this->language->get('error_name');
		// 	}

		// 	if ((utf8_strlen($value['meta_title']) < 3) || (utf8_strlen($value['meta_title']) > 255)) {
		// 		$this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
		// 	}
		// }

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'membership/membership')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'membership/membership')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) ) {
			$this->load->model('membership/membership');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_membership_membership->getMemberships($filter_data);

			foreach ($results as $result) {
				$option_data = array();

				$json[] = array(
					'membership_id' => $result['membership_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}