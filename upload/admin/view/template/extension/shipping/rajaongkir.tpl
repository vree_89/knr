<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-rajaongkir" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action?>" method="post" enctype="multipart/form-data" id="form-rajaongkir" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
            <div class="col-sm-10">
              <select name="rajaongkir_tax_class_id" id="input-tax-class" class="form-control">
                <option value="0"><?php echo $text_none; ?></option>
                <?php foreach ($tax_classes as $tax_class) { ?>
                <?php if ($tax_class['tax_class_id'] == $rajaongkir_tax_class_id) { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="rajaongkir_status" id="input-status" class="form-control">
                <?php if ($rajaongkir_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>



          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="rajaongkir_sort_order" value="<?php echo $rajaongkir_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>


          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_key; ?></label>
            <div class="col-sm-10">
              <input type="text" name="rajaongkir_key" value="<?php echo $rajaongkir_key; ?>" placeholder="<?php echo $entry_key; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Country</label>
            <div class="col-sm-10">
              <select name="rajaongkir_country" class="form-control">
                <option value="id">Indonesia</option>
                <option value="sg">Singapore</option>
                <option value="my">Malaysia</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_default_origin; ?></label>
            <div class="col-sm-10">
              <select name="rajaongkir_default_origin" class="form-control rajaongkir_default_origin"  >
                <option value="">----</option>
                <?php
                foreach ($cities as $city) {
                  ?>
                                  <option  value="<?php echo $city['city_id']?>" <?php if ($city['city_id'] == $rajaongkir_default_origin) echo 'selected';?>><?php echo $city['province']." - ".$city['type']." ".$city['city_name'];?></option>

                  <?php
                }
                ?>
              </select>
            </div>
          </div>



          <?php
          $arr_services = explode(",",$rajaongkir_services);
          ?>
    <div class="form-group">
      <label for="basic2" class="col-lg-2 control-label">Services</label>

      <div class="col-lg-10">
        <input type="hidden" name="rajaongkir_services" id="rajaongkir_services" value="<?php echo $rajaongkir_services?>">
        <select id="basic2" class="show-tick rajaongkir_services" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" onchange="$('#rajaongkir_services').val($(this).val());">
          <optgroup label="JNE">
            <option <?php echo in_array('JNE CTC',$arr_services)? 'selected' : ''?>>JNE CTC</option>
            <option <?php echo in_array('JNE CTCSPS',$arr_services)? 'selected' : ''?>>JNE CTCSPS</option>
            <option <?php echo in_array('JNE CTCYES',$arr_services)? 'selected' : ''?>>JNE CTCYES</option>
            <option <?php echo in_array('JNE REG',$arr_services)? 'selected' : ''?>>JNE REG</option>
            <option <?php echo in_array('JNE OKE',$arr_services)? 'selected' : ''?>>JNE OKE</option>
            <option <?php echo in_array('jJNEne YES',$arr_services)? 'selected' : ''?>>JNE YES</option>
          </optgroup>
          <optgroup label="Pos Indonesia" >
            <option <?php echo in_array('POS Paket Kilat Khusus',$arr_services)? 'selected' : ''?>>POS Paket Kilat Khusus</option>
            <option <?php echo in_array('POS Express Sameday Barang',$arr_services)? 'selected' : ''?>>POS Express Sameday Barang</option>
            <option <?php echo in_array('POS Express Next Day Barang',$arr_services)? 'selected' : ''?>>POS Express Next Day Barang</option>
          </optgroup>
        </select>
      </div>
    </div>

          <?php
          /*
          <table id="zone-to-geo-zone" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left">From</td>
                <td class="text-left">To</td>
                <td class="text-left">Reguler</td>
                <td class="text-left">YES</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php
              if(count($rajaongkir_cost)>0){

                $zone_to_geo_zone_row = 1;
                foreach($rajaongkir_cost as $_rajaongkir_cost){
                  $exp = explode("#",$_rajaongkir_cost);
                  ?>
                  
                  <tr id="zone-to-geo-zone-row<?php echo $zone_to_geo_zone_row; ?>">
                  <td class="text-left">
                  <select name="rajaongkir_zone_from[]" class="form-control" required="required">
                  <?php foreach ($zones as $zone1) { ?>
                  <option value="<?php echo $zone1['city']; ?>" <?php echo $zone1['city']==$exp[0]? 'selected="selected"' : '' ?>><?php echo addslashes($zone1['city']) ; ?></option>
                  <?php } ?>
                  </select>
                  </td>
                  <td class="text-left">
                  <select name="rajaongkir_zone_to[]" class="form-control" required="required">
                  <?php foreach ($zones as $zone2) { ?>
                  <option value="<?php echo $zone2['city']; ?>" <?php echo $zone2['city']==$exp[1]? 'selected="selected"' : '' ?>><?php echo addslashes($zone2['city']) ; ?></option>
                  <?php } ?>
                  </select>
                  </td>
                  <td class="text-left"><input type="text" name="rajaongkir_reguler[]" class="form-control" value="<?php echo $exp[2]?>" required="required"></td>
                  <td class="text-left"><input type="text" name="rajaongkir_yes[]" class="form-control" value="<?php echo $exp[3]?>" required="required"></td>
                  <td class="text-left"><button type="button" onclick="$('#zone-to-geo-zone-row<?php echo $zone_to_geo_zone_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  <?php
                   $zone_to_geo_zone_row++;
                }
              }
              else{
                ?>
                <?php $zone_to_geo_zone_row = 1; ?>
                <tr id="zone-to-geo-zone-row<?php echo $zone_to_geo_zone_row; ?>">
                <td class="text-left">
                <select name="rajaongkir_zone_from[]" class="form-control" required="required">
                <?php foreach ($zones as $zone1) { ?>
                <option value="<?php echo $zone1['city']; ?>"><?php echo addslashes($zone1['city']); ?></option>
                <?php } ?>
                </select>
                </td>
                <td class="text-left">
                <select name="rajaongkir_zone_to[]" class="form-control" required="required">
                <?php foreach ($zones as $zone2) { ?>
                <option value="<?php echo $zone2['city']; ?>"><?php echo addslashes($zone2['city']); ?></option>
                <?php } ?>
                </select>
                </td>
                <td class="text-left"><input type="text" name="rajaongkir_reguler[]" class="form-control" required="required"></td>
                <td class="text-left"><input type="text" name="rajaongkir_yes[]" class="form-control" required="required"></td>
                <td class="text-left"><button type="button" onclick="$('#zone-to-geo-zone-row<?php echo $zone_to_geo_zone_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                </tr>
                <?php
              }
              ?>

            </tbody>
            <tfoot>
              <tr>
                <td colspan="4"></td>
                <td class="text-left"><button type="button" onclick="addGeoZone();" data-toggle="tooltip" title="<?php echo $button_geo_zone_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </table>
          */

          ?>



        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function setVal(val){
  alert(val);
  $.ajax({
    url: 'index.php?route=extension/shipping/rajaongkir/city&token=<?php echo $token; ?>',
    data:{ val:val},
    type:"post",
    success:function(data){
      alert(data);
    }

  });

}
    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 100
    });
//var zone_to_geo_zone_row = <?php echo $zone_to_geo_zone_row; ?>;

// function addGeoZone() {
//   html  = '<tr id="zone-to-geo-zone-row' + zone_to_geo_zone_row + '">';
//   html += '  <td class="text-left"><select name="rajaongkir_zone_from[]" class="form-control" required="required">';
//   <?php foreach ($zones as $zone3) { ?>
//   html += '    <option value="<?php echo $zone3['city']; ?>"><?php echo addslashes($zone3['city']); ?></option>';
//   <?php } ?>   
//   html += '</select></td>';

//   html += '  <td class="text-left"><select name="rajaongkir_zone_to[]" class="form-control" required="required">';
//   <?php foreach ($zones as $zone4) { ?>
//   html += '    <option value="<?php echo $zone4['city']; ?>"><?php echo addslashes($zone4['city']); ?></option>';
//   <?php } ?>   
//   html += '</select></td>';

// html+='<td class="text-left"><input type="text" name="rajaongkir_reguler[]" class="form-control" required="required"></td>';
// html+='<td class="text-left"><input type="text" name="rajaongkir_yes[]" class="form-control" required="required"></td>';
//   html += '  <td class="text-left"><button type="button" onclick="$(\'#zone-to-geo-zone-row' + zone_to_geo_zone_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
//   html += '</tr>';
  
//   $('#zone-to-geo-zone tbody').append(html);
  
//   $('zone_to_geo_zone[' + zone_to_geo_zone_row + '][country_id]').trigger();
      
//   zone_to_geo_zone_row++;
// }

// function removeTarif(zone_from, zone_to, reguler, yes, num){
//   if(confirm('Hapus tarif ini?')){
//     $.ajax({
//       url: 'index.php?route=extension/shipping/rajaongkir/remove&token=<?php echo $token; ?>',
//       data:{
//         zone_from:zone_from,
//         zone_to:zone_to,
//         reguler:reguler,
//         yes:yes,
//       },
//       type:'post',
//       success: function(data) {
//         $('tr#row_tarif_'+num).remove();
//       }
//     });

//   }  
//   return false;
// }

</script>
<style type="text/css">
select.rajaongkir_default_origin:hover, select.rajaongkir_services:hover{
  background: none;
}
</style>
<?php echo $footer; ?> 