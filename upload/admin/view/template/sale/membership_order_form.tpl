<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-membership" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-membership" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo isset($name) ? $name : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
            <div class="col-sm-10">
              <input type="text" name="price" value="<?php echo isset($price) ? $price : ''; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              <?php if ($error_price) { ?>
              <div class="text-danger"><?php echo $error_price; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-period"><?php echo $entry_period; ?></label>
            <div class="col-sm-10">
              <input type="text" name="period" value="<?php echo isset($period) ? $period : ''; ?>" placeholder="<?php echo $entry_period; ?>" id="input-period" class="form-control" />
              <?php if ($error_period) { ?>
              <div class="text-danger"><?php echo $error_period; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-discount"><?php echo $entry_discount; ?></label>
            <div class="col-sm-10">
              <input type="text" name="discount" value="<?php echo isset($discount) ? $discount : ''; ?>" placeholder="<?php echo $entry_period; ?>" id="input-discount" class="form-control" />
              <?php if ($error_discount) { ?>
              <div class="text-danger"><?php echo $error_discount; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-loyalty_point"><?php echo $entry_loyalty_point; ?></label>
            <div class="col-sm-10">
              <input type="text" name="loyalty_point" value="<?php echo isset($loyalty_point) ? $loyalty_point : ''; ?>" placeholder="<?php echo $entry_loyalty_point; ?>" id="input-loyalty_point" class="form-control" />
              <?php if ($error_loyalty_point) { ?>
              <div class="text-danger"><?php echo $error_loyalty_point; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-bonus_point"><?php echo $entry_bonus_point; ?></label>
            <div class="col-sm-10">
              <input type="text" name="bonus_point" value="<?php echo isset($bonus_point) ? $bonus_point : ''; ?>" placeholder="<?php echo $entry_bonus_point; ?>" id="input-bonus_point" class="form-control" />
              <?php if ($error_bonus_point) { ?>
              <div class="text-danger"><?php echo $error_bonus_point; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
                <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
                <select name="status" id="input-status" class="form-control">
                    <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-featured"><?php echo $entry_featured; ?></label>
            <div class="col-sm-10">
                <select name="featured" id="input-featured" class="form-control">
                    <?php if ($featured) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
		// $('#language a:first').tab('show');
		// $('#option a:first').tab('show');
//--></script> 
</div>
<!-- <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script> -->
<!-- <link href="view/javascript/summernote/summernote.css" rel="stylesheet" /> -->
<!-- <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script> -->
<?php echo $footer; ?> 