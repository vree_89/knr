<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>

<body>


<div class="container bg print">
  <br/>
  <br/>
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always;">
    <h1><?php echo $text_invoice; ?> #<?php echo $order['order_id']; ?></h1>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td colspan="2"><?php echo $text_order_detail; ?></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width: 50%;"><address>
            <strong><?php echo $order['store_name']; ?></strong><br />
            <?php echo $order['store_address']; ?>
            </address>
            <b><?php echo $text_telephone; ?></b> <?php echo $order['store_telephone']; ?><br />
            <?php if ($order['store_fax']) { ?>
            <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
            <?php } ?>
            <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
            <b><?php echo $text_website; ?></b> <a href="<?php echo $order['store_url']; ?>"><?php echo $order['store_url']; ?></a></td>
          <td style="width: 50%;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <?php if ($order['invoice_no']) { ?>
            <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
            <?php } ?>
            <b><?php echo $text_order_id; ?></b> <?php echo $order['order_id']; ?><br />
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
            <?php if ($order['shipping_method']) { ?>
            <b><?php echo $text_shipping_method; ?></b> <?php echo $order['shipping_method']; ?><br />
            <?php } ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td style="width: 50%;"><b><?php echo $text_payment_address; ?></b></td>
          <td style="width: 50%;"><b><?php echo $text_shipping_address; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><address>
            <?php echo $order['payment_address']; ?>
            </address></td>
          <td><address>
            <?php echo $order['shipping_address']; ?>
            </address></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_product; ?></b></td>
          <td><b><?php echo $column_model; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
          <td class="text-right"><b><?php echo $column_total; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td><?php echo $product['model']; ?></td>
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['price']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
        </tr>







        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
          <td class="text-right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $text_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
  </div>
  <?php } ?>
</div>

<div class="invoice_bg_top_left"><img src="<?php echo $invoice_bg_top_left?>"></div>
<div class="invoice_bg_top_center"><img src="<?php echo $invoice_bg_top_center?>"></div>
<div class="invoice_bg_top_right"><img src="<?php echo $invoice_bg_top_right?>"></div>
<div class="invoice_bg_bottom_left"><img src="<?php echo $invoice_bg_bottom_left?>"></div>
<div class="invoice_bg_bottom_center"><img src="<?php echo $invoice_bg_bottom_center?>"></div>
<div class="invoice_bg_bottom_right"><img src="<?php echo $invoice_bg_bottom_right?>"></div>

<style type="text/css" media="screen">
table{
  padding: 25px;
  
}
.container,table td, table th{
  background: transparent;
}

.invoice_bg_top_left{background: url("<?php echo $invoice_bg_top_left?>") no-repeat top left;}
.invoice_bg_top_left img{display: none;}
.invoice_bg_top_center{background url("<?php echo $invoice_bg_top_center?>") no-repeat top center;}
.invoice_bg_top_center img{display: none;}
.invoice_bg_top_right{background: url("<?php echo $invoice_bg_top_right?>") no-repeat top right;}
.invoice_bg_top_right img{display: none;}

.invoice_bg_bottom_left{background: url("<?php echo $invoice_bg_bottom_left?>") no-repeat bottom left;}
.invoice_bg_bottom_left img{display: none;}
.invoice_bg_bottom_center{background: url("<?php echo $invoice_bg_bottom_center?>") no-repeat bottom center;}
.invoice_bg_bottom_center img{display: none;}
.invoice_bg_bottom_right{background: url("<?php echo $invoice_bg_bottom_right?>") no-repeat bottom right;}
.invoice_bg_bottom_right img{display: none;}


/*body {
    background: url(<?php echo $invoice_bg_top_left?>) no-repeat top left, 
                      url(<?php echo $invoice_bg_top_center?>) no-repeat top center,
                      url(<?php echo $invoice_bg_top_right?>) no-repeat top right,
                      url(<?php echo $invoice_bg_bottom_left?>) no-repeat bottom left,
                      url(<?php echo $invoice_bg_bottom_center?>) no-repeat bottom center,
                      url(<?php echo $invoice_bg_bottom_right?>) no-repeat bottom right;

}*/
.bg{
  position: relative;
  width: 100%;
      background: url(<?php echo $invoice_bg_top_left?>) no-repeat top 10px left 10px, 
                      url(<?php echo $invoice_bg_top_center?>) no-repeat top 10px center,
                      url(<?php echo $invoice_bg_top_right?>) no-repeat top 10px right 10px,
                      url(<?php echo $invoice_bg_bottom_left?>) no-repeat bottom 10px left 10px,
                      url(<?php echo $invoice_bg_bottom_center?>) no-repeat bottom 10px center,
                      url(<?php echo $invoice_bg_bottom_right?>) no-repeat bottom 10px right 10px;

}
</style>
  <style media="print">
.invoice_bg_top_left img{display: inline;position: fixed;top: 0;width: 100%;}
.invoice_bg_top_center img{display: inline;position: fixed;top: 0; left:0; width: 100%;}
.invoice_bg_top_right img{display: inline;position: fixed;top:0;right: 0;width: 100%;}
.invoice_bg_bottom_left img{display: inline;position: fixed;bottom: 0;width: 100%;}
.invoice_bg_bottom_center img{display: inline;position: fixed;bottom: 0; left:0; width: 100%;}
.invoice_bg_bottom_right img{display: inline;position: fixed;bottom:0;right: 0;width: 100%;}


  </style>
</body>
</html>

