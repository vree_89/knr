<?php
class ModelAccountCustomer extends Model {
	public function getCustomerDeposit($customer_id){
		$sql = "SELECT `deposit` from `".DB_PREFIX."customer` WHERE customer_id = ".(int) $customer_id;
		//echo $sql;
		$customer = $this->db->query($sql);
		if($customer->row['deposit']!=NULL){
			return $customer->row['deposit'];
		}
		else{
			return 0;
		}
	}
	
	public function getCustomerMembership($customer_id){
			$sqlMembership = "SELECT cm.*, c.deposit, c.membership_expired FROM ".DB_PREFIX."customer c 
					LEFT JOIN ".DB_PREFIX."customer_membership cm ON (c.customer_id = cm.customer_id)
					WHERE 
					c.customer_id = ".(int) $customer_id."
					AND c.membership_expired >= NOW()
					AND cm.status = 1
					AND cm.payment_date is not null

					ORDER BY cm.customer_membership_id DESC
					LIMIT 1";
					// echo $sqlMembership;exit;
			$membership = $this->db->query($sqlMembership);
			if($membership->row){
				return array(
					'membership_name'       => $membership->row['membership_name'],
					'membership_deposit'             => $membership->row['deposit'],
					'membership_expired'      => $membership->row['membership_expired'],
					'membership_discount'       => $membership->row['membership_discount'],
					'membership_loyalty_point' => $membership->row['membership_loyalty_point'],
					'membership_bonus_point'     => $membership->row['membership_bonus_point'],
				);
			}
			return false;

		// $query_customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		// $query_membership = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_membership WHERE customer_id = '" . (int)$customer_id . "' and `status` = 1");
		// if ($query_customer->num_rows) {
		// 	$deposit = $query_customer->row['deposit'];
		// 	$membership_expired = $query_customer->row['membership_expired'];
		// 	if(($deposit>0) && (strtotime($membership_expired)>strtotime(date("Y-m-d H:i:s")))){
		// 		return array(
		// 			'membership_name'       => $query_membership->row['membership_name'],
		// 			'membership_deposit'             => $deposit,
		// 			'membership_expired'      => $membership_expired,
		// 			'membership_discount'       => $query_membership->row['membership_discount'],
		// 			'membership_loyalty_point' => $query_membership->row['membership_loyalty_point'],
		// 			'membership_bonus_point'     => $query_membership->row['membership_bonus_point'],
		// 		);
		// 	}

		// }

	}
	
	public function addCustomer($data) {
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$city 		= (isset($data['city'])) ? explode('_', $data['city']) : [0,'city'];
		$city_id	= $city[0];
		$city_name	= $city[1];
		$email		= isset($data['email']) ? $this->db->escape($data['email']) : '';
		$fax		= isset($data['fax']) ? $this->db->escape($data['fax']) : '';
		$company	= isset($data['company']) ? $this->db->escape($data['company']) : '';
		$address_2	= isset($data['address_2']) ? $this->db->escape($data['address_2']) : '';
		$token 		= token(40);

		if (isset($data['reference'])) {
			$ref = $this->getCustomerByRefCode($data['reference']);

			if ($ref) {
				$ref_id 		= $ref['customer_id'];
				$voucher_name	= 'Voucher for Referal';

				do {
					$voucher_code = 'RF'.strtoupper(token(8));
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon WHERE code = '" . $voucher_code . "'");
				} while ($query->num_rows);

				$this->db->query("INSERT INTO " . DB_PREFIX . "coupon SET name = '". $voucher_name ."', code = '" . $voucher_code . "', discount = 50000, type = 'F', total = 500000, logged = 1, shipping = 0, date_start = '0000-00-00', date_end = '0000-00-00', uses_total = 1, uses_customer = 1, status = 1, date_added = NOW()");

				// send voucher to referal
				if (!empty($ref['email'])){
					$this->load->language('mail/customer');

					$subject = sprintf($voucher_name, html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

					$message = "Congratulation, \n\n";
					$message .= "You got gift voucher : ".$voucher_code." \n\n";
					$message .= $this->language->get('text_thanks')." \n";
					$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($ref['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					$mail->setSubject($subject);
					$mail->setText($message);
					$mail->send();
				} else {
					// $smstext = 'Your gift voucher from '.html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8').' : '.$voucher_code;
					// $data_sms = array(
					// 	"no_hp"    =>  $ref['telephone'],
					// 	"smstext"  =>  urlencode($smstext),
					// );
					// // create curl resource
					// $ch = curl_init();
					// curl_setopt($ch, CURLOPT_URL, "http://api.popay.co.id:8182/SMSGW/kirimsms");
					// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					// curl_setopt($ch, CURLOPT_POST, 1);
					// curl_setopt($ch, CURLOPT_POSTFIELDS,$data_sms);
					// $output = curl_exec($ch);
					// curl_close($ch);   
				}
			} else {
				$ref_id = 0;
			}
		}

		do {
			$ref_code	= token(15);
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE reference_code = '" . $ref_code . "'");
		} while ($query->num_rows);
		
		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($email) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $fax . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', code = '" . $this->db->escape($salt = $token) . "', date_added = NOW(), id_city = '" . (int)$city_id . "', reference_id = '" . (int)$ref_id . "', reference_code = '" . $ref_code . "', verified = 1");

		$customer_id = $this->db->getLastId();

		$this->sendSMS($data['telephone'], $token);

		// $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $company . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $address_2 . "', city = '" . $this->db->escape($city_name) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "', id_city = '" . (int)$city_id . "'");
		// $address_id = $this->db->getLastId();
		// $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

		if (!empty($email)){
			$this->load->language('mail/customer');

			// if ($_SERVER['HTTP_HOST'] == 'localhost') { 
			// 	$activation_url = 'http://'.getHostByName(getHostName()).'/knr/upload/index.php?route=account/verification&code='.$token;
			// } else {
			// 	$activation_url = $this->url->link('account/verification', 'code=' . $token);
			// }

			$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

			$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

			if (!$customer_group_info['approval']) {
				$message .= $this->language->get('text_login') . "\n";
			} else {
				$message .= $this->language->get('text_approval') . "\n";
			}

			$message .= $this->url->link('account/login', '', true) . "\n\n";
			// $message .= $activation_url . "\n\n";
			$message .= $this->language->get('text_services') . "\n\n";
			$message .= "You can use this voucher code \"KRCUSTOMER\" to get discount from us. Happy Shopping!! \n\n";
			$message .= $this->language->get('text_thanks') . "\n";
			$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($email);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject($subject);
			$mail->setText($message);
			$mail->send();
		}

		// echo '<pre>';
		// var_dump($mail);
		// var_dump($mail->send());
		// exit;

		// Send to main admin email if new account email is enabled
		if (in_array('account', (array)$this->config->get('config_mail_alert'))) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		return $customer_id;
	}

	public function sendSMS($phone, $token) {
		if ($_SERVER['HTTP_HOST'] == 'localhost') { 
			$url = 'http://'.getHostByName(getHostName()).'/knr/upload/index.php?route=account/verification&code='.$token;
		} else {
			$url = $this->url->link('account/verification', 'code=' . $token);
		}
			
		$smstext = 'Your verification link : '.$url;

		$data = array(
			"no_hp"    =>  $phone,
			"smstext"  =>  urlencode($smstext),
		);

		// create curl resource
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://api.popay.co.id:8182/SMSGW/kirimsms");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		     

		$output = curl_exec($ch);
		curl_close($ch);      
		// echo $output;
	}

	public function editCustomer($data) {
		$customer_id = $this->customer->getId();

		// $this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}

	public function editPassword($email, $password) {
		//die('x');
		// //add count for password change, check last password change date
		$count_password_change = 0;
		$sqlCustomer = "SELECT * FROM `".DB_PREFIX."customer` where LOWER(email) = '".$this->db->escape(utf8_strtolower($email))."'";
		//echo $sqlCustomer;
		$customer = $this->db->query($sqlCustomer);
		if($customer->row){
			$last_password_change = $customer->row['last_password_change'];
			$count_password_change = (int) $customer->row['count_password_change'];
			if($last_password_change==date('Y-m-d')){ //today
				//echo "A";
				$count_password_change = $count_password_change + 1;
			}
			else{ //yesterday or another passed day
				//echo "B";
				$count_password_change = 1;
			}
		}		
		//echo $count_password_change;
		//exit;

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET count_password_change = ".(int)$count_password_change.", last_password_change = NOW(), salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editVerify($telephone) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET verified = '1', code = '' WHERE LOWER(telephone) = '" . $this->db->escape(utf8_strtolower($telephone)) . "'");
	}

	public function editCode($telephone, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(telephone) = '" . $this->db->escape(utf8_strtolower($telephone)) . "'");
	}

	public function editCodeByTelephone($telephone, $code) {
				$date = strtotime("+5 minute");
				$expired_date = date('Y-m-d H:i:s', $date);
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET code = '" . $this->db->escape($code) . "', code_expired_date = '".$expired_date."' WHERE telephone = '" . $this->db->escape(($telephone)) . "'");
	}


	public function editLastPasswordChange($telephone) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET last_password_change = NOW() WHERE LCASE(telephone) = '" . $this->db->escape(utf8_strtolower($telephone)) . "'");
	}

	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}

	// do not change this one, also used to validate phone number is unique
	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		if (!$query->num_rows) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(telephone) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		}

		return $query->row;
	}

	public function getCustomerByTelephone($telephone) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE (email) = '" . $this->db->escape(($telephone)) . "'");

		if (!$query->num_rows) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE (telephone) = '" . $this->db->escape(($telephone)) . "'");
		}

		return $query->row;
	}

	// public function getCustomerByTelephone($telephone) {
	// 	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(telephone) = '" . $this->db->escape(utf8_strtolower($telephone)) . "'");

	// 	return $query->row;
	// }

	public function getCustomerByRefCode($code) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer` WHERE reference_code = '" . $this->db->escape($code) . "' AND reference_code != ''");

		return $query->row;
	}

	public function getCustomerByCode($code) {
		$query = $this->db->query("SELECT customer_id, firstname, lastname, email, telephone, code_expired_date FROM `" . DB_PREFIX . "customer` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

		return $query->row;
	}

	public function getVerifyNumber($telephone){
		$query = $this->db->query("SELECT count(*) as count FROM " . DB_PREFIX . "verify_number WHERE (telephone) = '" . $this->db->escape(($telephone)) . "' and `date` like '%".date('Y-m-d')."%'");

		return $query->row;
	}

	public function getVerifyByToken($token){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "verify_number WHERE (token) = '" . $this->db->escape(($token)) . "' and `date` = '".date('Y-m-d')."' order by verify_number_id desc limit 1");

		return $query->row;

	}

	public function addVerifyNumber($telephone,$token){
		$this->db->query("INSERT INTO  " . DB_PREFIX . "verify_number (`telephone`,`counter`,`token`,`date`,`expired_date`) VALUES ('" . $this->db->escape(($telephone)) . "',1,'".$token."','".date('Y-m-d')."', DATE_ADD(NOW(), INTERVAL 5 MINUTE))");


		
	}
	public function updateVerifyNumber($telephone,$token){
		$this->db->query("UPDATE " . DB_PREFIX . "verify_number SET `counter` = `counter` + 1, `token` = '".$token."' WHERE (telephone) = '" . $this->db->escape(($telephone)) . "'");

		
	}


	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		if (!$query->row['total']) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(telephone) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		}

		return $query->row['total'];
	}

	public function getTotalCustomersByPhone($telephone) {
		
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(telephone) = '" . $this->db->escape(($telephone)) . "'");

		if (!$query->row['total']) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(telephone) = '" . $this->db->escape(($telephone)) . "'");
		}

		return $query->row['total'];
	}

	// public function getTotalCustomersByTelephone($telephone) {
	// 	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(telephone) = '" . $this->db->escape(utf8_strtolower($telephone)) . "'");

	// 	return $query->row['total'];
	// }

	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	// no need to change this, it will record either email / phone number
	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
}
