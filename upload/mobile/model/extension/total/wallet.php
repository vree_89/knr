<?php
class ModelExtensionTotalWallet extends Model {
	public function getCouponCallback($code) {
		$wallet_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "wallet` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
		if ($wallet_query->num_rows) {
			return array(
				'wallet_id'     => $wallet_query->row['wallet_id'],
				'code'          => $wallet_query->row['code'],
				'name'          => $wallet_query->row['name'],
				'type'          => $wallet_query->row['type'],
				'discount'      => $wallet_query->row['discount'],
				'shipping'      => $wallet_query->row['shipping'],
				'total'         => $wallet_query->row['total'],
				'date_start'    => $wallet_query->row['date_start'],
				'date_end'      => $wallet_query->row['date_end'],
				'uses_total'    => $wallet_query->row['uses_total'],
				'uses_customer' => $wallet_query->row['uses_customer'],
				'status'        => $wallet_query->row['status'],
				'date_added'    => $wallet_query->row['date_added'],
			);			
		}
	}
	public function getWallet($code) {
		$status = true;
		$message = "";


			return array(
				'discount' => $code,
				'status' => true,
			);

	}

	public function getTotal($total) {
		//echo "session payment wallet ".$this->session->data['input_payment_wallet'];
		if (isset($this->session->data['input_payment_wallet']) && $this->customer->isLogged()) {
			// $this->load->model('account/customer');
			// $membership_info = $this->model_account_customer->getCustomerMembership($this->session->data['customer_id']);
			// if($membership_info && $membership_info['membership_deposit']>0){
			// 	$total['totals'][] = array(
			// 		'type'       => 'input',
			// 		'code'       => 'wallet',
			// 		'title'      => "Use Wallet",
			// 		'value'      => -$this->session->data['input_payment_wallet'],
			// 		'sort_order' => $this->config->get('wallet_sort_order')
			// 	);
			// 	var_dump($total['totals']);


			// 	$total['total'] -= $this->session->data['input_payment_wallet'];

			// }
				$total['totals'][] = array(
					'button' => true,
					'type'       => 'input',
					'code'       => 'wallet',
					'title'      => "Use Wallet",
					'value'      => -$this->session->data['input_payment_wallet'],
					'sort_order' => $this->config->get('wallet_sort_order'),
					'button_cancel' => true,
				);
				//var_dump($total['totals']);


				$total['total'] -= $this->session->data['input_payment_wallet'];


		}
	}

	public function confirm($order_info, $order_total) {		
		$code = '';

		$start = strpos($order_total['title'], '(') + 1;
		$end = strrpos($order_total['title'], ')');

		if ($start && $end) {
			$code = substr($order_total['title'], $start, $end - $start);
		}
		//echo $code;exit;
		if ($code) {
			//$wallet_info = $this->getCoupon($code);
			$wallet_info = $this->getCouponCallback($code);

			if ($wallet_info) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "wallet_history` SET wallet_id = '" . (int)$wallet_info['wallet_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', customer_id = '" . (int)$order_info['customer_id'] . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
			} else {
				return $this->config->get('config_fraud_status_id');
			}
		}
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "wallet_history` WHERE order_id = '" . (int)$order_id . "'");
	}
}
