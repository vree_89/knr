<?php
class ModelMembershipMembership extends Model {

    public function addMembership($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "membership SET name = '" . $this->db->escape($data['name']) . "', price = '" . (int)$data['price'] . "', period = '" . (int)$data['period'] . "', discount = '" . (int)$data['discount'] . "', loyalty_point = '" . (double)$data['loyalty_point'] . "', bonus_point = '" . (double)$data['bonus_point'] . "', featured = '" . (int)$data['featured'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW()");

		$membership_id = $this->db->getLastId();

		$this->cache->delete('membership');

		return $membership_id;
	}

	public function editMembership($membership_id, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "membership SET name = '" . $this->db->escape($data['name']) . "', price = '" . (int)$data['price'] . "', period = '" . (int)$data['period'] . "', discount = '" . (int)$data['discount'] . "', loyalty_point = '" . (double)$data['loyalty_point'] . "', bonus_point = '" . (double)$data['bonus_point'] . "', featured = '" . (int)$data['featured'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE membership_id = '" . (int)$membership_id . "'");

		$this->cache->delete('article');
	}

	public function copyMembership($membership_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "membership WHERE membership_id = '" . (int)$membership_id . "'");

		if ($query->num_rows) {
			$data = array();

			$data = $query->row;
            $data['status'] = '0';

			$this->addMembership($data);
		}
	}

	public function deleteMembership($membership_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "membership WHERE membership_id = '" . (int)$membership_id . "'");

		$this->cache->delete('article');
	}

	public function getMembership($membership_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "membership WHERE membership_id = '" . (int)$membership_id . "'");

		return $query->row;
	}

	public function getMemberships($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "membership WHERE status = 1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}
		
		$sql .= " GROUP BY membership_id";

        $sql .= " ORDER BY sort_order ASC, date_modified DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalMemberships($data = array()) {
		$sql = "SELECT COUNT(DISTINCT membership_id) AS total FROM " . DB_PREFIX . "membership";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

    public function addOrder($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_membership SET customer_id = '" . (int)$data['customer_id'] . "', membership_id = '" . (int)$data['membership_id'] . "', membership_name = '" . $this->db->escape($data['membership_name']) . "', membership_deposit = '" . (int)$data['membership_deposit'] . "', membership_period = '" . (int)$data['membership_period'] . "', membership_discount = '" . (int)$data['membership_discount'] . "', membership_loyalty_point = '" . (float)$data['membership_loyalty_point'] . "', membership_bonus_point = '" . (float)$data['membership_bonus_point'] . "', date_added = NOW(), ip = '" . $this->db->escape($data['ip']) . "', payment_total = '" . (int)$data['payment_total'] . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', language_id = '" . (int)$data['language_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', uniqueID = '".$data['uniqueID']."'");

		return $this->db->getLastId();
	}

	public function getTotalOrder() {
		$sql = "SELECT COUNT(DISTINCT customer_membership_id) AS total FROM " . DB_PREFIX . "customer_membership WHERE customer_id = '" . (int)$this->customer->getId() . "' AND status = '1'";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getOrder($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "customer_membership WHERE customer_id = '" . (int)$this->customer->getId() . "' AND status = '1'";

		$sort_data = array(
			'points',
			'description',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getActiveMembership($customer_id) {
		$sql = "SELECT cm.* 
				FROM " . DB_PREFIX . "customer c
				LEFT JOIN " . DB_PREFIX . "customer_membership cm ON (c.customer_id = cm.customer_id)
				WHERE c.customer_id = '" . (int)$customer_id . "' 
				AND c.membership_expired >= NOW()
				AND cm.status = 1
				ORDER BY cm.customer_membership_id DESC";

		$query = $this->db->query($sql);

		return $query->row;
	}

	// public function addOrderHistory($order_id, $comment = '') {
	// 	$order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_membership WHERE customer_membership_id = '" . (int)$order_id . "'");
		
	// 	if ($order_query->num_rows) {
	// 		$this->load->model('account/customer');

	// 		$customer_info = $this->model_account_customer->getCustomer($order_query->row['customer_id']);

	// 		$language = new Language($this->config->get('config_language'));
	// 		$language->load($this->config->get('config_language'));
	// 		$language->load('mail/membership');

	// 		if ($this->request->server['HTTPS']) {
	// 			$store_url = HTTPS_SERVER;
	// 		} else {
	// 			$store_url = HTTP_SERVER;
	// 		}

	// 		$store_name = $this->config->get('config_name');
	// 		$order_status = 'Pending';

	// 		$subject = sprintf($language->get('text_new_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'), $order_id);
	
	// 		// HTML Mail
	// 		$data = array();

	// 		$data['title'] = sprintf($language->get('text_new_subject'), $store_name, $order_id);

	// 		$data['text_greeting'] = sprintf($language->get('text_new_greeting'), $store_name);
	// 		$data['text_link'] = $language->get('text_new_link');
	// 		$data['text_order_detail'] = $language->get('text_new_order_detail');
	// 		$data['text_instruction'] = $language->get('text_new_instruction');
	// 		$data['text_order_id'] = $language->get('text_new_order_id');
	// 		$data['text_date_added'] = $language->get('text_new_date_added');
	// 		$data['text_payment_method'] = $language->get('text_new_payment_method');
	// 		$data['text_package_name'] = $language->get('text_new_package_name');
	// 		$data['text_email'] = $language->get('text_new_email');
	// 		$data['text_telephone'] = $language->get('text_new_telephone');
	// 		$data['text_ip'] = $language->get('text_new_ip');
	// 		$data['text_order_status'] = $language->get('text_new_order_status');
	// 		$data['text_total'] = $language->get('text_new_total');
	// 		$data['text_footer'] = $language->get('text_new_footer');

	// 		$data['logo'] = $store_url . 'image/' . $this->config->get('config_logo');
	// 		$data['store_name'] = $store_name;
	// 		$data['store_url'] = $store_url;
	// 		$data['customer_id'] = $order_query->row['customer_id'];
	// 		$data['link'] = $store_url . 'index.php?route=account/membership';

	// 		$data['order_id'] = $order_id;
	// 		$data['date_added'] = date($language->get('date_format_short'), strtotime($order_query->row['date_added']));
	// 		$data['payment_method'] = $order_query->row['payment_method'];
	// 		$data['package_name'] = $order_query->row['membership_name'];
	// 		$data['total'] = 'IDR'.number_format($order_query->row['payment_total']);
	// 		$data['email'] = $customer_info['email'];
	// 		$data['telephone'] = $customer_info['telephone'];
	// 		$data['ip'] = $order_query->row['ip'];
	// 		$data['order_status'] = $order_status;

	// 		if ($comment) {
	// 			$data['comment'] = nl2br($comment);
	// 		} else {
	// 			$data['comment'] = '';
	// 		}

	// 		// Text Mail
	// 		$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8')) . "\n\n";
	// 		$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
	// 		$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_query->row['date_added'])) . "\n";
	// 		$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

	// 		if ($comment) {
	// 			$text .= $language->get('text_new_instruction') . "\n\n";
	// 			$text .= $comment . "\n\n";
	// 		}

	// 		if ($order_query->row['customer_id']) {
	// 			$text .= $language->get('text_new_link') . "\n";
	// 			$text .= $store_url . 'index.php?route=account/membership';
	// 		}

	// 		$text .= $language->get('text_new_footer') . "\n\n";

	// 		$mail = new Mail();
	// 		$mail->protocol = $this->config->get('config_mail_protocol');
	// 		$mail->parameter = $this->config->get('config_mail_parameter');
	// 		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
	// 		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
	// 		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
	// 		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
	// 		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

	// 		$mail->setTo($customer_info['email']);
	// 		$mail->setFrom($this->config->get('config_email'));
	// 		$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
	// 		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
	// 		$mail->setHtml($this->load->view('mail/membership', $data));
	// 		$mail->setText($text);
	// 		$mail->send();
	// 	}
	// }

	public function addDokuHistory($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "doku` SET transidmerchant = '" . $this->db->escape($data['transidmerchant']) . "', totalamount = '" . (double)$data['totalamount'] . "', words = '" .  $this->db->escape($data['words']) . "', payment_channel = '" . $this->db->escape($data['payment_channel']) . "', session_id = '" . $this->db->escape($data['session_id']) . "', payment_date_time = NOW(), trxstatus = 'Requested'");
	}
}