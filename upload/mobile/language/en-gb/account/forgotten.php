<?php
// Heading
$_['heading_title']   = 'Forgot Your Password?';

// Text
$_['text_account']    = 'Account';
$_['text_forgotten']  = 'Forgotten Password';
$_['text_your_email'] = 'Your E-Mail Address';
$_['text_email']      = 'Enter the e-mail address associated with your account. Click submit to have a password reset link e-mailed to you.';
$_['text_success']    = 'An email with a confirmation link has been sent to your email address / phone number.';

// Entry
$_['entry_email']     = 'E-Mail Address';
$_['entry_password']  = 'New Password';
$_['entry_confirm']   = 'Confirm';

// Error
$_['error_email']     = 'Warning: The E-Mail Address / Phone Number was not found in our records, please try again!';
$_['error_telephone']     = 'Warning: The Phone Number was not found in our records, please try again!';
$_['error_approved']  = 'Warning: Your account requires approval before you can login.';
$_['error_verified']  = 'Warning: Your account requires verification before you can login.';
$_['error_password']  = 'Password must be between 4 and 20 characters!';
$_['error_confirm']   = 'Password and password confirmation do not match!';
$_['error_limit']     = 'You have reached the limit of password change today.';
