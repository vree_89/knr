<?php
// header
$_['heading_title']  = 'Account Verification';

// Text
// $_['text_account']   = 'Account';
// $_['text_password']  = 'Enter the new password you wish to use.';
$_['text_message']   = '<p>Hooraaayyy! Your account successfuly verified. Now you can log into your account!</p> <p>You will automatically redirect to login page in <b><span class="counter"></span> second</b></p> <p>If this page not redirect automatically please refresh your browser.</p>';

// Entry
// $_['entry_password'] = 'Password';
// $_['entry_confirm']  = 'Confirm';

// Error
// $_['error_password'] = 'Password must be between 4 and 20 characters!';
// $_['error_confirm']  = 'Password and password confirmation do not match!';
$_['error_code']     = 'Varification code is invalid or was used previously!';