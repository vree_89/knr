<?php
// Heading
$_['heading_title'] = 'Use Wallet';

// Text
$_['text_coupon']   = 'Wallet (%s)';
$_['text_success']  = 'Success: Your wallet discount has been applied!';

// Entry
$_['entry_coupon']  = 'Enter your wallet here';

// Error
$_['error_coupon']  = 'Warning: Wallet is either invalid, expired or reached its usage limit!';
$_['error_empty']   = 'Warning: Please enter a wallet code!';
