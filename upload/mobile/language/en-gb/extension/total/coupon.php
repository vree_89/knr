<?php
// Heading
$_['heading_title'] = 'Use Voucher Code';

// Text
$_['text_coupon']   = 'Voucher (%s)';
$_['text_success']  = 'Success: Your voucher discount has been applied!';

// Entry
$_['entry_coupon']  = 'Enter your voucher here';

// Error
$_['error_coupon']  = 'Warning: Voucher is either invalid, expired, reached its usage limit, or your total order has not reached the vouchers minimum!';
$_['error_empty']   = 'Warning: Please enter a voucher code!';
