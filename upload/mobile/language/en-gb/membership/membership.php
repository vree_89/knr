<?php
// Heading
$_['heading_title']        			 = 'Your order has been placed!';

// Text
$_['text_customer']        			 = '<p>Your order has been successfully processed!</p><p>You can view your order history by going to the <a href="%s">my account</a> page and by clicking on <a href="%s">history</a>.</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';

// Error
$_['text_agree']                     = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['error_agree']                    = 'Warning: You must agree to the %s!';
$_['error_warning']                  = 'There was a problem while trying to process your order! If the problem persists please try selecting a different membership package or you can contact the store owner by <a href="%s">clicking here</a>.';
$_['error_payment']                  = 'Warning: Payment method required!';