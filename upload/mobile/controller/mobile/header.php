<?php
class ControllerMobileHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		// $data['text_home'] = $this->language->get('text_home');

		// Wishlist
		// if ($this->customer->isLogged()) {
		// 	$this->load->model('account/wishlist');

		// 	$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		// } else {
		// 	$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		// }

		// $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		// $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		// $data['text_account'] = $this->language->get('text_account');
		// $data['text_register'] = $this->language->get('text_register');
		// $data['text_login'] = $this->language->get('text_login');
		// $data['text_order'] = $this->language->get('text_order');
		// $data['text_transaction'] = $this->language->get('text_transaction');
		// $data['text_download'] = $this->language->get('text_download');
		// $data['text_logout'] = $this->language->get('text_logout');
		// $data['text_checkout'] = $this->language->get('text_checkout');
		// $data['text_category'] = $this->language->get('text_category');
		// $data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('mobile/home');
		$data['blog'] = $this->url->link('blog/blog');
		// $data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		// $data['account'] = $this->url->link('account/account', '', true);
		// $data['register'] = $this->url->link('account/register', '', true);
		// $data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		//$data['download'] = $this->url->link('account/download', '', true);
		// $data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		// $data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		// $data['telephone'] = $this->config->get('config_telephone');

		// Menu
		$this->load->model('catalog/category');

		// $this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => ucwords(strtolower($child['name'])),
						'href'  => $this->url->link('mobile/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('mobile/category', 'path=' . $category['category_id'])
				);
			}
		}

		// Static Menu
		$data['categories'][] = array(
			'name'     => 'Static Menu',
			'children' => array (
							array(
								'name'  => 'Best Seller',
								'href'  => $this->url->link('mobile/bestseller', 'path=best_seller')
							)
						),
			'column'   => 0,
			'href'     => '#'
		);

		usort($data['categories'], function($a, $b) {
		    return $a['column'] - $b['column'];
		});

		// Menu More
		$data['menu_more'] = [
			[
				'name'  => 'About',
				'icon'  => 'fa fa-info',
				'href'  => $this->url->link('mobile/about', '', true)
			],
			[
				'name'  => 'FAQ',
				'icon'  => 'fa fa-question',
				'href'  => $this->url->link('mobile/faq', '', true)
			],
			[
				'name'  => 'Contact',
				'icon'  => 'fa fa-envelope',
				'href'  => $this->url->link('mobile/contact', '', true)
			],
			[
				'name'  => 'Privacy',
				'icon'  => 'fa fa-gavel',
				'href'  => $this->url->link('mobile/privacy', '', true)
			]
		];

		if ($this->customer->isLogged()) {
			$data['menu_more'] = array_merge($data['menu_more'], [
				[
					'name'  => 'Redeem',
					'icon'  => 'fa fa-money',
					'href'  => $this->url->link('mobile/redeem', '', true)
				],
				[
					'name'  => 'Profile',
					'icon'  => 'fa fa-sign-out',
					'href'  => $this->url->link('account/account', '', true)
				],
				[
					'name'  => 'Logout',
					'icon'  => 'fa fa-user',
					'href'  => $this->url->link('account/logout', '', true)
				]
			]);
		} else {
			$data['menu_more'] = array_merge($data['menu_more'], [
				[
					'name'  => 'Login',
					'icon'  => 'fa fa-sign-in',
					'href'  => $this->url->link('account/login', '', true)
				]
			]);
		}

		// echo '<pre>';
		// var_dump($data['menu_more']);
		// exit;

		// $data['language'] = $this->load->controller('common/language');
		// $data['currency'] = $this->load->controller('common/currency');
		// $data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('mobile/cart');

		// echo '<pre>';
		// var_dump($data['cart']);
		// exit;

		// For page specific css
		// if (isset($this->request->get['route'])) {
		// 	if (isset($this->request->get['product_id'])) {
		// 		$class = '-' . $this->request->get['product_id'];
		// 	} elseif (isset($this->request->get['path'])) {
		// 		$class = '-' . $this->request->get['path'];
		// 	} elseif (isset($this->request->get['manufacturer_id'])) {
		// 		$class = '-' . $this->request->get['manufacturer_id'];
		// 	} elseif (isset($this->request->get['information_id'])) {
		// 		$class = '-' . $this->request->get['information_id'];
		// 	} else {
		// 		$class = '';
		// 	}

		// 	$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		// } else {
		// 	$data['class'] = 'common-home';
		// }

		return $this->load->view('common/header', $data);
	}
}