<?php
class ControllerCheckoutShippingMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		if (isset($this->session->data['shipping_address'])) {

			// Shipping Methods
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('shipping');

			// foreach ($results as $result) {
			// 	if ($this->config->get($result['code'] . '_status')) {
			// 		$this->load->model('extension/shipping/' . $result['code']);

			// 		$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

			// 		if ($quote) {
			// 			$method_data[$result['code']] = array(
			// 				'title'      => $quote['title'],
			// 				'quote'      => $quote['quote'],
			// 				'sort_order' => $quote['sort_order'],
			// 				'error'      => $quote['error']
			// 			);
			// 		}
			// 	}
			// }

			// echo '<pre>';
			// var_dump($quote);
			// exit;

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			if (!isset($this->session->data['shipping_methods'])) {
				$this->session->data['shipping_methods'] = $method_data;
			}
		}

		//var_dump($this->session->data['shipping_methods']);

		// $data['couriers'] = [
		// 	'jne' 	=> 'Jalur Nugraha Ekakurir (JNE)',
		// 	'pos' 	=> 'POS Indonesia (POS)',
		// 	'tiki' 	=> 'Citra Van Titipan Kilat (TIKI)'
		// ];

		$data['couriers'] = array();
		if($this->session->data['location']=="id"){
		$data['couriers'] = [
			'jne' 	=> 'Jalur Nugraha Ekakurir (JNE)',
			'pos' 	=> 'POS Indonesia (POS)',
			'tiki' 	=> 'Citra Van Titipan Kilat (TIKI)'
		];

		}

		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		// if (empty($this->session->data['shipping_methods'])) {
		// 	$data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		// } else {
		// 	$data['error_warning'] = '';
		// }



		if (empty($data['couriers'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['shipping_courier'])) {
			$data['shipping_courier'] = $this->session->data['shipping_courier'];
		} else {
			$data['shipping_courier'] = '';
		}

		if (isset($this->session->data['shipping_methods'])) {
			$data['shipping_methods'] = $this->session->data['shipping_methods'];
		} else {
			$data['shipping_methods'] = array();
		}

		if (isset($this->session->data['shipping_method']['code'])) {
			$data['code'] = $this->session->data['shipping_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		// echo '<pre>';
		// var_dump($this->session->data['shipping_method']);

		$this->response->setOutput($this->load->view('checkout/shipping_method', $data));
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if shipping is required. If not the customer should not have reached this page.
		if (!$this->cart->hasShipping()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate if shipping address has been set.
		if (!isset($this->session->data['shipping_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}
		if (!isset($this->request->post['shipping_method'])) {
			$json['error']['warning'] = $this->language->get('error_shipping');
		} else {
			$shipping = explode('.', html_entity_decode($this->request->post['shipping_method']));

			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			}
		}

		if (!$json) {
			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];

			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		// var_dump($this->session->data['shipping_method']);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function service() {
		function api($postData) {
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'http://api.rajaongkir.com/starter/cost',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => http_build_query($postData),
			  CURLOPT_HTTPHEADER => array(
			    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  return "cURL Error #:" . $err;
			} else {
			  return $response;
			}
		}

		$json = array();
		$i = 0;
		$postDAta = array(
			'origin' 		=> 151, //set origin to Jakarta Barat
			'destination' 	=> explode('_',$this->session->data['shipping_address']['city'])[0],
			'weight' 		=> $this->cart->getWeight() * 1000, //convert from kg into gram
			'courier' 		=> $this->request->post['courier']
		);

		$service_list = json_decode(api($postDAta), true);

		if (!empty($service_list['rajaongkir']['results'][0]['costs'])){

			if ($service_list['rajaongkir']['status']['code'] == 200) {
				foreach ($service_list['rajaongkir']['results'][0]['costs'] as $service) {
					$code = $service_list['rajaongkir']['results'][0]['code'].'.'.strtolower(str_replace(' ', '_', $service['service']));
					$price_text = 'IDR'.number_format($service['cost'][0]['value']);

					$json[$i++] = array(
						'code'		=> $code,
						'service' 	=> $service['service'],
						'cost'		=> $price_text
					);

					$quote[strtolower(str_replace(' ', '_', $service['service']))] = [
						'code'			=> $code,
						'title'			=> strtoupper($service_list['rajaongkir']['results'][0]['code']).' '.$service['service'],
						'cost'			=> $service['cost'][0]['value'],
						'tax_class_id' 	=> 0,
				        'text'			=> $price_text
					];
				}

				$method_data[$service_list['rajaongkir']['results'][0]['code']] = array(
					'title'      => $service_list['rajaongkir']['results'][0]['name'],
					'quote'      => $quote,
					'sort_order' => 1,
					'error'      => false
				);
			}
		}

		$this->session->data['shipping_methods'] = isset($method_data) ? $method_data : array();
		$this->session->data['shipping_courier'] = $this->request->post['courier'];

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}