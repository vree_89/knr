<?php
class ControllerCheckoutPaymentMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		if(isset($this->request->post['input_payment_wallet'])){
			$this->session->data['input_payment_wallet'] = $this->request->post['input_payment_wallet'];
		}
		
		if(isset($this->request->post['input_payment_total'])){			
			$this->session->data['input_payment_total'] = $this->request->post['input_payment_total'];
		}
		// if (isset($this->session->data['payment_address'])) {
		if (isset($this->session->data['shipping_address'])) {
			// Totals
			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);
					
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$data['order_total'] = $this->currency->format($total_data['total']+$this->session->data['input_payment_wallet'], $this->session->data['currency']);	
			$data['order_total_num'] = $total_data['total']+$this->session->data['input_payment_wallet'];	

			$data['input_payment_wallet_num'] = $this->session->data['input_payment_wallet'];
			$data['input_payment_total_num'] = $this->session->data['input_payment_total'];

			if(isset($this->session->data['input_payment_total'])){
				$data['input_payment_total'] = $this->currency->format($this->session->data['input_payment_total'], $this->session->data['currency']);	
			}
			else{
				$data['input_payment_total'] = $this->currency->format($total_data['total'], $this->session->data['currency']);
			}

			if(isset($this->session->data['input_payment_wallet'])){
				$data['input_payment_wallet'] = $this->currency->format($this->session->data['input_payment_wallet'], $this->session->data['currency']);
			}
			else{
				$data['input_payment_wallet'] = $this->currency->format(0, $this->session->data['currency']);	
			}

			//if (!isset($this->session->data['payment_methods']) && $this->session->data['location']=="id") {
				$method_data = [
					'bank_transfer_permata' => [
						'code' 			=> 'bank_transfer_permata',
						'title'			=> 'Bank Transfer',
						'sort_order'	=> 1,
						'image'			=> 'mobile/view/theme/default/assets/img/payment/jaringanatm.jpg',
					],
					'doku_wallet' => [
						'code' 			=> 'doku_wallet',
						'title'			=> 'Doku Wallet',
						'sort_order'	=> 2,
						'image'			=> 'mobile/view/theme/default/assets/img/payment/dokuwallet.jpg',
					],
					'otc_alfa' => [
						'code' 			=> 'otc_alfa',
						'title'			=> 'Alfa Group',
						'sort_order'	=> 3,
						'image'			=> 'mobile/view/theme/default/assets/img/payment/alfagroup.jpg',
					]

				];
				$this->session->data['payment_methods'] = $method_data;
			//}
		}

		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['payment_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['payment_methods'])) {
			$data['payment_methods'] = $this->session->data['payment_methods'];
		} else {
			$data['payment_methods'] = array();
		}
		//var_dump($data['payment_methods']);

		if (isset($this->session->data['payment_method']['code'])) {
			$data['code'] = $this->session->data['payment_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		$data['scripts'] = $this->document->getScripts();

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_checkout_id'), true), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->session->data['agree'])) {
			$data['agree'] = $this->session->data['agree'];
		} else {
			$data['agree'] = '';
		}

		if ($this->customer->isLogged()) {
			$this->load->model('account/customer');
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

			if ($customer_info['deposit'] > 0) {
				$data['deposit'] = number_format($customer_info['deposit']);
			}
		}

		$this->response->setOutput($this->load->view('checkout/payment_method', $data));
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if payment address has been set.
		// if (!isset($this->session->data['payment_address'])) {
		// 	$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		// }

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!isset($this->request->post['payment_method'])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		} elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		}

		// if ($this->config->get('config_checkout_id')) {
		// 	$this->load->model('catalog/information');

		// 	$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

		// 	if ($information_info && !isset($this->request->post['agree'])) {
		// 		$json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
		// 	}
		// }

		if (!$json) {
			$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];

			// $this->session->data['comment'] = strip_tags($this->request->post['comment']);

			// $json['payment'] = $this->load->controller('extension/payment/' . $this->session->data['payment_method']['code']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function confirm() {
		$this->load->language('checkout/checkout');

		$json = array();

		if ($this->cart->hasShipping()) {
			// Validate if shipping address has been set.
			if (!isset($this->session->data['shipping_address'])) {
				$json['redirect'] = $this->url->link('checkout/checkout', '', true);
			}

			// Validate if shipping method has been set.
			if (!isset($this->session->data['shipping_method'])) {
				$json['redirect'] = $this->url->link('checkout/checkout', '', true);
			}
		} else {
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
		}

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		if (isset($this->request->post['use_deposit'])) {
			if ($this->customer->isLogged()) {
				$this->load->model('account/customer');
				$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

				if ($this->request->post['deposit_amount'] < 1) {
					$json['error'] = $this->language->get('error_deposit');
				} elseif ($this->request->post['deposit_amount'] > $customer_info['deposit']) {
					$json['error'] = $this->language->get('error_deposit');
				}
			} else {
				$json['redirect'] = $this->url->link('checkout/checkout', '', true);
			}
		}

		// Validate if payment method has been set.
		if (!isset($this->session->data['payment_method'])) {
			// $json['redirect'] = $this->url->link('checkout/checkout', '', true);
			$json['error'] = $this->language->get('error_payment');
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		$this->load->language('checkout/checkout');

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$json['error'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		$order_data = array();

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;


		$reward_percentage = null;
		if($this->customer->isLogged()){
			if(isset($this->session->data['membership']['membership_loyalty_point']) && isset($this->session->data['membership']['membership_bonus_point'])){
				$reward_percentage = $this->session->data['membership']['membership_loyalty_point'];

			}
			else{
				$reward_percentage = $this->config->get('config_global_reward') ;
			}
		}

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);

		$this->load->model('extension/extension');

		$sort_order = array();

		$results = $this->model_extension_extension->getExtensions('total');

		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('extension/total/' . $result['code']);

				// We have to put the totals in an array so that they pass by reference.
				$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
			}
		}

		$sort_order = array();

		foreach ($totals as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $totals);

		$order_data['totals'] = $totals;

		
		if($this->request->post['payment_total']!=$this->session->data['input_payment_total'] || $total_data['total']!=$this->request->post['payment_total']){
			//echo "selisih<br/>";
			echo "payment_total:".$this->request->post['payment_total']."<br/>";
			echo "session input_payment_total:".$this->session->data['input_payment_total']."<br/>";
			echo "total_data total:".$total_data['total']."<br/>";
			echo "post payment_total:".$this->request->post['payment_total']."<br/>";
			$json['error'] = "Something went wrong, please refresh the page";	
		}
		/*payment_total:&lt;b&gt;Notice&lt;/b&gt;: Undefined variable: input_payment_total_num in &lt;b&gt;/var/www/html/mobile/view/theme/default/template/checkout/payment_method.tpl&lt;/b&gt; on line &lt;b&gt;7&lt;/b&gt;<br/>session input_payment_total:129000<br/>total_data total:129000<br/>post payment_total:&lt;b&gt;Notice&lt;/b&gt;: Undefined variable: input_payment_total_num in &lt;b&gt;/var/www/html/mobile/view/theme/default/template/checkout/payment_method.tpl&lt;/b&gt; on line &lt;b&gt;7&lt;/b&gt;<br/>*/

		/*
<b>Notice</b>: Undefined variable: input_payment_wallet_num in <b>/var/www/html/mobile/view/theme/default/template/checkout/payment_method.tpl</b> on line <b>6</b>
<b>Notice</b>: Undefined variable: input_payment_total_num in <b>/var/www/html/mobile/view/theme/default/template/checkout/payment_method.tpl</b> on line <b>7</b>
		*/

		//check wallet again, at server
		if($this->customer->isLogged()){
			$this->load->model('account/customer');
			if(isset($this->session->data['input_payment_wallet']) && $this->session->data['input_payment_wallet']>0){
				$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
				$customer_wallet = $customer_info['deposit'];
				if($customer_wallet<$this->session->data['input_payment_wallet'] || ((int) $this->session->data['customer_deposit']!=(int) $customer_wallet)){	// or even deposit not same				
					$json['error'] = "Something went wrong, please refresh the page";

					//reload totals
					//set payment wallet to 0
					$this->session->data['input_payment_wallet'] = 0;
					$this->session->data['customer_deposit'] = $customer_wallet;

					// Totals
					$this->load->model('extension/extension');

					$totals = array();
					$taxes = $this->cart->getTaxes();
					$total = 0;

					// Because __call can not keep var references so we put them into an array. 			
					$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
					);		

					// Display prices					
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {		
					if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);					

					$data['order_total'] = $total_data['total'];

					$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
					$this->session->data['input_payment_total'] = $data['order_total'];
					$data['input_payment_total'] = $this->session->data['input_payment_total'];
				}

			}
		}

		if (empty($json)) {
			$this->load->language('checkout/checkout');

			$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$order_data['store_id'] = $this->config->get('config_store_id');
			$order_data['store_name'] = $this->config->get('config_name');

			if ($order_data['store_id']) {
				$order_data['store_url'] = $this->config->get('config_url');
			} else {
				if ($this->request->server['HTTPS']) {
					$order_data['store_url'] = HTTPS_SERVER;
				} else {
					$order_data['store_url'] = HTTP_SERVER;
				}
			}

			if ($this->customer->isLogged()) {
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

				$order_data['customer_id'] = $this->customer->getId();
				$order_data['customer_group_id'] = $customer_info['customer_group_id'];
				$order_data['firstname'] = $customer_info['firstname'];
				$order_data['lastname'] = $customer_info['lastname'];
				$order_data['email'] = $customer_info['email'];
				$order_data['telephone'] = $customer_info['telephone'];
				$order_data['fax'] = $customer_info['fax'];
				$order_data['custom_field'] = json_decode($customer_info['custom_field'], true);
			} elseif (isset($this->session->data['guest'])) {
				$order_data['customer_id'] = 0;
				$order_data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
				$order_data['firstname'] = $this->session->data['guest']['firstname'];
				$order_data['lastname'] = $this->session->data['guest']['lastname'];
				$order_data['email'] = $this->session->data['guest']['email'];
				$order_data['telephone'] = $this->session->data['guest']['telephone'];
				$order_data['fax'] = $this->session->data['guest']['fax'];
				$order_data['custom_field'] = $this->session->data['guest']['custom_field'];
			}

			$order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
			$order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
			$order_data['payment_company'] = $this->session->data['payment_address']['company'];
			$order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
			$order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
			$order_data['payment_city'] = $this->session->data['payment_address']['city'];
			$order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
			$order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
			$order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
			$order_data['payment_country'] = $this->session->data['payment_address']['country'];
			$order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
			$order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
			$order_data['payment_custom_field'] = (isset($this->session->data['payment_address']['custom_field']) ? $this->session->data['payment_address']['custom_field'] : array());

			if (isset($this->session->data['payment_method']['title'])) {
				$order_data['payment_method'] = $this->session->data['payment_method']['title'];
			} else {
				$order_data['payment_method'] = '';
			}

			if (isset($this->session->data['payment_method']['code'])) {
				$order_data['payment_code'] = $this->session->data['payment_method']['code'];
			} else {
				$order_data['payment_code'] = '';
			}

			if ($this->cart->hasShipping()) {
				$order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
				$order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
				$order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
				$order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
				$order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
				$order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
				$order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
				$order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
				$order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
				$order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
				$order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
				$order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
				$order_data['shipping_custom_field'] = (isset($this->session->data['shipping_address']['custom_field']) ? $this->session->data['shipping_address']['custom_field'] : array());

				if (isset($this->session->data['shipping_method']['title'])) {
					$order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
				} else {
					$order_data['shipping_method'] = '';
				}

				if (isset($this->session->data['shipping_method']['code'])) {
					$order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
				} else {
					$order_data['shipping_code'] = '';
				}
			} else {
				$order_data['shipping_firstname'] = '';
				$order_data['shipping_lastname'] = '';
				$order_data['shipping_company'] = '';
				$order_data['shipping_address_1'] = '';
				$order_data['shipping_address_2'] = '';
				$order_data['shipping_city'] = '';
				$order_data['shipping_postcode'] = '';
				$order_data['shipping_zone'] = '';
				$order_data['shipping_zone_id'] = '';
				$order_data['shipping_country'] = '';
				$order_data['shipping_country_id'] = '';
				$order_data['shipping_address_format'] = '';
				$order_data['shipping_custom_field'] = array();
				$order_data['shipping_method'] = '';
				$order_data['shipping_code'] = '';
			}

			$order_data['products'] = array();
			$doku_basket = '';

			foreach ($this->cart->getProducts() as $product) {
				$option_data = array();

				foreach ($product['option'] as $option) {
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'option_id'               => $option['option_id'],
						'option_value_id'         => $option['option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {					
					$price_num = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
				} else {
					$price_num = false;
				}	

				$reward = "";
				if($reward_percentage && $price_num){
					$reward = $reward_percentage*$price_num*$product['quantity'];
				}


				$order_data['products'][] = array(
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $option_data,
					'download'   => $product['download'],
					'quantity'   => $product['quantity'],
					'subtract'   => $product['subtract'],
					'price'      => $product['price'],
					'total'      => $product['total'],
					'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
					'reward'     => $reward//$product['reward']
				);

				$doku_basket .= $product['name'].','.number_format($product['price'], 2, '.', '').','.$product['quantity'].','.number_format($product['total'], 2, '.', '').';';
			}

			// Gift Voucher
			$order_data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $voucher) {
					$order_data['vouchers'][] = array(
						'description'      => $voucher['description'],
						'code'             => token(10),
						'to_name'          => $voucher['to_name'],
						'to_email'         => $voucher['to_email'],
						'from_name'        => $voucher['from_name'],
						'from_email'       => $voucher['from_email'],
						'voucher_theme_id' => $voucher['voucher_theme_id'],
						'message'          => $voucher['message'],
						'amount'           => $voucher['amount']
					);
				}
			}

			// $order_data['comment'] = isset($this->session->data['comment']) ? $this->session->data['comment'] : '';
			// $order_data['total'] = $total_data['total'];
			$order_data['comment'] = isset($this->session->data['comment']) ? $this->session->data['comment'] : '';
			$order_data['total'] = $total_data['total'];
			if(isset($this->session->data['input_payment_wallet']) && isset($this->session->data['input_payment_total'])){
				$order_data['total'] = $this->session->data['input_payment_wallet'] + $this->session->data['input_payment_total'];
			}
			$order_data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
			$order_data['input_payment_total'] = $this->session->data['input_payment_total'];

			if (isset($this->request->cookie['tracking'])) {
				$order_data['tracking'] = $this->request->cookie['tracking'];

				$subtotal = $this->cart->getSubTotal();

				// Affiliate
				$this->load->model('affiliate/affiliate');

				$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

				if ($affiliate_info) {
					$order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
					$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
				} else {
					$order_data['affiliate_id'] = 0;
					$order_data['commission'] = 0;
				}

				// Marketing
				$this->load->model('checkout/marketing');

				$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

				if ($marketing_info) {
					$order_data['marketing_id'] = $marketing_info['marketing_id'];
				} else {
					$order_data['marketing_id'] = 0;
				}
			} else {
				$order_data['affiliate_id'] = 0;
				$order_data['commission'] = 0;
				$order_data['marketing_id'] = 0;
				$order_data['tracking'] = '';
			}

			$order_data['language_id'] = $this->config->get('config_language_id');
			$order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
			$order_data['currency_code'] = $this->session->data['currency'];
			$order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
			$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
			} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
			} else {
				$order_data['forwarded_ip'] = '';
			}

			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
			} else {
				$order_data['user_agent'] = '';
			}

			if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
				$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
			} else {
				$order_data['accept_language'] = '';
			}

			$this->load->model('checkout/order');

			$this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

			// $this->load->controller('extension/payment/' . $this->session->data['payment_method']['code'] . '/confirm');

			// $transidmerchant 	= 'PRD-'.$this->session->data['order_id'];
			// //$total 				= number_format($order_data['total'], 2, '.', '');
			// $total 				= number_format($order_data['input_payment_total'], 2, '.', '');
			// $payment_channel	= $this->session->data['payment_method']['id'];
			// $mallid 			= 4565;
			// $sharedkey 			= 'RzN4pKta12R9';
			// $words 				= sha1($total.$mallid.$sharedkey.$transidmerchant);
			// $name 				= $order_data['shipping_firstname'].' '.$order_data['shipping_lastname'];
			// $session_id			= session_id();
			// //expired date
			// $date = strtotime("+1 day");
			// $expired_date = date('Y-m-d H:i:s', $date);

			// $doku_data = [
			// 	'transidmerchant'	=> $transidmerchant,
			// 	'totalamount'		=> $total,
			// 	'words'				=> $words,
			// 	'payment_channel'	=> $payment_channel,
			// 	'session_id'		=> $session_id,
			// 	'order_id' 			=> $this->session->data['order_id']."",
			// 	'expired_date'		=> $expired_date,
			// ];

			// $this->model_checkout_order->addDokuHistory($doku_data);

			// $json['doku_data'] = [
			// 	'MALLID'			=> $mallid,
			// 	'CHAINMERCHANT'		=> 'NA',
			// 	'AMOUNT'			=> $total,
			// 	'PURCHASEAMOUNT'	=> $total,
			// 	'TRANSIDMERCHANT'	=> $transidmerchant,
			// 	'WORDS'				=> $words,
			// 	'REQUESTDATETIME'	=> date("YmdHis"),
			// 	'CURRENCY'			=> 360,
			// 	'PURCHASECURRENCY'	=> 360,
			// 	'SESSIONID'			=> session_id(),
			// 	'NAME'				=> $name,
			// 	'EMAIL'				=> $order_data['email'],
			// 	'BASKET'			=> $doku_basket,
			// 	'SHIPPING_ADDRESS'	=> $order_data['shipping_address_1'],
			// 	'SHIPPING_CITY'		=> explode('_', $order_data['shipping_city'])[1],
			// 	'SHIPPING_STATE'	=> $order_data['shipping_zone'],
			// 	'SHIPPING_COUNTRY'	=> 'ID',
			// 	'SHIPPING_ZIPCODE'	=> $order_data['shipping_postcode'],
			// 	'PAYMENTCHANNEL'	=> $this->session->data['payment_method']['id']
			// ];

			// //$json['doku_url'] = ($_SERVER['HTTP_HOST'] == 'localhost') ? 'https://staging.doku.com/Suite/Receive' : 'https://pay.doku.com/Suite/Receive';
			// $json['doku_url'] = 'https://staging.doku.com/Suite/Receive';


				/*NEW payment method*/
				$merchant_transaction_id 	= 'PRD-'.$this->session->data['order_id'];
				if($this->customer->isLogged()){
					$total = $order_data['input_payment_total'];
				}
				else{
					$total = $order_data['total'];
				}
				
				$payment_channel	= $this->session->data['payment_method']['code'];

				if($this->customer->isLogged()){
					$user_id = $order_data['customer_id'];
				}
				else{
					$user_id = $order_data['email'];

				}



			$json['data'] = array(
				"timestamp" => time(),
				"user_id" => $user_id,
				"merchant_transaction_id" => $merchant_transaction_id,
				"transaction_description" => "Transaction Product #".$merchant_transaction_id,
				"payment_channel" => $payment_channel,
				"currency" => "IDR",
				"amount" => $total,
				"item_id" => "",
				"item_name" => "Many items",
				"redirect_url" => "",
				"redirect_target" => "_top",
				"custom" => "",
				"order_id" => $this->session->data['order_id'],
				);
			$json['redirect_payment_url'] = $this->url->link('checkout/payment_method/pay');			

			//reset customer deposit
			if(isset($this->session->data['customer_deposit']) && $this->session->data['input_payment_wallet']>0){
				$this->session->data['customer_deposit'] = $this->session->data['customer_deposit'] - $this->session->data['input_payment_wallet'];	
			}

			$this->cart->clear();
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);

			$this->session->data['input_payment_wallet'] = 0;
			$this->session->data['input_payment_total'] = 0;	


			
						

		}

		// $json['continue'] = $this->url->link('checkout/success');

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function pay(){
		// var_dump($this->request);exit;
				$url = "https://api.simplepayment.solutions/api/v1/create";
				$appkey   = "5a797f06d7fc016849a82e45";
				$appid   = "84a6673c17364fa5bc01";
				$secretkey  = "g33696P326224hE274GTb6T59QJ794jN";

				$timestamp = time();
				$user_id = $this->request->post['user_id'];
				$merchant_transaction_id = $this->request->post['merchant_transaction_id'];
				$transaction_description = $this->request->post['transaction_description'];
				$payment_channel = $this->request->post['payment_channel'];
				$amount = $this->request->post['amount'];
				//$redirect_url = $this->request->post['redirect_url'];
				$order_id = $this->request->post['order_id'];


				$data = array(
					'timestamp' => time(),
					'user_id' => $user_id,
					'merchant_transaction_id' => $merchant_transaction_id,
					'transaction_description' => $transaction_description,
					'payment_channel' => "airtime_testing",//$payment_channel,
					'currency' => 'IDR',
					'amount' => '10000',//$amount,
					'item_id' => "",
					'item_name' => 'KR House '.$merchant_transaction_id,
					'redirect_url' => 'http://128.199.70.89/index.php?route=checkout/success',
					'redirect_target' => '_top',
					'custom' => "",
					);
				//var_dump($data);exit;
				$json_data = json_encode($data);
				//echo $json_data;exit;

				// echo base64_encode($json_data);exit;
				$signature = hash_hmac('sha256', base64_encode($json_data), $secretkey);
				//$signature = $stringBodySign = strtr(base64_encode(hash_hmac('sha256',  $json_data, $secretkey , true )), '+/', '-_');
				// echo $signature;exit;



		$header = [ 
			'Content-Type: application/json',
			'AppId: '.$appid,
			'Bodysign: '.$signature
		];


				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
				$output = curl_exec($ch);
				//echo $output;exit;
				curl_close($ch);
				$arrayResult = json_decode( $output, true );		
	        if ( isset($arrayResult['data'])){
	        	$this->load->model('checkout/order');
	        	//addOrderHistory($order_id, $order_status_id, $comment = '', $notify = false, $override = false,$expired_date=null,$reference_id = null) {
	        	$this->model_checkout_order->addOrderHistory($order_id, 1, '', true,false,null,null);
	        	$redirectUrl = $arrayResult['data']['links']['href'];
	        	$this->response->redirect($redirectUrl);
	        }else{
	        	die('Request failed! Err '.$arrayResult['errors']['id'].' : '.$arrayResult['errors']['title']);
	        }


	}


}
