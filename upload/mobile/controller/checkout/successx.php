<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$transidmerchant 	= $this->request->post['TRANSIDMERCHANT'];
		$statuscode 		= $this->request->post['STATUSCODE'];
		$paymentchannel 	= $this->request->post['PAYMENTCHANNEL'];
		$paymentcode 		= $this->request->post['PAYMENTCODE'];

		if (isset($transidmerchant) && ($statuscode == '5511' || $statuscode == '5111')) {
			$trans_type = explode('-',$transidmerchant)[0];
			$payment_info = [
				'36' => 'How to Pay at the ATM

					    1. Enter PIN
					    2. Choose "Transfer". If using ATM BCA, choose "Others" then "Transfer"
					    3. Choose "Other Bank Account"
					    4. Enter the bank code (Permata is 013) followed by the 16 digit payment code {paymentcode} as the destination account, then choose "Correct"
					    5. Enter the exact amount as your transaction value. Incorrect transfer amount will result in failed payment
					    6. Confirm that the bank code, payment code, and transaction amount is correct, then choose "Correct"
					    7. Done


						How to Pay Using Internet Banking

						Note: Payment cannot be done using BCA Internet Banking (KlikBCA)

					    1. Login to your internet banking account
					    2. Choose "Transfer" and choose "Other Bank Account". Enter the bank code (Permata is 013) as the destination account
					    3. Enter the exact amount as your transaction value
					    4. Enter the destination amount using your 16 digit payment code {paymentcode}
					    5. Confirm that the bank code, payment code, and transaction amount is correct, then choose "Correct"
					    6. Done',
				'35' => 'How to Pay at AlfaMart

					    1. Take note of your payment code and go to your nearest Alfamart, Alfa Midi, Alfa Express, Lawson, Indomaret or DAN+DAN store
					    2. Tell the cashier that you wish to make a DOKU payment
					    3. If the cashier is unaware of DOKU, provide the instruction to open the e-transaction terminal, choose "2", then "2", then "2" which will then display DOKU option
					    4. The cashier will request for the payment code which you will provide {paymentcode}
					    5. Make a payment to the cashier according to your transaction amount
					    6. Get your receipt as a proof of payment. Your merchant will be directly notified of the payment status
					    7. Done',
				'04' => 'Payment has been received, please wait for the next process'
			];

			$data['payment_info'] = nl2br(str_replace('{paymentcode}', $paymentcode, $payment_info[$paymentchannel]));

			if ($trans_type == 'MBR') {
				$this->load->language('membership/membership');

				if (isset($this->session->data['membership_order_id'])) {
					// Add to activity log
					if ($this->config->get('config_customer_activity')) {
						$this->load->model('account/activity');

						$activity_data = array(
							'customer_id' 			=> $this->customer->getId(),
							'name'        			=> $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
							'membership_id' 		=> $this->session->data['membership_id'],
							'membership_name' 		=> $this->session->data['membership_name'],
							'membership_order_id'   => $this->session->data['membership_order_id']
						);
					}

					unset($this->session->data['membership_id']);
					unset($this->session->data['membership_name']);
					unset($this->session->data['membership_order_id']);
					unset($this->session->data['payment_methods']);
				}

				$this->document->setTitle($this->language->get('heading_title'));

				$data['heading_title'] = $this->language->get('heading_title');

				$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/membership', '', true), $this->url->link('information/contact'));

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');
				
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');

				return $this->response->setOutput($this->load->view('common/success', $data));
			} elseif ($trans_type == 'PRD') {
				$this->load->language('checkout/success');

				if (isset($this->session->data['order_id'])) {
					$this->cart->clear();

					// Add to activity log
					if ($this->config->get('config_customer_activity')) {
						$this->load->model('account/activity');

						if ($this->customer->isLogged()) {
							$activity_data = array(
								'customer_id' => $this->customer->getId(),
								'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
								'order_id'    => $this->session->data['order_id']
							);

							$this->model_account_activity->addActivity('order_account', $activity_data);
						} else {
							$activity_data = array(
								'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
								'order_id' => $this->session->data['order_id']
							);

							$this->model_account_activity->addActivity('order_guest', $activity_data);
						}
					}

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
					unset($this->session->data['guest']);
					unset($this->session->data['comment']);
					unset($this->session->data['order_id']);
					unset($this->session->data['coupon']);
					unset($this->session->data['reward']);
					unset($this->session->data['voucher']);
					unset($this->session->data['vouchers']);
					unset($this->session->data['totals']);
				}

				$this->document->setTitle($this->language->get('heading_title'));

				$data['breadcrumbs'] = array();

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_home'),
					'href' => $this->url->link('common/home')
				);

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_basket'),
					'href' => $this->url->link('checkout/cart')
				);

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_checkout'),
					'href' => $this->url->link('checkout/checkout', '', true)
				);

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_success'),
					'href' => $this->url->link('checkout/success')
				);

				$data['heading_title'] = $this->language->get('heading_title');

				if ($this->customer->isLogged()) {
					$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('account/download', '', true), $this->url->link('information/contact'));
				} else {
					$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
				}

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');

				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');

				return $this->response->setOutput($this->load->view('common/success', $data));
			}
		} else {
			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
			
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			return $this->response->setOutput($this->load->view('common/failed', $data));
		}
	}
}