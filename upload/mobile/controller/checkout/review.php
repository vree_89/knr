<?php
class ControllerCheckoutReview extends Controller {

	public function setwallet(){
		$json = array();

		if (isset($this->request->post['input_payment_wallet'])) {
			$input_payment_wallet = $this->request->post['input_payment_wallet'];
			$this->session->data['input_payment_wallet'] = $input_payment_wallet;
			$this->session->data['success'] = $this->language->get('text_success');
			$json['redirect'] = $this->url->link('checkout/cart');
		}
		else{
			$json['error'] = $this->language->get('error_coupon');
		} 
		//echo $this->session->data['input_payment_wallet'];
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));	
	}

	public function index() {
		$order_data = array();

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);

		$this->load->model('extension/extension');

		$sort_order = array();

		$results = $this->model_extension_extension->getExtensions('total');

		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('extension/total/' . $result['code']);

				// We have to put the totals in an array so that they pass by reference.
				$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
			}
		}

		$sort_order = array();

		foreach ($totals as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $totals);
		$data['order_total'] = $total_data['total'];

		$data['totals'] = array();

		foreach ($totals as $total) {
			if ($total['title'] == 'Grand Total') {
				$data['totals'][] = array(
					'currency'=>$this->session->data['currency'],
					'type' => $total['type'],
					'code' => $total['code'],
					'value'=>abs($total['value']),
					'symbol'=> $total['value']>=0 ? '' : '-',
					'button' => $total['button'],
					'title' => '<b>'.$total['title'].'</b>',
					'text'  => '<b>'.$this->currency->format($total['value'], $this->session->data['currency']).'</b>'
				);
			}
			else if($total['title'] == 'Use Wallet' && $this->customer->isLogged()){
				$data['totals'][] = array(
					'currency'=>$this->session->data['currency'],
					'type' => $total['type'],
					'code' => $total['code'],
					'value'=>abs($total['value']),
					'symbol'=> $total['value']>=0 ? '' : '-',
					'button' => $total['button'],
					'title' => $total['title'].' (Your Wallet : '.(int) $this->session->data['customer_deposit'].')',
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}
			else {
				$data['totals'][] = array(
					'currency'=>$this->session->data['currency'],
					'type' => $total['type'],
					'code' => $total['code'],
					'value'=>abs($total['value']),
					'symbol'=> $total['value']>=0 ? '' : '-',
					'button' => $total['button'],
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}
		}





		if (isset($this->session->data['coupon'])) {
			$data['coupon'] = $this->session->data['coupon'];
		} else {
			$data['coupon'] = '';
		}


		if (isset($this->session->data['input_payment_wallet'])) {
			$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
		} else {
			$data['input_payment_wallet'] = 0;
		}
		$this->session->data['input_payment_total'] = $data['order_total'];
		$data['input_payment_total'] = $data['order_total'];	


		//echo $data['input_payment_wallet'];exit;

		if($this->customer->isLogged()){
			if(isset($this->session->data['membership'])){
				$data['is_wallet'] = true;	
			}
			else{
				$data['is_wallet'] = false;	
			}
		}
		else{
			$data['is_wallet'] = false;
		}


		//var_dump(@$this->session->data['shipping_method']);exit;
		$data['shipping_method'] = @$this->session->data['shipping_method'];
		$data['cart_url'] = $this->url->link('checkout/cart');
		$data['action'] = $this->url->link('checkout/cart/edit', '', true);



		$data['deposit'] = 0;
		if(isset($this->session->data['customer_deposit'])){
			$data['deposit'] = $this->session->data['customer_deposit'];
		}


		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['coupon'] = $this->load->controller('extension/total/coupon');



		$this->session->data['input_payment_total'] = $data['order_total'];		
		$data['input_payment_total'] = $this->session->data['input_payment_total'];


		if (isset($this->session->data['input_payment_wallet'])) {
			$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
		} else {
			$data['input_payment_wallet'] = 0;
		}
		//$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];



		$data['deposit'] = 0;
		if(isset($this->session->data['customer_deposit'])){
			$data['deposit'] = $this->session->data['customer_deposit'];
		}
		$data['text_loading'] = "Loading";
		

		//check wallet


		
		if (isset($this->session->data['input_payment_wallet'])) {
			$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
		} else {
			$data['input_payment_wallet'] = 0;
		}
		$this->session->data['input_payment_total'] = $data['order_total'];
		$data['input_payment_total'] = $data['order_total'];	

		if($this->customer->isLogged()){
			if($data['input_payment_total']<0 || $data['order_total']<0 || ($data['input_payment_total']>0 && $data['order_total']<50000 && $data['input_payment_wallet']>0)){
				//set payment wallet to 0
				$this->session->data['input_payment_wallet'] = 0;
				//echo "<p>Error : minus/salah perhitungan</p>";


				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;

				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
				);		

				// Display prices
				//if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);


				foreach ($results as $result) {		
				if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('extension/total/' . $result['code']);

				// We have to put the totals in an array so that they pass by reference.
				$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
				//var_dump($totals);exit;
				//}

				$data['order_total'] = $total_data['total'];
				$data['currency'] = $this->session->data['currency'];
				$data['logged'] = $this->customer->isLogged();
				//die('x');


				$data['totals'] = array();
				foreach ($totals as $total) {
					if ($total['title'] == 'Grand Total') {
						$data['totals'][] = array(
							'currency'=>$this->session->data['currency'],
							'type' => $total['type'],
							'code' => $total['code'],
							'value'=>abs($total['value']),
							'symbol'=> $total['value']>=0 ? '' : '-',
							'button' => $total['button'],
							'title' => '<b>'.$total['title'].'</b>',
							'text'  => '<b>'.$this->currency->format($total['value'], $this->session->data['currency']).'</b>'
						);
					} 
					else if($total['title'] == 'Use Wallet' && $this->customer->isLogged()){
						$data['totals'][] = array(
							'currency'=>$this->session->data['currency'],
							'type' => $total['type'],
							'code' => $total['code'],
							'value'=>abs($total['value']),
							'symbol'=> $total['value']>=0 ? '' : '-',
							'button' => $total['button'],
							'title' => $total['title'].' (Your Wallet : '.(int) $this->session->data['customer_deposit'].')',
							'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
						);
					}
					else {
						$data['totals'][] = array(
							'currency'=>$this->session->data['currency'],
							'type' => $total['type'],
							'code' => $total['code'],
							'value'=>abs($total['value']),
							'symbol'=> $total['value']>=0 ? '' : '-',
							'button' => $total['button'],
							'title' => $total['title'],
							'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
						);
					}
				}
				$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
				$this->session->data['input_payment_total'] = $data['order_total'];
				$data['input_payment_total'] = $this->session->data['input_payment_total'];
			}			
		}




		$this->response->setOutput($this->load->view('checkout/review', $data));
	}
}
