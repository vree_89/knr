<?php
class ControllerProductBestseller extends Controller {
	public function index() {
		if (!isset($this->session->data['location'])) {
			$this->session->data['redirect'] = $this->url->link('product/bestseller/preview', 'path=bestseller');

			$this->response->redirect($this->url->link('common/home', '', true));
		}

		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			// $sort = 'p.sort_order';
			$sort = 'total';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			// $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
			$limit = 12;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				// if ($category_info) {
				// 	$data['breadcrumbs'][] = array(
				// 		'text' => $category_info['name'],
				// 		'href' => $this->url->link('product/category', 'path=' . $path . $url)
				// 	);
				// }

					$data['breadcrumbs'][] = array(
						'text' => 'Best Seller',
						'href' => $this->url->link('product/bestseller')
					);

			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);


		// $this->document->setTitle($category_info['meta_title']);
		// $this->document->setDescription($category_info['meta_description']);
		// $this->document->setKeywords($category_info['meta_keyword']);

		//$data['heading_title'] = $category_info['name'];

		$this->document->setTitle('Best Seller');


		$data['heading_title'] = 'Best Seller';
		$data['text_refine'] = $this->language->get('text_refine');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');

		// Set the last category breadcrumb
		$data['breadcrumbs'][] = array(
			'text' => 'Best Seller',
			'href' => $this->url->link('product/bestseller')
		);

		$data['thumb'] = '';

		$data['description'] = '';
		$data['compare'] = $this->url->link('product/compare');

		$url = '';

		if (isset($this->request->get['filter'])) {
			$url .= '&filter=' . $this->request->get['filter'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['categories'] = array();

		$results = $this->model_catalog_category->getCategories($category_id);

		foreach ($results as $result) {
			$filter_data = array(
				'filter_category_id'  => $result['category_id'],
				'filter_sub_category' => true
			);

			$data['categories'][] = array(
				'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalBestSellers($filter_data) . ')' : ''),
				'href' => $this->url->link('product/bestseller')
			);
		}

		$data['products'] = array();

		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_filter'      => $filter,
			'sort'               => $sort,
			'order'              => $order,
			'start'              => ($page - 1) * $limit,
			'limit'              => $limit
		);

		$product_total = $this->model_catalog_product->getTotalBestSellers($filter_data);

		if ($this->customer->isLogged()) {
			$this->load->model('membership/membership');
			$membership_info = $this->model_membership_membership->getActiveMembership($this->customer->getId());
		}

		$results = $this->model_catalog_product->getBestSellers($filter_data);

		foreach ($results as $result) {
			if (file_exists('image/'.$result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			}

			// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			// 	if (!empty($membership_info)) {
			// 		$member_price = $result['price'] - (($result['price'] - $result['base_price']) * ($membership_info['membership_discount']/100));
			// 		$price = $this->currency->format($this->tax->calculate($member_price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			// 		$pricenum = $member_price;
			// 	} else {
			// 		$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			// 		$pricenum = $result['price'];
			// 	}
			// } else {
			// 	$price = false;
			// 	$pricenum = false;
			// }
				if($this->customer->isLogged() ){					
					$_price_retail = $result['price_retail'];
					$_price = $result['price'];
					$_base_price = $result['base_price'];
					//if membership user
					if(isset($this->session->data['membership'])){						
						$discount = $this->session->data['membership']['membership_discount'];
						$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						$discount_amount = (($discount/100)*($_price-$_base_price));
						$price = $_price - (($discount/100)*($_price-$_base_price));

						$discount_total  = $_price_retail-$price;

						$discount = round(($discount_total/$_price_retail)*100);

						$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);


					}
					else{
						if($_price_retail>0 && $_price>0){
							if($_price_retail>$_price){
								$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
								$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
								
								$price = $_price;
								$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							}
						}
						else{
							$discount = null;
							$price_before = null;
							$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}
					}
				}
				else{
					$_price_retail = $result['price_retail'];
					$_price = $result['price'];

					if($_price_retail>0 && $_price>0){
						if($_price_retail>$_price){
							$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
							$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

							$price = $_price;
							$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}
					}
					else{
						$discount = null;
						$price_before = null;
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					}
				}


			if ($result['price_retail'] > 0) {
				$price_retail = $this->currency->format($this->tax->calculate($result['price_retail'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price_retail = $this->session->data['currency'] . 0;
			}

			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
			} else {
				$tax = false;
			}

			if ($this->config->get('config_review_status')) {
				$rating = (int)$result['rating'];
			} else {
				$rating = false;
			}



			$data['products'][] = array(
				'product_id'  => $result['product_id'],
				'thumb'       => $image,
				'name'        => $result['name'],
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				'price'       => $price,
				'price_before'       => $price_before,
				//'price_retail'=> $price_retail,
				//'rate'		  => ($result['price_retail'] > 0) ? round(100 - (($pricenum / $result['price_retail']) * 100)) : 0,
				'special'     => $special,
				//'discount'	  => $special ? round(($result['special'] / $pricenum) * 100) : 0,
				'discount'	  => $discount,
				//'pricenum'    => $pricenum,
				//'specialnum'  => $specialnum,
				'tax'         => $tax,
				'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				'rating'      => ($result['rating'] / 5) * 100,
				'reviews'     => $result['reviews'],
				'manufacturer' => $result['manufacturer'],
				'href'        => $this->url->link('product/product', 'path=bestseller&product_id=' . $result['product_id'] . $url)
			);


		}

		$url = '';

		if (isset($this->request->get['filter'])) {
			$url .= '&filter=' . $this->request->get['filter'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.sort_order&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'value' => 'pd.name-ASC',
			'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=pd.name&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'value' => 'pd.name-DESC',
			'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=pd.name&order=DESC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_asc'),
			'value' => 'p.price-ASC',
			'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.price&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_desc'),
			'value' => 'p.price-DESC',
			'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.price&order=DESC' . $url)
		);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=rating&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => 'rating-ASC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=rating&order=ASC' . $url)
			);
		}

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_model_asc'),
			'value' => 'p.model-ASC',
			'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.model&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_model_desc'),
			'value' => 'p.model-DESC',
			'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.model&order=DESC' . $url)
		);

		$url = '';

		if (isset($this->request->get['filter'])) {
			$url .= '&filter=' . $this->request->get['filter'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['limits'] = array();

		$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('product/bestseller', 'path=bestseller'  . $url . '&limit=' . $value)
			);
		}

		$url = '';

		if (isset($this->request->get['filter'])) {
			$url .= '&filter=' . $this->request->get['filter'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$this->load->library('mobilePagination');

		$pagination = new mobilePagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->num_links = 6;
		$pagination->url = $this->url->link('product/bestseller', 'path=bestseller'  . $url . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
		    $this->document->addLink($this->url->link('product/bestseller', 'path=bestseller'  , true), 'canonical');
		} elseif ($page == 2) {
		    $this->document->addLink($this->url->link('product/bestseller', 'path=bestseller' , true), 'prev');
		} else {
		    $this->document->addLink($this->url->link('product/bestseller', 'path=bestseller&page='. ($page - 1), true), 'prev');
		}

		if ($limit && ceil($product_total / $limit) > $page) {
		    $this->document->addLink($this->url->link('product/bestseller', 'path=&page='. ($page + 1), true), 'next');
		}

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');

		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['column_right'] = $this->load->controller('common/column_right');
		// $data['content_top'] = $this->load->controller('common/content_top');
		// $data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['slideshow'] = $this->load->controller('extension/module/slideshow');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('product/bestseller', $data));
	}

	public function preview() {
		$this->document->setTitle('Best Seller');
		$data['heading_title'] = 'Best Seller';

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if ($this->customer->isLogged()) {
			$this->load->model('membership/membership');
			$membership_info = $this->model_membership_membership->getActiveMembership($this->customer->getId());
		}

		$data['products'] = array();
		$limit = 6;

		$results = $this->model_catalog_product->getBestSellerProducts($limit);

		if ($results) {
			foreach ($results as $result) {
				if (file_exists('image/'.$result['image'])) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					if (!empty($membership_info)) {
						$member_price = $result['price'] - (($result['price'] - $result['base_price']) * ($membership_info['membership_discount']/100));
						$price = $this->currency->format($this->tax->calculate($member_price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						$pricenum = $member_price;
					} else {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						$pricenum = $result['price'];
					}
				} else {
					$price = false;
					$pricenum = false;
				}

				if ($result['price_retail'] > 0) {
					$price_retail = $this->currency->format($this->tax->calculate($result['price_retail'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price_retail = $this->session->data['currency'] . 0;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$url = '';

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'price_retail'=> $price_retail,
					'rate'		  => ($result['price_retail'] > 0) ? round(100 - (($pricenum / $result['price_retail']) * 100)) : 0,
					'special'     => $special,
					'discount'	  => $special ? round(($result['special'] / $pricenum) * 100) : 0,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => ($result['rating'] / 5) * 100,
					'reviews'     => $result['reviews'],
					'manufacturer' => $result['manufacturer'],
					'href'        => $this->url->link('product/product', 'path=bestseller&product_id=' . $result['product_id'])
				);
			}
		}

		// echo '<pre>';
		// var_dump($results);
		// exit;

		$data['more_best'] = $this->url->link('product/bestseller', 'path=bestseller');

		$data['slideshow'] = $this->load->controller('extension/module/slideshow');
		$data['featured'] = $this->load->controller('extension/module/featured');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('product/bestseller_preview', $data));
	}
}
