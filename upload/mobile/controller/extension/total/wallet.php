<?php
class ControllerExtensionTotalWallet extends Controller {
	public function index() {
		if ($this->config->get('wallet_status')) {
			$this->load->language('extension/total/wallet');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_wallet'] = $this->language->get('entry_wallet');

			$data['button_wallet'] = $this->language->get('button_wallet');

			if (isset($this->session->data['wallet'])) {
				$data['wallet'] = $this->session->data['wallet'];
			} else {
				$data['wallet'] = '';
			}

			return $this->load->view('extension/total/wallet', $data);
		}
	}

	public function wallet() {
		echo "xx<br/>";
		$this->load->language('extension/total/wallet');

		$json = array();

		$this->load->model('extension/total/wallet');

		if (isset($this->request->post['wallet'])) {
			$wallet = $this->request->post['wallet'];
		} else {
			$wallet = '';
		}

		$wallet_info = $this->model_extension_total_wallet->getCoupon($wallet);
		//var_dump($wallet_info);EXIT;
		if (empty($this->request->post['wallet'])) {
			$json['error'] = $this->language->get('error_empty');

			unset($this->session->data['wallet']);
		} elseif ($wallet_info) {
			$this->session->data['wallet'] = $this->request->post['wallet'];

			$this->session->data['success'] = $this->language->get('text_success');

			$json['redirect'] = $this->url->link('checkout/cart');
		} else {
			$json['error'] = $this->language->get('error_wallet');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
