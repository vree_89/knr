<?php
class ControllerExtensionModuleBlog extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/blog');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('extension/module/blog');

		$this->load->model('tool/image');

		$data['blogs'] = array();
		//var_dump($this->model_extension_module_blog);
		//$results = $this->model_extension_module_blog->getBlogs($setting['limit']);

		// var_dump($setting['blog']);
		// exit;
		if (!empty($setting['blog'])) {
			$blogs = array_slice($setting['blog'], 0, (int)10);

			foreach ($blogs as $blog_id) {
				$blog_info = $this->model_extension_module_blog->getBlog($blog_id);
				if ($blog_info) {
					if ($blog_info['image']) {
						$image = $this->model_tool_image->resize($blog_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}



					$data['blogs'][] = array(
						'blog_id'  => $blog_info['blog_id'],
						'title'       => $blog_info['title'],
						'text'        => strip_tags(html_entity_decode($blog_info['text'], ENT_QUOTES, 'UTF-8')),
						'image' => HTTP_SERVER.'image/'.$blog_info['image'],
						'input_date'  => $blog_info['input_date']
					);
				}
			}

		}

		if ($data['blogs']) {
			return $this->load->view('extension/module/blog', $data);
		}		

		
		// if ($results) {
		// 	foreach ($results as $result) {
		// 		if ($result['image']) {
		// 			$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
		// 		} else {
		// 			$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
		// 		}





		// 		$data['blogs'][] = array(
		// 			'blog_id'  => $result['blog_id'],
		// 			'title'       => $result['title'],
		// 			'text'        => strip_tags(html_entity_decode($result['text'], ENT_QUOTES, 'UTF-8')),
		// 			'image' => HTTP_SERVER.'image/'.$result['image'],
		// 			'input_date'  => $result['input_date']
		// 		);
		// 	}
			
		// 	return $this->load->view('extension/module/blog', $data);
		// }
		//return $this->load->view('extension/module/blog', $data);
	}
}
