<?php
class ControllerExtensionModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if ($this->customer->isLogged()) {
			$this->load->model('membership/membership');
			$membership_info = $this->model_membership_membership->getActiveMembership($this->customer->getId());
		}

		$data['products'] = array();
		$data['bestseller'] = array();

		if (empty($setting)) {
			$this->load->model('extension/module');
			$setting = $this->model_extension_module->getModuleByName("Featured");
			$setting['limit'] = 6;
		}

		if (!isset($setting['limit'])) {
			$setting['limit'] = 12;
		}
		
		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);
				if ($product_info) {
					if (file_exists('image/'.$product_info['image'])) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('product-noimage.jpg', $setting['width'], $setting['height']);
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						if (!empty($membership_info)) {
							$member_price = $product_info['price'] - (($product_info['price'] - $product_info['base_price']) * ($membership_info['membership_discount']/100));
							$price = $this->currency->format($this->tax->calculate($member_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							$pricenum = $member_price;
						} else {
							$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							$pricenum = $product_info['price'];
						}
					} else {
						$price = false;
						$pricenum = false;
					}

					if ($product_info['price_retail'] > 0) {
						$price_retail = $this->currency->format($this->tax->calculate($product_info['price_retail'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price_retail = $this->session->data['currency'] . 0;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'price_retail'=> $price_retail,
						'rate'		  => ($product_info['price_retail'] > 0) ? round(100 - (($pricenum / $product_info['price_retail']) * 100)) : 0,
						'special'     => $special,
						'discount'	  => $special ? round(($product_info['special'] / $pricenum) * 100) : 0,
						'tax'         => $tax,
						'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
						'rating'      => ($product_info['rating'] / 5) * 100,
						'reviews'     => $product_info['reviews'],
						'manufacturer' => $product_info['manufacturer'],
						'href'        => $this->url->link('product/product', 'path=bestseller&product_id=' . $product_info['product_id'])
					);
				}
			}

		}

		if ($data['products']) {
			return $this->load->view('extension/module/featured', $data);
		}
	}
}