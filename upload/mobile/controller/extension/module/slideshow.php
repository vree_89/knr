<?php
class ControllerExtensionModuleSlideshow extends Controller {
	public function index($setting) {
		static $module = 0;

		if (!isset($setting['banner_id'])) {
			$this->load->model('extension/module');
			$setting = $this->model_extension_module->getModuleByName("Slideshow");
		}

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$data['slideshows'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['slideshows'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => HTTP_SERVER.'image/'.$result['image']
				);
			}
		}

		return $this->load->view('extension/module/slideshow', $data);
	}
}
