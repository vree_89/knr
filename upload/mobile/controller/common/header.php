<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		// $data['text_home'] = $this->language->get('text_home');

		// Wishlist
		// if ($this->customer->isLogged()) {
		// 	$this->load->model('account/wishlist');

		// 	$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		// } else {
		// 	$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		// }

		// $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		// $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		// $data['text_account'] = $this->language->get('text_account');
		// $data['text_register'] = $this->language->get('text_register');
		// $data['text_login'] = $this->language->get('text_login');
		// $data['text_order'] = $this->language->get('text_order');
		// $data['text_transaction'] = $this->language->get('text_transaction');
		// $data['text_download'] = $this->language->get('text_download');
		// $data['text_logout'] = $this->language->get('text_logout');
		// $data['text_checkout'] = $this->language->get('text_checkout');
		// $data['text_category'] = $this->language->get('text_category');
		// $data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['blog'] = $this->url->link('blog/blog');
		// $data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		// $data['account'] = $this->url->link('account/account', '', true);
		// $data['register'] = $this->url->link('account/register', '', true);
		// $data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		//$data['download'] = $this->url->link('account/download', '', true);
		// $data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		// $data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		// $data['telephone'] = $this->config->get('config_telephone');

		// Menu
		$this->load->model('catalog/category');

		// $this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => ucwords(strtolower($child['name'])),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
						'path'	=> $category['category_id'] . '_' . $child['category_id']
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		// Static Menu
		$data['categories'][] = array(
			'name'     => 'Static Menu',
			'children' => array (
							array(
								'name'  => 'Best Seller',
								'href'  => $this->url->link('product/bestseller/preview', 'path=bestseller'),
								'path'	=> 'bestseller'
							)
						),
			'column'   => 0,
			'href'     => '#'
		);

		usort($data['categories'], function($a, $b) {
		    return $a['column'] - $b['column'];
		});

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			$data['informations'][] = array(
				'title' => $result['title'],
				'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
			);
		}

		// $data['current_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		// Menu More
		$data['menu_more'] = [
			[
				'name'  => 'About',
				'icon'  => 'fa fa-info',
				'href'  => $this->url->link('information/information', 'information_id=4')
			],
			// [
			// 	'name'  => 'FAQ',
			// 	'icon'  => 'fa fa-question',
			// 	'href'  => $this->url->link('information/faq', '', true)
			// ],
			[
				'name'  => 'Terms',
				'icon'  => 'fa fa-check-square-o',
				'href'  => $this->url->link('information/information', 'information_id=5')
			],
			[
				'name'  => 'Privacy',
				'icon'  => 'fa fa-gavel',
				'href'  => $this->url->link('information/information', 'information_id=3')
			],
			[
				'name'  => 'Contact',
				'icon'  => 'fa fa-envelope',
				'href'  => $this->url->link('information/contact', '', true)
			],
			[
				'name'  => 'Member',
				'icon'  => 'fa fa-child',
				'href'  => $this->url->link('membership/membership', '', true)
			],


		];

		if ($this->customer->isLogged()) {
			$data['menu_more'] = array_merge($data['menu_more'], [
				// [
				// 	'name'  => 'Redeem',
				// 	'icon'  => 'fa fa-money',
				// 	'href'  => $this->url->link('mobile/redeem', '', true)
				// ],
				[
					'name'  => 'Profile',
					'icon'  => 'fa fa-user',
					'href'  => $this->url->link('account/account', '', true)
				],
				[
					'name'  => 'Logout',
					'icon'  => 'fa fa-sign-out',
					'href'  => $this->url->link('account/logout', '', true)
				]
			]);
		} else {
			$data['menu_more'] = array_merge($data['menu_more'], [
				[
					'name'  => 'Login',
					'icon'  => 'fa fa-sign-in',
					'href'  => $this->url->link('account/login', '', true)
				],
				[
					'name'  => 'Register',
					'icon'  => 'fa fa-user-plus',
					'href'  => $this->url->link('account/register', '', true)
				]
			]);
		}



		$data['text_country'] = "";
		if(isset($this->session->data['location'])){
			$location = $this->session->data['location'];
			if($location=="id"){
				$data['text_country'] = "Indonesia";
			}
			if($location=="my"){
				$data['text_country'] = "Malaysia";
			}
			if($location=="sg"){
				$data['text_country'] = "Singapore";
			}
		}
		$data['code_currency'] = $this->session->data['currency'];



/*
      <?php foreach ($languages as $language) { ?>
        <a class="language-select" name="<?php echo $language['code']; ?>" style="cursor:pointer;"><img src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" <?php echo $code_language==$language['code'] ? 'style="border:1px solid white;"' : '' ?>/></a>
      <?php } ?>


*/

		$text_language = $this->language->get('text_language');
		$code_language = $this->session->data['language'];
		$languages = array();
		$results = $this->model_localisation_language->getLanguages();
		foreach ($results as $result) {
			if ($result['status']) {
				$languages[] = array(
					'name' => $result['name'],
					'code' => $result['code']
				);
			}
		}

   //    $html = '<form id="form-language">';
   //    foreach ($languages as $language) {
   //    	# code...
      	
   //    	$style= '';
   //    	if($code_language==$language['code']){
   //    	$style = 'style="border:1px solid black;"';
   //    	}
   //    	$html .= '<a class="language-select" name="'.$language['code'].'" style="cursor:pointer;"><img src="catalog/language/'.$language['code'].'/'.$language['code'].'.png" alt="'.$language['name'].'" title="'. $language['name'].'" '.$style.' /></a>&nbsp;';
   //    }
	  // $html .= '</form>';


		$data['menu_more'] = array_merge($data['menu_more'],[
			[
				'name'  => 'Location',
				'icon'  => 'fa fa-flag',
				'href'  => $this->url->link('common/home/changelocation')

			],
			// [
			// 	'name'  => 'Language',
			// 	'icon'  => 'fa fa-flag',
			// 	'href'  => $this->url->link('common/home/changelocation'),
			// 	'html' => $html,

			// ],

			]);

		// echo '<pre>';
		// var_dump($data['menu_more']);
		// exit;

		// $data['language'] = $this->load->controller('common/language');
		// $data['currency'] = $this->load->controller('common/currency');
		// $data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('mobile/cart');

		// echo '<pre>';
		// var_dump($data['cart']);
		// exit;

		// For page specific css
		// if (isset($this->request->get['route'])) {
		// 	if (isset($this->request->get['product_id'])) {
		// 		$class = '-' . $this->request->get['product_id'];
		// 	} elseif (isset($this->request->get['path'])) {
		// 		$class = '-' . $this->request->get['path'];
		// 	} elseif (isset($this->request->get['manufacturer_id'])) {
		// 		$class = '-' . $this->request->get['manufacturer_id'];
		// 	} elseif (isset($this->request->get['information_id'])) {
		// 		$class = '-' . $this->request->get['information_id'];
		// 	} else {
		// 		$class = '';
		// 	}

		// 	$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		// } else {
		// 	$data['class'] = 'common-home';
		// }


		//reload membership (if user by a moment ago)
		if($this->customer->isLogged()){
			$this->load->model('account/customer');
			$membership_info = $this->model_account_customer->getCustomerMembership($this->session->data['customer_id']);
			if($membership_info){
				$this->session->data['membership']['membership_name'] = $membership_info['membership_name'];
				$this->session->data['membership']['membership_deposit'] = $membership_info['membership_deposit'];
				$this->session->data['membership']['membership_expired'] = $membership_info['membership_expired'];
				$this->session->data['membership']['membership_discount'] = $membership_info['membership_discount'];
				$this->session->data['membership']['membership_loyalty_point'] = $membership_info['membership_loyalty_point'];
				$this->session->data['membership']['membership_bonus_point'] = $membership_info['membership_bonus_point'];
			}					


			//get customer deposit
			$deposit = $this->model_account_customer->getCustomerDeposit($this->session->data['customer_id']);
			$this->session->data['customer_deposit'] = $deposit;




		}


		return $this->load->view('common/header', $data);
	}

	public function product() {
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		$this->load->language('common/header');

		$data['home'] = $this->url->link('common/home');
		$data['shopping_cart'] = $this->url->link('checkout/cart');

		if ($this->customer->isLogged()) {
			$data['user_link'] = $this->url->link('account/account', '', true);
		} else {
			$data['user_link'] = $this->url->link('account/login', '', true);
		}

		$data['cart'] = $this->load->controller('mobile/cart');

		return $this->load->view('common/header_product', $data);
	}

	public function account() {
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		$this->load->language('common/header');

		$data['home'] = $this->url->link('common/home');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['reward'] = $this->url->link('account/reward', '', true);
		$data['membership'] = $this->url->link('account/membership', '', true);
		$data['address'] = $this->url->link('account/address', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['current'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$data['active'] = 'class="active"';

		return $this->load->view('common/header_account', $data);
	}
}