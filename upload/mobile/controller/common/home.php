<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['slideshow'] = $this->load->controller('extension/module/slideshow');
		$data['manufacturer'] = $this->load->controller('extension/module/manufacturer');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['logo'] = HTTP_SERVER.'image/logowhite.png';
		$data['id_flag'] = [
			'title'=>'Indonesia',
			'icon'=>HTTP_SERVER.'image/indonesia_flag.png',
			'enable'=>1,
			'href'=>$this->url->link('common/home/setlocation','location=id'),
		];
		$data['my_flag'] = [
			'title'=>'Malaysia',
			'icon'=>HTTP_SERVER.'image/malaysia_flag.png',
			'enable'=>0,
			'href'=>$this->url->link('common/home/setlocation','location=my'),
		];
		$data['sg_flag'] = [
			'title'=>'Singapore',
			'icon'=>HTTP_SERVER.'image/singapore_flag.png',
			'enable'=>0,
			'href'=>$this->url->link('common/home/setlocation','location=sg'),
		];
		// $data['id_flag'] = HTTP_SERVER.'image/indonesia_flag.png';
		// $data['my_flag'] = HTTP_SERVER.'image/malaysia_flag.png';
		// $data['sg_flag'] = HTTP_SERVER.'image/singapore_flag.png';

		// $this->response->setOutput($this->load->view('common/home', $data));

		if(isset($this->session->data['location'])){
			//die($this->session->data['location']);
			$this->response->setOutput($this->load->view('common/home', $data));
		}
		else{
			$data['href_id'] = $this->url->link('common/home/setlocation','location=id');
			$data['href_my'] = $this->url->link('common/home/setlocation','location=my');
			$data['href_sg'] = $this->url->link('common/home/setlocation','location=sg');
			$this->response->setOutput($this->load->view('common/landing', $data));	
		}		

	}
	public function changelocation(){
		unset($this->session->data['location']);
		unset($this->session->data['shipping_method']);
		unset($this->session->data['payment_method']);
		$this->response->redirect($this->url->link('common/home'));

	}

	public function setlocation(){
		$location = $this->request->get['location'];
		$this->session->data['location'] = $location;


		$current_default_currency = $this->session->data['currency'];
		$current_default_language = $this->session->data['language'];

		//set default currency and language
		$currency_info = $this->model_localisation_currency->getCurrencyByCountry($this->request->get['location']);
		if($currency_info){
			$this->session->data['currency'] = $currency_info['code'];
		}

		$language_info = $this->model_localisation_language->getLanguageByCountry($this->request->get['location']);
		if($language_info){
			$this->session->data['language'] = $language_info['code'];
		}



		//unset selected shipping method and payment method
		unset($this->session->data['payment_method']);
		unset($this->session->data['shipping_method']);


		$this->response->redirect($this->url->link('common/home'));
	}


}