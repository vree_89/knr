<?php
class ControllerCommonBrand extends Controller {
	public function index($setting) {

		$this->load->model('catalog/manufacturer');

		$this->load->model('tool/image');

		$results = $this->model_catalog_manufacturer->getManufacturers();

		foreach ($results as $result) {
			$data['brands'][] = array(
				'name' => $result['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']),
				'image' => HTTP_SERVER.'image/'.$result['image_mobile']

			);
		}

		return $this->load->view('common/brand', $data);
	}
}
