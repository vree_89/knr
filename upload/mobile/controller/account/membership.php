<?php
class ControllerAccountMembership extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/membership', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('membership/membership');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_package'] = $this->language->get('column_package');
		$data['column_deposit'] = $this->language->get('column_deposit');
		$data['column_date_expired'] = $this->language->get('column_date_expired');

		$data['text_total'] = $this->language->get('text_total');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['rewards'] = array();

		$limit = 5;

		$filter_data = array(
			'sort'  => 'date_added',
			'order' => 'DESC',
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);

		$order_total = $this->model_membership_membership->getTotalOrder();

		$results = $this->model_membership_membership->getOrder($filter_data);
		$data['orders'] = array();
		foreach ($results as $result) {
			$data['orders'][] = array(
				'order_id'    	=> $result['customer_membership_id'],
				'package'      	=> $result['membership_name'],
				'deposit' 		=> $this->session->data['currency'].number_format($result['membership_deposit']),
				'expired_date' 	=> date($this->language->get('date_format_short'), strtotime($result['payment_date'] . " +".$result['membership_period']." days")),
				'date_added'  	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				// 'href'        	=> $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true)
			);
		}

		$this->load->library('mobilePagination');

		$pagination = new mobilePagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->num_links = 6;
		$pagination->url = $this->url->link('account/membership', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($order_total - $limit)) ? $order_total : ((($page - 1) * $limit) + $limit), $order_total, ceil($order_total / $limit));

		$data['continue'] = $this->url->link('account/account', '', true);
		$data['transaction'] = $this->url->link('account/order', '', true);
		$data['reward'] = $this->url->link('account/reward', '', true);

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header/account');

		$this->response->setOutput($this->load->view('account/membership', $data));
	}
}