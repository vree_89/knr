<?php
class ControllerAccountForgotten extends Controller {
	private $error = array();


	public function verify_token(){
		$status = "";
		$message = "";
		$redirect = "";

		$this->load->language('account/forgotten');
		$this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomerByCode($this->request->post['token']);
		if($customer_info){
			if(strtotime($customer_info['code_expired_date'])>strtotime(date('Y-m-d H:i:s'))){
				$status  = "1";
				$message  = "User found";
				$redirect = $this->url->link('account/reset&code='. $this->request->post['token']);
			}
			else{
				$status  = "0";
				$message  = "Token not match or already expired";
				$redirect = "";
			}
		}
		else{
			$status  = "0";
			$message  = "Token not match";
			$redirect = "";
		}
		$res = array(
			"status" =>$status,
			"message" =>$message,
			"redirect"=>$redirect,
			);
		echo json_encode($res);

	}

	public function request_token(){
		$status = "";
		$message = "";

		$this->load->language('account/forgotten');
		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['telephone']);
		if(!$customer_info){
			$res = array(
				"status" =>"2",
				"message" =>$this->language->get('error_phone_not_exists'),
				);
			echo json_encode($res);
			return;
		}


			// $url = 'http://104.199.196.122/gateway/v1/call';

			// $arrayBody = [ 
			// 'userid' => "GamesPark",
			// 'password' => "GPark!Intv",
			// 'msisdn' => $this->beautify_id_number($this->request->post['telephone']),
			// 'gateway' => 0,
			// ];

			// $json = json_encode( $arrayBody );

			// $ch = curl_init($url);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			// curl_setopt($ch, CURLOPT_POST, 1);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			// curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
			// $output = curl_exec($ch);
				
			// curl_close($ch);
			// $arrayResult = json_decode( $output, true );

			$arrayResult['result'] = "Success";
			$arrayResult['rc'] = "00";
			$arrayResult['token'] = "6221304".$this->randomNumber();

			if(@$arrayResult['result']=="Success" && @$arrayResult['rc']=="00"){
				$this->model_account_customer->editCodeByTelephone($this->request->post['telephone'], substr($arrayResult['token'],  -5));
				$this->model_account_customer->editLastPasswordChange($this->request->post['telephone']);

				$status = "1";
				$message = sprintf($this->language->get('text_miscall_success'), $this->masking($arrayResult['token']));
			}
			else if(@$arrayResult['rc']!="00"){
				$status = "0";
				if($arrayResult['rc']=="06"){
				$message = "Phone number is invalid.";
				}
				else{
				$message = "Verify failed, please try again later.";
				}
			}
			else{
				$status = "0";
				$message = $this->language->get('text_miscall_failed');
			}


		$res = array(
			"status" =>$status,
			"message" =>$message,
			);
		echo json_encode($res);

	}

	public function index() {
		if (!isset($this->session->data['location'])) {
			$this->session->data['redirect'] = $this->url->link('account/forgotten');
			$this->response->redirect($this->url->link('common/home', '', true));
		}

		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->language('mail/forgotten');

			// $code = token(40);

			// $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

			// $this->model_account_customer->editCode($customer_info['telephone'], $code);

			// $this->model_account_customer->editLastPasswordChange($customer_info['telephone']);

			// if (!empty($customer_info['email'])){
			// 	$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

			// 	$message  = sprintf($this->language->get('text_greeting'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";
			// 	$message .= $this->language->get('text_change') . "\n\n";
			// 	$message .= $this->url->link('account/reset', 'code=' . $code, true) . "\n\n";
			// 	$message .= sprintf($this->language->get('text_ip'), $this->request->server['REMOTE_ADDR']) . "\n\n";

			// 	$mail = new Mail();
			// 	$mail->protocol = $this->config->get('config_mail_protocol');
			// 	$mail->parameter = $this->config->get('config_mail_parameter');
			// 	$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			// 	$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			// 	$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			// 	$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			// 	$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			// 	$mail->setTo($customer_info['email']);
			// 	$mail->setFrom($this->config->get('config_email'));
			// 	$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			// 	$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			// 	$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			// 	$mail->send();
			// } else {
			// 	if ($_SERVER['HTTP_HOST'] == 'localhost') { 
			// 		$url = 'http://'.getHostByName(getHostName()).'/knr/upload/index.php?route=account/reset&code='.$code;
			// 	} else {
			// 		$url = $this->url->link('account/reset', 'code=' . $code, true);
			// 	}
					
			// 	$smstext = 'Your reset password link : '.$url;

			// 	$data = array(
			// 		"no_hp"    =>  $customer_info['telephone'],
			// 		"smstext"  =>  urlencode($smstext),
			// 	);

			// 	// create curl resource
			// 	$ch = curl_init();
			// 	curl_setopt($ch, CURLOPT_URL, "http://api.popay.co.id:8182/SMSGW/kirimsms");
			// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			// 	curl_setopt($ch, CURLOPT_POST, 1);
			// 	curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
				     

			// 	$output = curl_exec($ch);
			// 	curl_close($ch);
			// }

			// $this->session->data['success'] = $this->language->get('text_success');

			// // Add to activity log
			// if ($this->config->get('config_customer_activity')) {
			// 	$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

			// 	if ($customer_info) {
			// 		$this->load->model('account/activity');

			// 		$activity_data = array(
			// 			'customer_id' => $customer_info['customer_id'],
			// 			'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
			// 		);

			// 		$this->model_account_activity->addActivity('forgotten', $activity_data);
			// 	}
			// }

			// $this->response->redirect($this->url->link('account/login', '', true));


			$this->load->language('account/forgotten');
			$this->load->model('account/customer');

			// $url = 'http://104.199.196.122/gateway/v1/call';

			// $arrayBody = [ 
			// 'userid' => "GamesPark",
			// 'password' => "GPark!Intv",
			// 'msisdn' => $this->beautify_id_number($this->request->post['telephone']),
			// 'gateway' => 0,
			// ];

			// $json = json_encode( $arrayBody );

			// $ch = curl_init($url);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			// curl_setopt($ch, CURLOPT_POST, 1);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			// curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
			// $output = curl_exec($ch);
			// echo $output;	
			// curl_close($ch);
			// $arrayResult = json_decode( $output, true );
			$arrayResult['result'] = "Success";
			$arrayResult['rc'] = "00";
			$arrayResult['token'] = "6221304".$this->randomNumber(5);
			if(@$arrayResult['result']=="Success" && @$arrayResult['rc']=="00"){
				//echo "a";

				$this->model_account_customer->editCodeByTelephone($this->request->post['telephone'], substr($arrayResult['token'],  -5));
				//$this->model_account_customer->editLastPasswordChange($this->request->post['telephone']);


				//$data['success']  = sprintf($this->language->get('text_miscall_success'), $this->masking($arrayResult['token']));
				//$data['success']  = sprintf($this->language->get('text_miscall_success'), $this->masking($arrayResult['token']));


				$prenumber = substr($arrayResult['token'],0,  (strlen($arrayResult['token'])-5));
				$prenumber = "+".$this->beautify_id_number($prenumber);
				//echo $prenumber;
				$this->session->data['prenumber'] = $prenumber;
				//echo $this->session->data['prenumber'];exit;
				$this->response->redirect($this->url->link('account/forgotten/verify', '', true));
				//exit;
			}
			else if(@$arrayResult['rc']!="00"){
				//echo "b";
				if($arrayResult['rc']=="06"){
					$data['error_warning'] = "Phone number is invalid.";
				}
				else{
					$data['error_warning'] = "Verify failed, please try again later.";
				}
				//exit;

			}
			


			else{
				$data['error_warning'] = $this->language->get('text_miscall_failed');
			}





		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('account/forgotten', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_your_email'] = $this->language->get('text_your_email');
		$data['text_email'] = $this->language->get('text_email');

		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten', '', true);

		$data['back'] = $this->url->link('account/login', '', true);

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['column_right'] = $this->load->controller('common/column_right');
		// $data['content_top'] = $this->load->controller('common/content_top');
		// $data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/forgotten', $data));
	}

private function randomNumber($length = 4) {
    $result = '';

    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(0, 9);
    }

    return $result;
}		

	protected function validate() {
		if (!isset($this->request->post['telephone'])) {
			$this->error['warning'] = $this->language->get('error_telephone');
		} elseif (!$this->model_account_customer->getTotalCustomersByPhone($this->request->post['telephone'])) {
			$this->error['warning'] = $this->language->get('error_telephone');
		} 

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['telephone']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if ($customer_info && !$customer_info['verified']) {
			$this->error['warning'] = $this->language->get('error_verified');
		}	
		
		// if ($customer_info && $customer_info['last_password_change'] == date('Y-m-d') && !$customer_info['email']) {
		// 	$this->error['warning'] = $this->language->get('error_limit');
		// }


		if((int)$customer_info['count_password_change']>=3 && $customer_info['last_password_change']==date('Y-m-d')){
			$this->error['warning'] = $this->language->get('error_limit');
		}

		// if (!isset($this->request->post['email'])) {
		// 	$this->error['warning'] = $this->language->get('error_email');
		// } elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
		// 	// if (!$this->model_account_customer->getTotalCustomersByTelephone($this->request->post['email'])) {
		// 	// 	$this->error['warning'] = $this->language->get('error_email');
		// 	// }
		// 	$this->error['warning'] = $this->language->get('error_email');
		// } 

		// $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		// if ($customer_info && !$customer_info['approved']) {
		// 	$this->error['warning'] = $this->language->get('error_approved');
		// }

		// if ($customer_info && !$customer_info['verified']) {
		// 	$this->error['warning'] = $this->language->get('error_verified');
		// }	
		
		// if ($customer_info && $customer_info['last_password_change'] == date('Y-m-d') && !$customer_info['email']) {
		// 	$this->error['warning'] = $this->language->get('error_limit');
		// }

		return !$this->error;
	}

	protected function validateVerify(){
		$this->load->language('account/forgotten');
		$this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomerByCode($this->request->post['token']);
		if(!$customer_info){
			$this->error['warning'] = "Token not match";
		}

		return !$this->error;
	}


	private  function beautify_id_number( $mdn , $zero = false ){
		$check = true;
		while ( $check == true ){
			$check = false;
			if( !is_numeric(substr( $mdn , 0 , 1 )) ){
				$mdn = substr( $mdn, 1 );
				$check = true;
			}

			if( substr( $mdn , 0 , 2 ) == '62' ){
				$mdn = substr( $mdn, 2 );
				$check = true;
			}

			while ( substr( $mdn , 0 , 1 ) == '0' ){
				$mdn = substr( $mdn, 1 );
				$check = true;
			}
		}

		if ( $zero ){
			$mdn = '0'.$mdn;
		}else{
			$mdn = '62'.$mdn;
		}

		return $mdn;

    }



function masking($number, $maskingCharacter = 'X') {
	$string = "";
	$arr = str_split($number);
	for($i=0;$i<strlen($number);$i++){
		if($i>=(strlen($number)-5)){ 
			$string .= "x";
		}
		else{
			$string .= $arr[$i];
		}
	}
	return $string;
} 

	public function verify() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateVerify()) {
			$this->response->redirect($this->url->link('account/reset&code='. $this->request->post['token']));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('account/forgotten', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten/verify', '', true);


		if (isset($this->request->post['token'])) {
			$data['token'] = $this->request->post['token'];
		} else {
			$data['token'] = '';
		}

		if (isset($this->session->data['prenumber'])) {
			$data['prenumber'] = $this->session->data['prenumber'];
		} else {
			$data['prenumber'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/forgotten_verify', $data));
	}

}
