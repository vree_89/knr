<?php
class ControllerMembershipSuccess extends Controller {
	public function index() {
		$this->load->language('membership/membership');

		if (isset($this->session->data['membership_order_id'])) {
			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' 			=> $this->customer->getId(),
					'name'        			=> $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
					'membership_id' 		=> $this->session->data['membership_id'],
					'membership_name' 		=> $this->session->data['membership_name'],
					'membership_order_id'   => $this->session->data['membership_order_id']
				);
			}

			unset($this->session->data['membership_id']);
			unset($this->session->data['membership_name']);
			unset($this->session->data['membership_order_id']);
			unset($this->session->data['payment_methods']);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/membership', '', true), $this->url->link('information/contact'));

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}