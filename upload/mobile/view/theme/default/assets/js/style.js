$(document).ready(function() {
	$("a.dropdown").on("click", function(event){
		event.preventDefault();

		var container = $(this).data("target");
		var list = $(this).closest("li");

		if ($(container).css("display") == "block"  && list.hasClass("active") === true){
			$("body").removeClass("navbar-more-show");
			$(".navbar-nav li").removeClass("active");
			$(".navbar-more").delay(500).hide(0);
		} else {
			$("body").addClass("navbar-more-show");
			$(".navbar-nav li").removeClass("active");
			$(list).addClass("active");
			$(".navbar-more").hide();
			$(container).show();
		}
	});

	$(".navbar-more-overlay").on("click", function(event){
		$("body").removeClass("navbar-more-show");
		$(".navbar-nav li").removeClass("active");
		$(".navbar-more").delay(500).hide(0);
	});

	//Enable swiping...
	$(".carousel-inner").swipe( {
		//Generic swipe handler for all directions
		swipeLeft:function(event, direction, distance, duration, fingerCount){
			$(this).parent().carousel("next"); 
		},
		swipeRight: function() {
			$(this).parent().carousel("prev"); 
		},
		//Default is 75px, set to 0 for demo so any distance triggers swipe
		threshold:0
	});

	$(document).on("click",".value-control",function(){
	    var action = $(this).attr("data-action")
	    var target = $(this).attr("data-target")
	    var value  = parseFloat($("[id='"+target+"']").val());
	    if(action == "plus"){
	      	value++;
	    }
	    if(action == "minus"){
	    	if(value > 1){
	      		value--;
	      	}
	    }
	    $("[id='"+target+"']").val(value)
	});

	$(document).on("click",".notes-edit",function(){
		$(this).closest(".notes-text").hide();
		$(this).closest(".well").find(".notes-input").show();
	});

	$(document).on("click",".notes-update",function(){
		$(this).closest(".notes-input").hide();
		$(this).closest(".well").find(".notes-text").show();
	});

	$(document).on("click",".color-edit",function(){
		$(this).closest(".color-text").hide();
		$(this).closest(".well").find(".color-input").show();
	});

	$(document).on("click",".color-update",function(){
		$(this).closest(".color-input").hide();
		$(this).closest(".well").find(".color-text").show();
	});

	$("input:checkbox#shipping-option").click(function(){
		var target = $(this).data("target");
        $(target).animate({height: "toggle"});
    }); 

	$(document).on("click","#update-profile, #profile-close",function(){
		$("#profile-info").animate({height: "toggle"});
		$("#profile-form").animate({height: "toggle"});
	});

	$(document).on("click",".update-address, #address-close, #add-address",function(){
		$("#address-list").animate({height: "toggle"});
		$("#address-form").animate({height: "toggle"});
	}); 

	$(document).on("click",".detail-transaction, #transaction-close",function(){
		$("#transaction-list").animate({height: "toggle"});
		$("#transaction-detail").animate({height: "toggle"});
	});

	$("input[name='address']").change(function(){
	   if($(this).is(":checked")) {
	        $("#address-list").modal("hide");
	   }
	});
});