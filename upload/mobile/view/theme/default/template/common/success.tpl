<?php echo $header; ?>
<!-- <div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div> -->
<div class="container-fluid default-height">
  <div class="success-message">
    <i class="fa fa-check-circle"></i>
    <h3><?php echo $heading_title; ?></h3>
    <?php echo $text_message; ?>
    <?php if (isset($payment_info)) { ?>
    <div class="well"><?php echo $payment_info; ?></div>
    <?php } ?>
    <a href="<?php echo $continue; ?>" class="btn btn-sm btn-pink"><?php echo $button_continue; ?> <span class="fa fa-chevron-right"></span></a>
  </div>
</div>
<?php echo $footer; ?>