<!DOCTYPE html>
<!--
  Name: KR-House Mobile Version
  Version: 1.0.0
  Author: Yudhistira EP <yudhistira.ep@gmail.com>
  Copyright 2017.
-->
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Icon -->
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <!-- <link rel="shortcut icon" type="image/x-icon" href="mobile/view/theme/default/assets/img/favicon.ico"> -->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/plugins/bootstrap/css/bootstrap.min.css" />

  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/plugins/font-awesome/css/font-awesome.min.css" />

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/assets/css/style.css" />

  <!-- jQuery -->
  <script type="text/javascript" src="mobile/view/theme/default/assets/js/jquery.min.js"></script>

  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>
</head>

<body>
  <!-- Navbar -->
  <div class="navbar-more-overlay"></div>
  <nav class="navbar navbar-inverse navbar-fixed-top animate">
    <div class="container navbar-more" id="menu-more">
      <div class="logo">
        <?php if ($logo) { ?>
        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/></a>
        <?php } else { ?>
        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
        <?php } ?>
      </div>
      <ul class="list-inline">
        <?php foreach ($menu_more as $menu) { ?>
        <?php
        if(isset($menu['html'])){
          ?>
        <li>
          
            <span class="menu-icon <?php echo $menu['icon']; ?>"></span>
            <?php echo $menu['name']; ?>
            <?php echo $menu['html']; ?>
          
        </li>

          <?php

        }
        else{
          ?>
        <li>
          <a href="<?php echo $menu['href']; ?>">
            <span class="menu-icon <?php echo $menu['icon']; ?>"></span>
            <?php echo $menu['name']; ?>
          </a>
        </li>

          <?php

        }
        ?>
        <?php } ?>
      </ul>
      <div class="text-right" style="font-size:smaller;">Location : <?php echo $text_country?> | Currency : <?php echo $code_currency?></div>
    </div>

    <div class="container navbar-more" id="product-more">
      <form class="navbar-form navbar-left form-search" role="search">
        <div class="form-group">
          <div class="input-group" id="search">
            <input type="text" name="search" class="form-control" placeholder="Search for product">
            <span class="input-group-btn">
              <button class="btn btn-default btn_search" type="button">Find</button>
            </span>
          </div>
        </div>
      </form>
      <h5>Category :</h5>
      <ul class="list-inline">
        <?php foreach ($categories as $category) { ?>
          <?php if ($category['children']) { ?>
            <?php foreach ($category['children'] as $child) { ?>
              <?php $active = (isset($_GET['path']) && $child['path'] == $_GET['path']) ? 'class="active"' : ''; ?>
              <li <?php echo $active; ?>><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      </ul>
    </div>
    <div class="container">

      <ul class="nav navbar-nav mobile-bar">
        <li>
          <a href="<?php echo $home; ?>">
            <span class="menu-icon fa fa-home"></span>
            Home
          </a>
        </li>
        <li>
          <a href="#" class="dropdown" data-target="#product-more">
            <span class="menu-icon fa fa-cubes"></span>
            Product
          </a>
        </li>
        <li>
          <a href="<?php echo $blog; ?>">
            <span class="menu-icon fa fa-file-text-o"></span>
            Blog
          </a>
        </li>
        <li>
          <a href="<?php echo $shopping_cart; ?>">
            <span class="menu-icon fa fa-shopping-bag">
              <span class="badge"><?php echo $cart; ?></span>
            </span>
            Cart
          </a>
        </li>
        <li>
          <a href="#" class="dropdown" data-target="#menu-more">
            <span class="menu-icon fa fa-bars"></span>
            More
          </a>
        </li>
      </ul>

    </div>
  </nav>
  <!-- /Navbar -->