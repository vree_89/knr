<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KR House - Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="catalog/view/theme/default/agency/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="catalog/view/theme/default/agency/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <!-- <link href="catalog/view/theme/default/agency/css/agency.css" rel="stylesheet"> -->

  </head>

  <body id="landing-bg">

      <section class="container-fluid landing-container">
          <div class="landing_logo"><img src="<?php echo $logo?>"></div>
          <div class="landing_header">Choose your Location</div>

          <div class="row landing_region_list">
                    <div class="col-sm-4 text-center">
                      <?php
                      if($id_flag['enable']){
                        ?>
                        <a class="region-link" href="<?php echo $id_flag['href']?>"><img src="<?php echo $id_flag['icon']?>" class="img-fluid <?php echo $id_flag['enable']==1 ? '' : 'disabled_flag'; ?>"><br/><span><?php echo $id_flag['title']?></span></a>
                        <?php
                      }
                      else{
                        ?>
                        <a class="region-link disabled_flag" href="#" onclick="return false;"><img src="<?php echo $id_flag['icon']?>" class="img-fluid "><br/><span>(Coming soon)</span></a>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="col-sm-4 text-center">
                      <?php
                      if($my_flag['enable']){
                        ?>
                        <a class="region-link" href="<?php echo $my_flag['href']?>"><img src="<?php echo $my_flag['icon']?>" class="img-fluid <?php echo $my_flag['enable']==1 ? '' : 'disabled_flag'; ?>"><br/><span><?php echo $my_flag['title']?></span></a>
                        <?php
                      }
                      else{
                        ?>
                        <a class="region-link disabled_flag" href="#" onclick="return false;"><img src="<?php echo $my_flag['icon']?>" class="img-fluid "><br/><span>(Coming soon)</span></a>
                        <?php
                      }
                      ?>


                    </div>
                    <div class="col-sm-4 text-center">
                      <?php
                      if($sg_flag['enable']){
                        ?>
                        <a class="region-link" href="<?php echo $sg_flag['href']?>"><img src="<?php echo $sg_flag['icon']?>" class="img-fluid <?php echo $sg_flag['enable']==1 ? '' : 'disabled_flag'; ?>"><br/><span><?php echo $sg_flag['title']?></span></a>
                        <?php
                      }
                      else{
                        ?>
                        <a class="region-link disabled_flag" href="#" onclick="return false;"><img src="<?php echo $sg_flag['icon']?>" class="img-fluid "><br/><span>(Coming soon)</span></a>
                        <?php
                      }
                      ?>

                    </div>
          </div>          
          <div class="landing_footer">
            <p style="font-style:italic">"House to Enchant your Beauty"</p>
          </div>
      </section>



    <!-- Bootstrap core JavaScript -->
    <script src="catalog/view/theme/default/agency/vendor/jquery/jquery.min.js"></script>
    <script src="catalog/view/theme/default/agency/vendor/popper/popper.min.js"></script>
    <script src="catalog/view/theme/default/agency/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="catalog/view/theme/default/agency/vendor/jquery-easing/jquery.easing.min.js"></script>


  </body>

</html>
<style type="text/css">
#landing-bg{
    background: #222222;
}
.landing-container{
    margin-top: 5%;
    text-align: center;
}
.landing-container>.landing_logo{
    margin-bottom: 10%;
}

.landing-container>.landing_header{
    margin-bottom: 1%;
    font-size: xx-large;
    text-transform: uppercase;
}
.landing-container>.landing_region_list{
  margin-bottom: 10%;
}
.landing-container, .landing-container a.region-link{
    color: white;
    text-transform: uppercase;
}
.landing-container>.landing_footer p{
  font-style: italic
}
</style>