<!DOCTYPE html>
<!--
  Name: KR-House Mobile Version
  Version: 1.0.0
  Author: Yudhistira EP <yudhistira.ep@gmail.com>
  Copyright 2017.
-->
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Icon -->
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <!-- <link rel="shortcut icon" type="image/x-icon" href="mobile/view/theme/default/assets/img/favicon.ico"> -->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/plugins/bootstrap/css/bootstrap.min.css" />

  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/plugins/font-awesome/css/font-awesome.min.css" />

  <!-- DataTables -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/plugins/dataTables/css/dataTables.bootstrap.min.css" />

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/assets/css/style.css" />

  <!-- jQuery -->
  <script type="text/javascript" src="mobile/view/theme/default/assets/js/jquery.min.js"></script>

  <!-- DataTables -->
  <script type="text/javascript" src="mobile/view/theme/default/plugins/dataTables/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="mobile/view/theme/default/plugins/dataTables/js/dataTables.bootstrap.min.js"></script>
  
  <!-- ShareThis -->
  <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=58cbad2b0743420011ab1659&product=inline-share-buttons' async='async'></script>

  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>
</head>

<body>
  <!-- Navbar -->
  <div class="navbar-more-overlay"></div>
  <nav class="navbar navbar-inverse navbar-fixed-top animate">
    <div class="container">
      <ul class="nav navbar-nav mobile-bar">
        <li>
          <a href="<?php echo $home; ?>">
            <span class="menu-icon fa fa-home"></span>
            Home
          </a>
        </li>
        <li <?= ($current == $account) ? $active : ($current == $wishlist) ? $active : ''; ?>>
          <a href="<?php echo $account; ?>">
            <span class="menu-icon fa fa-user"></span>
            Profile
          </a>
        </li>
        <li <?= ($current == $order) ? $active : ($current == $reward) ? $active : ($current == $membership) ? $active : ''; ?>>
          <a href="<?php echo $order; ?>">
            <span class="menu-icon fa fa-exchange"></span>
            History
          </a>
        </li>
        <li <?= ($current == $address) ? $active : ''; ?>>
          <a href="<?php echo $address; ?>">
            <span class="menu-icon fa fa-address-book">
            </span>
            Address
          </a>
        </li>
        <li>
          <a href="<?php echo $logout; ?>">
            <span class="menu-icon fa fa-sign-out"></span>
            Logout
          </a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /Navbar -->