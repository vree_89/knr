<footer>
    <div class="wrapper">
      <!-- Social Buttons -->
      <div class="social">
        <h4>Connect socially with <strong>KR HOUSE</strong></h4>

        <div class="row icons">
          <div class="col-xs-offset-3 col-xs-2 col-sm-offset-3 col-sm-2">
            <a href="<?php echo $config_facebookid; ?>" target="_blank">
              <i class="fa fa-facebook-square"></i>
            </a>
          </div>
          <div class="col-xs-2 col-sm-2">
            <a href="<?php echo $config_instagramid; ?>" target="_blank">
              <i class="fa fa-instagram"></i>
            </a>
          </div>
          <div class="col-xs-2 col-sm-2">
            <a href="<?php echo $config_youtubeid; ?>" target="_blank">
              <i class="fa fa-youtube-square"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /Social Buttons -->
      <!-- Copyright -->
      <div class="copyright">
        <div class="container">
          <strong>KR HOUSE</strong> &copy; 2017. All rights reserved
        </div>
      </div>
      <!-- /Copyright -->
    </div>
  </footer>

  <!-- touchSwipe -->
  <script type="text/javascript" src="mobile/view/theme/default/assets/js/jquery.touchSwipe.min.js"></script>

  <!-- Bootstrap -->
  <script type="text/javascript" src="mobile/view/theme/default/plugins/bootstrap/js/bootstrap.min.js"></script>

  <!-- Styles -->
  <script type="text/javascript" src="mobile/view/theme/default/assets/js/style.js"></script>
  <script type="text/javascript" src="mobile/view/javascript/common.js"></script>
</body>
</html>