<!-- Main Banner -->
<section class="container-fluid" id="main-banner">
  <div id="carousel-main-banner" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    	<?php
				$a=0;
				foreach ($slideshows as $slideshow) {
					$active = ($a == 0) ? 'class="active"' : '';
			?>    
      <li data-target="#carousel-main-banner" data-slide-to="<?php echo $a; ?>" <?php echo $active; ?>></li>
      <?php
					$a++;
	    	} 
      ?>
    </ol>

    <!-- Wrapper for slides -->
    <!-- Images size 800 x 400 -->
    <div class="carousel-inner">
			<?php
				$a=0;
				foreach ($slideshows as $slideshow) {
					$active = ($a == 0) ? 'active' : '';
					$a++;
			?>        
      <div class="item <?php echo $active; ?>">
        <img src="<?php echo $slideshow['image']; ?>" alt="KR-House <?php echo $slideshow['title']; ?>" title="KR-House <?php echo $slideshow['title']; ?>">
        <div class="carousel-caption">
          <a href="<?php echo $slideshow['link']; ?>"><?php echo $slideshow['title']; ?></a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</section>
<!-- /Main Banner -->