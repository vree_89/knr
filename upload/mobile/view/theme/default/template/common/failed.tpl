<?php echo $header; ?>
<div class="container-fluid default-height">
  <div class="success-message">
    <i class="fa fa-times-circle"></i>
    <h3>Oops! Something went wrong.</h3>
    <p>Server encountered an internal error. Please try again after some time.</p>
    <a href="<?php echo $continue; ?>" class="btn btn-sm btn-pink"><?php echo $button_continue; ?> <span class="fa fa-chevron-right"></span></a>
  </div>
</div>
<?php echo $footer; ?>