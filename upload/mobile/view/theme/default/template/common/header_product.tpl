<!DOCTYPE html>
<!--
  Name: KR-House Mobile Version
  Version: 1.0.0
  Author: Yudhistira EP <yudhistira.ep@gmail.com>
  Copyright 2017.
-->
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Icon -->
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/plugins/bootstrap/css/bootstrap.min.css" />

  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/plugins/font-awesome/css/font-awesome.min.css" />
  
  <!-- Jquery Star Rating -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/assets/css/rating.css" />

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="mobile/view/theme/default/assets/css/style.css" />

  <!-- jQuery -->
  <script type="text/javascript" src="mobile/view/theme/default/assets/js/jquery.min.js"></script>

  <!-- ShareThis -->
  <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=58cbad2b0743420011ab1659&product=inline-share-buttons"></script>

  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>

  <?php foreach ($analytics as $analytic) { ?>
  <?php echo $analytic; ?>
  <?php } ?>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar navbar-inverse navbar-fixed-top detail">
    <div class="container">
      <ul class="nav navbar-nav mobile-bar">
        <li>
          <a href="javascript:void(0)" onclick="goBack()">
            <span class="menu-icon fa fa-arrow-left"></span>
          </a>
        </li>
        <li>
          <a href="<?php echo $home; ?>">
            <span class="menu-icon fa fa-home"></span>
          </a>
        </li>
        <li class="product-name">
          <a><?php echo $title; ?></a>
        </li>
        <li>
          <a href="<?php echo $user_link; ?>">
            <span class="menu-icon fa fa fa-user"></span>
          </a>
        </li>
        <li>
          <a href="<?php echo $shopping_cart; ?>">
            <span class="menu-icon fa fa-shopping-bag" id="cart">
              <span class="badge"><?php echo $cart; ?></span>
            </span>
          </a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /Navbar -->

  <script>
  function goBack() {
      window.history.back();
  }
  </script>