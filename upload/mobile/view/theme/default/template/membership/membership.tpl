<?php
/*
<?php echo $header; ?>
  <!-- Membership -->
  <section class="container-fluid default-height">
    <h1 class="page-title"><?php echo $heading_title; ?></h1>
    <?php if(!$logged){ ?>
    <p>You need to <a href="<?php echo $login; ?>">login</a> or <a href="<?php echo $register; ?>">create an account</a> before selecting our membership package.</p>
    <?php } ?>

    <div class="membership mbot10">
      <?php if(count($memberships)) { ?>
      <?php foreach ($memberships as $membership) { ?>
      <div class="panel <?php echo ($membership['featured']) ? 'panel-warning' : 'panel-primary'; ?>">
        <div class="panel-heading">
          <h4 class="text-center"><?php echo $membership['name']; ?></h4>
        </div>
        <ul class="list-group list-group-flush text-center">
          <li class="list-group-item">
            Deposit <h4><?php echo $membership['price']; ?></h4>
          </li>
          <li class="list-group-item">
            Product Discount <h4><?php echo $membership['discount']; ?></h4>
          </li>
          <li class="list-group-item">
            Reward Point <h4><?php echo $membership['loyalty_point']; ?></h4>
          </li>
          <li class="list-group-item">
            Period <h4><?php echo $membership['period']; ?></h4>
          </li>
        </ul>
        <div class="panel-footer btn-hide"> <a class="btn btn-md btn-block <?php echo ($membership['featured']) ? 'btn-danger' : 'btn-primary';?>" href="<?php echo $membership['href']; ?>">JOIN NOW</a> </div>
      </div>
      <?php } ?>
      <?php } else { ?>
        <div class="not-found">
          <h3>No memberships found</h3>
        </div>
      <?php } ?>
      <div class="clearfix"></div>
    </div>
  </section>
  <!-- /Membership -->
<?php echo $footer; ?>

*/
?>
<?php echo $header; ?>
  <!-- Membership -->
  <section class="container-fluid default-height">
    <h1 class="page-title"><?php echo $heading_title; ?></h1>
    <?php if(!$logged){ ?>
    <p>You need to <a href="<?php echo $login; ?>">login</a> or <a href="<?php echo $register; ?>">create an account</a> before selecting our membership package.</p>
    <?php } ?>

    <div class="membership mbot10">
      <?php if(count($memberships)) { ?>
      <?php foreach ($memberships as $membership) { ?>
        <div class="col-xs-6 portrait" style="padding:0px 8px;margin-bottom:25px;" title="<?php echo $membership['name']; ?>" >
          <div style="padding:0;background:#ecf0f1;">
            <div class="ribbon" ><img src="<?php echo $membership['image']?>" class="img-responsive"></div>
            <h4 style="color:<?php echo $membership['text_color']?>; text-shadow:0px 1px #ffffff;margin:0;padding:0;padding-left:15px;padding-top:15px;padding-bottom:10px"><b><i><?php echo $membership['name']; ?></i></b></h4>
            <ul style="padding-left:15px;list-style-type: none;">
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $membership['price']?> deposit</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $membership['name']?> price</li>
            <!--<li><i class="fa fa-check" aria-hidden="true"></i> <?php //echo $membership['discount']?>% discount</li>-->
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $membership['loyalty_point']?>% reward</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $membership['period']?> days period</li>
            </ul>

          <div style="background:#ff3399;padding:7px 0px;color:white;margin-top:50px;" class="text-center" onclick="javascript:window.location='<?php echo $membership['href']; ?>'"><b style="font-size:x-large;">BUY PACKAGE</b></div>

          </div>
        </div>


      <?php } ?>
      <?php } else { ?>
        <div class="not-found">
          <h3>No memberships found</h3>
        </div>
      <?php } ?>
      <div class="clearfix"></div>
    </div>
  </section>
  <!-- /Membership -->
<?php echo $footer; ?>
<style type="text/css">
.ribbon{
  position: absolute;
  width: 54px;
  height: 86px;
  right: 17px;
  top:-7px;
}



@media (max-width: 479px) and (orientation:portrait) {
    .col-xs-6 {
         width: 100%;
    }
}
</style>
