<?php echo $header; ?>
  <!-- Membership -->
  <section class="container-fluid default-height" id="membership-confirm">
    <h1 class="page-title"><?php echo $heading_title; ?></h1>
    <p>You just selected this package for your membership, make sure you have pick the package that is suitable for you.</p>

    <table class="table">
      <tbody>
        <tr>
          <td>Package</td>
          <td>: <?php echo $name; ?></td>
        </tr>
        <tr>
          <td>Deposit</td>
          <td>: <?php echo $price; ?></td>
        </tr>
        <tr>
          <td>Discount</td>
          <td>: <?php echo $discount; ?></td>
        </tr>
        <tr>
          <td>Period</td>
          <td>: <?php echo $period; ?></td>
        </tr>
        <tr>
          <td>Reward Point</td>
          <td>: <?php echo $reward_point; ?></td>
        </tr>
        <tr>
          <td>Referal Point</td>
          <td>: <?php echo $referal_point; ?></td>
        </tr>
      </tbody>
    </table>

    <h4 class="checkout-subtitle">PAYMENT METHOD</h4>

    <div class="payment-selector">
    <?php if ($payment_methods) { ?>
    <?php foreach ($payment_methods as $payment_method) { ?>
    <input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" />
    <label class="drinkcard-payment" for="<?php echo $payment_method['code']; ?>" style="background-image:url(<?php echo $payment_method['image']; ?>)" title="<?php echo $payment_method['title']; ?>"></label>
    <?php } ?>
    <?php } ?>
    </div>

    <?php if ($text_agree) { ?>
    <div class="checkbox">
      <label>
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        <?php echo $text_agree; ?>
      </label>
    </div>
    <?php } ?>

    <input type="hidden" name="membership_id" id="membership_id" value="<?php echo $membership_id; ?>" />

    <div class="buttons">
      <div class="pull-left">
        <input type="button" value="Back" id="button-membership-confirm-back" class="btn btn-default btn-sm" />
      </div>
      <div class="pull-right">
        <input type="button" value="Proceed Payment" id="button-membership-confirm" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink btn-sm" />
      </div>
    </div>
    <div class="clearfix mbot10"></div>
  </section>
  <!-- /Membership -->
<?php echo $footer; ?>
<script type="text/javascript"><!--
$('#button-membership-confirm-back').on('click', function() {
  location = '<?php echo $back_link; ?>';
});

$(document).delegate('#button-membership-confirm', 'click', function() {
    $.ajax({
        url: 'index.php?route=membership/membership/save',
        type: 'post',
        data: $('#membership-confirm input[type=\'hidden\'], #membership-confirm input[type=\'checkbox\']:checked, #membership-confirm input[type=\'radio\']:checked'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-membership-confirm').button('loading');
        },
        success: function(json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#button-membership-confirm').button('reset');
                
                if (json['error']) {
                    $('#membership-confirm').prepend('<div class="alert alert-danger mtop10">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
      else if(json['data'] && json['redirect_payment_url']){
        $.redirect(json['redirect_payment_url'],json['data']); 

      }
             else {
                // location = json['continue'];
                $.redirect(json['doku_url'],json['doku_data']); 
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});
//--></script>