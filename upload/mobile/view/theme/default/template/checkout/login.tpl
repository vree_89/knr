<div class="modal-header">        
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>        
  <h4 class="modal-title"><?php echo $text_returning_customer; ?></h4>      
</div>
<div class="modal-body">
  <div class="row">
    <!-- <div class="col-sm-6">
      <h2><?php echo $text_new_customer; ?></h2>
      <p><?php echo $text_checkout; ?></p>
      <div class="radio">
        <label>
          <?php if ($account == 'register') { ?>
          <input type="radio" name="account" value="register" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="account" value="register" />
          <?php } ?>
          <?php echo $text_register; ?></label>
      </div>
      <?php if ($checkout_guest) { ?>
      <div class="radio">
        <label>
          <?php if ($account == 'guest') { ?>
          <input type="radio" name="account" value="guest" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="account" value="guest" />
          <?php } ?>
          <?php echo $text_guest; ?></label>
      </div>
      <?php } ?>
      <p><?php echo $text_register_account; ?></p>
      <input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
    </div> -->
    <div class="col-sm-12">
      <!-- <h2><?php echo $text_returning_customer; ?></h2>
      <p><?php echo $text_i_am_returning_customer; ?></p> -->
      <div class="form-group">
        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
        <input type="text" name="email" value="" id="input-email" class="form-control input-sm" />
      </div>
      <div class="form-group">
        <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
        <input type="password" name="password" value="" id="input-password" class="form-control input-sm" />
        <a class="pink" href="<?php echo $forgotten; ?>"><?php //echo $text_forgotten; ?> Forgot your password?</a></div>
      <input type="button" value="<?php echo strtoupper($button_login); ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink btn-sm btn-block" />
    </div>
  </div>
</div>
