<?php echo $header; ?>
  <div class="container-fluid mbot10 default-height">

    <?php if (!$logged) { ?>
    <h1 class="page-title">ONE STEP CHECKOUT</h1>
    <p>Please enter your details below to complete your purchase.</p>
    <p>Already have an account? <a class="pink" href="javascript:void(0)" id="show-login-form"> Click here to login</a></p>
    <?php } ?>

    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <form>
        <div id="shipping-address"></div>
        <div id="delivery-method"></div>
        <div id="order-review" class="order-review"></div>
        <div id="payment-method"></div>
    </form>
  </div>

  <div id="modal-login" class="modal in">  
    <div class="modal-dialog">   
      <div class="modal-content">      
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
var input_payment_total = <?php echo isset($input_payment_total) ? $input_payment_total : "0" ?>;
var input_payment_wallet = <?php echo isset($input_payment_wallet) ? $input_payment_wallet : "0" ?>;
var subtotal = 0;
var deposit = <?php echo isset($deposit) ? (int)$deposit : "0" ?>;
var payment_method = "";
var active = "collapse-payment-address";
var timer = false;
var interval = null;
var intervalCheck = null;

var err_wallet = false;
var err_payment = false;
var order_total = 0;
var err = false;

// hide all form except shipping address
$('#delivery-method, #order-review, #payment-method').hide();

<?php if (!$logged) { ?>
$(document).on('click', '#show-login-form', function() {
    $.ajax({
        url: 'index.php?route=checkout/login',
        dataType: 'html',
        success: function(html) {
           $('#modal-login .modal-content').html(html);
           $('#modal-login').modal('show');
          },
          error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
    });
});

$(document).ready(function() {
    $.ajax({
        url: 'index.php?route=checkout/guest_shipping',
        dataType: 'html',
        success: function(html) {
            $('#shipping-address').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});
<?php } else { ?>
$(document).ready(function() {
    // alert(input_payment_wallet);
    // alert(deposit);
    <?php if ($shipping_required) { ?>
    $.ajax({
        url: 'index.php?route=checkout/shipping_address',
        dataType: 'html',
        success: function(html) {
            $('#shipping-address').html(html);
            $('#shipping-address').show();
            $('#billing-address').hide();
            $("html, body").animate({ scrollTop: 0 }, "slow");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
    <?php } else { ?>
    $.ajax({
        url: 'index.php?route=checkout/payment_method',
        dataType: 'html',
        complete: function() {
            $('#button-shipping-method').button('reset');
        },
        success: function(html) {
            $('#payment-method').html(html);
            $('#payment-method').show();
            $('#delivery-method').hide();
            $("html, body").animate({ scrollTop: 0 }, "slow");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
    <?php } ?>
});
<?php } ?>

// Login
$(document).delegate('#button-login', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/login/save',
        type: 'post',
        data: $('#modal-login .modal-body :input'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-login').button('loading');
        },
        complete: function() {
            $('#button-login').button('reset');
        },
        success: function(json) {
            $('.alert, .text-danger').remove();
            $('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#modal-login .modal-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                // Highlight any found errors
                $('input[name=\'email\']').parent().addClass('has-error');
                $('input[name=\'password\']').parent().addClass('has-error');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

// Shipping Address
$(document).delegate('#button-shipping-address', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/shipping_address/save',
        type: 'post',
        data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'date\'], #shipping-address input[type=\'datetime-local\'], #shipping-address input[type=\'time\'], #shipping-address input[type=\'password\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address textarea, #shipping-address select'),
        dataType: 'json',
        beforeSend: function() {
      $('#button-shipping-address').button('loading');
      },
        success: function(json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#button-shipping-address').button('reset');

                if (json['error']['warning']) {
                    $('#shipping-address').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }

                for (i in json['error']) {
                    var element = $('#input-shipping-' + i.replace('_', '-'));

                    if ($(element).parent().hasClass('input-group')) {
                        $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                    } else {
                        $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                    }
                }

                // Highlight any found errors
                $('.text-danger').parent().addClass('has-error');
            } else {
                $.ajax({
                    url: 'index.php?route=checkout/shipping_method',
                    dataType: 'html',
                    complete: function() {
                        $('#button-shipping-address').button('reset');
                    },
                    success: function(html) {
                        $('#delivery-method').html(html);
                        $('#delivery-method').show();
                        $('#shipping-address').hide();
                        $("html, body").animate({ scrollTop: 0 }, "slow");

                        $.ajax({
                            url: 'index.php?route=checkout/shipping_address',
                            dataType: 'html',
                            success: function(html) {
                                $('#shipping-address').html(html);

                                $('#shipping-existing select[name=\'address_id\']').trigger('change');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

// Guest Shipping
$(document).delegate('#button-guest-shipping', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/guest_shipping/save',
        type: 'post',
        data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'date\'], #shipping-address input[type=\'datetime-local\'], #shipping-address input[type=\'time\'], #shipping-address input[type=\'password\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address textarea, #shipping-address select'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-guest-shipping').button('loading');
        },
        success: function(json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#button-guest-shipping').button('reset');

                if (json['error']['warning']) {
                    $('#shipping-address').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }

                for (i in json['error']) {
                  var element = $('#input-shipping-' + i.replace('_', '-'));

                  if ($(element).parent().hasClass('input-group')) {
                    $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                  } else {
                    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                  }
                }

                // Highlight any found errors
                $('.text-danger').parent().addClass('has-error');
            } else {
                $.ajax({
                    url: 'index.php?route=checkout/shipping_method',
                    dataType: 'html',
                    complete: function() {
                        $('#button-guest-shipping').button('reset');
                    },
                    success: function(html) {
                        $('#delivery-method').html(html);
                        $('#delivery-method').show();
                        $('#shipping-address').hide();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$(document).delegate('#button-shipping-method', 'click', function() {
    // alert('e');return false;
    $.ajax({
        url: 'index.php?route=checkout/shipping_method/save',
        type: 'post',
        data: $('#delivery-method select, #delivery-method textarea'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-shipping-method').button('loading');
        },
        success: function(json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#button-shipping-method').button('reset');

                if (json['error']['warning']) {
                    $('#delivery-method').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            } else {
                // $.ajax({
                //     url: 'index.php?route=checkout/payment_method',
                //     dataType: 'html',
                //     complete: function() {
                //         $('#button-shipping-method').button('reset');
                //     },
                //     success: function(html) {
                //         $('#payment-method').html(html);
                //         $('#payment-method').show();
                //         $('#delivery-method').hide();
                //         $("html, body").animate({ scrollTop: 0 }, "slow");
                //     },
                //     error: function(xhr, ajaxOptions, thrownError) {
                //         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                //     }
                // });

                $.ajax({
                    url: 'index.php?route=checkout/review',
                    dataType: 'html',
                    complete: function() {
                        $('#button-shipping-method').button('reset');
                    },
                    success: function(html) {
                        $('#order-review').html(html);
                        $('#order-review').show();
                        $('#delivery-method').hide();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$(document).delegate('#button-review', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/payment_method',
        dataType: 'html',
        beforeSend: function() {
            $('#button-review').button('loading');
        },
        complete: function() {
            $('#button-review').button('reset');
        },
        success: function(html) {
            $('#payment-method').html(html);
            $('#payment-method').show();
            $('#order-review').hide();
            $("html, body").animate({ scrollTop: 0 }, "slow");

            $.ajax({
                url: 'index.php?route=checkout/payment_method/save',
                type: 'post',
                data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-payment-method').button('loading');
                },
                complete: function() {
                    $('#button-payment-method').button('reset');
                },
                success: function(json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        if (json['error']['warning']) {
                            $('#payment-method').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                    // } else {
                    //     $('#payment-method-detail').html(json['payment']);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$(document).delegate('#button-payment-method', 'click', function() {    
    $.ajax({
        url: 'index.php?route=checkout/payment_method/confirm',
        type: 'post',
        data: $('#payment-method input[type=\'hidden\'], #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'text\'], #payment-method textarea'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-payment-method').button('loading');
        },
        success: function(json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {                
                $('#button-payment-method').button('reset');
                
                if (json['error']) {
                    $('#payment-method').prepend('<div class="alert alert-danger">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
      else if(json['data'] && json['redirect_payment_url']){
        $.redirect(json['redirect_payment_url'],json['data']); 

      }
      else if(json['success_url']){
        window.location.href = json['success_url'];
      }


             else {
                // location = json['continue'];
                $.redirect(json['doku_url'],json['doku_data']); 
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});
//--></script>
<?php echo $footer; ?>
