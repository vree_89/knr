<h4 class="checkout-subtitle">DELIVERY METHOD</h4>
<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php }  ?>
<p><?php echo $text_shipping_method; ?></p>
<?php if ($couriers) { ?>
<div class="form-group">
  <label class="control-label">Couriers</label>
  <select name="shipping_courier" class="form-control input-sm">
  <option value="">-- Select Courier --</option>
  <?php foreach ($couriers as $key=>$val) { ?>
  <?php if ($key == $shipping_courier) { ?>
  <option value="<?php echo $key?>" selected="selected"><?php echo $val?></option>
  <?php } else { ?>
  <option value="<?php echo $key?>"><?php echo $val?></option>
  <?php } ?>
  <?php } ?>
  </select>
</div>
<div class="form-group">
  <label class="control-label">Services</label>
  <select name="shipping_method" class="form-control input-sm">
  <option value="">-- Select Service --</option>
  </select>
</div>
<?php }?>

<p><strong><?php echo $text_comments; ?></strong></p>
<p>
  <textarea name="comment" rows="3" class="form-control"><?php echo $comment; ?></textarea>
</p>
<div class="buttons">
  <div class="pull-left">
    <input type="button" value="Back" id="button-shipping-method-back" class="btn btn-default btn-sm" />
  </div>
  <div class="pull-right">
    <input type="button" value="Next" id="button-shipping-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink btn-sm" />
  </div>
</div>
<script type="text/javascript"><!--
$('#delivery-method select[name=\'shipping_courier\']').on('change', function() {
  $.ajax({
        url: 'index.php?route=checkout/shipping_method/service',
        type: 'post',
        data: {'courier' : this.value},
        dataType: 'json',
        beforeSend: function() {
            $('#delivery-method select[name=\'shipping_method\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
            $('#delivery-method select[name=\'shipping_courier\']').prop('disabled', 'disabled');
        },
        complete: function() {
            $('#delivery-method .fa-spin').remove();
            $('#delivery-method select[name=\'shipping_courier\']').prop('disabled', false);
        },
        success: function(json) {
            if (json && json != '') {
              html = '<option value="" selected="selected">-- Select Service --</option>';
              for (i = 0; i < json.length; i++) {
                html += '<option value="' + json[i]['code'] + '"';

                if (json[i]['code'] == '<?php echo $code; ?>') {
                  html += ' selected="selected"';
                }

                html += '>' + json[i]['service'] + ' => '+ json[i]['cost'] +'</option>';
              }
            } else {
              html = '<option value="" selected="selected">-- No Services Available --</option>';
            }

            $('#delivery-method select[name=\'shipping_method\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('#delivery-method select[name=\'shipping_courier\']').trigger('change');

$('#button-shipping-method-back').on('click', function() {
  var shipping_address = $('#billing-address input[name=\'shipping_address\']:checked').prop('value');
  
  $('#delivery-method').hide();
  
  if (shipping_address) {
    $('#billing-address').show();
  } else {
    $('#shipping-address').show();
  }
  
  $("html, body").animate({ scrollTop: 0 }, "slow");
});
//--></script>