<h4 class="checkout-subtitle">SHIPPING ADDRESS</h4>
<form class="form-horizontal mbot10">
  <?php if ($addresses) { ?>
  <div class="radio">
    <label>
      <input type="radio" name="shipping_address" value="existing" checked="checked" />
      <?php echo $text_address_existing; ?></label>
  </div>
  <div id="shipping-existing">
    <select name="address_id" class="form-control input-sm">
      <?php foreach ($addresses as $address) { ?>
      <?php if ($address['address_id'] == $address_id) { ?>
      <option value="<?php echo $address['address_id']; ?>" data-detail="<?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?><br> <?php echo $address['address_1']; ?> <?php echo $address['postcode']; ?><br> <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
      <?php } else { ?>
      <option value="<?php echo $address['address_id']; ?>" data-detail="<?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?><br> <?php echo $address['address_1']; ?> <?php echo $address['postcode']; ?><br> <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
      <?php } ?>
      <?php } ?>
    </select>
    <div id="shipping-detail"></div>
  </div>
  <div class="radio">
    <label>
      <input type="radio" name="shipping_address" value="new" />
      <?php echo $text_address_new; ?></label>
  </div>
  <?php } ?>

  <div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
    <div class="col-sm-12">
      <div class="form-group required">
        <label class="control-label" for="input-shipping-firstname"><?php echo $entry_firstname; ?></label>
        <input type="text" name="firstname" value="" id="input-shipping-firstname" class="form-control input-sm" />
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-shipping-lastname"><?php echo $entry_lastname; ?></label>
        <input type="text" name="lastname" value="" id="input-shipping-lastname" class="form-control input-sm" />
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-shipping-address-1"><?php echo $entry_address_1; ?></label>
        <input type="text" name="address_1" value="" id="input-shipping-address-1" class="form-control input-sm" />
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-shipping-country"><?php echo $entry_country; ?></label>
        <select name="country_id" id="input-shipping-country" class="form-control input-sm">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($countries as $country) { ?>
          <?php if ($country['country_id'] == $country_id) { ?>
          <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-shipping-zone"><?php echo $entry_zone; ?></label>
        <select name="zone_id" id="input-shipping-zone" class="form-control input-sm">
        </select>
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-shipping-city"><?php echo $entry_city; ?></label>
        <select name="city" id="input-shipping-city" class="form-control input-sm">
          <option value="" selected="selected"><?php echo $text_select; ?></option>
        </select>
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-shipping-postcode"><?php echo $entry_postcode; ?></label>
        <input type="text" name="postcode" value="" id="input-shipping-postcode" class="form-control input-sm" />
      </div>
    </div>
  </div>
</form>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="Next" id="button-shipping-address" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink btn-sm" />
  </div>
</div>
<script type="text/javascript"><!--
$('input[name=\'shipping_address\']').on('change', function() {
	if (this.value == 'new') {
		$('#shipping-existing').hide();
		$('#shipping-new').show();
	} else {
		$('#shipping-existing').show();
		$('#shipping-new').hide();
	}
});

// $('#shipping-existing select[name=\'address_id\']').on('change', function() {
//   var $this = this;
//   $.ajax({
//         url: 'index.php?route=checkout/shipping_address/save',
//         type: 'post',
//         data: $('#shipping-address-form input[type=\'text\'], #shipping-address-form input[type=\'date\'], #shipping-address-form input[type=\'datetime-local\'], #shipping-address-form input[type=\'time\'], #shipping-address-form input[type=\'password\'], #shipping-address-form input[type=\'checkbox\']:checked, #shipping-address-form input[type=\'radio\']:checked, #shipping-address-form input[type=\'hidden\'], #shipping-address-form textarea, #shipping-address-form select'),
//         dataType: 'json',
//         beforeSend: function() {
//             $('#shipping-address-form .radio:first-child').append('<i class="fa fa-circle-o-notch fa-spin"></i>');
//             $('#shipping-existing select[name=\'address_id\']').prop('disabled', 'disabled');
//         },
//         complete: function() {
//             $('#shipping-address-form .fa-spin').remove();
//             $('#shipping-existing select[name=\'address_id\']').prop('disabled', false);
//         },
//         success: function(json) {
//             $('#shipping-address-form .alert, .text-danger').remove();

//             if (json['redirect']) {
//                 location = json['redirect'];
//             } else if (json['error']) {
//                 if (json['error']['warning']) {
//                     $('#shipping-address-form').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
//                 }
//             } else {
//               $('#shipping-detail').html($($this).find(':selected').data('detail'));

//               // trigger delivery method change
//               if ($('#delivery-method select[name=\'shipping_courier\']').val() != '') {
//                 $('#delivery-method select[name=\'shipping_courier\']').trigger('change');
//               }
//             }
//         },
//         error: function(xhr, ajaxOptions, thrownError) {
//             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
//         }
//     });
// });

// $('#shipping-existing select[name=\'address_id\']').trigger('change');

$('#shipping-existing select[name=\'address_id\']').on('change', function() {
    $('#shipping-detail').html($(this).find(':selected').data('detail'));
});

$('#shipping-existing select[name=\'address_id\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('#collapse-shipping-address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group').eq(parseInt($(this).attr('data-sort'))+2).before(this);
	}

	if ($(this).attr('data-sort') > $('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-shipping-address button[id^=\'button-shipping-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
// $('.date').datetimepicker({
// 	pickTime: false
// });

// $('.time').datetimepicker({
// 	pickDate: false
// });

// $('.datetime').datetimepicker({
// 	pickDate: true,
// 	pickTime: true
// });
//--></script>
<script type="text/javascript"><!--
$('#shipping-new select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('#shipping-new select[name=\'zone_id\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#shipping-new select[name=\'country_id\']').prop('disabled', 'disabled');
    },
    complete: function() {
      $('#shipping-new .fa-spin').remove();
      $('#shipping-new select[name=\'country_id\']').prop('disabled', false);
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('#shipping-new input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('#shipping-new input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '" data-name="' + json['zone'][i]['name'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#shipping-new select[name=\'zone_id\']').html(html);
      $('#shipping-new select[name=\'zone_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('#shipping-new select[name=\'country_id\']').trigger('change');

$('#shipping-new select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/city&province_name=' + $(this).find(':selected').data('name'),
    dataType: 'json',
    beforeSend: function() {
      $('#shipping-new select[name=\'city\']').before(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#shipping-new select[name=\'zone_id\']').prop('disabled', 'disabled');
    },
    complete: function() {
      $('#shipping-new .fa-spin').remove();
      $('#shipping-new select[name=\'zone_id\']').prop('disabled', false);
    },
    success: function(json) {
      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '"';

          // if (json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] == '<?php echo $city; ?>') {
          //   html += ' selected="selected"';
          // }

          html += '>' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#shipping-new select[name=\'city\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});
//--></script>
