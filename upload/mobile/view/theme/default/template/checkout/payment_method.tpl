<h4 class="checkout-subtitle">PAYMENT METHOD</h4>
<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>

<input type="hidden" name="payment_wallet" id="payment_wallet" value="<?php echo $input_payment_wallet_num?>">
<input type="hidden" name="payment_total" id="payment_total" value="<?php echo $input_payment_total_num?>">

<div class="payment-selector">
<!--PAYMENT SUMMARY-->
<div class="col-md-6">
  <table class="table table-bordered">
    <tr>
      <td>Total</td>
      <td><?php echo $order_total?></td>
    </tr>
    <tr>
      <td>Use Wallet</td>
      <td><?php echo $input_payment_wallet?></td>
    </tr>
    <tr>
      <td>Total Payment</td>
      <td><?php echo $input_payment_total?></td>
    </tr>
  </table>
</div>


<?php
/*
if ($payment_methods) {  
  $disabled = '';
  if($input_payment_total_num<=0){
    $disabled = 'disabled="disabled"';
  }
  foreach ($payment_methods as $payment_method) { 
    if ($payment_method['code'] == $code) { 
      $checked = 'checked="checked"'; 
      $code = $payment_method['code']; 
    } 
    else { 
      $checked = ''; 
    } ?>


     <div class="col-md-12">
      <label for="<?php echo $payment_method['code']; ?>">
      <input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" <?php echo $checked; ?> <?php echo $disabled?> />
      <?php echo $payment_method['title']?>
    </label>
     </div>



    <?php 
  }
} 

*/
?>

<?php if ($payment_methods) { ?>
  <?php foreach ($payment_methods as $payment_method) { ?>
    <?php 
    if ($payment_method['code'] == $code || empty($code)) { 
      $checked = 'checked="checked"'; 
      $code = $payment_method['code']; 
    } 
    else { 
      $checked = ''; 
    } 
    //echo $payment_method['image'];
    ?>
    <input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" <?php echo $checked; ?> />
    <label class="drinkcard-payment" for="<?php echo $payment_method['code']; ?>" style="background-image:url(<?php echo $payment_method['image']; ?>)" title="<?php echo $payment_method['title']; ?>"></label>
  <?php } ?>
<?php } ?>
</div>


<!-- <div id="payment-method-detail"></div> -->
<!-- <p><strong><?php //echo $text_comments; ?></strong></p>
<p>
  <textarea name="comment" rows="3" class="form-control"><?php //echo $comment; ?></textarea>
</p> -->
<?php if ($text_agree) { ?>
<div class="checkbox">
  <label>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    <?php echo $text_agree; ?>
  </label>
</div>
<?php } ?>
<div class="clearfix"></div>
<div class="msg-additional-charge"><?php echo $text_additional_fee?></div>
<div class="clearfix"></div>
<div class="buttons">
  <div class="pull-left">
    <input type="button" value="Back" id="button-payment-method-back" class="btn btn-default btn-sm" />
  </div>
  <div class="pull-right">
    <input type="button" value="Proceed Payment" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink btn-sm" />
  </div>
</div>


<script type="text/javascript"><!--
$('#button-payment-method-back').on('click', function() {
  $('#payment-method').hide();
  $('#order-review').show();
  $("html, body").animate({ scrollTop: 0 }, "slow");
});

$('#payment-method input[name=\'payment_method\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/payment_method/save',
    type: 'post',
    data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-payment-method').button('loading');
    },
    complete: function() {
      $('#button-payment-method').button('reset');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();
      
      if (json['redirect']) {
          location = json['redirect'];
      } else if (json['error']) {
          if (json['error']['warning']) {
              $('#payment-method').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              $("html, body").animate({ scrollTop: 0 }, "slow");
          }
      // } else {
      //   $('#payment-method-detail').html(json['payment']);
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('#deposit').click(function(){
    $('#deposit_amount').toggle();
});
//--></script>
<style type="text/css">
.msg-additional-charge{
  font-style: italic;
}
</style>