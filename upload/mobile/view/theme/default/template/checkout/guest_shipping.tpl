<h4 class="checkout-subtitle">SHIPPING ADDRESS</h4>
<form class="form-horizontal">
  <div class="col-sm-12">
    <div class="row"><h2 class="legend"><?php echo strtoupper($text_your_details); ?></h2></div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-telephone"><?php echo $entry_telephone; ?></label>
      <div class="input-group">
        <input type="text" name="telephone" value="<?php echo $telephone; ?>" id="input-shipping-telephone" class="form-control input-sm" />
        <span class="input-group-btn">
          <button class="btn btn-primary btn-sm" id="guest-check" data-loading-text="loading..." type="button">check</button>
        </span>
      </div>
      <p class="help-block"><small><em>If you have place order before, input your phone number and hit check button</em></small></p>
    </div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-firstname"><?php echo $entry_firstname; ?></label>
      <input type="text" name="firstname" value="<?php echo $firstname; ?>" id="input-shipping-firstname" class="form-control input-sm" />
    </div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-lastname"><?php echo $entry_lastname; ?></label>
      <input type="text" name="lastname" value="<?php echo $lastname; ?>" id="input-shipping-lastname" class="form-control input-sm" />
    </div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-email"><?php echo $entry_email; ?></label>
      <input type="text" name="email" value="<?php echo $email; ?>" id="input-shipping-email" class="form-control input-sm" />
    </div>
  </div>
  <div class="col-sm-12">
    <div class="row"><h2 class="legend"><?php echo strtoupper($text_your_address); ?></h2></div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-address-1"><?php echo $entry_address_1; ?></label>
      <input type="text" name="address_1" value="<?php echo $address_1; ?>" id="input-shipping-address-1" class="form-control input-sm" />
    </div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-country"><?php echo $entry_country; ?></label>
      <select name="country_id" id="input-shipping-country" class="form-control input-sm">
        <option value=""><?php echo $text_select; ?></option>
        <?php foreach ($countries as $country) { ?>
        <?php if ($country['country_id'] == $country_id) { ?>
        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-zone"><?php echo $entry_zone; ?></label>
      <select name="zone_id" id="input-shipping-zone" class="form-control input-sm">
      </select>
    </div>
    <div class="form-group required">
      <label class="control-label" for="input-shipping-city"><?php echo $entry_city; ?></label>
      <select name="city" id="input-shipping-city" class="form-control input-sm input-sm">
        <option value="" selected="selected"><?php echo $text_select; ?></option>
      </select>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-shipping-postcode"><?php echo $entry_postcode; ?></label>
      <input type="text" name="postcode" value="<?php echo $postcode; ?>" id="input-shipping-postcode" class="form-control input-sm" />
    </div>

    <?php echo $captcha; ?>
  </div>
  <div class="buttons">
    <div class="pull-right">
      <input type="button" value="Next" id="button-guest-shipping" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink btn-sm" />
    </div>
  </div>
</form>
<script type="text/javascript"><!--
// Sort the custom fields
$('#collapse-shipping-address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group').eq(parseInt($(this).attr('data-sort'))+2).before(this);
	}

	if ($(this).attr('data-sort') > $('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#collapse-shipping-address .form-group').length-2) {
		$('#collapse-shipping-address .form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-shipping-address button[id^=\'button-shipping-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').attr('value', json['file']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
// $('.date').datetimepicker({
// 	pickTime: false
// });

// $('.time').datetimepicker({
// 	pickDate: false
// });

// $('.datetime').datetimepicker({
// 	pickDate: true,
// 	pickTime: true
// });
//--></script>
<script type="text/javascript"><!--
$('#shipping-address select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      // $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#shipping-address select[name=\'zone_id\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#shipping-address select[name=\'country_id\']').prop('disabled', 'disabled');
    },
    complete: function() {
      $('#shipping-address .fa-spin').remove();
      $('#shipping-address select[name=\'country_id\']').prop('disabled', false);
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('#shipping-address input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('#shipping-address input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '" data-name="' + json['zone'][i]['name'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#shipping-address select[name=\'zone_id\']').html(html);
      $('#shipping-address select[name=\'zone_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('#shipping-address select[name=\'country_id\']').trigger('change');

$('#shipping-address select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/city&province_name=' + $(this).find(':selected').data('name'),
    dataType: 'json',
    beforeSend: function() {
      $('#shipping-address select[name=\'city\']').before(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#shipping-address select[name=\'zone_id\']').prop('disabled', 'disabled');
      $('#button-guest-shipping').button('loading');
    },
    complete: function() {
      $('#shipping-address .fa-spin').remove();
      $('#shipping-address select[name=\'zone_id\']').prop('disabled', false);
      $('#button-guest-shipping').button('reset');
    },
    success: function(json) {
      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '"';

          if (json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] == '<?php echo $city; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#shipping-address select[name=\'city\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});
$(document).on('click', '#guest-check', function() {
    $.ajax({
        url: 'index.php?route=checkout/guest/check',
        type: 'post',
        data: $('#input-shipping-telephone'),
        dataType: 'json',
        beforeSend: function() {
          $('#guest-check').button('loading');
        },
        complete: function() {
          $('#guest-check').button('reset');
        },
        success: function(jsonGuest) {
              $('#input-shipping-firstname').val(jsonGuest.firstname);
              $('#input-shipping-lastname').val(jsonGuest.lastname);
              $('#input-shipping-email').val(jsonGuest.email);
              $('#input-shipping-address-1').val(jsonGuest.address);
              $('#input-shipping-postcode').val(jsonGuest.postcode);
              $('#input-shipping-country').val(jsonGuest.country);

              $.ajax({
                url: 'index.php?route=account/account/country&country_id=' + $('#shipping-address select[name=\'country_id\']').val(),
                dataType: 'json',
                beforeSend: function() {
                  $('#shipping-address select[name=\'zone_id\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
                  $('#shipping-address select[name=\'country_id\']').prop('disabled', 'disabled');
                  $('#button-guest').button('loading');
                },
                complete: function() {
                  $('#shipping-address .fa-spin').remove();
                  $('#shipping-address select[name=\'country_id\']').prop('disabled', false);
                },
                success: function(json) {
                  html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

                  if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                      html += '<option value="' + json['zone'][i]['zone_id'] + '" data-name="' + json['zone'][i]['name'] + '"';

                      if (json['zone'][i]['zone_id'] == jsonGuest.zone) {
                        html += ' selected="selected"';
                      }

                      html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                  } else {
                    html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                  }

                  $('#shipping-address select[name=\'zone_id\']').html(html);

                  $.ajax({
                    url: 'index.php?route=account/account/city&province_name=' + $('#shipping-address select[name=\'zone_id\']').find(':selected').data('name'),
                    dataType: 'json',
                    beforeSend: function() {
                      $('#shipping-address select[name=\'city\']').before(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                      $('#shipping-address select[name=\'zone_id\']').prop('disabled', 'disabled');
                    },
                    complete: function() {
                      $('#shipping-address .fa-spin').remove();
                      $('#shipping-address select[name=\'zone_id\']').prop('disabled', false);
                      $('#button-guest').button('reset');
                    },
                    success: function(json) {
                      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

                      if (json['city'] && json['city'] != '') {
                        for (i = 0; i < json['city'].length; i++) {
                          html += '<option value="' + json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '"';

                          if (json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] == jsonGuest.city) {
                            html += ' selected="selected"';
                          }

                          html += '>' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '</option>';
                        }
                      } else {
                        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                      }

                      $('#shipping-address select[name=\'city\']').html(html);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                  });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
          },
          error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
    });
});
//--></script>