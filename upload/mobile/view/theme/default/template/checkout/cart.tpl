<?php echo $header; ?>
  <!-- Cart -->
  <!-- Images size 75 x 50 -->
  <section class="container-fluid" id="cart">
    <!-- cart header -->
    <div class="cart-header">
      <div class="clearfix">
        <h5 class="pull-left"><span class="glyphicon glyphicon-shopping-cart"></span>Shopping Cart 
          <?php if ($weight) { ?>
            (<?php echo $weight; ?>)
          <?php } ?>
        </h5>
        <a href="<?php echo $continue; ?>" class="btn btn-primary btn-sm pull-right">
          <span class="glyphicon glyphicon-share-alt"></span> Continue shopping
        </a>
      </div>
    </div>
    <!-- /cart header -->

    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <!-- cart body -->
    <div class="cart-body">
      <!-- cart item -->
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <?php foreach ($products as $product) { ?>
      <div class="cart-item">
        <div class="images">
          <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
          </a>
        </div>
        <div class="title">
          <h5><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if (!$product['stock']) { ?>
            <span class="text-danger">***</span>
            <?php } ?>
          </h5>
          <p>by <a href="<?php echo $product['manufacturer_href']; ?>"><?php echo $product['manufacturer']; ?></a></p>
        </div>
        <div class="action">
          <button type="button" class="btn btn-danger btn-sm" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-trash-o"></i></button>
        </div>
        <div class="desc">
          <div class="well">
            <table class="table borderless">
              <tr>
                <th>Status</th>
                <td>:</td>
                <td><?php echo $product['stock_info']; ?></td>
              </tr>
              <?php if ($product['option']) { ?>
              <?php foreach ($product['option'] as $option) { ?>
              <tr>
                <th><?php echo $option['name']; ?></th>
                <td>:</td>
                <td><?php echo $option['value']; ?></td>
              </tr>
              <?php } ?>
              <?php } ?>
              <?php if ($product['reward']) { ?>
              <tr>
                <th>Rewards</th>
                <td>:</td>
                <td><?php echo $product['reward']; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <th>Price</th>
                <td>:</td>
                <td><?php echo $product['price']; ?></td>
              </tr>
              <tr>
                <th>Qty</th>
                <td>:</td>
                <td>
                  <div class="input-group btn-block" style="max-width: 50px;">
                    <input type="number" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control" min="1" style="padding:3px;margin:0;height:34px;width:50px;" />
                    <span class="input-group-btn">
                      <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-primary"><i class="fa fa-refresh"></i></button>
                    </span>
                  </div>
                </td>
              </tr>
            </table>
          </div>
          <h5 class="text-right"><strong><?php echo $product['total']; ?></strong></h5>
        </div>
        <div class="clearfix"></div>
      </div>
      <?php } ?>
      </form>
      <!-- /cart item -->
    </div>
    <!-- /cart body -->

    <!-- cart info -->
    <div class="cart-info">
      <?php foreach ($totals as $total) { ?>
      <div class="clearfix">
        <h5 class="pull-left"><strong><?php echo $total['title']; ?></strong></h5>
        <h5 class="pull-right"><strong><?php echo $total['text']; ?></strong></h5>
      </div>
      <?php } ?>

      <a href="<?php echo $checkout; ?>" class="btn btn-block btn-pink checkout">CHECKOUT</a>
    </div>
    <!-- /cart info -->
  </section>
  <!-- /Cart -->
<?php echo $footer; ?>
