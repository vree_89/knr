<h4 class="checkout-subtitle">BILLING ADDRESS</h4>
<div class="row">
  <div class="col-sm-6">
    <fieldset id="account">
    <h2 class="legend"><?php echo strtoupper($text_your_details); ?></h2>
      <div class="form-group" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
        <label class="control-label"><?php echo $entry_customer_group; ?></label>
        <?php foreach ($customer_groups as $customer_group) { ?>
        <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
        <div class="radio">
          <label>
            <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
            <?php echo $customer_group['name']; ?></label>
        </div>
        <?php } else { ?>
        <div class="radio">
          <label>
            <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" />
            <?php echo $customer_group['name']; ?></label>
        </div>
        <?php } ?>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-payment-telephone"><?php echo $entry_telephone; ?></label>
        <div class="input-group">
          <input type="text" name="telephone" value="<?php echo $telephone; ?>" id="input-payment-telephone" class="form-control input-sm" />
          <span class="input-group-btn">
            <button class="btn btn-primary btn-sm" id="guest-check" data-loading-text="loading..." type="button">check</button>
          </span>
        </div>
        <p class="help-block"><small><em>If you have place order before, input your phone number and hit check button</em></small></p>
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-payment-firstname"><?php echo $entry_firstname; ?></label>
        <input type="text" name="firstname" value="<?php echo $firstname; ?>" id="input-payment-firstname" class="form-control input-sm" />
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-payment-lastname"><?php echo $entry_lastname; ?></label>
        <input type="text" name="lastname" value="<?php echo $lastname; ?>" id="input-payment-lastname" class="form-control input-sm" />
      </div>
      <div class="form-group">
        <label class="control-label" for="input-payment-email"><?php echo $entry_email; ?></label>
        <input type="text" name="email" value="<?php echo $email; ?>" id="input-payment-email" class="form-control input-sm" />
      </div>
    </fieldset>
  </div>
  <div class="col-sm-6">
    <fieldset id="address">
      <h2 class="legend"><?php echo strtoupper($text_your_address); ?></h2>
      <div class="form-group required">
        <label class="control-label" for="input-payment-address-1"><?php echo $entry_address_1; ?></label>
        <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-payment-address-1" class="form-control input-sm" />
      </div>
      <!-- <div class="form-group required">
        <label class="control-label" for="input-payment-city"><?php echo $entry_city; ?></label>
        <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-payment-city" class="form-control input-sm" />
      </div> -->
      <div class="form-group required">
        <label class="control-label" for="input-payment-country"><?php echo $entry_country; ?></label>
        <select name="country_id" id="input-payment-country" class="form-control input-sm">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($countries as $country) { ?>
          <?php if ($country['country_id'] == $country_id) { ?>
          <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-payment-zone"><?php echo $entry_zone; ?></label>
        <select name="zone_id" id="input-payment-zone" class="form-control input-sm">
        </select>
      </div>
      <div class="form-group required">
        <label class="control-label" for="input-payment-city"><?php echo $entry_city; ?></label>
        <select name="city" id="input-payment-city" class="form-control input-sm">
          <option value="" selected="selected"><?php echo $text_select; ?></option>
        </select>
      </div>
      <div class="form-group">
        <label class="control-label" for="input-payment-postcode"><?php echo $entry_postcode; ?></label>
        <input type="text" name="postcode" value="<?php echo $postcode; ?>" id="input-payment-postcode" class="form-control input-sm" />
      </div>
    </fieldset>
    
    <?php echo $captcha; ?>
  </div>
</div>
<?php if ($shipping_required) { ?>
<div class="checkbox">
  <label>
    <?php if ($shipping_address) { ?>
    <input type="checkbox" name="shipping_address" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="shipping_address" value="1" />
    <?php } ?>
    <?php echo $entry_shipping; ?></label>
</div>
<?php } ?>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="Next" id="button-guest" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink btn-sm" />
  </div>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('#collapse-payment-address input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('#collapse-payment-address .custom-field').hide();
			$('#collapse-payment-address .custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#payment-custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#payment-custom-field' + custom_field['custom_field_id']).addClass('required');
				} else {
					$('#payment-custom-field' + custom_field['custom_field_id']).removeClass('required');
				}
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-payment-address input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
// $('.date').datetimepicker({
// 	pickTime: false
// });

// $('.time').datetimepicker({
// 	pickDate: false
// });

// $('.datetime').datetimepicker({
// 	pickDate: true,
// 	pickTime: true
// });
//--></script>
<script type="text/javascript"><!--
$('#billing-address select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      // $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#billing-address select[name=\'zone_id\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#billing-address select[name=\'country_id\']').prop('disabled', 'disabled');
    },
    complete: function() {
      $('#billing-address .fa-spin').remove();
      $('#billing-address select[name=\'country_id\']').prop('disabled', false);
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('#billing-address input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('#billing-address input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '" data-name="' + json['zone'][i]['name'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#billing-address select[name=\'zone_id\']').html(html);
      $('#billing-address select[name=\'zone_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('#billing-address select[name=\'country_id\']').trigger('change');

$('#billing-address select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/city&province_name=' + $(this).find(':selected').data('name'),
    dataType: 'json',
    beforeSend: function() {
      $('#billing-address select[name=\'city\']').before(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('#billing-address select[name=\'zone_id\']').prop('disabled', 'disabled');
      $('#button-guest').button('loading');
    },
    complete: function() {
      $('#billing-address .fa-spin').remove();
      $('#billing-address select[name=\'zone_id\']').prop('disabled', false);
      $('#button-guest').button('reset');
    },
    success: function(json) {
      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '"';

          if (json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] == '<?php echo $city; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#billing-address select[name=\'city\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).on('click', '#guest-check', function() {
    $.ajax({
        url: 'index.php?route=checkout/guest/check',
        type: 'post',
        data: $('#input-payment-telephone'),
        dataType: 'json',
        beforeSend: function() {
          $('#guest-check').button('loading');
        },
        complete: function() {
          $('#guest-check').button('reset');
        },
        success: function(jsonGuest) {
              $('#input-payment-firstname').val(jsonGuest.firstname);
              $('#input-payment-lastname').val(jsonGuest.lastname);
              $('#input-payment-email').val(jsonGuest.email);
              $('#input-payment-address-1').val(jsonGuest.address);
              $('#input-payment-postcode').val(jsonGuest.postcode);
              $('#input-payment-country').val(jsonGuest.country);

              $.ajax({
                url: 'index.php?route=account/account/country&country_id=' + $('#billing-address select[name=\'country_id\']').val(),
                dataType: 'json',
                beforeSend: function() {
                  $('#billing-address select[name=\'zone_id\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
                  $('#billing-address select[name=\'country_id\']').prop('disabled', 'disabled');
                  $('#button-guest').button('loading');
                },
                complete: function() {
                  $('#billing-address .fa-spin').remove();
                  $('#billing-address select[name=\'country_id\']').prop('disabled', false);
                },
                success: function(json) {
                  html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

                  if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                      html += '<option value="' + json['zone'][i]['zone_id'] + '" data-name="' + json['zone'][i]['name'] + '"';

                      if (json['zone'][i]['zone_id'] == jsonGuest.zone) {
                        html += ' selected="selected"';
                      }

                      html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                  } else {
                    html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                  }

                  $('#billing-address select[name=\'zone_id\']').html(html);

                  $.ajax({
                    url: 'index.php?route=account/account/city&province_name=' + $('#billing-address select[name=\'zone_id\']').find(':selected').data('name'),
                    dataType: 'json',
                    beforeSend: function() {
                      $('#billing-address select[name=\'city\']').before(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                      $('#billing-address select[name=\'zone_id\']').prop('disabled', 'disabled');
                    },
                    complete: function() {
                      $('#billing-address .fa-spin').remove();
                      $('#billing-address select[name=\'zone_id\']').prop('disabled', false);
                      $('#button-guest').button('reset');
                    },
                    success: function(json) {
                      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

                      if (json['city'] && json['city'] != '') {
                        for (i = 0; i < json['city'].length; i++) {
                          html += '<option value="' + json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '"';

                          if (json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] == jsonGuest.city) {
                            html += ' selected="selected"';
                          }

                          html += '>' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '</option>';
                        }
                      } else {
                        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                      }

                      $('#billing-address select[name=\'city\']').html(html);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                  });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
          },
          error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
    });
});
//--></script>
