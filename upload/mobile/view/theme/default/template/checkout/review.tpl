<?php if (!isset($redirect)) { ?>
<h4 class="checkout-subtitle">VOUCHER</h4>
<?php echo $coupon; ?>

<h4 class="checkout-subtitle">ORDER REVIEW</h4>
<?php foreach ($totals as $total) { ?>
<?php
if($total['type']=="text"){
?>
<div class="clearfix">
  <p class="pull-left"><?php echo $total['title']; ?></p>
  <p class="pull-right"><?php echo $total['text']; ?></p>
</div>
<?php
}
else if($total['type']=="input"){
 ?>
<p><?php echo $total['title']; ?></p>
<div class="input-group">
  <input type="text" name="input_payment_<?php echo $total['code']?>"  placeholder="" id="input_payment_<?php echo $total['code']?>" class="form-control input-sm" value="<?php echo $input_payment_wallet && $total['code']=="wallet"? $input_payment_wallet : $total['value']?>"/>
  <span class="input-group-btn">
    <input type="button" value="Apply" id="button-<?php echo $total['code']?>" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary btn-sm" />
  </span>
</div>
 <?php 
}
else if($total['type']=="input-disabled"){
?>
<div class="clearfix">
  <p class="pull-left"><?php echo $total['title']; ?></p>
  <p class="pull-right"><?php echo $total['text']; ?></p>
</div>
<?php

  }

?>
<?php } ?>

<p class="pull-right">Forgot an item? <a class="pink" href="<?php echo $shopping_cart; ?>">Edit your CART</a></p>
<div class="clearfix"></div>

<div class="buttons">
  <div class="pull-left">
    <input type="button" value="Back" id="button-review-back" class="btn btn-default btn-sm" />
  </div>
  <div class="pull-right">
    <input type="button" value="Next" id="button-review" data-loading-text="Loading..." class="btn btn-pink btn-sm" />
  </div>
</div>
<?php } else { ?>
<script type="text/javascript"><!--
location = '<?php echo $redirect; ?>';
//--></script>
<?php } ?>

<script type="text/javascript"><!--
$(document).ready(function(){
   order_total = <?php echo isset($input_payment_wallet) ? $input_payment_wallet : "0" ?> + <?php echo isset($input_payment_total) ? $input_payment_total : "0" ?>;
  });
$('#button-review-back').on('click', function() {
  $('#order-review').hide();
  $('#delivery-method').show();
  $("html, body").animate({ scrollTop: 0 }, "slow");
});


$('#button-wallet').on('click', function() {
    //alert($("#input_payment_wallet").val());return false;
    var sisa = eval(order_total - $("#input_payment_wallet").val());
    //alert(sisa);
    if($("#input_payment_wallet").val()>deposit){
      alert('Wallet not enough');
      $("#input_payment_wallet").val("<?php echo $input_payment_wallet?>");
      return false;
    }

    else if(sisa<50000 && sisa>0){
      alert('Sisa minimal IDR 50000');
      $("#input_payment_wallet").val("");
      return false;
    }    
    else{
      if($("#input_payment_wallet").val()==""){
        $("#input_payment_wallet").val("0");
        
      }
      else if(sisa<=0 && $("#input_payment_wallet").val()>=order_total){
        $("#input_payment_wallet").val(order_total);
        
      } 
    
    } 
    input_payment_wallet = $("#input_payment_wallet").val();
    
   //alert(sisa);


  //alert($('input[name=\'input_payment_wallet\']').val());return false;
  //alert('a');return false;
  $.ajax({
    url: 'index.php?route=checkout/review/setwallet',
    type: 'post',
    data: 'input_payment_wallet=' + encodeURIComponent($('input[name=\'input_payment_wallet\']').val()),
    dataType: 'json',
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#order-review').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        $('html, body').animate({ scrollTop: 0 }, 'slow');
      }

      if (json['redirect']) {
        // location = json['redirect'];
        $.ajax({
        url: 'index.php?route=checkout/review',
        dataType: 'html',
        success: function(html) {      
          //input_payment_wallet = $("#input_payment_wallet").val();
          //alert("input wallet now "+input_payment_wallet);  

        //alert(html);
        $('#order-review').html(html);
        //alert($("#input_payment_wallet").val());
        },
        error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
        });
      }
    }
  });
});

//--></script>