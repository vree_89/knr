<h4 class="checkout-subtitle">ORDER REVIEW</h4>
<?php if (!isset($redirect)) { ?>
<?php foreach ($totals as $total) { ?>
<div class="clearfix">
  <p class="pull-left"><?php echo $total['title']; ?></p>
  <p class="pull-right"><?php echo $total['text']; ?></p>
</div>
<?php } ?>
<?php echo $payment; ?>
<?php } else { ?>
<script type="text/javascript"><!--
location = '<?php echo $redirect; ?>';
//--></script>
<?php } ?>
