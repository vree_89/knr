<?php echo $header; ?>
  <!-- Blog Detail -->
  <section class="container-fluid default-height">
    <h1 class="blog-title"><?php echo $heading_title; ?></h1>
    <img src="<?php echo $image; ?>" class="blog-image img-responsive" alt="<?php echo $heading_title; ?>">
    <div class="blog-meta">
      <ul class="list-inline text-muted">
        <li><i class="fa fa-calendar"></i> <?php echo $date; ?></li>
        <li><i class="fa fa-eye"></i> <?php echo $viewed; ?></li>
        <li><i class="fa fa-tag"></i> <?php echo $article_type; ?></li>
      </ul>
    </div>

    <!-- <div class="share">
      <a href="#" class="btn btn-sm btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
      <a href="#" class="btn btn-sm btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i></a>
      <a href="#" class="btn btn-sm btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a>
      <a href="#" class="btn btn-sm btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
    </div> -->

    <div class="share">
      <div class="sharethis-inline-share-buttons"></div>
    </div>

    <div><?php echo $description; ?></div>
  </section>
  <!-- /Blog Detail -->
<?php echo $footer; ?>
