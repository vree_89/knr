<?php echo $header; ?>
<?php echo $slideshow; ?>
  <section class="container-fluid default-height">
    <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <div class="input-group" id="search-blog">
          <input name="search" class="form-control" placeholder="Search for <?php echo $article_type; ?>" type="text" data-type="<?php echo $article_type; ?>">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </div>
    </form>
    <ul class="list-inline blog-menu">
        <li <?php if($article_type == 'review') echo 'class="active"'; ?>><a href="<?php echo $review_url; ?>"><h4><span>KR REVIEW</span></h4></a></li>
        <li <?php if($article_type == 'video') echo 'class="active"'; ?>><a href="<?php echo $video_url; ?>"><h4><span>KR VIDEO</span></h4></a></li>
    </ul>

    <div class="blog mbot10">
      <?php if(!empty($articles)) { ?>
      <?php foreach ($articles as $article) { ?>

        <?php if($article_type == 'review') { ?>
        <!-- Blog -->
        <!-- Images size 300 x 200 -->
        <div class="item">
          <a href="<?php echo $article['href']; ?>">
            <div class="thumbnail">
              <span class="date"><?php echo date('d F Y', strtotime($article['date_modified'])); ?></span>
              <img src="<?php echo $article['image']; ?>" alt="<?php echo $article['name']; ?>">
              <div class="caption">
                <p><?php echo $article['name']; ?></p>
              </div>
            </div>
          </a>
        </div>
        <?php } elseif($article_type == 'video') { ?>
        <!-- Video -->
        <!-- Images size 300 x 200 -->
          <div class="item">
            <a href="<?php echo $article['href']; ?>">
              <div class="thumbnail">
                <span class="date"><?php echo date('d F Y', strtotime($article['date_modified'])); ?></span>
                <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" src="<?php echo strip_tags($article['intro_text']); ?>"></iframe>
                <div class="caption">
                  <p><?php echo $article['name']; ?></p>
                </div>
              </div>
            </a>
          </div>
        <?php } ?>

      <?php } ?>
      <?php }else{ ?>
        <div class="col-sm-12"><?php echo $text_empty; ?></div>
      <?php } ?>
      <div class="clearfix"></div>
    </div>

    <div class="text-center">
      <?php echo $pagination; ?>
    </div>
  </section>
  <!-- /Blog -->
<?php echo $footer; ?>