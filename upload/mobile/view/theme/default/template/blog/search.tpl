<?php echo $header; ?>
<?php echo $slideshow; ?>
  <section class="container-fluid default-height">
    <h3>Search result for "<?php echo $search; ?>" </h3>
    <div class="blog mbot10">
      <?php if(!empty($articles)) { ?>
      <?php foreach ($articles as $article) { ?>

        <?php if($article_type == 'review') { ?>
        <!-- Blog -->
        <!-- Images size 300 x 200 -->
        <div class="item">
          <a href="<?php echo $article['href']; ?>">
            <div class="thumbnail">
              <span class="date"><?php echo date('d F Y', strtotime($article['date_modified'])); ?></span>
              <img src="<?php echo $article['image']; ?>" alt="<?php echo $article['name']; ?>">
              <div class="caption">
                <p><?php echo $article['name']; ?></p>
              </div>
            </div>
          </a>
        </div>
        <?php } elseif($article_type == 'video') { ?>
        <!-- Video -->
        <!-- Images size 300 x 200 -->
          <div class="item">
            <a href="<?php echo $article['href']; ?>">
              <div class="thumbnail">
                <span class="date"><?php echo date('d F Y', strtotime($article['date_modified'])); ?></span>
                <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" src="<?php echo strip_tags($article['intro_text']); ?>"></iframe>
                <div class="caption">
                  <p><?php echo $article['name']; ?></p>
                </div>
              </div>
            </a>
          </div>
        <?php } ?>

      <?php } ?>
      <?php }else{ ?>
        <div class="col-sm-12">
          <p><?php echo $text_empty; ?></p>
          <a href="<?php echo $button_back; ?>" class="btn btn-pink btn-sm">Back</a>
        </div>
      <?php } ?>
      <div class="clearfix"></div>
    </div>

    <div class="text-center">
      <?php echo $pagination; ?>
    </div>
  </section>
  <!-- /Blog -->
<?php echo $footer; ?>