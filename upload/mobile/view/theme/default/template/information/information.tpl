<?php echo $header; ?>
<div class="container">
  <div>
    <div id="content" class="<?php echo $class; ?>">
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $description; ?>
    </div>
  </div>
</div>
<?php echo $footer; ?>