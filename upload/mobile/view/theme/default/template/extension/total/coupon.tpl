<div class="input-group">
	<input type="text" name="coupon" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon" class="form-control input-sm" />
	<span class="input-group-btn">
		<input type="button" value="Apply" id="button-coupon" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary btn-sm" />
	</span>
</div>
<script type="text/javascript"><!--
$('#button-coupon').on('click', function() {
	$.ajax({
		url: 'index.php?route=extension/total/coupon/coupon',
		type: 'post',
		data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
		dataType: 'json',
		beforeSend: function() {
			$('#button-coupon').button('loading');
		},
		complete: function() {
			$('#button-coupon').button('reset');
		},
		success: function(json) {
			console.log(json);
			$('.alert').remove();

			if (json['error']) {
				$('#order-review').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}

			if (json['redirect']) {
				// location = json['redirect'];
				$.ajax({
                    url: 'index.php?route=checkout/review',
                    dataType: 'html',
                    success: function(html) {
                        $('#order-review').html(html);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
			}
		}
	});
});
//--></script>
