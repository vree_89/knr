<!-- Main Banner -->
<section class="container-fluid" id="main-banner">
  <div id="carousel-main-banner" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    	<?php
				$a=0;
        $sliders='';
				foreach ($slideshows as $slideshow) {
					$active = ($a == 0) ? 'active' : '';
			?>    
      <li data-target="#carousel-main-banner" data-slide-to="<?php echo $a; ?>" class="<?php echo $active; ?>"></li>
      <?php
          $sliders .= '
            <div class="item '.$active.'">
              <img src="'.$slideshow['image'].'" alt="KR-House '.$slideshow['title'].'" title="KR-House '.$slideshow['title'].'">
              <div class="carousel-caption">
                <a href="'.$slideshow['link'].'">'.$slideshow['title'].'</a>
              </div>
            </div>
          ';
					$a++;
	    	} 
      ?>
    </ol>

    <!-- Wrapper for slides -->
    <!-- Images size 800 x 400 -->
    <div class="carousel-inner">
			<?php echo $sliders; ?>
    </div>
  </div>
</section>
<!-- /Main Banner -->