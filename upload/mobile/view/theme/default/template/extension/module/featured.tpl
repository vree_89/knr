    <div class="product featured">
      <h4><span>FEATURED</span></h4>
      <?php if(count($products)) { ?>
      <?php foreach ($products as $product) { ?>
      <div class="item">
        <a href="<?php echo $product['href']; ?>">
          <div class="thumbnail">
            <?php if($product['special']){ ?>
            <div class="ribbon"><span><?php echo $product['discount']; ?>% OFF</span></div>
            <?php } ?>
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
            <div class="info">
              <p><?php echo $product['manufacturer']; ?></p>
              <h5><?php echo $product['name']; ?></h5>
              <div class="ratings">
                <div class="star-ratings">
                  <span style="width:<?php echo $product['rating']; ?>%" class="star-ratings-sprite"></span>
                </div>
                <small>(<?php echo $product['reviews']; ?> review)</small>
              </div>
              <div class="price">
                <span class="rate"><?php echo $product['rate'];?>%</span>
                <span class="retail"><?php echo $product['price_retail'];?></span>
                <span class="our"><?php echo $product['price'];?></span> 
              </div>
            </div>
          </div>
        </a>
      </div>
      <?php } ?>
      <?php } else { ?>
        <div class="not-found">
          <h3>No products found</h3>
        </div>
      <?php } ?>
      <div class="clearfix"></div>
    </div>