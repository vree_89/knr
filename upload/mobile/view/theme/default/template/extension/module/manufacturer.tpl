<!-- Brand -->
<!-- Images size 300 x 200 -->
<section class="container-fluid">
  <div class="brand mbot20">
		<?php foreach ($manufacturers as $manufacturer) { ?>
		<div class="item">
		  <a href="<?php echo $manufacturer['href']; ?>">
		    <div class="thumbnail">
		      <img src="<?php echo $manufacturer['image']; ?>" alt="<?php echo $manufacturer['name']; ?>" title="<?php echo $manufacturer['name']; ?>" width="<?php echo $manufacturer_image_width; ?>" height="<?php echo $manufacturer_image_height; ?>">
		      <div class="caption">
		        <p><?php echo $manufacturer['name']; ?></p>
		      </div>
		    </div>
		  </a>
		</div>
		<?php } ?>
    <div class="clearfix"></div>
  </div>

  <!--div class="text-center">
    <nav aria-label="Page navigation">
      <ul class="pagination pagination-sm">
        <li class="disabled">
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  </div-->
</section>
<!-- /Brand -->