<?php echo $header; ?>
<?php echo $slideshow; ?>

  <!-- Product -->
  <!-- Images size 300 x 200 -->
  <section class="container-fluid">
    <div class="search-area">
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
      <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />

      <select name="category_id" class="form-control mtop10">
        <option value="0"><?php echo $text_category; ?></option>
        <?php foreach ($categories as $category_1) { ?>
        <?php /* if ($category_1['category_id'] == $category_id) { ?>
        <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
        <?php } */ ?>
        <?php foreach ($category_1['children'] as $category_2) { ?>
        <?php if ($category_2['category_id'] == $category_id) { ?>
        <option value="<?php echo $category_2['category_id']; ?>" selected="selected"><?php echo $category_2['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_2['category_id']; ?>"><?php echo $category_2['name']; ?></option>
        <?php } ?>
        <?php /* foreach ($category_2['children'] as $category_3) { ?>
        <?php if ($category_3['category_id'] == $category_id) { ?>
        <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
        <?php } ?>
        <?php } */ ?>
        <?php } ?>
        <?php } ?>
      </select>

      <label class="mtop10">
      <?php if ($sub_category) { ?>
      <input type="checkbox" name="sub_category" value="1" checked="checked"  />
      <?php } else { ?>
      <input type="checkbox" name="sub_category" value="1" />
      <?php } ?>
      <?php echo $text_sub_category; ?></label>

      <label>
      <?php if ($description) { ?>
      <input type="checkbox" name="description" value="1" id="description" checked="checked" />
      <?php } else { ?>
      <input type="checkbox" name="description" value="1" id="description" />
      <?php } ?>
      <?php echo $entry_description; ?></label>
      <br>
      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-sm btn-primary pull-right" />
      <div class="clearfix"></div>
    </div>

    <h3><?php echo $heading_title; ?> </h3>

    <!-- Sort -->
    <div class="sort">
      <div class="btn-group">
        <button type="button" class="btn btn-pink btn-xs">Sort Product</button>
        <button type="button" class="btn btn-pink dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <!-- <span class="caret"></span> -->
          <i class="fa fa-sort"></i>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu">
          <?php foreach ($sorts as $sorts) { ?>
          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
          <li class="active"><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
          <?php } else { ?>
          <li><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </div>
    <!-- /Sort -->

    <div class="product mbot10">
      <?php if(count($products)) { ?>
      <?php foreach ($products as $product) { ?>
      <div class="item">
        <a href="<?php echo $product['href']; ?>">
          <div class="thumbnail">
            <?php if($product['special']){ ?>
            <div class="ribbon"><span><?php echo $product['discount']; ?>% OFF</span></div>
            <?php } ?>
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
            <div class="info">
              <p><?php echo $product['manufacturer']; ?></p>
              <h5><?php echo $product['name']; ?></h5>
              <div class="ratings">
                <div class="star-ratings">
                  <span style="width:<?php echo $product['rating']; ?>%" class="star-ratings-sprite"></span>
                </div>
                <small>(<?php echo $product['reviews']; ?> review)</small>
              </div>
              <div class="price">
                <span class="rate"><?php echo $product['rate'];?>%</span>
                <span class="retail"><?php echo $product['price_retail'];?></span>
                <span class="our"><?php echo $product['price'];?></span> 
              </div>
            </div>
          </div>
        </a>
      </div>
      <?php } ?>
      <?php } else { ?>
        <div class="not-found">
          <h3>There is no product that matches the search criteria</h3>
        </div>
      <?php } ?>
      <div class="clearfix"></div>
    </div>

    <div class="text-center">
      <?php echo $pagination; ?>
    </div>
  </section>
  <!-- /Product -->

<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('.search-area input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('.search-area select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('.search-area input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('.search-area input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('.search-area input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>