<?php echo $header; ?>
  <!-- Product Gallery -->
  <section class="container-fluid" id="product-gallery">
    <div id="carousel-product-gallery" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <?php if ($thumb) { ?>
        <li data-target="#carousel-product-gallery" data-slide-to="0" class="active"></li>
        <?php 
            $sliders = '
              <div class="item active">
                <img src="'.$thumb.'" alt="KR-House '.$heading_title.'" title="KR-House '.$heading_title.'">
              </div>
            ';
            $a = 1;
          } else {
            $sliders = '';
            $a = 0;
          }
        ?>
        <?php if ($images) { ?>
        <?php
          foreach ($images as $image) {
            $active = ($a == 0) ? 'active' : '';
        ?>    
        <li data-target="#carousel-product-gallery" data-slide-to="<?php echo $a++; ?>" class="<?php echo $active; ?>"></li>
        <?php
            $sliders .= '
              <div class="item '.$active.'">
                <img src="'.$image['thumb'].'" alt="KR-House '.$heading_title.'" title="KR-House '.$heading_title.'">
              </div>
            ';
          } 
        ?>
        <?php } ?>
      </ol>

      <!-- Ribbon -->
      <?php if($special_disc){ ?>
      <div class="ribbon"><span><?php echo $special_disc; ?>% OFF</span></div>
      <?php } ?>

      <!-- Wrapper for slides -->
      <!-- Images size 800 x 400 -->
      <div class="carousel-inner">
        <?php echo $sliders; ?>
      </div>
    </div>
  </section>
  <!-- /Product Gallery -->

  <!-- Product Info -->
  <section class="container-fluid" id="product-detail">
    <div class="info">
      <p><?php echo $manufacturer; ?></p>
      <h5><?php echo $heading_title; ?></h5>
      <div class="ratings">
        <div class="star-ratings">
          <span style="width:<?php echo $rating; ?>%" class="star-ratings-sprite"></span>
        </div>
        <small>(<?php echo $reviews; ?>)</small>
      </div>
      <div class="price">
        <span class="rate"><?php echo $rate; ?>%</span>
        <span class="retail"><?php echo $price_retail; ?></span>
        <span class="our"><?php echo $price; ?></span> 
      </div>
      <p>Reward : <strong><?php echo $reward; ?> Point</strong></p>
      <p>Availability : 
              <?php
              if($is_preorder){
                ?>
                Pre-Order
                <br/>
                Date Available : <?php echo $date_available?>

                <?php
              }
              else{
                ?>
                <strong><?php echo $stock; ?></strong>
                <?php  
              }
              ?>
      </p>
    </div>

    <div class="share">
      <div class="sharethis-inline-share-buttons"></div>
    </div>

    <div id="product">
      <form class="form">
        <?php if ($options) { ?>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'select') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control input-sm">
            <option value="">Select <?php echo $option['name']; ?></option>
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <!-- <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label> -->
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="3" placeholder="<?php echo $option['value']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"></textarea>
        </div>
        <?php } ?>
        <?php } ?>
        <?php } ?>

        <?php if ($discounts && isset($membership)) { ?>
        <table class="table table-bordered">
          <thead>
            <tr><td colspan="2" class="text-center"><b>Membership Special Offer</b></td></tr>
          </thead>
          <tbody>
            <?php foreach ($discounts as $discount) { ?>
            <tr>
              <td class="text-center"><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php } ?>

        <div class="form-group">
          <div class="input-group">
            <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-sm value-control" data-action="minus" data-target="product-amount">
                <span class="glyphicon glyphicon-minus"></span>
              </button>
            </span>
            <input type="text" name="quantity" value="<?php echo $minimum; ?>" class="form-control input-sm" id="product-amount"  readonly>
            <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-sm value-control" data-action="plus" data-target="product-amount">
                <span class="glyphicon glyphicon-plus"></span>
              </button>
            </span>
          </div>
        </div>

        <div class="action">
          <button type="button" class="btn btn-primary" onclick="wishlist.add('<?php echo $product_id; ?>');">WISHLIST <i class="fa fa-heart"></i></button>

          <?php
          if($is_preorder){
          ?>
          <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink">PRE ORDER <i class="fa fa-shopping-cart"></i></button>
          <?php
          }
          else{
          ?>
          <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink">ADD TO CART <i class="fa fa-shopping-cart"></i></button>
          <?php
          }
          ?>          
        </div>

        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />

        <?php if ($minimum > 1) { ?>
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
        <?php } ?>
      </form>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li role="description" class="active"><a href="#tab-description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
      <?php if ($review_status) { ?>
      <li role="review"><a href="#tab-review" aria-controls="review" role="tab" data-toggle="tab">Reviews <?php echo $tab_review; ?></a></li>
      <?php } ?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="tab-description">
        <?php echo $description; ?>
      </div>
      <?php if ($review_status) { ?>
      <div role="tabpanel" class="tab-pane" id="tab-review">
        <div id="review"></div>
        <form class="form-horizontal" id="form-review">
          

        </form>
      <?php } ?>
    </div>
  </section>
  <!-- /Product Info -->  

<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
  $.ajax({
    url: 'index.php?route=product/product/getRecurringDescription',
    type: 'post',
    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#recurring-description').html('');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();

      if (json['success']) {
        $('#recurring-description').html(json['success']);
      }
    }
  });
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
  $.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-cart').button('loading');
    },
    complete: function() {
      $('#button-cart').button('reset');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            var element = $('#input-option' + i.replace('_', '-'));

            if (element.parent().hasClass('input-group')) {
              element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            } else {
              element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            }
          }
        }

        if (json['error']['recurring']) {
          $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
        }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
      }

      if (json['success']) {
        // $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        $('#product-gallery').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

        $('html, body').animate({ scrollTop: 0 }, 'slow');

        $('#cart > span').load('index.php?route=mobile/cart/info');

        $('.form')[0].reset();
      }
    },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
});
//--></script>
<script type="text/javascript"><!--
// $('.date').datetimepicker({
//   pickTime: false
// });

// $('.datetime').datetimepicker({
//   pickDate: true,
//   pickTime: true
// });

// $('.time').datetimepicker({
//   pickDate: false
// });

$('button[id^=\'button-upload\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').val(json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('fast');

    $('#review').load(this.href);

    $('#review').fadeIn('fast');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
  $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
    type: 'post',
    dataType: 'json',
    data: $("#form-review").serialize(),
    beforeSend: function() {
      $('#button-review').button('loading');
    },
    complete: function() {
      $('#button-review').button('reset');
    },
    success: function(json) {
      $('.alert-success, .alert-danger').remove();

      if (json['error']) {
        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
        $('#review').after('<div class="alert alert-success mtop10"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('input[name=\'name\']').val('');
        $('textarea[name=\'text\']').val('');
        $('input[name=\'rating\']:checked').prop('checked', false);
      }
    }
  });
});

$(document).ready(function() {
  // $('.thumbnails').magnificPopup({
  //   type:'image',
  //   delegate: 'a',
  //   gallery: {
  //     enabled:true
  //   }
  // });
  $(".review-rating").rating();
});
//--></script>        
<?php echo $footer; ?>
