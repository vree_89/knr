<?php echo $header; ?>
<?php echo $slideshow; ?>

  <!-- Product -->
  <!-- Images size 300 x 200 -->
  <section class="container-fluid">
    <h3><?php echo $heading_title; ?> </h3>
    <!-- Sort -->
    <div class="sort">
      <div class="btn-group">
        <button type="button" class="btn btn-pink btn-xs">Sort Product</button>
        <button type="button" class="btn btn-pink dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <!-- <span class="caret"></span> -->
          <i class="fa fa-sort"></i>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu">
          <?php foreach ($sorts as $sorts) { ?>
          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
          <li class="active"><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
          <?php } else { ?>
          <li><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </div>
    <!-- /Sort -->

    <div class="product mbot10">
      <?php if(count($products)) { ?>
      <?php foreach ($products as $product) { ?>
      <div class="item">
        <a href="<?php echo $product['href']; ?>">
          <div class="thumbnail">
            <?php if($product['special']){ ?>
            <div class="ribbon"><span><?php echo $product['discount']; ?>% OFF</span></div>
            <?php } ?>
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
<?php
if($product['quantity']<=0){
?>
<div class="out-of-stock"><?php echo $product['stock_status']?></div>            
<?php
}
else if($product['is_preorder']){
?>
<div class="out-of-stock"><span style="font-size:smaller;">Date Available:<br/><?php echo $product['date_available']?></span></div>
<?php
}?> 
            
            <div class="info">
              <p><?php echo $product['manufacturer']; ?></p>
              <h5><?php echo $product['name']; ?></h5>
              <div class="ratings">
                <div class="star-ratings">
                  <span style="width:<?php echo $product['rating']; ?>%" class="star-ratings-sprite"></span>
                </div>
                <small>(<?php echo $product['reviews']; ?> review)</small>
              </div>
              <div class="price">
                <span class="rate"><?php echo $product['rate'];?>%</span>
                <span class="retail"><?php echo $product['price_retail'];?></span>
                <span class="our"><?php echo $product['price'];?></span> 
              </div>
            </div>
          </div>
        </a>
      </div>
      <?php } ?>
      <?php } else { ?>
        <div class="not-found">
          <h3>No products found</h3>
        </div>
      <?php } ?>
      <div class="clearfix"></div>
    </div>

    <div class="text-center">
      <?php echo $pagination; ?>
    </div>
  </section>
  <!-- /Product -->

<?php echo $footer; ?>
<style type="text/css">
.out-of-stock{
position: absolute;
left: 0;
top:20%;
float: left;
width: 100%;
//background: rgba(0,0,0,.5);
background: rgba(255,020,147,.5);
padding: 15px 0px;
text-align: center;
font-size: 150%;
color:white;
font-weight: bold;

}
</style>