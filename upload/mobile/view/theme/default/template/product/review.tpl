<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="panel panel-white review panel-shadow">
  <div class="post-heading">
    <div class="pull-left meta">
      <div class="title h5">
        <b><?php echo $review['author']; ?></b>
      </div>
      <h6 class="text-muted time"><?php echo $review['date_added']; ?></h6>
    </div>
    <div class="pull-right meta">
      <div class="ratings comments">
        <div class="star-ratings">
          <?php $rating = ($review['rating'] / 5) * 100?>
          <span style="width:<?php echo $rating; ?>%" class="star-ratings-sprite"></span>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div> 
  <div class="post-description"> 
    <p><?php echo $review['text']; ?></p>
  </div>
</div>
<?php } ?>
<div class="text-center"><?php echo $pagination; ?></div>
<?php } else { ?>
<br/>
<div class="well text-center no-reviews"><?php echo $text_no_reviews; ?></div>
<p style="padding:0;margin:0;"><b>Keterangan:</b></p>
<ol style="padding:12px;margin:0;">
<li>Ulasan hanya bisa diberikan oleh pembeli yang transaksinya sudah berhasil</li>
<li>Berikanlah ulasan yang sejujur-jujurnya, karena komunitas akan menilai sendiri nantinya seberapa jujur setiap pembeli dan penjual yang ada di KR House Beauty.</li>
<li>Ulasan <b>Kualitas Produk</b> menandakan kualitas dari barang yang telah diterima oleh pembeli</li>
</ol>
<style type="text/css">.no-reviews{font-weight: bold;text-transform: capitalize;font-size: normal;}</style>
<!-- <p><?php //echo $text_no_reviews; ?></p> -->
<?php } ?>