<?php echo $header; ?>
  <div class="container-fluid default-height" id="forgot-password">
    <h1 class="page-title">RESET YOUR PASSWORD</h1>
    <p>Enter the new password you wish to use.</p>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <div class="form-group required">
        <label class="control-label"><?php echo $entry_password; ?></label>
        <input class="form-control input-sm" type="password" name="password" value="<?php echo $password; ?>" id="input-password">
        <?php if ($error_password) { ?>
        <div class="text-danger"><?php echo $error_password; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label"><?php echo $entry_confirm; ?></label>
        <input class="form-control input-sm" type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm">
        <?php if ($error_confirm) { ?>
        <div class="text-danger"><?php echo $error_confirm; ?></div>
        <?php } ?>
      </div>

      <p class="required"><small>*Required fields</small></p>

      <div class="button-set">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $back; ?>"><small><i class="fa fa-angle-double-left"></i> <?php echo $button_back; ?></small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block"><?php echo $button_continue; ?></button>
        </div>
        <div class="clearfix"></div>
      </div>
    </form>
  </div>
<?php echo $footer; ?>