<?php echo $header; ?>
  <!-- Address -->
  <section class="container-fluid default-height">
    <div class="user-block">
      <h4><?php echo $text_address_book; ?></h4>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
      <?php } ?>

      <table class="table datatable table-bordered address mtop10">
        <thead>
          <tr>
            <td>Hidden Header</td>
            <td>Hidden Header</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($addresses as $result) { ?>
          <tr>
            <td>
              <?php if($result['primary']) { ?>
              <p><i class="fa fa-home"></i> Primary</p>
              <?php } ?>
              <p><?php echo $result['address']; ?></p>
            </td>
            <td>
              <a href="<?php echo $result['update']; ?>" class="btn btn-sm btn-block btn-primary"><?php echo $button_edit; ?></a>
              <a href="<?php echo $result['delete']; ?>" class="btn btn-sm btn-block btn-danger"><?php echo $button_delete; ?></a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </section>
  <!-- /Address -->

  <!-- DataTables -->
  <script type="text/javascript">
    $(document).ready(function() {
      $(".datatable").DataTable({
        "info"        : false,
        "pageLength"  : 5,
        "sort"        : false,
        "dom"         : "<'row'<'col-xs-12'<'pull-left'f> <'anchor'>>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-12'p>>",
        "pagingType"  : "simple",
        "language"    : {
          "emptyTable"  : "<?php echo $text_empty; ?>",
          "paginate"    : {
            "previous": "&laquo;",
            "next": "&raquo;"
          }
        }
      });

      $("div.anchor").html("<a href='<?php echo $add; ?>' class='btn btn-pink btn-sm pull-right'><i class='fa fa-plus'></i> Add</a>");
    });
  </script>
<?php echo $footer; ?>