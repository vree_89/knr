<?php echo $header; ?>
  <!-- History -->
  <section class="container-fluid default-height" id="history">
    <div class="user-block">
      <h4>History</h4>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs history" role="tablist">
        <li><a href="<?php echo $transaction; ?>">Transaction</a></li>
        <li><a href="<?php echo $reward; ?>">Point Reward</a></li>
        <li class="active"><a>Membership</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content mtop10">
        <div id="transaction">
          <?php if ($orders) { ?>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <tbody>
                <?php foreach ($orders as $order) { ?>
                <tr>
                  <td>
                    <table>
                      <tr>
                        <td width="45%"><b><?php echo $column_order_id; ?></b></td>
                        <td>: #<?php echo $order['order_id']; ?></td>
                      </tr>
                      <tr>
                        <td><?php echo $column_package; ?></td>
                        <td>: <?php echo $order['package']; ?></td>
                      </tr>
                      <tr>
                        <td><?php echo $column_deposit; ?></td>
                        <td>: <?php echo $order['deposit']; ?></td>
                      </tr>
                      <tr>
                        <td><?php echo $column_date_expired; ?></td>
                        <td>: <?php echo $order['expired_date']; ?></td>
                      </tr>
                      <tr>
                        <td><?php echo $column_date_added; ?></td>
                        <td>: <?php echo $order['date_added']; ?></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="text-center">
            <?php echo $pagination; ?>
          </div>
          <?php } else { ?>
          <p><?php echo $text_empty; ?></p>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
  <!-- /History -->
<?php echo $footer; ?>