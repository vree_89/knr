<?php echo $header; ?>
  <!-- Profile -->
  <section class="container-fluid default-height">
    <div class="user-block">
      <h4>Profile Information</h4>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li><a href="<?php echo $continue; ?>">Profile</a></li>
        <li class="active"><a>Wish List</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <!-- <div id="success-msg"></div> -->
        <div id="wishlist">
          <table class="table datatable table-bordered wishlist mtop10">
            <thead>
              <tr>
                <td>Hidden Header</td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td>
                  <div class="wishlist-item">
                    <div class="images">
                      <?php if ($product['thumb']) { ?>
                      <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                      <?php } ?>
                    </div>
                    <div class="title">
                      <h5><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h5>
                      <p>by <?php echo $product['manufacturer']; ?></p>
                    </div>
                    <div class="desc">
                      <div class="well">
                        <table class="table borderless">
                          <tbody>
                            <tr>
                              <th>Status
                              </th><td>:</td>
                              <td><span class="text-success">In Stock</span></td>
                            </tr>
                            <?php if ($product['special']) { ?>
                            <tr>
                              <th>Discount
                              </th><td>:</td>
                              <td><?php echo $product['discount']; ?>%</td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <th>Price
                              </th><td>:</td>
                              <td>
                                <?php if ($product['price']) { ?>
                                <?php if (!$product['special']) { ?>
                                <?php echo $product['price']; ?>
                                <?php } else { ?>
                                <b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s>
                                <?php } ?>
                                <?php } ?>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>',1,'','#success-msg-<?php echo $product['product_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_cart; ?>" class="btn btn-sm btn-primary"><i class="fa fa-shopping-bag"></i> </button>
                  <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> </a>
                  <div id="success-msg-<?php echo $product['product_id']; ?>"></div>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
  <!-- /Profile -->

  <!-- DataTables -->
  <script type="text/javascript">
    $(document).ready(function() {
      $(".datatable").DataTable({
        "info"        : false,
        "pageLength"  : 5,
        "sort"        : false,
        "dom"         : "<'row'<'col-xs-12'<'pull-left'f>>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-12'p>>",
        "pagingType"  : "simple",
        "language"    : {
          "emptyTable"  : "<?php echo $text_empty; ?>",
          "paginate"    : {
            "previous": "&laquo;",
            "next": "&raquo;"
          }
        }
      });
    });
  </script>
<?php echo $footer; ?> 