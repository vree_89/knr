<?php echo $header; ?>
  <div class="container-fluid">
    <h4><?php echo $heading_title; ?></h4>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <div class="form-group required">
        <label class="control-label">First Name</label>
        <input class="form-control input-sm" type="text" name="firstname" value="<?php echo $firstname; ?>" id="input-firstname">
        <?php if ($error_firstname) { ?>
        <div class="text-danger"><?php echo $error_firstname; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Last Name</label>
        <input class="form-control input-sm" type="text" name="lastname" value="<?php echo $lastname; ?>" id="input-lastname">
        <?php if ($error_lastname) { ?>
        <div class="text-danger"><?php echo $error_lastname; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Address</label>
        <input class="form-control input-sm" type="text" name="address_1" value="<?php echo $address_1; ?>" id="input-address-1">
        <?php if ($error_address_1) { ?>
        <div class="text-danger"><?php echo $error_address_1; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Country</label>
        <select name="country_id" id="input-country" class="form-control input-sm">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($countries as $country) { ?>
          <?php if ($country['country_id'] == $country_id) { ?>
          <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
        <?php if ($error_country) { ?>
        <div class="text-danger"><?php echo $error_country; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Province</label>
        <select name="zone_id" id="input-zone" class="form-control input-sm">
        </select>
        <?php if ($error_zone) { ?>
        <div class="text-danger"><?php echo $error_zone; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">City</label>
        <select name="city" id="input-city" class="form-control input-sm">
          <option value="" selected="selected"><?php echo $text_select; ?></option>
        </select>
        <?php if ($error_city) { ?>
        <div class="text-danger"><?php echo $error_city; ?></div>
        <?php } ?>
      </div>
      <div class="form-group">
        <label class="control-label">Postcode</label>
        <input class="form-control input-sm" type="text" name="postcode" value="<?php echo $postcode; ?>" id="input-postcode" />
        <?php if ($error_postcode) { ?>
        <div class="text-danger"><?php echo $error_postcode; ?></div>
        <?php } ?>
      </div>
      <div class="form-group">
        <label class="control-label">Primary Address</label>
        <?php if ($default) { ?>
        <div class="radio">
          <label class="radio-inline">
            <input type="radio" name="default" value="1" checked="checked" />
            <?php echo $text_yes; ?>
          </label>
          <label class="radio-inline">
            <input type="radio" name="default" value="0" />
            <?php echo $text_no; ?>
          </label>
        </div>
        <?php } else { ?>
        <div class="radio">
          <label class="radio-inline">
            <input type="radio" name="default" value="1" />
            <?php echo $text_yes; ?>
          </label>
          <label class="radio-inline">
            <input type="radio" name="default" value="0" checked="checked" />
            <?php echo $text_no; ?>
          </label>
        </div>
        <?php } ?>
      </div>

      <p class="required"><small>*Required fields</small></p>

      <div class="button-set mbot10">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $back; ?>"><small><i class="fa fa-angle-double-left"></i> <?php echo $button_back; ?></small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block"><?php echo $button_continue; ?></button>
        </div>
        <div class="clearfix"></div>
      </div>
    </form>
  </div>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length-2) {
		$('.form-group').eq(parseInt($(this).attr('data-sort'))+2).before(this);
	}

	if ($(this).attr('data-sort') > $('.form-group').length-2) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('.form-group').length-2) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('.form-group').length-2) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      // $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('select[name=\'zone_id\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
      $('select[name=\'country_id\']').prop('disabled', 'disabled');
    },
    complete: function() {
      $('.fa-spin').remove();
      $('select[name=\'country_id\']').prop('disabled', false);
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '" data-name="' + json['zone'][i]['name'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('select[name=\'zone_id\']').html(html);
      $('select[name=\'zone_id\']').trigger('change');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('select[name=\'country_id\']').trigger('change');

$('select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/city&province_name=' + $(this).find(':selected').data('name'),
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'city\']').before(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('select[name=\'zone_id\']').prop('disabled', 'disabled');
    },
    complete: function() {
      $('.fa-spin').remove();
      $('select[name=\'zone_id\']').prop('disabled', false);
    },
    success: function(json) {
      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '"';

          if (json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] == '<?php echo $city; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('select[name=\'city\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});
//--></script>
<?php echo $footer; ?>
