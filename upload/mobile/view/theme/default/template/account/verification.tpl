<?php echo $header; ?>
<div class="container-fluid default-height">
  <div class="success-message">
    <i class="fa fa-check-circle"></i>
    <h3><?php echo $heading_title; ?></h3>
    <?php echo $text_message; ?>
    <a href="<?php echo $continue; ?>" class="btn btn-sm btn-pink">Continue <span class="fa fa-chevron-right"></span></a>
  </div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
  var counter = 6;
  var interval = setInterval(function() {
      counter--;
      // Display 'counter' wherever you want to display it.
      $("span.counter").html(counter);
      if (counter == 0) {
          window.location = "<?php echo $redirect; ?>";
          clearInterval(interval);
      }
  }, 1000);
</script>