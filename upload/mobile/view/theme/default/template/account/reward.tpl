<?php echo $header; ?>
  <!-- History -->
  <section class="container-fluid default-height" id="history">
    <div class="user-block">
      <h4>History</h4>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs history" role="tablist">
        <li><a href="<?php echo $transaction; ?>">Transaction</a></li>
        <li class="active"><a>Point Reward</a></li>
        <li><a href="<?php echo $membership; ?>">Membership</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content mtop10">
        <div id="transaction">
          <p><?php echo $text_total; ?> <b><?php echo $total; ?></b>.</p>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left"><?php echo $column_date_added; ?></td>
                  <td class="text-left"><?php echo $column_description; ?></td>
                  <td class="text-right"><?php echo $column_points; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($rewards) { ?>
                <?php foreach ($rewards  as $reward) { ?>
                <tr>
                  <td class="text-left"><?php echo $reward['date_added']; ?></td>
                  <td class="text-left"><?php if ($reward['order_id']) { ?>
                    <a href="<?php echo $reward['href']; ?>"><?php echo $reward['description']; ?></a>
                    <?php } else { ?>
                    <?php echo $reward['description']; ?>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $reward['points']; ?></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="3"><?php echo $text_empty; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="text-center">
            <?php echo $pagination; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /History -->
<?php echo $footer; ?>