<?php echo $header; ?>
  <div class="container-fluid">
    <h1 class="page-title">CREATE AN ACCOUNT</h1>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <h2 class="legend">PERSONAL INFORMATION</h2>
      <div class="form-group required">
        <label class="control-label">First Name</label>
        <input class="form-control input-sm" type="text" name="firstname" value="<?php echo $firstname; ?>" id="input-firstname">
        <?php if ($error_firstname) { ?>
        <div class="text-danger"><?php echo $error_firstname; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Last Name</label>
        <input class="form-control input-sm" type="text" name="lastname" value="<?php echo $lastname; ?>" id="input-lastname">
        <?php if ($error_lastname) { ?>
        <div class="text-danger"><?php echo $error_lastname; ?></div>
        <?php } ?>
      </div>

      <?php
      /*
      <div class="form-group required">
        <label class="control-label">Phone</label>
        <input class="form-control input-sm" type="tel" name="telephone" value="<?php echo $telephone; ?>" id="input-telephone">
        <?php if ($error_telephone) { ?>
        <div class="text-danger"><?php echo $error_telephone; ?></div>
        <?php } ?>
      </div>
      
      */
      ?>


          <input type="hidden" name="verified" id="verified" value="">
          <div class="form-group required">
            
            <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
            
<div class="input-group">
  <input type="tel" name="telephone" class="form-control numeric" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" value="<?php echo $telephone; ?>">
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>


  <span class="input-group-btn">
    <button class="btn btn-default btn-verify" type="button">
        Verify
    </button>
  </span>

</div>
<div class="verify_phone" style="margin-top:5px;display:none;">
</div>
              <?php
              /*
              <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>
              
              */

              ?>
            
          </div>











      <div class="form-group required">
        <label class="control-label">Email</label>
        <input class="form-control input-sm" type="email" name="email" value="<?php echo $email; ?>" id="input-email">
        <?php if ($error_email) { ?>
        <div class="text-danger"><?php echo $error_email; ?></div>
        <?php } ?>
      </div>
      <div class="form-group">
        <label class="control-label">Reference Code</label>
        <input class="form-control input-sm" type="text" name="reference" value="<?php echo $reference; ?>" id="input-reference">
      </div>

      <?php /* <h2 class="legend">ADDRESS INFORMATION</h2>
      <div class="form-group required">
        <label class="control-label">Address</label>
        <input class="form-control input-sm" type="text" name="address_1" value="<?php echo $address_1; ?>" id="input-address-1">
        <?php if ($error_address_1) { ?>
        <div class="text-danger"><?php echo $error_address_1; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Country</label>
        <select name="country_id" id="input-country" class="form-control input-sm">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($countries as $country) { ?>
          <?php if ($country['country_id'] == $country_id) { ?>
          <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
        <?php if ($error_country) { ?>
        <div class="text-danger"><?php echo $error_country; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Province</label>
        <select name="zone_id" id="input-zone" class="form-control input-sm">
        </select>
        <?php if ($error_zone) { ?>
        <div class="text-danger"><?php echo $error_zone; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">City</label>
        <select name="city" id="input-city" class="form-control input-sm">
          <option value="" selected="selected"><?php echo $text_select; ?></option>
        </select>
        <?php if ($error_city) { ?>
        <div class="text-danger"><?php echo $error_city; ?></div>
        <?php } ?>
      </div>
      <div class="form-group">
        <label class="control-label">Postcode</label>
        <input class="form-control input-sm" type="text" name="postcode" value="<?php echo $postcode; ?>" id="input-postcode" />
          <?php if ($error_postcode) { ?>
          <div class="text-danger"><?php echo $error_postcode; ?></div>
          <?php } ?>
      </div>
      */ ?>

      <h2 class="legend">LOGIN INFORMATION</h2>
      <div class="form-group required">
        <label class="control-label">Password</label>
        <input class="form-control input-sm" type="password" name="password" value="<?php echo $password; ?>" id="input-password">
        <?php if ($error_password) { ?>
        <div class="text-danger"><?php echo $error_password; ?></div>
        <?php } ?>
      </div>
      <div class="form-group required">
        <label class="control-label">Confirm Password</label>
        <input class="form-control input-sm" type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm">
        <?php if ($error_confirm) { ?>
        <div class="text-danger"><?php echo $error_confirm; ?></div>
        <?php } ?>
      </div>
      <div class="checkbox">
        <label>
          <?php if ($newsletter) { ?>
          <input type="checkbox" name="newsletter" value="1" checked="checked">
          <?php } else { ?>
          <input type="checkbox" name="newsletter" value="1">
          <?php } ?>
          Sign Up for Newsletter
        </label>
      </div>
      <div class="checkbox">
        <label>
          <?php if ($agree) { ?>
          <input type="checkbox" name="agree" value="1" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="agree" value="1" />
          <?php } ?>
          <?php echo $text_agree; ?>
        </label>
      </div>

      <p class="required"><small>*Required fields</small></p>

      <?php echo $captcha; ?>

      <div class="button-set">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $login; ?>"><small><i class="fa fa-angle-double-left"></i> Login</small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block">SUBMIT</button>
          <!-- <button type="button" class="btn btn-primary btn-sm btn-block">FACEBOOK LOGIN</button> -->
        </div>
        <div class="clearfix"></div>
      </div>
    </form>
  </div>
<script type="text/javascript"><!--
// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('.custom-field').hide();
			$('.custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#custom-field' + custom_field['custom_field_id']).addClass('required');
				}
			}


		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
      // $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
			$('select[name=\'zone_id\']').before('<i class="fa fa-circle-o-notch fa-spin"></i>');
      $('select[name=\'country_id\']').prop('disabled', 'disabled');
		},
		complete: function() {
			$('.fa-spin').remove();
      $('select[name=\'country_id\']').prop('disabled', false);
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '" data-name="' + json['zone'][i]['name'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
      $('select[name=\'zone_id\']').trigger('change');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');

$('select[name=\'zone_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/city&province_name=' + $(this).find(':selected').data('name'),
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'city\']').before(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      $('select[name=\'zone_id\']').prop('disabled', 'disabled');
    },
    complete: function() {
      $('.fa-spin').remove();
      $('select[name=\'zone_id\']').prop('disabled', false);
    },
    success: function(json) {
      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

      if (json['city'] && json['city'] != '') {
        for (i = 0; i < json['city'].length; i++) {
          html += '<option value="' + json['city'][i]['city_id'] + '_' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '"';

          if (json['city'][i]['city_id'] + '_' + json['city'][i]['city_name'] == '<?php echo $city; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['city'][i]['type'] + ' ' + json['city'][i]['city_name'] + '</option>';
        }
      } else {
        html = '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('select[name=\'city\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});


$(document).delegate("input.verification", "input", function(){
    if (this.value.length > 1)         
        this.value = this.value.slice(0,1); 
});

$(document).delegate("input.verification", "keyup", function(e){

    if ((e.which == 8 || e.which == 46) && $(this).val() =='') {
        $(this).prev('input.verification').focus();
    }
});


$(document).delegate("input.numeric", "keydown", function(e){
  
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


$(document).delegate("input.autotab", "keyup", function(e){

  // alert(e.which);
  var val = $(this).val();
  var id = $(this).attr('id');
  var num = id.substring(11, 12);
  var next = (parseInt(num)+1);
  var keyCode = e.keyCode || e.which;    


    if(val!=""){

      if((e.shiftKey && e.keyCode == 9)===false){
        if(num<$('.autotab').length){
          if($('#input-token'+next).val()==""){
            //alert('maju');
            $('#input-token'+next).focus();
          }
        }
      }

    }

  return false;

  //alert('x');
});
$(document).delegate(".btn-verify", "click", function(){
  // alert("<?php echo $action_request?>");return false;
  if($("#input-telephone").val()!=""){
  //$('.btn-verify').click(function(){
  $.ajax({
  url:"<?php echo $action_request?>",
  data:{telephone:$("#input-telephone").val()},
  type:"post",
  dataType:"json",
  beforeSend: function() { $('.btn-verify').button('loading'); },
  complete: function() { $('.btn-verify').button('reset'); },
  success:function(data){
  if(data.status=="1"){ 
    // alert('a');
    //sukses
    $('.btn-verify').attr("disabled", 'disabled');


var res = data.prenumber.split("");
var html = "";
for (var i=0; i<res.length; i++) {
html += '<input type="text" size="1" maxlength="1" value="'+res[i]+'" class="text-center autotab numeric" disabled />';
}
    html += '&nbsp;<input type="number" size="1" name="token1" value="" id="input-token1" class="text-center autotab numeric verification"  /><input type="number" size="1" name="token2" value="" id="input-token2" class="text-center autotab numeric verification" /><input type="number" size="1" name="token3" value="" id="input-token3" class="text-center autotab numeric verification" /><input type="number" size="1" name="token4" value="" id="input-token4" class="text-center autotab numeric verification" /><input type="number" size="1" name="token5" value="" id="input-token5" class="text-center autotab numeric verification" />&nbsp;<button type="button" class="btn-verify-token btn btn-default">SUBMIT</button><div class="verify_message"></div>';



    $('.verify_phone').html(html);
    $('.verify_phone').fadeIn();
  }
  else if(data.status=="0"){
    //error / failed
    // alert('b');
    $('.verify_phone').html('<div class="alert alert-warning">'+data.message+'</div>').fadeIn();

  }
  else if(data.status=="2"){
    // alert('c');
    //phone used by another account
    $('.verify_phone').html('<div class="alert alert-warning">'+data.message+'</div>').fadeIn();

  }
  },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }

  });


  

  }
  return false;



});




$(document).delegate(".btn-verify-token", "click", function(){
//$('.btn-verify-token').click(function(){
  var token = $("#input-token1").val()+$("#input-token2").val()+$("#input-token3").val()+$("#input-token4").val()+$("#input-token5").val();
  $('.verify_message').html('');
  $.ajax({
  url:"<?php echo $action_verify?>",
  data:{token:token},
  type:"post",
  dataType:"json",
beforeSend: function() { $('.btn-verify-token').button('loading'); },
complete: function() { $('.btn-verify-token').button('reset'); },
  success:function(data){
  if(data.status=="1"){ 
      var counter = 4;
      var interval = setInterval(function() {
      counter--;
      // Display 'counter' wherever you want to display it.
      // $("span.counter").html(counter);
          $('.verify_message').html(data.message+" Close in : "+counter);

      if (counter == 0) {
        $('.verify_phone').fadeOut();
        clearInterval(interval);


      $("#input-telephone").attr('readonly','readonly');
      $(".btn-verify").text('Verified');
      $(".btn-verify").attr('disabled','disabled');
      $("#verified").val("1");


      }
      }, 1000);

      //verified

  }
  else{
    $('.verify_message').html(data.message).fadeIn();

  }
  },
            error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }

  });
  return false;

});
//--></script>
<?php echo $footer; ?>
<style type="text/css">

input.autotab{
  font-size: small;
  padding:3px;
  width: 23.5px;
}

</style>
