<?php echo $header; ?>
  <div class="container-fluid">
    <h1 class="page-title">REGISTERED CUSTOMERS</h1>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label class="control-label">Email Address / Phone Number</label>
        <input class="form-control input-sm" type="text" name="email" value="<?php echo $email; ?>" id="input-email">
      </div>
      <div class="form-group">
        <label class="control-label">Password</label>
        <input class="form-control input-sm" type="password" name="password" value="<?php echo $password; ?>" id="input-password">
      </div>
      <?php if ($redirect) { ?>
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
      <?php } ?>
      <div class="button-set">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $forgotten; ?>"><small>Forgot Your Password?</small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block">LOGIN</button>
          <!-- <button type="button" class="btn btn-primary btn-sm btn-block">FACEBOOK LOGIN</button> -->
        </div>
        <div class="clearfix"></div>
      </div>
    </form>

    <h1 class="page-title">NEW CUSTOMERS</h1>
    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>

    <div class="button-set">
      <div class="pull-right">
        <a href="<?php echo $register; ?>" class="btn btn-pink btn-sm btn-block">CREATE AN ACCOUNT</a>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
<?php echo $footer; ?>