<?php
/*
<?php echo $header; ?>
  <div class="container-fluid default-height" id="forgot-password">
    <h1 class="page-title">FORGOT YOUR PASSWORD?</h1>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <h2 class="legend">RETRIEVE YOUR PASSWORD HERE</h2>
      <p>Please enter your email address / phone number below. You will receive a link to reset your password.</p>
      <div class="form-group required">
        <label class="control-label">Email Address / Phone Number</label>
        <input class="form-control input-sm" type="text" name="email" value="<?php echo $email; ?>" id="input-email">
      </div>

      <p class="required"><small>*Required fields</small></p>

      <div class="button-set">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $back; ?>"><small><i class="fa fa-angle-double-left"></i> Login</small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block">SUBMIT</button>
        </div>
        <div class="clearfix"></div>
      </div>
    </form>
  </div>
<?php echo $footer; ?>
*/

?>

<?php echo $header; ?>
  <div class="container-fluid default-height" id="forgot-password">
    <h1 class="page-title">FORGOT YOUR PASSWORD?</h1>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post">
      <h2 class="legend">RETRIEVE YOUR PASSWORD HERE</h2>
      <p>Please enter your phone number below. You will receive a link to reset your password.</p>
      <div class="form-group required">
        <label class="control-label">Phone Number</label>
        <input class="form-control input-sm numeric" type="number" name="telephone" value="<?php echo $telephone; ?>" id="input-telephone" required="required">
      </div>

      <p class="required"><small>*Required fields</small></p>

      <div class="button-set">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $back; ?>"><small><i class="fa fa-angle-double-left"></i> Login</small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block">SUBMIT</button>
        </div>
        <div class="clearfix"></div>
      </div>
    </form>


  </div>
<?php echo $footer; ?>
<script type="text/javascript">
$(".numeric").keydown(function (e) {
  
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });




</script>
