<?php echo $header; ?>
  <!-- Profile -->
  <section class="container-fluid">
    <div class="user-block">
      <h4>Profile Information</h4>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
      <?php } ?>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a>Profile</a></li>
        <li><a href="<?php echo $wishlist; ?>">Wish List</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div id="profile">
          <p class="heading">Account</p>
          <p><small class="text-muted contact">Name</small> <?php echo $customer_info['firstname'] .' '. $customer_info['lastname']; ?></p>
          <p><small class="text-muted contact">Email</small> <?php echo $customer_info['email']; ?></p>
          <p><small class="text-muted contact">Phone</small> <?php echo $customer_info['telephone']; ?></p>
          <p><small class="text-muted contact">Point</small> <?php echo $reward_total; ?> <!-- <a href="redeem.html"><small>(Redeem Now)</small></a> --></p>
          <p><small class="text-muted contact">Wallet</small> <?php echo number_format($customer_info['deposit']); ?></p>
          <a href="<?php echo $edit; ?>" class="btn btn-pink btn-sm"><i class="fa fa-edit"></i> Edit Account</a>
          <a href="<?php echo $password; ?>" class="btn btn-primary btn-sm"><i class="fa fa-key"></i> Change Password</a>

          <p class="heading">Membership</p>
          <?php 
            $membership_name = isset($membership_info['membership_name']) ? $membership_info['membership_name'] : 'Non-Membership';
            $membership_expired = !isset($membership_info['membership_name']) ? '-' : date('d F Y - H:i:s', strtotime($customer_info['membership_expired']));
          ?>
          <p><small class="text-muted contact">Name</small> <?php echo $membership_name; ?></p>
          <p><small class="text-muted contact">Expired</small> <?php echo $membership_expired; ?></p>

          <p class="heading">Address</p>
          <?php if ($address_info) { ?>
          <p>
          <?php echo $address_info['address_1'].', '.explode('_', $address_info['city'])[1].', '.$address_info['zone'].', '.$address_info['country'] ; ?> 
          <br> 
          <?php echo $address_info['postcode']; ?>
          </p>
          <?php } else { ?>
          <p>You have no addresses in your account. <a href="<?php echo $address; ?>">Click here to add address</a>.</p>
          <?php } ?>

          <p class="heading">Referal Link</p>
          <div class="input-group">
            <input type="text" id="referal_link" class="form-control" value="<?php echo $referal_link; ?>" readonly>
            <span class="input-group-btn">
              <button id="copy_btn" class="btn btn-default" type="button">Copy</button>
            </span>
          </div><br>
          <div class="share">
            <div class="sharethis-inline-share-buttons" data-url="<?php echo $referal_link; ?>"></div>
          </div>

          <p class="heading">Others Actions</p>
          <ul class="list-unstyled">
            <?php /*<li><a href="<?php echo $return; ?>" class="pink"><?php echo $text_return; ?></a></li>*/?>
            <?php /*<li><a href="<?php echo $download; ?>" class="pink"><?php echo $text_download; ?></a></li>*/?>
            <li><a href="<?php echo $newsletter; ?>" class="pink"><?php echo $text_newsletter; ?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <script>
    $(document).ready(function() {
      var copyBtn = document.querySelector('#copy_btn');

      copyBtn.addEventListener('click', function(event) {
        var referalLink = document.querySelector('#referal_link');
        referalLink.select();

        try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Copying text command was ' + msg);
        } catch (err) {
          console.log('Oops, unable to copy');
        }
      });
    });
  </script>
  <!-- /Profile -->
<?php echo $footer; ?> 