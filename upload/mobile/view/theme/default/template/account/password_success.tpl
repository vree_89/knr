<?php echo $header; ?>
  <div class="container-fluid default-height" id="forgot-password">
    <h1 class="page-title">RESET YOUR PASSWORD</h1>
    
    <p>Your password successfully changed</p>
    <a href="<?php echo $back?>"><button class="btn btn-default">Back</button></a>
  </div>
<?php echo $footer; ?>