<?php echo $header; ?>
  <!-- History -->
  <section class="container-fluid default-height" id="history">
    <div class="user-block">
      <h4>History</h4>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs history" role="tablist">
        <li class="active"><a>Transaction</a></li>
        <li><a href="<?php echo $reward; ?>">Point Reward</a></li>
        <li><a href="<?php echo $membership; ?>">Membership</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content mtop10">
        <div id="transaction">
          <?php if ($orders) { ?>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-right"><?php echo $column_order_id; ?></td>
                  <td class="text-left"><?php echo $column_customer; ?></td>
                  <td class="text-right"><?php echo $column_product; ?></td>
                  <td class="text-left"><?php echo $column_status; ?></td>
                  <td class="text-right"><?php echo $column_total; ?></td>
                  <td class="text-left"><?php echo $column_date_added; ?></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($orders as $order) { ?>
                <tr>
                  <td class="text-right">#<?php echo $order['order_id']; ?></td>
                  <td class="text-left"><?php echo $order['name']; ?></td>
                  <td class="text-right"><?php echo $order['products']; ?></td>
                  <td class="text-left"><?php echo $order['status']; ?></td>
                  <td class="text-right"><?php echo $order['total']; ?></td>
                  <td class="text-left"><?php echo $order['date_added']; ?></td>
                  <td class="text-right">
                    <a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                  <?php
                  if($order['cancel']){
                    ?>
                    <a onclick="return confirm('Cancel your transaction?')" href="<?php echo $order['cancel']?>"  title="Cancel" class="btn btn-info">Cancel</a>
                    <?php
                  }
                  else{
                    ?>
                    <button type="button" class="btn btn-info" disabled>Cancel</button>
                    <?php
                  }
                  ?>

                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="text-center">
            <?php echo $pagination; ?>
          </div>
          <?php } else { ?>
          <p><?php echo $text_empty; ?></p>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
  <!-- /History -->

  <!-- DataTables -->
  <!-- <script type="text/javascript">
    $(document).ready(function() {
      $(".datatable").DataTable({
        "info"        : false,
        "pageLength"  : 5,
        "sort"        : false,
        "dom"         : "<'row'<'col-xs-12'<'pull-left'f>>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-12'p>>",
        "pagingType"  : "simple",
        "language"    : {
          "emptyTable"  : "<?php echo $text_empty; ?>",
          "paginate"    : {
            "previous": "&laquo;",
            "next": "&raquo;"
          }
        }
      });
    });
  </script> -->
<?php echo $footer; ?>
