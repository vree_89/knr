<?php
/*
<?php echo $header; ?>
  <div class="container-fluid default-height" id="forgot-password">
    <h1 class="page-title">FORGOT YOUR PASSWORD?</h1>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <h2 class="legend">RETRIEVE YOUR PASSWORD HERE</h2>
      <p>Please enter your email address / phone number below. You will receive a link to reset your password.</p>
      <div class="form-group required">
        <label class="control-label">Email Address / Phone Number</label>
        <input class="form-control input-sm" type="text" name="email" value="<?php echo $email; ?>" id="input-email">
      </div>

      <p class="required"><small>*Required fields</small></p>

      <div class="button-set">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $back; ?>"><small><i class="fa fa-angle-double-left"></i> Login</small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block">SUBMIT</button>
        </div>
        <div class="clearfix"></div>
      </div>
    </form>
  </div>
<?php echo $footer; ?>
*/

?>

<?php echo $header; ?>
  <div class="container-fluid default-height" id="forgot-password">
    <h1 class="page-title">FORGOT YOUR PASSWORD?</h1>



    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" onsubmit="return combineToken();">
      <input type="hidden" name="token" id="token">
      <h2 class="legend">Verify</h2>
      <p>We just make a miscall to your phone, please verify by fill the last 5 dialer number</p>
      <div class="form-group required input-token">
           <span class="prenumber">
            <?php
  if(($prenumber)){
    
    //echo $prenumber;
    $arr = str_split($prenumber);
    for($i=0;$i<count($arr);$i++){
      ?><input type="text" size="1" value="<?php echo $arr[$i]?>" class="text-center prenumber " disabled /><?php
    }

  }
            ?>

           </span>&nbsp;&nbsp;              
              <input type="number" size="1" name="token1"  placeholder="" id="input-token1" class="text-center autotab numeric" required="required" /><input type="number" size="1" name="token2"  placeholder="" id="input-token2" class="text-center autotab numeric" required="required" /><input type="number" size="1" name="token3"  placeholder="" id="input-token3" class="text-center autotab numeric" required="required" /><input type="number" size="1" name="token4"  placeholder="" id="input-token4" class="text-center autotab numeric" required="required" /><input type="number" size="1" name="token5"  placeholder="" id="input-token5" class="text-center autotab numeric" required="required" />
       

      </div>

      <p class="required"><small>*Required fields</small></p>

      <div class="button-set">
        <div class="pull-left text-right">
          <a class="pink" href="<?php echo $back; ?>"><small><i class="fa fa-angle-double-left"></i> Login</small></a>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-pink btn-sm btn-block">SUBMIT</button>
        </div>
        <div class="clearfix"></div>
      </div>
    </form>

  </div>
<?php echo $footer; ?>
<script type="text/javascript">
function combineToken(){
  var token = $("#input-token1").val()+$("#input-token2").val()+$("#input-token3").val()+$("#input-token4").val()+$("#input-token5").val();
  $("#token").val(token);
  return true;
}




$('input').on("input", function () {     
    if (this.value.length > 1)         
        this.value = this.value.slice(0,1); 
});


$(':input').keydown(function(e) {
    if ((e.which == 8 || e.which == 46) && $(this).val() =='') {
        $(this).prev('input').focus();
    }
});
$(".numeric").keydown(function (e) {
  
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

$(".autotab").keyup(function(e){
  // alert(e.which);
  var val = $(this).val();
  var id = $(this).attr('id');
  var num = id.substring(11, 12);
  var next = (parseInt(num)+1);
  var keyCode = e.keyCode || e.which;    


    if(val!=""){

      if((e.shiftKey && e.keyCode == 9)===false){
        if(num<$('.autotab').length){
          if($('#input-token'+next).val()==""){
            //alert('maju');
            $('#input-token'+next).focus();
          }
        }
      }

    }

  return false;

  //alert('x');
});


</script>
<style type="text/css">

input.autotab,input.prenumber{
  font-size: small;
  padding:3px;
  width: 23.5px;
}
</style>
