<?php
// Heading
$_['heading_title']   = 'Lupa kata laluan Anda?';

// Text
$_['text_account']    = 'Akaun';
$_['text_forgotten']  = 'Lupa kata laluan';
$_['text_your_email'] = 'Alamat Emel Anda';
$_['text_email']      = 'Masukkan alamat e-mel yang dikaitkan dengan akaun anda. Klik tatal untuk mempunyai pautan set semula kata laluan dihantar kepada anda.';
$_['text_your_phone'] = 'Nomor Telepoln Anda';
$_['text_phone']      = 'Masukkan nomor telepon yang terkait dengan akun Anda.';
$_['text_success']    = 'Email dengan link konfirmasi telah dikirim alamat email Anda.';

// Entry
$_['entry_email']     = 'Alamat Email';
$_['entry_phone']     = 'Nomor Telepon';
$_['entry_password']  = 'Kata Sandi Baru';
$_['entry_confirm']   = 'Konfirmasi';

// Error
$_['error_email']     = 'Peringatan: alamat email tidak ditemukan dalam catatan kami, silakan coba lagi!';
$_['error_approved']  = 'Peringatan: Akun Anda memerlukan persetujuan sebelum Anda dapat login.';
$_['error_password']  = 'Password harus antara 4 dan 20 karakter!';
$_['error_confirm']   = 'Sandi dan konfirmasi sandi tidak cocok!';

$_['error_phone_not_exists']     = 'Nomor Telepon tidak valid atau tidak terdaftar';

