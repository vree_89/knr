<?php
// Heading
$_['heading_title']      = 'Akaun Saya';

// Text
$_['text_account']       = 'Akaun';
$_['text_my_account']    = 'Akaun Saya';
$_['text_my_orders']     = 'Pesanan Saya';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit maklumat akaun Anda';
$_['text_password']      = 'Perubahan kata laluan Anda';
$_['text_address']       = 'Pengubahsuaian entri buku alamat Anda';
$_['text_credit_card']   = 'Mengurus Kartu Kredit';
$_['text_wishlist']      = 'Pengubahsuaian senarai keinginan Anda';
$_['text_order']         = 'Lihat riwayat pesanan';
$_['text_download']      = 'Senarai muat turun';
$_['text_reward']        = 'Points Ganjaran Anda';
$_['text_return']        = 'Lihat permintaan kembali Anda ';
$_['text_transaction']   = 'Transaksi Anda';
$_['text_newsletter']    = 'Melanggan / berhenti melanggan newsletter';
$_['text_recurring']     = 'Pembayaran berulang';
$_['text_transactions']  = 'Transaksi';
$_['text_my_account'] = 'Akaun Saya';

		$_['text_category_account'] = 'Maklumat Dasar';
		$_['text_category_membership'] = 'Keahlian';
		$_['text_category_address'] = 'Alamat';
		$_['text_category_referal'] = 'Pautan Rujukan';
