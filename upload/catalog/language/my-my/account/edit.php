<?php
// Heading
$_['heading_title']      = 'Maklumat Akaun Saya';

// Text
$_['text_account']       = 'Akaun';
$_['text_edit']          = 'Edit Maklumat';
$_['text_your_details']  = 'Data Peribadi Anda';
$_['text_success']       = 'Kejayaan: Akaun anda telah berjaya dikemas kini.';

// Entry
$_['entry_firstname']    = 'Nama Pertama';
$_['entry_lastname']     = 'Nama Terakhir';
$_['entry_email']        = 'E-Mel';
$_['entry_telephone']    = 'Telefon';
$_['entry_fax']          = 'Faks';

// Error
$_['error_exists']                = 'Amaran: Alamat e-mel sudah didaftarkan!!';
$_['error_firstname']             = 'Nama Pertama mestilah antara 1 hingga 32 aksara!';
$_['error_lastname']              = 'Nama Terakhir mestilah antara 1 hingga 32 aksara!';
$_['error_email']                 = 'Alamat e-mel tidak sah!';
$_['error_telephone']             = 'Telfon mestilah antara 3 hingga 32 aksara!';
$_['error_custom_field']          = '%s diperlukan!';