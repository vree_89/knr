<?php
// Heading
$_['heading_title']     = 'Muat Turun Akaun';

// Text
$_['text_account']      = 'Akaun';
$_['text_downloads']    = 'Muat Turun';
$_['text_empty']        = 'Anda belum membuat sebarang pesanan yang boleh dimuat turun sebelum ini!';

// Column
$_['column_order_id']   = 'ID Pesanan';
$_['column_name']       = 'Nama';
$_['column_size']       = 'Saiz';
$_['column_date_added'] = 'Tarikh Ditambah';