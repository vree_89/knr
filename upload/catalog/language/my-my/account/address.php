<?php
// Heading
$_['heading_title']        = 'Buku Alamat';

// Text
$_['text_account']         = 'Akaun';
$_['text_address_book']    = 'Penyertaan Buku Alamat';
$_['text_edit_address']    = 'Edit Alamat';
$_['text_add']             = 'Alamat Anda telah berjaya dimasukkan';
$_['text_edit']            = 'Alamat Anda telah berjaya dikemas kini';
$_['text_delete']          = 'Alamat Anda telah berjaya dipadamkan';
$_['text_empty']           = 'Anda tidak mempunyai alamat dalam akaun anda.';
$_['text_warning_delete'] = 'Ini adalah alamat utama anda, sila tetapkan alamat lain ke lalai sebelum memadamkan ini.';

// Entry
$_['entry_firstname']      = 'Nama Pertama';
$_['entry_lastname']       = 'Nama Terakhir';
$_['entry_company']        = 'Syarikat';
$_['entry_address_1']      = 'Alamat 1';
$_['entry_address_2']      = 'Alamat 2';
$_['entry_postcode']       = 'Kod Pos';
$_['entry_city']           = 'Bandar';
$_['entry_country']        = 'Negara';
$_['entry_zone']           = 'Wilayah';
$_['entry_kota']           = 'Kota/Daerah';
$_['entry_default']        = 'Alamat Lalai';

// Error
$_['error_delete']                = 'Amaran: Anda mesti mempunyai sekurang-kurangnya satu alamat!';
$_['error_default']               = 'Amaran: Anda tidak boleh memadamkan alamat lalai anda!';
$_['error_firstname']             = 'Nama Pertama mestilah antara 1 hingga 32 aksara!';
$_['error_lastname']              = 'Nama Akhir mestilah antara 1 hingga 32 aksara!';
$_['error_vat']                   = 'Jumlah VAT tidak sah!';
$_['error_address_1']             = 'Alamat mestilah antara 3 dan 128 aksara!';
$_['error_postcode']              = 'Poskod mestilah antara 2 hingga 10 aksara!';
$_['error_city']                  = 'Bandar mestilah antara 2 dan 128 aksara!';
$_['error_country']               = 'Sila pilih negara!';
$_['error_zone']                  = 'Sila pilih satu wilayah!';
$_['error_kota']                  = 'Sila pilih kota/daerah!';
$_['error_custom_field']          = '%s diperlukan!';