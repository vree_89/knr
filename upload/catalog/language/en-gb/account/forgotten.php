<?php
// Heading
$_['heading_title']   = 'Forgot Your Password?';

// Text
$_['text_account']    = 'Account';
$_['text_forgotten']  = 'Forgotten Password';
$_['text_your_email'] = 'Your E-Mail Address';
$_['text_email']      = 'Enter the e-mail address associated with your account. Click submit to have a password reset link e-mailed to you.';
$_['text_your_phone'] = 'Your Telephone Number';
$_['text_phone']      = 'Enter the telephone number associated with your account.';
$_['text_success']    = 'An email with a confirmation link has been sent your email address.';

$_['text_miscall_success']    = 'A miscall from %s has been added to your phone number. Please verify by fill the last 5 number of dialer.';
$_['text_miscall_failed']    = 'The number you\'ve entered is invalid';


// Entry
$_['entry_email']     = 'Email Address';
$_['entry_phone']     = 'Telephone';
$_['entry_password']  = 'New Password';
$_['entry_confirm']   = 'Confirm';

// Error
$_['error_email']     = 'Warning: The E-Mail Address was not found in our records, please try again!';
$_['error_approved']  = 'Warning: Your account requires approval before you can login.';
$_['error_password']  = 'Password must be between 4 and 20 characters!';
$_['error_confirm']   = 'Password and password confirmation do not match!';
$_['error_limit']     = 'Password can only be change once a day!';

$_['error_phone_not_exists']     = 'This phone number is invalid or not associated with any account';
$_['error_limit'] = 'You have reached the limit of password change today.';
