<?php
// Heading
$_['heading_title']        = 'Payment';

// Text
$_['text_membership']    = 'Membership';
$_['text_success']         = 'Success';
$_['text_customer']        = '
<p>Your order has been successfully processed!</p>
<p>You can view your order history by going to the <a href="%s">my account</a> page and by clicking on <a href="%s">Membership</a>.</p>';

$_['text_guest']           = '<p>Your order has been successfully processed!</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';