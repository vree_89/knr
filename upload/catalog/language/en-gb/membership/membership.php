<?php
// Heading
$_['heading_title']      = 'Membership';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'Membership';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Modify your address book entries';
$_['text_credit_card']   = 'Manage Stored Credit Cards';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points';
$_['text_return']        = 'View your return requests';
$_['text_transaction']   = 'Your Transactions';
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';
$_['text_membership']    = 'Membership';
$_['text_agree']         = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['error_agree']                    = 'Warning: You must agree to the %s!';
$_['error_payment']                  = 'Warning: Payment method required!';
$_['text_membership_title'] = 'Membership Package';
$_['text_membership_info1'] = 'By joining our membership, you can get special price for products in KR House Beauty.';
$_['text_membership_info2'] = 'You need to login or create an account before selecting our membership package.';

$_['text_membership_confirm_info'] = 'You just selected this package for your membership, make sure you have pick the package that is suitable for you.';

$_['text_payment_method'] = 'PAYMENT METHOD';