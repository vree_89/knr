        <div class="the-blog-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="the-blog-title">
                           <h3>FROM THE BLOG</h3>
                        </div>
                    </div>
                </div>
                <!--single blog-from sart-->
                <div class="row">
                    <div class="blog-from-list">

                        <?php foreach ($blogs as $blog) { ?>
                        <div class="col-md-4">
                            <div class="single-blog-from">
                                <div class="blog-from-block">
                                    <a href="#"><img src="<?php echo $blog['image']; ?>" alt=""></a>
                                    <div class="blog-img-block">
                                        <h3><a href="#"><?php echo $blog['title']; ?></a></h3>
                                       <div class="blog-img-info">
                                          <a href="#"><i class="fa fa-calendar"></i><?php echo date('Y-m-d',strtotime($blog['input_date'])); ?></a>
                                           <a href="#"><i class="fa fa-comment-o"></i>0 comment</a>
                                       </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <?php } ?>





                        <?php
                        /*
                        <div class="col-md-4">
                            <div class="single-blog-from">
                                <div class="blog-from-block">
                                    <a href="#"><img src="./image/catalog/demo/blog-2.jpg" alt=""></a>
                                    <div class="blog-img-block">
                                        <h3><a href="#">TRANDING FOR 2016</a></h3>
                                       <div class="blog-img-info">
                                          <a href="#"><i class="fa fa-calendar"></i>02-11-2015</a>
                                           <a href="#"><i class="fa fa-comment-o"></i>0 comment</a>
                                       </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-blog-from">
                                <div class="blog-from-block">
                                    <a href="#"><img src="./image/catalog/demo/blog-3.jpg" alt=""></a>
                                    <div class="blog-img-block">
                                        <h3><a href="#">TRANDING FOR 2016</a></h3>
                                       <div class="blog-img-info">
                                          <a href="#"><i class="fa fa-calendar"></i>02-11-2015</a>
                                           <a href="#"><i class="fa fa-comment-o"></i>0 comment</a>
                                       </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-blog-from">
                                <div class="blog-from-block">
                                    <a href="#"><img src="./image/catalog/demo/blog-1.jpg" alt=""></a>
                                    <div class="blog-img-block">
                                        <h3><a href="#">TRANDING FOR 2016</a></h3>
                                       <div class="blog-img-info">
                                          <a href="#"><i class="fa fa-calendar"></i>02-11-2015</a>
                                           <a href="#"><i class="fa fa-comment-o"></i>0 comment</a>
                                       </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-blog-from">
                                <div class="blog-from-block">
                                    <a href="#"><img src="./image/catalog/demo/blog-2.jpg" alt=""></a>
                                    <div class="blog-img-block">
                                        <h3><a href="#">TRANDING FOR 2016</a></h3>
                                       <div class="blog-img-info">
                                          <a href="#"><i class="fa fa-calendar"></i>02-11-2015</a>
                                           <a href="#"><i class="fa fa-comment-o"></i>0 comment</a>
                                       </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-blog-from">
                                <div class="blog-from-block">
                                    <a href="#"><img src="./image/catalog/demo/blog-3.jpg" alt=""></a>
                                    <div class="blog-img-block">
                                        <h3><a href="#">TRANDING FOR 2016</a></h3>
                                       <div class="blog-img-info">
                                          <a href="#"><i class="fa fa-calendar"></i>02-11-2015</a>
                                           <a href="#"><i class="fa fa-comment-o"></i>0 comment</a>
                                       </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        */
                        ?>                          








                    </div>
                </div>
                <!--single blog-from end-->
            </div>
        </div>
<?php
/*
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-bestseller" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-bestseller" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-limit"><?php echo $entry_limit; ?></label>
            <div class="col-sm-10">
              <input type="text" name="limit" value="<?php echo $limit; ?>" placeholder="<?php echo $entry_limit; ?>" id="input-limit" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-width"><?php echo $entry_width; ?></label>
            <div class="col-sm-10">
              <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
              <?php if ($error_width) { ?>
              <div class="text-danger"><?php echo $error_width; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-height"><?php echo $entry_height; ?></label>
            <div class="col-sm-10">
              <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
              <?php if ($error_height) { ?>
              <div class="text-danger"><?php echo $error_height; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
*/
?>