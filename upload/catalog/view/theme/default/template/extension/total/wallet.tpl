<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#collapse-wallet" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $heading_title; ?> <i class="fa fa-caret-down"></i></a></h4>
  </div>
  <div id="collapse-wallet" class="panel-collapse collapse">
    <div class="panel-body">
      <label class="col-sm-2 control-label" for="input-wallet"><?php echo $entry_wallet; ?></label>
      <div class="input-group">
        <input type="text" name="wallet" value="<?php echo $wallet; ?>" placeholder="<?php echo $entry_wallet; ?>" id="input-wallet" class="form-control" />
        <span class="input-group-btn">
        <input type="button" value="<?php echo $button_wallet; ?>" id="button-wallet" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary" />
        </span></div>
      <script type="text/javascript"><!--
$('#button-wallet').on('click', function() {
	$.ajax({
		url: 'index.php?route=extension/total/wallet/wallet',
		type: 'post',
		data: 'wallet=' + encodeURIComponent($('input[name=\'wallet\']').val()),
		dataType: 'json',
		beforeSend: function() {
			$('#button-wallet').button('loading');
		},
		complete: function() {
			$('#button-wallet').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}

			if (json['redirect']) {
				location = json['redirect'];
			}
		}
	});
});
//--></script>
    </div>
  </div>
</div>
