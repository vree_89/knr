<?php $i=0; ?>
<?php foreach ($banners as $banner) { ?>

<?php
$i++;
$array[$i]['image'] = $banner['image'];
$array[$i]['title'] = $banner['title'];
$array[$i]['link'] = $banner['link'];
?>

<?php } 
?>


<div class="banner-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-9 col-lg-9">
                <div class="banner-image-area">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="litele-img">
                                <a href="<?php echo $array[1]['link']!='' ? $array[1]['link'] : ''?>"><img src="<?php echo $array[1]['image']?>" alt=""></a>
                                <?php
                                if($array[1]['title']!=""){
                                  ?>
                                <div class="li-banner-new">
                                    <span><?php echo $array[1]['title']?></span>
                                </div>
                                <?php
                                }
                                ?>
                            </div>                                      
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="large-img">
                                <a href="<?php echo $array[2]['link']!='' ? $array[2]['link'] : ''?>"><img src="<?php echo $array[2]['image']?>" alt=""></a>
                                <?php
                                if($array[2]['title']!=""){
                                  ?>
                                <div class="li-banner-new">
                                    <span><?php echo $array[2]['title']?></span>
                                </div>
                                <?php
                                }
                                ?>
                            </div>   
                       </div>
                    </div>
                    <div class="row banner-bottom">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="large-img">
                                <a href="<?php echo $array[3]['link']!='' ? $array[3]['link'] : ''?>"><img src="<?php echo $array[3]['image']?>" alt=""></a>
                                <?php
                                if($array[3]['title']!=""){
                                  ?>
                                <div class="lr-banner-new">
                                    <span><?php echo $array[3]['title']?></span>
                                </div>
                                <?php
                                }
                                ?>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">       
                            <div class="litele-img">
                                <a href="<?php echo $array[4]['link']!='' ? $array[4]['link'] : ''?>"><img src="<?php echo $array[4]['image']?>" alt=""></a>

                                <?php
                                if($array[4]['title']!=""){
                                  ?>
                                <div class="lr-banner-new">
                                    <span><?php echo $array[4]['title']?></span>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-xs-12 hidden-sm col-md-3">
                <div class="banner-another-img">
                    <a href="<?php echo $array[5]['link']!='' ? $array[5]['link'] : ''?>"><img src="<?php echo $array[5]['image']?>" alt=""></a>
                    <?php
                    if($array[5]['title']!=""){
                      ?>
                    <div class="banner-a-img-block">
                        <h2><?php echo $array[5]['title']?></h2>
                        <a href="#">SHOP NOW</a> 
                    </div> 
                      <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>