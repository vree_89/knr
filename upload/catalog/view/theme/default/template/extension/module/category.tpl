<div class="list-group">
  <?php foreach ($categories as $category) { ?>
    <?php if ($category['category_id'] == $category_id) { ?>
      <a href="<?php echo $category['href']; ?>" class="list-group-item active"><?php echo $category['name']; ?></a>
 
            <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>

            <?php foreach ($children as $child) { ?>
            <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>

            <?php if(isset($child['children_lv3']) && count($child['children_lv3'])>0){ 
            //var_dump($child['children_lv3']);
            ?>
            <?php foreach ($child['children_lv3'] as $child_lv3) { ?>
            <a href="<?php echo $child_lv3['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-<?php echo $child_lv3['name']; ?></a>
            <?php  } ?>
            <?php } ?>                                            
            <?php } ?>
            <?php } ?>
      <?php /*if ($category['children']) { ?>
        <?php foreach ($category['children'] as $child) { ?>
        <?php if ($child['category_id'] == $child_id) { ?>
        <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
        <?php } else { ?>
        <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
        <?php } ?>
        <?php } ?>
      <?php }*/ ?>
    <?php } else { ?>
    <a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['name']; ?></a>
    <?php } ?>
  <?php } ?>
</div>
