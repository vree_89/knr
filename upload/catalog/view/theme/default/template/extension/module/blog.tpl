            <div class="container best-seller-area another">
                <div class="row">
                    <div class="col-md-12">
                        <div class="the-blog-title">
                           <h3 style="cursor:pointer;" onclick="window.location=''">Blog</h3>
                        </div>
                    </div>
                </div>    


                <h3>Reviews</h3>
                <?php
                if($reviews){
                  ?>
                  <div class="row" style="padding:5px 0px;">
                          <?php foreach ($reviews as $review) { ?>
                          <div class="col-md-4">
                              <div class="single-blog-from">
                                  <div class="blog-from-block">
                                      <a href="<?php echo $review['href']; ?>"><img src="<?php echo $review['image']; ?>" alt=""></a>
                                      <div class="blog-img-block">
                                          <h3><a href="<?php echo $review['href']; ?>"><?php echo $review['name']; ?></a></h3>
                                         <div class="blog-img-info">
                                            <a href="<?php echo $review['href']; ?>"><i class="fa fa-calendar"></i><?php echo $review['date_modified']; ?></a>
                                             <!-- <a href="#"><i class="fa fa-comment-o"></i>0 comment</a> -->
                                         </div>
                                      </div>
                                  </div>    
                              </div>
                          </div>
                          <?php } ?>          
                  </div>
                  <?php

                }
                else{
                  echo  'No reviews';
                }

                ?>

                <h3>Videos</h3>
                <?php
                if($videos){
                  ?>
                  <div class="row" style="padding:5px 0px;">
                          <?php foreach ($videos as $video) { ?>
                          <div class="col-md-4">
                              <div class="single-blog-from">
                                  <div class="blog-from-block">
                                      <a href="<?php echo $video['href']; ?>"><img src="<?php echo $video['image']; ?>" alt=""></a>
                                      <div class="blog-img-block">
                                          <h3><a href="<?php echo $video['href']; ?>"><?php echo $video['name']; ?></a></h3>
                                         <div class="blog-img-info">
                                            <a href="<?php echo $video['href']; ?>"><i class="fa fa-calendar"></i><?php echo $video['date_modified']; ?></a>
                                             <!-- <a href="#"><i class="fa fa-comment-o"></i>0 comment</a> -->
                                         </div>
                                      </div>
                                  </div>    
                              </div>
                          </div>
                          <?php } ?>          
                  </div>
                  <?php
                }
                else{
                  echo 'No videos';
                }
                ?>


            </div>