        <div class="our-brand-area owl-indicator">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="brand-title">
                           <h3>Brand</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="brand-list">

                        <?php 
                        // var_dump($banners);
                        // exit;
                        ?>
                        <?php foreach ($banners as $banner) { ?>
                        <div class="col-md-12">
                            <div class="single-brand">
                                <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt=""></a>
                            </div>    
                        </div> 
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>