<div class="container best-seller-area another">
<div class="row">
<div class="col-md-12">
<div class="the-blog-title">
<h3>Best Seller</h3>
</div>
</div>
</div>    
<div class="row" style="padding:5px 0px;">
<?php foreach ($products as $product) { ?>
<div class="col-md-3" style="margin-bottom:15px;border:none">
<div class="single-product" style="border:none;">
<div class="product-img" style="border:none;">
<a class="" href="<?php echo $product['href']; ?>">
<img class="primary-img" src="<?php echo $product['thumb']; ?>" alt="">
<?php
if($product['special']){
?>
<div class="special"></div>            
<?php  
}
if($product['quantity']<=0){
?>
<div class="out-of-stock"><?php echo $product['stock_status']?></div>            
<?php
}
else if($product['is_preorder']){
?>
<div class="out-of-stock"><span style="font-size:smaller;">Date Available:<?php echo $product['date_available']?></span></div>
<?php
}
?>       
</a>
</div>
<div class="product-block-text">
<div style="height:37px;width:100%;padding:0 2%;display:table;text-align:left;" >
  <div style="line-height:10px;vertical-align:middle;display: table-cell;" >
    <div style="padding-bottom:2px;" class="product-manufacturer"><b ><?php echo $product['manufacturer']; ?></b></div>
    <div class="product-name"><?php echo ucwords(strtolower($product['name'])); ?></div>
  </div>
</div>
<?php if ($product['rating']) { ?>
<div class="rating" style="display:inline-block;margin-bottom:0;">
<?php for ($i = 1; $i <= 5; $i++) { ?>
<?php if ($product['rating'] < $i) { ?>
<span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star-o fa-stack-2x"></i></span>
<?php } else { ?>
<span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star fa-stack-2x" style="color:#FF3399 ;"></i><i class="fa fa-star-o fa-stack-2x" style="color:#FF3399 ;"></i></span>
<?php } ?>
<?php } ?>
</div>
<?php 
}
else{
?>
<div class="rating" style="display:inline-block;margin-bottom:0;">
<?php for ($i = 1; $i <= 5; $i++) { ?>
<span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star-o fa-stack-2x"></i></span>
<?php } ?>
</div>
<?php
}
?>
<div class="clearfix"></div>
<div class="clearfix"></div>
<?php if ($product['price']) { ?>
<div style="min-height:40px;">
    <?php
    if($product['special']){
      ?>
      <div class="potongan_persen">
      <?php
      if($product['discount']){
      ?><?php echo $product['discount'];?>%<?php
      }
      ?>
      </div>
      <div class="harga" style="text-align:left;">
      <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
      <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $product['price_before']; ?></span>
      </span>                                    
      <span class="final-price"><?php echo $product['special']; ?></span>

      </div>
      <?php
    }
    else{
      ?>
      <div class="potongan_persen">
      <?php
      if($product['discount']){
      ?><?php echo $product['discount'];?>%<?php
      }
      ?>
      </div>
      <div class="harga" style="text-align:left;">
      <?php
      if($product['discount']){
      ?>
      <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
      <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $product['price_before']; ?></span>
      </span>                                    
      <span class="final-price"><?php echo $product['price']; ?></span>
      <?php
      }
      else{
      ?>
      <span class="final-price"><?php echo $product['price']; ?></span>
      <?php
      }
      ?>
      </div>
      <div class="clearfix"></div>
      <?php

    }

    ?>
</div>
<div class="clearfix"></div>
<?php } ?>   
<div class="clearfix"></div>
<?php

if($product['quantity']<=0){
  ?>
<a class="p-b-t-a-c" style="cursor:pointer;" style="font-size:80%;padding:0;margin:0;padding:5px 10px;margin-bottom:10px;margin-top:0;" onclick="return false;">add to cart</a>
  <?php 
  

  
}
else if($product['is_preorder']){
  ?>

<a class="p-b-t-a-c" style="cursor:pointer;" style="font-size:80%;padding:0;margin:0;padding:5px 10px;margin-bottom:10px;margin-top:0;" onclick="cart.add('<?php echo $product['product_id']; ?>', 1,'bestseller',this);">Pre Order</a>

  <?php
}
else{
?>
<a class="p-b-t-a-c" style="cursor:pointer;" style="font-size:80%;padding:0;margin:0;padding:5px 10px;margin-bottom:10px;margin-top:0;" onclick="cart.add('<?php echo $product['product_id']; ?>', 1,'bestseller',this);">add to cart</a>
<?php  
}
?>
</div>
</div>   
</div>
<?php } ?>         
</div>
	<div class="pull-right"><a href="<?php echo $url?>">View All >></a></div>
</div>
<style type="text/css">
.out-of-stock{
position: absolute;
left: 0;
top:50%;
float: left;
width: 100%;
//background: rgba(0,0,0,.5);
background: rgba(255,020,147,.5);
padding: 15px 0px;
text-align: center;
font-size: 150%;
color:white;
font-weight: bold;

}
.disc{
position: absolute;
right: 0;
top:0;
float: left;
background: rgba(255,020,147,.5);
background: rgba(255,0,0,0.3);
padding: 15px 0px;
text-align: center;
font-size: 200%;
color:white;
font-weight: bold;
width: 50px;
height: 50px;

}   

.alert {
position: absolute;
bottom: 0px;
left: 20px;
right: 20px;
}

.potongan_persen{
//background: red;
text-align: center;
padding: 10px 5px;
min-height: 40px;
float: left;
width: 30%;
color: #ffcce6;
color: #FF3399;
font-weight: bold;
font-size: 245%;

}
.harga{
//background: green;
text-align: center;
padding: 10px 5px;
min-height: 40px;
float: left;
width: 70%;
line-height: 10px;
padding: 0;
margin:0;
}
.final-price{
font-weight: bold;
font-size: 150%;
color: black;
}
.alert-success{
  background: #ffc2e3;color:#fc3a99;
}
</style>



