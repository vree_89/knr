<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        <!-- favicon
        ============================================ -->        
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        
        <!-- Google Fonts
        ============================================ -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,500,600,700,100' rel='stylesheet' type='text/css'>
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" /> 
 
        





<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js"></script>



    </head>
<body class="<?php echo $class; ?>"> 


        <div class="header-area Home2">
            <div class="header-top supertop">
                <div class="container" >
                    
                    <div class="row" >
                        <div class="col-xs-12 col-sm-4 col-md-4" >
                            <img src="<?php echo $topimage?>" class="img-responsive">
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4"><?php echo $search; ?></div>
                        <div class="col-xs-12 col-sm-4 col-md-4 ">
                            <div class="row">
                                <div class="col-xs-12 hidden-sm hidden-xs" style="padding-top:2px;">
                                    <?php //echo $currency; ?>
                                    <?php echo $language; ?>
                                    <?php echo $location?>                    
                                </div>
                                <div class="col-xs-12 hidden-md hidden-lg text-right">
                                    <?php echo $text_location?> : <?php echo $text_location?> <a href="<?php echo $change;?>" style="font-size:smaller;color:red;"><?php echo $text_change_location?></a>
                                    <?php
                                    ?>
                                    &nbsp;
                                    <?php echo $text_language?> :
                                    <?php foreach ($languages as $language) { ?>
                                    <a class="language-select" name="<?php echo $language['code']; ?>" style="cursor:pointer;"><img src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" <?php echo $code_language==$language['code'] ? 'style="border:1px solid white;"' : '' ?>/></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            
                            


                            <?php if ($logged) { ?>
                            <div style="float:right;margin-left:10px;"><a href="<?php echo $logout; ?>" ><?php echo $text_logout; ?></a></div>
                            <div style="float:right;margin-left:10px;"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></div>
                            <?php } ?>
                            <?php if (!$logged) { ?>
                            <div style="float:right;margin-left:10px;"><a href="<?php echo $register; ?>" ><?php echo $text_register; ?></a></div>
                            <div style="float:right;margin-left:10px;"><a href="<?php echo $login; ?>" ><?php echo $text_login; ?></a></div>
                            <?php } ?>
                            <div style="float:right;margin-left:10px;"><a href="<?php echo $shopping_cart?>">Cart</a></div>
                            <?php
                            if($logged){
                            ?>
                            <div style="float:right;margin-left:10px;"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></div>                            
                            <?php
                            }
                            ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>

            </div>

            <div class="header-top">
            <div class="container">
            <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="cl-h-t-info">
            </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="classic-logo">
            <a href="<?php echo $home; ?>"><img src="<?php echo $logo;?>" alt=""></a>
            </div>      
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">

            </div>
            </div>
            </div>
            </div>




            <div class="header-bottom">
            <div class="container">
            <div class="row">
                <div class="hidden-xs col-sm-12 col-md-12 text-center" >
                    <div class="header-menu home2">                                                  
                    <ul id="nav">
                    <li style="margin-right:10px;"><a href="#" style=""><i class="fa fa-home fa-2x" aria-hidden="true" style=""></i> </a></li>
                    <?php foreach ($categories as $category) { ?>

                    <?php if ($category['children']) { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>                  
                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?> 

                    <?php 
                    $countChild = count($children);
                    $col = null;
                    if((int) $countChild%2==0){ 
                    $col = 2;
                    ?>
                    <div class="mega-menu-2">
                    <?php 
                    } 
                    else{ 
                    $col = 3;
                    ?>
                    <div class="mega-menu-3">
                    <?php 
                    } 
                    ?>

                    <?php $i=0; foreach ($children as $child) { $i++; ?>
                    <span style="margin-bottom:15px;">
                    <?php
                    if(isset($child['children_lv3']) && count($child['children_lv3'])>0){ // ada anak ke 3
                    if(isset($child['image'])){
                    ?>
                    <!-- <img src="<?php //echo $child['image']?>"> -->
                    <b style="font-size:larger;"><?php echo $child['name']; ?></b>
                    <?php
                    }
                    else{
                    ?>
                    <b style="font-size:larger;"><?php echo $child['name']; ?></b>
                    <?php    
                    }
                    ?>                                
                    <?php
                    }
                    else{ //ga ada anak ke 3
                    if(isset($child['image'])){
                    ?>

                    <b><a href="<?php echo $child['href']; ?>" ><?php echo $child['name']; ?></a></b>
                    <?php
                    }
                    else{
                    ?>
                    <b><a href="<?php echo $child['href']; ?>" ><?php echo $child['name']; ?></a></b>
                    <?php
                    }
                    ?>
                    <?php
                    }


                    ?>
                    <?php if(isset($child['children_lv3']) && count($child['children_lv3'])>0){ 
                    ?>
                    <?php foreach ($child['children_lv3'] as $child_lv3) { ?>
                    <a href="<?php echo $child_lv3['href']; ?>" style="padding:0;margin:0;"><?php echo $child_lv3['name']; ?></a>
                    <?php  } ?>
                    <?php } ?>                                            
                    </span>

                    <?php } ?>
                    <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <?php } ?>

                    </li>
                    <?php } else { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                    <?php } ?>
                    <?php } ?>
                    </ul>
                    </div>
                </div>
            <?php
            /*
            <div class="col-xs-12 col-sm-3 col-md-3">
            <?php echo $search; ?>
            </div>
            */
            ?>
            </div>
            </div>
            </div>





            <!--mobile menu satrt-->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <nav id="dropdown">


                <ul>
                <li><a href="#" style="">Home</a></li>
                <?php foreach ($categories as $category) { ?>
                <?php if ($category['children']) { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>                   
                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?> 
<ul>                    
                        
                        <?php foreach ($children as $child) { ?>
                            <span>
                            <div class="subcategory"><?php echo $child['name']; ?></div>
                            <?php if(isset($child['children_lv3']) && count($child['children_lv3'])>0){ 
                            ?>
                            <?php foreach ($child['children_lv3'] as $child_lv3) { ?>
                            <a href="<?php echo $child_lv3['href']; ?>"><?php echo $child_lv3['name']; ?></a>
                            <?php  } ?>
                            <?php } ?>                                            
                            </span>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                    
                    </li>
                <?php } else { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } ?>
                <?php } ?>
                </ul>


                            </nav>
                        </div>
                    </div>
                </div>    
            </div>       
            <div class="clearfix"></div> 
        </div>


<style type="text/css">
select.select_location{
background: none;
}
select.select_location option {

background: black;
color: #fff;
text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
}

</style>


<style type="text/css">
    .supertop{
        background: #ebf6ef;
        background: black;  
        color:white;
        
        
    }
    .supertop span.text{
        font-size: smaller;
        padding:0;
        margin:0;
    }
    .supertop a{
        color: black;
        color:white;
    }
    ul#nav>li>a{
        font-weight: bold;
        //background: yellow;
        //font-weight: bold;
        //color:blue;


    }

    ul#nav>li>.mega-menu-3>span>a{
        padding-left: 5px;
        font-size: inherit;;
    }
    .header-info li.my-cart::before {
        background: #ba9043 none repeat scroll 0 0;
        border-radius: 50%;
        color: #fff;
        content: "<?php echo $total_items?>";
        font-size: 12px;
        height: 15px;
        line-height: 13px;
        position: absolute;
        right: 8px;
        text-align: center;
        top: 33%;
        width: 15px;
    }
    a#scrollUp{
        bottom:0;
        right: 1%;
    }
    body{
        font-family: 'Roboto', sans-serif;
    }
</style>



