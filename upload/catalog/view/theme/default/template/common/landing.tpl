<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KR House - Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="catalog/view/theme/default/agency/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="catalog/view/theme/default/agency/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <!-- <link href="catalog/view/theme/default/agency/css/agency.css" rel="stylesheet"> -->

  </head>

  <body id="landing-bg">

      <div class="container landing-container">
          <div class="landing_logo"><img src="<?php echo $logo?>"></div>
          <div class="landing_header"><?php echo $text_choose;?></div>
          <div class="row landing_region_list" >
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="row region-list">
                    <div class="col-sm-4 text-center">
                      <?php
                      if($id_flag['enable']){
                        ?>
                        <a class="region-link" href="<?php echo $id_flag['href']?>"><img src="<?php echo $id_flag['icon']?>" class="img-fluid <?php echo $id_flag['enable']==1 ? '' : 'disabled_flag'; ?>"><br/><span><?php echo $id_flag['title']?></span></a>
                        <?php
                      }
                      else{
                        ?>
                        <a class="region-link disabled_flag" href="#" onclick="return false;"><img src="<?php echo $id_flag['icon']?>" class="img-fluid "><br/><span>(Coming soon)</span></a>
                        <?php
                      }
                      ?>





                    </div>


                    <div class="col-sm-4 text-center">
                      <?php
                      if($my_flag['enable']){
                        ?>
                        <a class="region-link" href="<?php echo $my_flag['href']?>"><img src="<?php echo $my_flag['icon']?>" class="img-fluid <?php echo $my_flag['enable']==1 ? '' : 'disabled_flag'; ?>"><br/><span><?php echo $my_flag['title']?></span></a>
                        <?php
                      }
                      else{
                        ?>
                        <a class="region-link disabled_flag" href="#" onclick="return false;"><img src="<?php echo $my_flag['icon']?>" class="img-fluid "><br/><span>(Coming soon)</span></a>
                        <?php
                      }
                      ?>


                      

                    </div>
                    <div class="col-sm-4 text-center">
                      <?php
                      if($sg_flag['enable']){
                        ?>
                        <a class="region-link" href="<?php echo $sg_flag['href']?>"><img src="<?php echo $sg_flag['icon']?>" class="img-fluid <?php echo $sg_flag['enable']==1 ? '' : 'disabled_flag'; ?>"><br/><span><?php echo $sg_flag['title']?></span></a>
                        <?php
                      }
                      else{
                        ?>
                        <a class="region-link disabled_flag" href="#" onclick="return false;"><img src="<?php echo $sg_flag['icon']?>" class="img-fluid "><br/><span>(Coming soon)</span></a>
                        <?php
                      }
                      ?>
                      

                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="clearfix"></div>
          </div>
          <div class="landing_footer">
            <p>"House to Enhance your Beauty"</p>
          </div>
      </div>

    <!-- Navigation -->

    <!-- Header -->
<!--     <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in"><img src="<?php //echo $logo?>"></div>
          <div class="intro-heading">Choose your Location</div>
          <div class="row" style="margin-bottom:100px;">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="row region-list">
                    <div class="col-sm-4"><a href="<?php //echo $href_id?>"><img src="<?php //echo $id_flag?>" class="img-fluid"><br/>Indonesia</a></div>
                    <div class="col-sm-4"><a href="<?php //echo $href_my?>"><img src="<?php //echo $my_flag?>" class="img-fluid"><br/>Malaysia</a></div>
                    <div class="col-sm-4"><a href="<?php //echo $href_sg?>"><img src="<?php //echo $sg_flag?>" class="img-fluid"><br/>Singapore</a></div>
                </div>
            </div>
            <div class="col-md-4"></div>
            <div class="clearfix"></div>
          </div>
          <div class=" text-center" style="text-align:center;font-style:italic;">
            <p>"House to Enchant your Beauty"</p>
          </div>
          
        </div>
      </div>
    </header>
 -->
    <!-- Services -->

    <!-- Portfolio Grid -->


    <!-- About -->

    <!-- Team -->


    <!-- Clients -->

    <!-- Contact -->


    <!-- Footer -->


    <!-- Portfolio Modals -->


    <!-- Bootstrap core JavaScript -->
    <script src="catalog/view/theme/default/agency/vendor/jquery/jquery.min.js"></script>
    <script src="catalog/view/theme/default/agency/vendor/popper/popper.min.js"></script>
    <script src="catalog/view/theme/default/agency/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="catalog/view/theme/default/agency/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <!--script src="catalog/view/theme/default/agency/js/jqBootstrapValidation.js"></script-->
    <!--script src="catalog/view/theme/default/agency/js/contact_me.js"></script-->

    <!-- Custom scripts for this template -->
    <!--script src="catalog/view/theme/default/agency/js/agency.min.js"></script-->

  </body>

</html>
<style type="text/css">
a.disabled_flag>span{
  //opacity: 0.1;
}
a.disabled_flag:hover{
  text-decoration: none;
}
.region-list span{
  font-size: normal;
}
body{
  height: 100%;
  width: 100%;
  margin:0;
}
#landing-bg{
    background: #222222;
    height: 100%;
}
.landing-container{
    margin-top: 5%;
    text-align: center;
    color: white;
}
.landing-container>.landing_logo{
    margin-bottom: 10%;
}

.landing-container>.landing_header{
    margin-bottom: 1%;
    font-size: xx-large;
    text-transform: uppercase;
}
.landing-container .landing_header, .landing-container a.region-link{
    color: white;
    text-transform: uppercase;
}
.landing-container>.landing_region_list{
    margin-bottom: 15%;
}
.landing-container>.landing_footer{
  font-style: italic;
}
</style>