        <div class="footer-area">
            <div class="footer-list">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="single-footer classic">
                                <div class="logo">
                                    <a href="#"><img src="image/logowhite.png" alt=""></a>
                                </div>
                                <div class="f-classic-text">
                                    <p class="text-justify" style="line-height:100%;">KR House adalah sebuah toko online yang menyediakan kosmetik dengan brand-brand ternama dari Korea. Berdiri sejak awal tahun 2017, KR House berkomitmen untuk memberikan produk-produk terbaik dengan harga yang tepat, didukung dengan jaminan ke-asli-an produk yang langsung dikirim dari Korea. KR House tentunya akan menjembatani anda untuk mendapatkan produk dengan lebih efektif dan efisien!</p>
                                </div>
                                <div class="classic-adress">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-phone"></i></a><div style="padding-top:7.5px;padding-bottom:7.5px;">0812 9819 3073 (WA only)</div></li>
                                        <li><a href="#" class="line"><img src="image/line.png" alt="" class="img-responsive'"></a> <div style="padding-top:7.5px;padding-bottom:7.5px;">@KRHouse</div></li>
                                        <li><a href="#"><i class="fa fa-envelope"></i></a><div style="padding-top:7.5px;padding-bottom:7.5px;">krhousebeauty@gmail.com</div></li>
                                    </ul>
                                </div>
                            </div>




                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-xs-4 col-sd-4">
                                    <div class="single-footer conatact-us">
                                        <h3 style="color:#ffffff;"><?php echo $text_social?></h3>
                                        <div class="account-list">
                                            <ul>
                                                <li><a href="<?php echo $config_instagramid; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
                                                <li><a href="<?php echo $config_facebookid; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                                                <?php /*<li><a href="#"><img src="image/blog.png" alt=""> Blog</a></li>*/?>
                                                <li><a href="<?php echo $config_youtubeid; ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i> Youtube</a></li>
                                                <?php /*<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</a></li>*/ ?>                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sd-4">
                                    <div class="single-footer conatact-us">
                                        <h3 style="color:#ffffff;"><?php echo $text_information?></h3>
                                        <div class="account-list">
                                            <ul>

      <?php if ($informations) { ?>
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
      <?php } ?>
                                                
                                                
                                                
                                                <?php
                                                /*<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>*/
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-4 col-sd-4">
                                    <div class="single-footer conatact-us">
                                        <h3 style="color:#ffffff;"><?php echo $text_service?></h3>
                                        <div class="account-list">
                                            <ul>
                                                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                                <?php /*<li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>*/ ?>
                                                <li><a href="<?php echo $loyalty; ?>"><?php echo $text_loyalty; ?></a></li>
                                                <?php /*<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>*/?>
                                                <?php /*<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>*/?>
                                                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                                                <!-- <li><a href="#">Live Chat</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                        </div>
                                              


                    </div>    
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="footer-copyright">
                                <p>
                                    Copyright © <?php echo date('Y')?>
                                    KR House
                                    All Rights Reserved
                                </p>
                            </div>    
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
  
                        </div>
                    </div>    
                </div>
            </div>
        </div>

     
        <!-- price-slider JS
        ============================================ -->        
        <!--script src="catalog/view/javascript/jquery-price-slider.js"></script-->
        
        <!-- bootstrap JS
        ============================================ -->        
        <!--script src="catalog/view/javascript/bootstrap.min.js"></script-->
        <!-- wow JS
        ============================================ -->        
        <!--script src="catalog/view/javascript/wow.min.js"></script-->
        
        <!--script src="catalog/view/theme/default/stylesheet/lib/js/jquery.nivo.slider.js" type="text/javascript"></script-->
        <!--script src="catalog/view/theme/default/stylesheet/lib/home.js" type="text/javascript"></script-->  
            
        <!--script src="catalog/view/javascript/jquery.meanmenu.js"></script-->
        
        
        <!--script src="catalog/view/javascript/jquery.mixitup.min.js"></script-->
        
        <!--script src="catalog/view/javascript/jquery.collapse.js"></script-->
        <!--script src="catalog/view/javascript/jquery.scrollUp.min.js"></script-->
        
        <!--script src="catalog/view/javascript/plugins.js"></script-->
        
        <!--script src="catalog/view/javascript/main.js"></script-->
        <script type="text/javascript">
(function ($) {
 "use strict";

    $("a.line").hover(function(){

        $(this).find("img").attr('src','image/line-hover.png');
    },function(){
        $(this).find("img").attr('src','image/line.png');
    }
    );
    //---------------------------------------------
    //Nivo slider
    //---------------------------------------------
       $('#ensign-nivoslider').nivoSlider({
        effect: 'slideInLeft',
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 500,
        pauseTime: 5000,
        startSlide: 0,
        directionNav: true,
        controlNavThumbs: false,
        pauseOnHover: false,
        manualAdvance: false
       });

       
       
})(jQuery); 
</script>

<!-- <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=68050304"></script> -->

</body></html>