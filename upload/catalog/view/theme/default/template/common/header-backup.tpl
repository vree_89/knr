<!doctype html>
<html class="no-js" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        <!-- favicon
        ============================================ -->        
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        
        <!-- Google Fonts
        ============================================ -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,500,600,700,100' rel='stylesheet' type='text/css'>  
        
        <!-- Bootstrap CSS
        ============================================ -->        
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/bootstrap.min.css">
        
        <!-- fontwesome CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/font-awesome.min.css">
        
        <!-- owl.carousel CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/owl.carousel.css">
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/owl.theme.css">
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/owl.transitions.css">
        
        <!-- nivo slider CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/lib/css/nivo-slider.css" type="text/css" />
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/lib/css/preview.css" type="text/css" media="screen" />
        
        <!-- animate CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/animate.css">
        
        <!-- meanmenu.min CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/meanmenu.min.css">
        
        <!-- jQuery-ui CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/jQuery-ui.css">
        
        <!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/normalize.css">
        
        <!-- main CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/main.css">
        
        <!-- style CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/style.css">
        
        <!-- responsive CSS
        ============================================ -->
        <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/responsive.css">
        
        <!-- modernizr JS
        ============================================ -->        
        <script src="catalog/view/javascript/vendor/modernizr-2.8.3.min.js"></script>

        <!-- jquery
        ============================================ -->        
        <script src="catalog/view/javascript/vendor/jquery-1.11.3.min.js"></script>
           <!-- owl.carousel JS
        ============================================ -->        
        <script src="catalog/view/javascript/owl.carousel.min.js"></script>

<!-- <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script> -->
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">

<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

    </head>
<body class="<?php echo $class; ?>"> 

<?php
/*
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div>
    </div>
  </div>
</header> 
*/
?>   
        <div class="header-area Home2">
            <div class="header-top">
            <div class="container">
            <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-4">
            <div class="cl-h-t-info">
            <?php echo $language; ?>
            <ul>
            <li><a href="<?php echo $account; ?>"><i class="fa fa-bars"></i> <?php echo $text_account; ?></a></li>
            <?php if ($logged) { ?>
            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            <?php } ?>
            </ul>
            </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4">
            <div class="classic-logo">
            <a href="<?php echo $home; ?>"><img src="image/logo.png" alt=""></a>
            </div>      
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="header-info c-l">
            <ul>
            <li class="my-cart">
            <a href="<?php echo $shopping_cart?>"><img src="image/cart.png" alt=""></a>
            <!--my cart content start-->
            </li>
            <?php if (!$logged) { ?>
            <li><a href="<?php echo $login; ?>" ><?php echo $text_login; ?></a> </li>
            <li><a href="<?php echo $register; ?>" ><?php echo $text_register; ?></a> </li>
            <?php } ?>
            <?php if ($logged) { ?>
            <!-- <li><a href="<?php //echo $account; ?>" ><?php //echo $text_account; ?></a></li> -->
            <?php } ?>
            <?php if ($logged) { ?>
            <li><a href="<?php echo $logout; ?>" ><?php echo $text_logout; ?></a></li>
            <?php } ?>
            <!-- <li><a href="my-account.html">login</a></li> -->
            </ul>
            </div>
            <?php //echo $cart; ?>
            </div>
            </div>
            </div>
            </div>
            <!--header-bottom-->
            <div class="container hidden-xs">
                <ul style="width:100%;margin:0;padding:0;">
                    <li style="display:inline;margin:0;padding:0;margin-right:15px;"><i class="fa fa-plane" aria-hidden="true"> GRATIS PENGIRIMAN</i></li>
                    <li style="display:inline;margin:0;padding:0;margin-right:15px;"><i class="fa fa-certificate" aria-hidden="true"> 100% ASLI</i></li>
                    <li style="display:inline;margin:0;padding:0;margin-right:15px;"><i class="fa fa-compress" aria-hidden="true"> 30 HARI PENGEMBALIAN</i></li>
                </ul>
            </div>
   

            <div class="header-bottom">
            <div class="container">
            <div class="row">
            <div class="hidden-xs col-sm-10 col-md-10" >
            <div class="header-menu home2">                                                  
                <ul id="nav">
                <li style="margin-right:10px;"><a href="#" style=""><i class="fa fa-home fa-2x" aria-hidden="true" style=""></i> </a></li>
                <?php foreach ($categories as $category) { ?>

                <?php if ($category['children']) { ?>
                    <?php
                    //echo count(array_chunk($category['children'], ceil(count($category['children']) / $category['column'])));
                    ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>                  
                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?> 
                        <?php  
                        //echo count($children);
                        //var_dump($children);
                        if(count($children)==4){
                        ?>
                            <div class="mega-menu-2">
                        <?php    
                        } 
                        else if(count($children)==3){
                            ?>
                            <div class="mega-menu-2">
                            <?php
                        }
                        else if(count($children)==2){
                            ?>
                            <div class="mega-menu-1">
                            <?php
                        }
                        else{ //for brand only
                            ?>
                            <div class="mega-menu-1">
                            <?php
                        }
                        ?>                       
                        
                        <?php foreach ($children as $child) { ?>
                            <span style="background:blue;margin:0;padding:0;">
                            <?php
                            if(isset($child['children_lv3']) && count($child['children_lv3'])>0){
                                if(isset($child['image'])){
                                    ?>
                                    <img src="<?php echo $child['image']?>">
                                    <?php
                                }
                                else{
                                ?>
                                    <div class="subcategory" style="background:#c0c0c0;padding:0;margin:0;"><?php echo $child['name']; ?></div>                                                             
                                <?php    
                                }
                                ?>                                
                                <?php
                            }
                            else{
                                if(isset($child['image'])){
                                    ?>
                                    <img title="<?php echo $child['name']; ?>" src="<?php echo $child['image']?>" style="cursor:pointer;" onclick="javascript:window.location='<?php echo $child['href']?>'">
                                    <?php
                                }
                                else{
                                    ?>
                                <div class="subcategory" style="background:#c0c0c0;padding:0;margin:0;"><a href="<?php echo $child['href']; ?>" ><?php echo $child['name']; ?></a></div>

                                    <?php

                                }
                                ?>
                                <?php
                            }


                            ?>
                            <?php if(isset($child['children_lv3']) && count($child['children_lv3'])>0){ 
                            ?>
                            <?php foreach ($child['children_lv3'] as $child_lv3) { ?>
                            <a href="<?php echo $child_lv3['href']; ?>" style="padding:0;margin:0;"><?php echo $child_lv3['name']; ?></a>
                            <?php  } ?>
                            <?php } ?>                                            
                            </span>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php } ?>
                    
                    </li>
                <?php } else { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } ?>
                <?php } ?>
                </ul>
            </div>
            </div>
            <div class="col-xs-12 col-sm-2 ol-md-2">
            <?php echo $search; ?>
            <!--                             
            <div class="h-search-btn">                                                  
            <div class="classic-search">
            <form action="#">
            <input type="text" placeholder="search">
            <button type="submit"><i class="fa fa-search"></i></button>
            </form>
            </div>    
            </div> 
            -->
            </div>
            </div>
            </div>
            </div>





            <!--mobile menu satrt-->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <nav id="dropdown">


                <ul>
                <li><a href="#" style="">Home</a></li>
                <?php foreach ($categories as $category) { ?>
                <?php if ($category['children']) { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>                   
                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?> 
<ul>                    
                        
                        <?php foreach ($children as $child) { ?>
                            <span>
                            <div class="subcategory"><?php echo $child['name']; ?></div>
                            <?php if(isset($child['children_lv3']) && count($child['children_lv3'])>0){ 
                            ?>
                            <?php foreach ($child['children_lv3'] as $child_lv3) { ?>
                            <a href="<?php echo $child_lv3['href']; ?>"><?php echo $child_lv3['name']; ?></a>
                            <?php  } ?>
                            <?php } ?>                                            
                            </span>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                    
                    </li>
                <?php } else { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } ?>
                <?php } ?>
                </ul>


                            </nav>
                        </div>
                    </div>
                </div>    
            </div>       
            <div class="clearfix"></div> 
        </div>
        <style type="text/css">
        ul#nav>li>a{
        font-weight: bold;
        //background: yellow;
        //font-weight: bold;
        //color:blue;

        }
        ul#nav>li>.mega-menu-3>span>a{
        padding-left: 5px;
        font-size: inherit;;
        }
        .header-info li.my-cart::before {
            background: #ba9043 none repeat scroll 0 0;
            border-radius: 50%;
            color: #fff;
            content: "<?php echo $total_items?>";
            font-size: 12px;
            height: 15px;
            line-height: 13px;
            position: absolute;
            right: 8px;
            text-align: center;
            top: 33%;
            width: 15px;
        }

        </style>

