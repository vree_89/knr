<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<div class="row">
  <div class="col-md-6 col-sm-6">
    <?php if ($payment_methods) { ?>
    <p><?php echo $text_payment_method; ?></p>
    <?php foreach ($payment_methods as $payment_method) { ?>
    <?php //echo $payment_method['code']?>
    <div class="radio">
      <label>
        <?php //if ($payment_method['code'] == $code || !$code) { ?>
        <?php /*if ($payment_method['code'] == $code) { ?>
        <?php $code = $payment_method['code']; ?>
        <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" class="radio-payment" />


        <?php } else { ?>
        <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" class="radio-payment" class="radio-payment" />
        <?php }

        */ ?>
        <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" class="radio-payment" class="radio-payment" />
        <?php echo $payment_method['title']; ?>
        <?php if ($payment_method['terms']) { ?>
        (<?php echo $payment_method['terms']; ?>)
        <?php } ?>
      </label>
    </div>
    <?php } ?>
    <?php } ?>

  </div>
  <div class="col-md-6 col-sm-6">
    <p><strong><?php echo $text_comments; ?></strong></p>
    <p>
      <textarea name="comment" rows="8" class="form-control"><?php echo $comment; ?></textarea>
    </p>
  </div>
  <div class="clearfix"></div>
</div>

<div class="clearfix"></div>
<div class="payment-wrapper"></div>
<div class="clearfix"></div>


<?php if ($text_agree) { ?>
<div class="buttons">
  <div class="pull-right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    &nbsp;
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" disabled="disabled" />
  </div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" disabled="disabled" />
  </div>
</div>
<?php } ?>



<?php /*if ($text_agree) { ?>
<div class="buttons">
  <div class="pull-right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    &nbsp;
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<?php }*/ ?>
