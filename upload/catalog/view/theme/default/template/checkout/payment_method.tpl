<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<div class="row">
  <!--<div class="col-md-6 col-sm-6">
    <div class="radio"><label><input type="radio" name="payment_method" value="bank_transfer" class="rd-payment-method radio-payment" />Doku Wallet</label></div>
    <div class="radio"><label><input type="radio" name="payment_method" value="alfa_group" class="rd-payment-method radio-payment"  />Alfamart</label></div>
    <div class="radio"><label><input type="radio" name="payment_method" value="doku_wallet" class="rd-payment-method radio-payment"  />ATM / Bank Transfer</label></div>
  </div>-->

<div class="payment-selector col-md-6">
<?php 
if ($payment_methods) {  
  $disabled = '';
  if($input_payment_total_num<=0){
    $disabled = 'disabled="disabled"';
  }
  foreach ($payment_methods as $payment_method) { 
    if ($payment_method['code'] == $code) { 
      $checked = 'checked="checked"'; 
      $code = $payment_method['code']; 
    } 
    else { 
      $checked = ''; 
    } ?>


     <div class="col-md-12">
      <?php
      /*
      <label for="<?php echo $payment_method['code']; ?>">
      <input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" <?php echo $checked; ?> <?php echo $disabled?> />
      <?php echo $payment_method['title']?>
      
    </label>

      */
      ?>
<input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>"  />
    <label class="drinkcard-payment" for="<?php echo $payment_method['code']; ?>" style="background-image:url(<?php echo $payment_method['image']; ?>)" title="<?php echo $payment_method['title']; ?>"></label>
      
     </div>



    <?php 
  }
} 
?>
</div>
<div class="col-md-6">
  <input type="hidden" name="payment_wallet" id="payment_wallet" value="<?php echo $input_payment_wallet_num?>">
  <input type="hidden" name="payment_total" id="payment_total" value="<?php echo $input_payment_total_num?>">
  <table class="table table-bordered">
    <tr>
      <td>Total</td>
      <td><?php echo $order_total?></td>
    </tr>
    <tr>
      <td>Use Wallet</td>
      <td><?php echo $input_payment_wallet?></td>
    </tr>
    <tr>
      <td>Total Payment</td>
      <td><?php echo $input_payment_total?></td>
    </tr>
  </table>

</div>
<div class="clearfix"></div>


  <?php
  /*
  <div class="col-md-6 col-sm-6">
    <p><strong><?php echo $text_comments; ?></strong></p>
    <p>
      <textarea name="comment" rows="8" class="form-control"><?php echo $comment; ?></textarea>
    </p>
  </div>
  */
  ?>
  <div class="clearfix"></div>
</div>
<div class="msg-additional-charge"><?php echo $text_additional_fee?></div>

<div class="clearfix"></div>
<div class="payment-wrapper"></div>
<div class="clearfix"></div>


<?php 
if ($text_agree) { 
?>
<div class="row" >
  <div class="col-md-6 col-md-offset-6" >
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    <?php echo $text_agree; ?>
    &nbsp;
    <?php
  if($logged){
    //echo "a";
    ?>
    <!-- <input type="button" value="<?php //echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php //echo $text_loading; ?>" class="btn btn-primary" /> -->
    <input type="image" src="<?php echo $btn_next; ?>" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="img-responsive" onclick="return false;" />

    <?php
  }
  else{
    //echo "b";
    ?>
    <!-- <input type="button" value="<?php //echo $button_continue; ?>" id="button-payment-method-guest" data-loading-text="<?php //echo $text_loading; ?>" class="btn btn-primary" /> -->
    <input type="image" src="<?php echo $btn_next; ?>" value="<?php echo $button_continue; ?>" id="button-payment-method-guest" data-loading-text="<?php echo $text_loading; ?>" class="img-responsive" onclick="return false;" />

    <?php
  }
    ?>

  </div>
</div>
<?php 
} 
else { 
  if($logged){
    ?>
    <div class="row">
    <div class="col-md-6 col-md-offset-6">
    <!-- <input type="button" value="<?php //echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php //echo $text_loading; ?>" class="btn btn-primary" /> -->
    <input type="image" src="<?php echo $btn_next; ?>" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="img-responsive" onclick="return false;" />
    </div>
    </div>
    <?php
  }
  else{
    ?>
    <div class="row">
    <div class="col-md-6 col-md-offset-6">
    <!-- <input type="button" value="<?php //echo $button_continue; ?>" id="button-payment-method-guest" data-loading-text="<?php //echo $text_loading; ?>" class="btn btn-primary" /> -->
    <input type="image" src="<?php echo $btn_next; ?>" value="<?php echo $button_continue; ?>" id="button-payment-method-guest" data-loading-text="<?php echo $text_loading; ?>" class="img-responsive" onclick="return false;" />
    </div>
    </div>
    <?php
  }
} ?>
<script type="text/javascript">
$('#collapse-payment-method input[name=\'payment_method\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/payment_method/save',
    type: 'post',
    data: $('#collapse-payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-payment-method').button('loading');
    },
    complete: function() {
      $('#button-payment-method').button('reset');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();
      
      if (json['redirect']) {
          location = json['redirect'];
      } else if (json['error']) {
          if (json['error']['warning']) {
              $('#payment-method').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              $("html, body").animate({ scrollTop: 0 }, "slow");
          }
      // } else {
      //   $('#payment-method-detail').html(json['payment']);
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});
</script>


<?php /*if ($text_agree) { ?>
<div class="buttons">
  <div class="pull-right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    &nbsp;
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<?php }*/ ?>
<style type="text/css">
a:hover{
  color: #FF3399;
}
.msg-additional-charge{
  font-style: italic;
}

.payment-selector input{
    margin:0;
    padding:0;
    display: none;
}
.payment-selector input:active +.drinkcard-payment{opacity: .9;}
.payment-selector input:checked +.drinkcard-payment{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
}
.drinkcard-payment{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    width: 100%;
    height: 80px;
    -webkit-transition: all 100ms ease-in;
       -moz-transition: all 100ms ease-in;
            transition: all 100ms ease-in;
    -webkit-filter: brightness(1.8) grayscale(1) opacity(.7);
       -moz-filter: brightness(1.8) grayscale(1) opacity(.7);
            filter: brightness(1.8) grayscale(1) opacity(.7);
}
.drinkcard-payment:hover{
    -webkit-filter: brightness(1.2) grayscale(.5) opacity(.9);
       -moz-filter: brightness(1.2) grayscale(.5) opacity(.9);
            filter: brightness(1.2) grayscale(.5) opacity(.9);
}
</style>