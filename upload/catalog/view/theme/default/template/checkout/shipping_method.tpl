<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<div class="row">
  <div class="col-md-6 col-sm-6">
<?php if ($shipping_methods) { ?>
<p><?php echo $text_shipping_method; ?></p>
<?php foreach ($shipping_methods as $shipping_method) { ?>
<p><strong><?php echo $shipping_method['title']; ?></strong></p>
<?php if (!$shipping_method['error']) { ?>
<?php foreach ($shipping_method['quote'] as $quote) { ?>
  <?php
  if(is_array($quote) && !isset($quote['code'])){
    //var_dump($quote);
    foreach($quote as $_quote){
      ?>
    <div class="radio">
      <label>
        <?php if ($_quote['code'] == $code || !$code) { ?>
        <?php $code = $_quote['code']; ?>
        <input type="radio" name="shipping_method" value="<?php echo $_quote['code']; ?>" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="shipping_method" value="<?php echo $_quote['code']; ?>" />
        <?php } ?>
        <?php echo $_quote['title']; ?> - <?php echo $_quote['text']; ?></label>
    </div>
      <?php
    }
  }
  else{
    ?>
  <div class="radio">
    <label>
      <?php if ($quote['code'] == $code || !$code) { ?>
      <?php $code = $quote['code']; ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" />
      <?php } ?>
      <?php echo $quote['title']; ?> - <?php echo $quote['text']; ?></label>
  </div>

    <?php

  }
  ?>
<?php } ?>
<?php } else { ?>
<div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
<?php } ?>
<?php } ?>
<?php } ?>

  </div>
  <div class="col-md-6 col-sm-6">
<p><strong><?php echo $text_comments; ?></strong></p>
<p>
  <textarea name="comment" rows="8" class="form-control"><?php echo $comment; ?></textarea>
</p>
<p>Pengiriman ke : <br/><?php echo $destination;?></p>

  </div>

</div>




<div class="buttons">
  <div class="pull-right">
    <input type="image" src="<?php echo $btn_next; ?>" alt="" class="img-responsive" id="button-shipping-method" data-loading-text="<?php echo $text_loading; ?>" />  
    <?php /*<input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />*/ ?>
  </div>
</div>
