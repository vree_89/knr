<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?>
        <?php if ($weight) { ?>
        &nbsp;(<?php echo $weight; ?>)
        <?php } ?>
      </h1>

        <div class="cart-main-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">      
                            <div class="table-content table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                          <td class="text-center"><?php echo $column_image; ?></td>
                                          <td class="text-left"><?php echo $column_name; ?></td>
                                          <?php /*<td class="text-left"><?php echo $column_model; ?></td>*/?>
                                          <td class="text-center"><?php echo $column_quantity; ?></td>
                                          <td class="text-right"><?php echo $column_price; ?></td>
                                          <td class="text-right"><?php echo $column_total; ?></td>
                                          <td class="text-right"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($products as $product) { ?>
                                        <tr>
                                        <td class="product-thumbnail"><?php if ($product['thumb']) { ?>
                                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                                        <?php } ?></td>
                                        <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        <?php if (!$product['stock']) { ?>
                                        <span class="text-danger">***</span>
                                        <?php } ?>


                                        <?php if ($product['is_preorder']) { ?>
                                        <br />
                                        <small style="font-style:italic;padding:0;margin:0;">*Pre Order, Date Available : <?php echo $product['date_available']?></small>
                                        <?php } ?>

                                        <?php if ($product['option']) { ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                        <br />
                                        <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php } 
                                        $option_name = $option['value'];
                                        ?>
                                        <?php } ?>
                                        <?php if ($product['reward']) { ?>
                                        <br />
                                        <small><?php echo $product['reward']; ?></small>
                                        <?php } ?>
                                        <?php if ($product['recurring']) { ?>
                                        <br />
                                        <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                                        <?php } ?></td>
                                        
                                        <?php
                                        /*
                                        <td class="text-left">
                                        <?php //echo $product['model']; ?>
                                        <?php
                                        if ($product['options']){ 
                                        //echo $product['options']['type'];exit;
                                        //var_dump($product['options']);exit;

                                        ?>

                                        <?php foreach ($product['options'] as $option){ ?>                
                                        <?php if ($option['type'] == 'select'){ ?>

                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

                                        <select name="option[<?php echo $product['cart_id']; ?>][<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <option value="<?php echo $option_value['product_option_value_id']; ?>" <?php if($option_name==$option_value['name']) echo 'selected="selected"'?>><?php echo $option_value['name']; ?>
                                        <?php if ($option_value['price']) { ?>
                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                        <?php } ?>
                                        </option>
                                        <?php } ?>
                                        </select>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                        <?php 
                                        } 
                                        ?>

                                        </td>
                                        */
                                        ?>


                                        <td class=" text-center">

                                        <div class="input-group btn-block" style="max-width: 50px;">
                                        <input type="number" min="1" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control" style="padding:3px;margin:0;height:34px;width:50px;" />
                                        <span class="input-group-btn">
                                        <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-primary"><i class="fa fa-refresh"></i></button>

                                        </span></div>
                                        </td>
                                        <td class="product-subtotal"><?php echo $product['price']; ?></td>
                                        <td class="product-remove"><?php echo $product['total']; ?></td>
                                        <td class="product-remove">
                                        <a style="cursor:pointer;" onclick="if(confirm('Remove?')) cart.remove('<?php echo $product['cart_id']; ?>');"><img src="<?php echo $btn_delete?>" class="" style="max-width:25%"></a>  
                                        <?php /*<a style="cursor:pointer;" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-times-circle"></i></a>*/?>
                                        </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                          </form>


      <?php /*if ($modules) { ?>
      <h2><?php echo $text_next; ?></h2>
      <p><?php echo $text_next_choice; ?></p>
      <div class="panel-group" id="accordion">
        <?php foreach ($modules as $module) { ?>
        <?php echo $module; ?>
        <?php } ?>
      </div>
      <?php }*/ ?>




      <div class="row">
        <div class="col-md-8 col-sm-7 col-xs-12">
          <?php
          if(isset($text_address_warning)){
            ?><p class="address-warning"><?php echo $text_address_warning?></p><?php
          }
          ?>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12">

<div class="cart_totals">
<h2>Cart Totals</h2>
          <table class="table table-bordered">            
            <?php foreach ($totals as $total) { ?>
            <tr class="<?php echo $total['code']; ?>">
              <td class="text-left"><strong><?php echo $total['title']; ?>:</strong></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
          </table>

<div class="clearfix"></div>
<div class="buttons">
<a href="<?php echo $continue; ?>" ><img src="<?php echo $btn_continue?>"></a>
<a href="<?php echo $checkout; ?>" ><img src="<?php echo $btn_checkout?>"></a>
</div>


<?php 
/*
<div class="wc-proceed-to-checkout">
   <a href="<?php echo $continue; ?>" ><?php echo $button_shopping; ?></a>
   <a href="<?php echo $checkout; ?>" ><?php echo $button_checkout; ?></a>
  <?php
  if(isset($text_address_warning) && $enable_checkout==0){
    ?>
    <a href="#" class="disabled" onclick="return false;" ><?php echo $button_checkout; ?></a>
    <?php
  }
  else{
    ?>
    <a href="<?php echo $checkout; ?>" ><?php echo $button_checkout; ?></a>
    <?php    
  }
  

</div>
*/
?>
</div>

        </div>
      </div>         


                         
                    </div>
                </div>
            </div>
        </div>


      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<style type="text/css">
.buttons{
  margin-top: 15px;
  //background: red;
}
a:hover{
  color: #FF3399;
}
.alert-success{
  background: #ffc2e3;color:#fc3a99;
} 
</style>