<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_option; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-checkout-option">
            <div class="panel-body"></div>
          </div>
        </div>
        <?php if (!$logged && $account != 'guest') { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_account; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-payment-address">
            <div class="panel-body"></div>
          </div>
        </div>
        <?php } else { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_payment_address; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-payment-address">
            <div class="panel-body"></div>
          </div>
        </div>
        <?php } ?>
        <?php if ($shipping_required) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_shipping_address; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-shipping-address">
            <div class="panel-body"></div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_shipping_method; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-shipping-method">
            <div class="panel-body"></div>
          </div>
        </div>
        <?php } ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_coupon; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-coupon">
            <div class="panel-body"></div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_payment_method; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-payment-method">
            <div class="panel-body"></div>
          </div>
        </div>
        <!--div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php //echo $text_checkout_confirm; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-checkout-confirm">
            <div class="panel-body"></div>
          </div>
        </div-->
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
var payment_method = "";
var active = "collapse-payment-address";
var timer = false;
var interval = null;
// $(document).ready(function(){

// });

function reloadCart(){
  $.ajax({
    url: 'index.php?route=checkout/coupon/reload',
    dataType: 'html',
    success: function(html) {
    $('#collapse-coupon .data-cart').html(html);
    },
  });  
}

$(document).on('change', 'input[name=\'account\']', function() {
	if ($('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
		if (this.value == 'register') {
			$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
		} else {
			$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
		}
	} else {
		if (this.value == 'register') {
			$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_account; ?>');
		} else {
			$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_address; ?>');
		}
	}
});

<?php if (!$logged) { ?>
$(document).ready(function() {
    $.ajax({
        url: 'index.php?route=checkout/login',
        dataType: 'html',
        success: function(html) {
           $('#collapse-checkout-option .panel-body').html(html);

			$('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_option; ?> <i class="fa fa-caret-down"></i></a>');

			$('a[href=\'#collapse-checkout-option\']').trigger('click');
      active = "collapse-checkout-option";
        }
    });
});
<?php } else { ?>
$(document).ready(function() {
   //  $.ajax({
   //      url: 'index.php?route=checkout/payment_address',
   //      dataType: 'html',
   //      success: function(html) {
   //          $('#collapse-payment-address .panel-body').html(html);

			// $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');

			// $('a[href=\'#collapse-payment-address\']').trigger('click');
   //    active = "collapse-payment-address";
   //      }
   //  });

        $.ajax({
          url: 'index.php?route=checkout/shipping_method',
          dataType: 'html',
          complete: function() { $('#button-shipping-address').button('reset'); },
          success: function(html) {
            $('#collapse-shipping-method .panel-body').html(html);


            $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
            $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

            $('a[href=\'#collapse-shipping-method\']').trigger('click');
            active = "collapse-shipping-method";

            $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_coupon; ?>');
            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');


            $.ajax({
              url: 'index.php?route=checkout/payment_address',
              dataType: 'html',
              success: function(html) {
                $('#collapse-payment-address .panel-body').html(html);   
            
              }
            });


            $.ajax({
              url: 'index.php?route=checkout/shipping_address',
              dataType: 'html',
              success: function(html) {
                $('#collapse-shipping-address .panel-body').html(html);
              }
            });



          }
        });

});
<?php } ?>

//CHOOSE CHECKOUT OPTION GUEST OR REGISTER NEW ACCOUNT OR LOGIN WITH EXISTING ACCOUNT
// Checkout (CHECKOUT OPTIONS CONTINUE) 
$(document).delegate('#button-account', 'click', function() {
    var option = $('input[name=\'account\']:checked').val();  //guest or register
    $.ajax({
        url: 'index.php?route=checkout/' + option,
        dataType: 'html',
        beforeSend: function() { $('#button-account').button('loading'); },
        complete: function() { $('#button-account').button('reset'); },
        success: function(html) {
            $('.alert, .text-danger').remove();

            $('#collapse-payment-address .panel-body').html(html);

			if (option == 'register') {
				$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
			} else {
				$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
			}

			$('a[href=\'#collapse-payment-address\']').trigger('click');
      active = "collapse-payment-address";
        }
    });
});

// Login (CHECKOUT OPTIONS LOGIN)
$(document).delegate('#button-login', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/login/save',
        type: 'post',
        data: $('#collapse-checkout-option :input'),
        dataType: 'json',
        beforeSend: function() { $('#button-login').button('loading'); },
        complete: function() { $('#button-login').button('reset'); },
        success: function(json) {
            $('.alert, .text-danger').remove();
            $('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				// Highlight any found errors
				$('input[name=\'email\']').parent().addClass('has-error');
				$('input[name=\'password\']').parent().addClass('has-error');
		   }
        }
    });
});

// Register (ACCOUNT & BILLING DETAILS REGISTER)
$(document).delegate('#button-register', 'click', function() {    
  $.ajax({
    url: 'index.php?route=checkout/register/save',
    type: 'post',
    data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
    dataType: 'json',
    beforeSend: function() { $('#button-register').button('loading'); },
    success: function(json) {
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');

      if (json['redirect']) {
        location = json['redirect'];
      } 
      else if (json['error']) {
        $('#button-register').button('reset');
        if (json['error']['warning']) {
        $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        for (i in json['error']) {
          var element = $('#input-payment-' + i.replace('_', '-'));
          if ($(element).parent().hasClass('input-group')) {
          $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
          } else {
          $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
          }
        }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
      } 
      else {
        <?php if ($shipping_required) { ?>
          var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');
          if (shipping_address) {
            $.ajax({
            url: 'index.php?route=checkout/shipping_method',
            dataType: 'html',
            success: function(html) {
              // Add the shipping address
              $.ajax({
                url: 'index.php?route=checkout/shipping_address',
                dataType: 'html',
                success: function(html) {
                $('#collapse-shipping-address .panel-body').html(html);

                $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
                }
              });
            $('#collapse-shipping-method .panel-body').html(html);
            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

            $('a[href=\'#collapse-shipping-method\']').trigger('click');
            active = "collapse-shipping-method";


            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
            }
            });
          } 
          else {
            $.ajax({
              url: 'index.php?route=checkout/shipping_address',
              dataType: 'html',
              success: function(html) {
              $('#collapse-shipping-address .panel-body').html(html);

              $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

              $('a[href=\'#collapse-shipping-address\']').trigger('click');
              active = "collapse-shipping-address";

              $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
              $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
              }
            });
          }
          <?php 
        } 
        else { 
          ?>
          $.ajax({
            url: 'index.php?route=checkout/coupon',
            dataType: 'html',
            success: function(html) {
              $('#collapse-coupon .panel-body').html(html);

              $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

              $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<a href="#collapse-coupon" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-coupon"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

              $('a[href=\'#collapse-coupon\']').trigger('click');
              active = "collapse-coupon";

              $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

              <?php
              if(isset($shipping_method)){
              ?>
              if(active=="collapse-coupon" && timer==false){
              timer = true;
              interval= setInterval(reloadCart, 7000);
              }  
              <?php
              }
              ?>
            }
          });
          <?php 
        } 
        ?>
        
        $.ajax({
          url: 'index.php?route=checkout/payment_address',
          dataType: 'html',
          complete: function() {
          $('#button-register').button('reset');
          },
          success: function(html) {
          $('#collapse-payment-address .panel-body').html(html);

          $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
          }
        });
        

      }
    }
  });
});

// Payment Address (BILLING DETAILS BUTTON)
$(document).delegate('#button-payment-address', 'click', function() {       
    $.ajax({
        url: 'index.php?route=checkout/payment_address/save',
        type: 'post',
        data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
        dataType: 'json',
        success: function(json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                for (i in json['error']) {
                var element = $('#input-payment-' + i.replace('_', '-'));
                if ($(element).parent().hasClass('input-group')) {
                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                } else {
                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                }
                }

                // Highlight any found errors
                $('.text-danger').parent().parent().addClass('has-error');
            } else {
                <?php if ($shipping_required) { ?>
                $.ajax({
                    url: 'index.php?route=checkout/shipping_address',
                    dataType: 'html',
                    success: function(html) {
                        $('#collapse-shipping-address .panel-body').html(html);

                        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                        $('a[href=\'#collapse-shipping-address\']').trigger('click');
                        active = "collapse-shipping-address";

                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                    }
                });
                <?php } else { ?>
                    $.ajax({
                        url: 'index.php?route=checkout/coupon',
                        dataType: 'html',
                        success: function(html) {
                            $('#collapse-coupon .panel-body').html(html);

                            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                            $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<a href="#collapse-coupon" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-coupon"><?php echo $text_checkout_coupon; ?> <i class="fa fa-caret-down"></i></a>');

                            $('a[href=\'#collapse-coupon\']').trigger('click');
                            active = "collapse-coupon";

                            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

                            <?php
                            if(isset($shipping_method)){
                            ?>
                              if(active=="collapse-coupon" && timer==false){
                                timer = true;
                                interval = setInterval(reloadCart, 7000);
                              }  
                            <?php
                            }

                            ?>                            
                        }
                    });


                <?php } ?>


                //RESET CONTENT OF THIS TAB
                $.ajax({
                    url: 'index.php?route=checkout/payment_address',
                    dataType: 'html',
                    success: function(html) {
                        $('#collapse-payment-address .panel-body').html(html);
                    }
                });
            }
        }
    });
});

// Shipping Address (DELIVERY DETAILS BUTTON CONTINUE)
$(document).delegate('#button-shipping-address', 'click', function() {    
  $.ajax({
    url: 'index.php?route=checkout/shipping_address/save',
    type: 'post',
    data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
    dataType: 'json',
    beforeSend: function() { $('#button-shipping-address').button('loading'); },
    success: function(json) {
      $('.alert, .text-danger').remove();

      if (json['redirect']) {
        location = json['redirect'];
      } else if (json['error']) {
        $('#button-shipping-address').button('reset');

        if (json['error']['warning']) {
        $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        for (i in json['error']) {
          var element = $('#input-shipping-' + i.replace('_', '-'));
          if ($(element).parent().hasClass('input-group')) {
            $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
          } else {
            $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
          }
        }

        // Highlight any found errors
        $('.text-danger').parent().parent().addClass('has-error');
      } else {
        $.ajax({
          url: 'index.php?route=checkout/shipping_method',
          dataType: 'html',
          complete: function() { $('#button-shipping-address').button('reset'); },
          success: function(html) {
            $('#collapse-shipping-method .panel-body').html(html);

            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

            $('a[href=\'#collapse-shipping-method\']').trigger('click');
            active = "collapse-shipping-method";

            $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_coupon; ?>');
            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

            $.ajax({
              url: 'index.php?route=checkout/shipping_address',
              dataType: 'html',
              success: function(html) {
                $('#collapse-shipping-address .panel-body').html(html);
              }
            });
          }
        });

        $.ajax({
          url: 'index.php?route=checkout/payment_address',
          dataType: 'html',
          success: function(html) {
            $('#collapse-payment-address .panel-body').html(html);
          }
        });
      }
    }
  });
});

// Guest
$(document).delegate('#button-guest', 'click', function() {
  $.ajax({
    url: 'index.php?route=checkout/guest/save',
    type: 'post',
    data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
    dataType: 'json',
    beforeSend: function() { $('#button-guest').button('loading'); },
    success: function(json){
      $('.alert, .text-danger').remove();
      if (json['redirect']) {
        location = json['redirect'];
      } 
      else if (json['error']) {
        $('#button-guest').button('reset');
        if (json['error']['warning']) {
          $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
        for (i in json['error']) {
          var element = $('#input-payment-' + i.replace('_', '-'));
          if ($(element).parent().hasClass('input-group')) {
            $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
          } else {
            $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
          }
        }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
      } 
      else {
        <?php if ($shipping_required) { ?>
          var shipping_address = $('#collapse-payment-address input[name=\'shipping_address\']:checked').prop('value');

          if (shipping_address) {
            $.ajax({
              url: 'index.php?route=checkout/shipping_method',
              dataType: 'html',
              complete: function() { $('#button-guest').button('reset'); },
              success: function(html) {
                // Add the shipping address
                $.ajax({
                  url: 'index.php?route=checkout/guest_shipping',
                  dataType: 'html',
                  success: function(html) {
                    $('#collapse-shipping-address .panel-body').html(html);
                    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
                  }
                });

                $('#collapse-shipping-method .panel-body').html(html);

                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                $('a[href=\'#collapse-shipping-method\']').trigger('click');
                active = "collapse-shipping-method";

                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
              }
            });
          } else {
            $.ajax({
              url: 'index.php?route=checkout/guest_shipping',
              dataType: 'html',
              complete: function() { $('#button-guest').button('reset'); },
              success: function(html) {
                $('#collapse-shipping-address .panel-body').html(html);

                $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                $('a[href=\'#collapse-shipping-address\']').trigger('click');
                active = "collapse-shipping-address";

                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
              }
            });
          }
        <?php } else { ?>
          $.ajax({
            url: 'index.php?route=checkout/coupon',
            dataType: 'html',
            complete: function() { $('#button-guest').button('reset'); },
            success: function(html) {
              $('#collapse-coupon .panel-body').html(html);

              $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<a href="#collapse-coupon" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-coupon"><?php echo $text_checkout_coupon; ?> <i class="fa fa-caret-down"></i></a>');

              $('a[href=\'#collapse-coupon\']').trigger('click');
              active = "collapse-coupon";

              <?php
              if(isset($shipping_method)){
              ?>
                if(active=="collapse-coupon" && timer==false){
                  timer = true;
                  interval = setInterval(reloadCart, 7000);
                }  
              <?php
              }
              ?> 

              $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                       
            }
          });
        <?php } ?>
      }
    }
  });
});

// Guest Shipping
$(document).delegate('#button-guest-shipping', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/guest_shipping/save',
        type: 'post',
        data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
        dataType: 'json',
        beforeSend: function() { $('#button-guest-shipping').button('loading'); },
        success: function(json) {
          $('.alert, .text-danger').remove();

          if (json['redirect']) {
            location = json['redirect'];
          } else if (json['error']) {
            $('#button-guest-shipping').button('reset');
            if (json['error']['warning']) {
            $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            for (i in json['error']) {
              var element = $('#input-shipping-' + i.replace('_', '-'));
              if ($(element).parent().hasClass('input-group')) {
                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
              } else {
                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
              }
            }

            // Highlight any found errors
            $('.text-danger').parent().addClass('has-error');
          } else {
            $.ajax({
              url: 'index.php?route=checkout/shipping_method',
              dataType: 'html',
              complete: function() { $('#button-guest-shipping').button('reset'); },
              success: function(html) {
                $('#collapse-shipping-method .panel-body').html(html);

                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i>');

                $('a[href=\'#collapse-shipping-method\']').trigger('click');
                active = "collapse-shipping-method";

                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
              }
            });
          }
        }
    });
});

$(document).delegate('#button-shipping-method', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/shipping_method/save',
        type: 'post',
        data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
        dataType: 'json',
        beforeSend: function() { $('#button-shipping-method').button('loading'); },
        success: function(json) {
            //alert('<?php echo $shipping_method?>');
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#button-shipping-method').button('reset');

                if (json['error']['warning']) {
                    $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
            } else {
                $.ajax({
                    url: 'index.php?route=checkout/coupon',
                    dataType: 'html',
                    success: function(html) {

                        $('#collapse-coupon .panel-body').html(html);





                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                        $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<a href="#collapse-coupon" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-coupon"><?php echo $text_checkout_coupon; ?> <i class="fa fa-caret-down"></i></a>');



                        $('a[href=\'#collapse-coupon\']').trigger('click');
                        active = "collapse-coupon";

                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

                        <?php
                        if(isset($shipping_method)){
                        ?>
                          if(active=="collapse-coupon" && timer==false){
                            timer = true;
                            interval = setInterval(reloadCart, 7000);
                          }  
                        <?php
                        }
                        ?>

                        
                        $('a#btn-collapse-shipping-method').click(function(){
                          if(active!="collapse-shipping-method"){
                            $.ajax({
                            url: 'index.php?route=checkout/shipping_method',
                            dataType: 'html',
                            complete: function() { $('#button-guest').button('reset'); },
                            success: function(html) {
                            $('#collapse-shipping-method .panel-body').html(html);
                            active = "collapse-shipping-method";
                            }
                            });
                          }
                        });
                        $('a#btn-collapse-coupon').click(function(){
                          if(active!="collapse-coupon"){
                            $.ajax({
                            url: 'index.php?route=checkout/coupon',
                            dataType: 'html',
                            success: function(html) {
                            $('#collapse-coupon .panel-body').html(html);
                            active = "collapse-coupon";
                            }
                            });
                          }
                        });

                    }
                });


            }
        }
    });
});







$(document).delegate('#button-apply-coupon', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/coupon/save',
        type: 'post',
        data: $('#collapse-coupon input[type=\'text\']'),
        dataType: 'json',        
        success: function(json) {
            $('.alert, .text-danger').remove();
            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {                
              if (json['error']['warning']) {
                $('#collapse-coupon .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              }
            } 
            else {
              if(json['success']){                
                $('#collapse-coupon .panel-body').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> '+ json['success'] +' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');     
              }

              //reload data cart
              reloadCart();

            }
        }
    });
});









$(document).delegate('#button-coupon', 'click', function() { 
  $.ajax({
      url: 'index.php?route=checkout/payment_method',
      dataType: 'html',
      complete: function() { $('#button-coupon').button('reset'); },
      success: function(html) {
          $('#collapse-payment-method .panel-body').html(html);


          $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

          $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<a href="#collapse-coupon" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-coupon"><?php echo $text_checkout_coupon; ?> <i class="fa fa-caret-down"></i></a>');


          $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-payment-method"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');


          $('a[href=\'#collapse-payment-method\']').trigger('click');
          active = "collapse-payment-method";

          clearInterval(interval);
          timer = false;
          interval = null;


          $('a#btn-collapse-shipping-method').click(function(){
            if(active!="collapse-shipping-method"){
              $.ajax({
              url: 'index.php?route=checkout/shipping_method',
              dataType: 'html',
              complete: function() { $('#button-guest').button('reset'); },
              success: function(html) {
              $('#collapse-shipping-method .panel-body').html(html);
              active = "collapse-shipping-method";
              }
              });
            }
          });
          $('a#btn-collapse-coupon').click(function(){
            if(active!="collapse-coupon"){
              $.ajax({
              url: 'index.php?route=checkout/coupon',
              dataType: 'html',
              success: function(html) {
              $('#collapse-coupon .panel-body').html(html);
              active = "collapse-coupon";
              }
              });
            }
          });

      }
  });
});








$(document).delegate('.btn-update-cart', 'click', function() {
    var err = 0;
    $("#form-cart select").each(function() {
        if((this.value)=="") {
          $(this).addClass("highlight");
            err++;
        }
    });
    $("#form-cart input[type=number]").each(function() {
        if((this.value)=="" || (this.value)=="0") {
          $(this).addClass("highlight");
            err++;
        }
    });
    if(err>0){
      alert('Please complete all required field!');
      return false;
    }



    $.ajax({
      url:'index.php?route=checkout/cart/ajax_edit',
      type: 'post',
      data: $('#form-cart').serialize(),
      dataType: 'json',
      success:function(json){
        $('#collapse-coupon .panel-body .alert').remove();

        if(json['error']){
          if (json['error']['warning']) {
          $('#collapse-coupon .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          }
          }
        else{
          if(json['success']){
            $('#collapse-coupon .panel-body').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> '+ json['success'] +' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          }


          //reload data cart
          reloadCart();

        }

      }

    });

});


$(document).delegate('.select_option', 'blur', function() {
    $(this).removeClass('highlight');
    if(this.value==""){
    $(this).addClass('highlight');

    }
  });

function removeCart(key){
    $.ajax({
      url: 'index.php?route=checkout/cart/remove',
      type: 'post',
      data: 'key=' + key,
      dataType: 'json',
      success: function(json) {
        reloadCart();
      }
    });  
}


$(document).delegate('.radio-payment','click',function(){
  $.ajax({
    url: 'index.php?route=checkout/payment_method/ajax_save',
    type: 'post',
    data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
    dataType: 'json',
    success: function(json) {
      $('#collapse-payment-method .payment-wrapper').html(json['payment']);

      payment_method = json['payment_method'];
      if(payment_method!=""){
        $('#collapse-payment-method #button-payment-method').removeAttr('disabled');
        $('#collapse-payment-method #button-payment-method').on('click', function() {

          $.ajax({
            type: 'post',
            data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
            dataType: 'json',
            url: 'index.php?route=extension/payment/'+payment_method+'/confirm',
            cache: false,
            beforeSend: function() { $('#collapse-payment-method #button-payment-method').button('loading'); },
            complete: function() { $('#collapse-payment-method #button-payment-method').button('reset'); },
            success: function(json2) {
              $('#collapse-payment-method .panel-body .alert').remove();
              if(json2['error']){
                if (json2['error']['warning']) {
                  $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-danger">' + json2['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');


                }
              }
              else if(json2['success']){
                if(json2['continue']){
                  location = json2['continue'];
                }
              }
            }
          });
        });
      }



    }
  });
});




$(document).delegate('#collapse-payment-method a','click',function(){
//$('a#btn-collapse-payment-method').click(function(){
  //alert('payment method');
  $.ajax({
  url: 'index.php?route=checkout/payment_method',
  dataType: 'html',
  success: function(html) {
  $('#collapse-payment-method .panel-body').html(html);
  active = "collapse-payment-method";
  }
  });
});





//--></script>
<style type="text/css">
.highlight{
  border: #FF0000 1px solid;
}
</style>
<?php echo $footer; ?>
