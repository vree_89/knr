

<div class="row data-cart">
<div class="col-md-12 col-sm-12 col-xs-12">
      <h1><?php echo $heading_title; ?>
        <?php if ($weight) { ?>
        &nbsp;(<?php echo $weight; ?>)
        <?php } ?>
      </h1>

	<form action="<?php echo $action; ?>" method="post" id="form-cart">      
<div class="">
<table class="table table-bordered">
<thead>
<tr>
                                          <td class="text-center" width="10%"><?php echo $column_image; ?></td>
                                          <td class="text-left"><?php echo $column_name; ?></td>
                                          <!-- <td class="text-left"><?php //echo $column_model; ?></td> -->
                                          <td class="text-center"><?php echo $column_quantity; ?></td>
                                          <td class="text-right"><?php echo $column_price; ?></td>
                                          <td class="text-right"><?php echo $column_total; ?></td>
                                          <!-- <td class="text-center"><?php //echo $column_remove; ?></td> -->
</tr>
</thead>
<tbody>
<?php foreach ($products as $product) { ?>
<tr>
                                        <td class="text-center"><?php if ($product['thumb']) { ?>
                                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                                        <?php } ?></td>
                                        <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        <?php if (!$product['stock']) { ?>
                                        <span class="text-danger">***</span>
                                        <?php } ?>

                                        <?php if ($product['is_preorder']) { ?>
                                        <br />
                                        <small style="font-style:italic;padding:0;margin:0;">*Pre Order, Date Available : <?php echo $product['date_available']?></small>
                                        <?php } ?>
                                        
                                        <?php if ($product['option']) { ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                        <br />
                                        <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php } 
                                        $option_name = $option['value'];
                                        ?>
                                        <?php } ?>
                                        <?php if ($product['reward']) { ?>
                                        <br />
                                        <small><?php echo $product['reward']; ?></small>
                                        <?php } ?>
                                        <?php if ($product['recurring']) { ?>
                                        <br />
                                        <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                                        <?php } ?></td>
                                        
                                      <!-- <td class="text-left"> -->
                                      <?php //echo $product['model']; ?>
                                      <?php
                                      /*if ($product['options']){ 
                                      //echo $product['options']['type'];exit;
                                      //var_dump($product['options']);exit;

                                      ?>

                                      <?php foreach ($product['options'] as $option){ ?>                
                                      <?php if ($option['type'] == 'select'){ ?>

                                      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

                                      <select name="option[<?php echo $product['cart_id']; ?>][<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control select_option">
                                      <option value=""><?php echo $text_select; ?></option>
                                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                      <option value="<?php echo $option_value['product_option_value_id']; ?>" <?php if($option_name==$option_value['name']) echo 'selected="selected"'?>><?php echo $option_value['name']; ?>
                                      <?php if ($option_value['price']) { ?>
                                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                      <?php } ?>
                                      </option>
                                      <?php } ?>
                                      </select>
                                      </div>
                                      <?php } ?>











                                      <?php } ?>
                                      <?php 
                                      } */
                                      ?>

                                      <!-- </td> -->


                                        <td class=" text-center">
                                          <?php echo $product['quantity']; ?>
                                        <?php /*
                                        <div class="input-group btn-block" style="max-width: 50px;">
                                        <input type="number" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control" style="padding:3px;margin:0;height:34px;width:50px;" min="1" />
                                        <span class="input-group-btn">
                                        <button type="button"  title="<?php echo $button_update; ?>" class="btn btn-primary btn-update-cart"><i class="fa fa-refresh"></i></button>

                                        </span></div>


                                        */?>
                                        </td>
                                        <td class="product-subtotal"><?php echo $product['price']; ?></td>
                                        <td class="product-total"><?php echo $product['total']; ?></td>
                                        <!-- <td class="product-remove text-center">
                                        <a style="cursor:pointer;" onclick="removeCart('<?php //echo $product['cart_id']?>');"><i class="fa fa-times-circle"></i></a></td> -->

</tr>
<?php } ?>
</tbody>
</table>
</div>
</form>






<div class="row">
<div class="col-md-7 col-sm-7 col-xs-12"></div>
<div class="col-md-5 col-sm-5 col-xs-12">

<div class="">
<table class="table table-bordered table-total">            
<?php foreach ($totals as $total) { ?>
  <?php
  if($total['type']=="text"){
?>
  <tr class="<?php echo $total['code']; ?>">
  <td class="text-left" width="50%"><strong><?php echo $total['title']; ?>:</strong></td>
  <td class="text-right" width="50%"><?php echo $total['symbol']?> <?php echo $total['currency']?><?php echo $total['value']; ?></td>
  </tr>

<?php    

  }
  else if($total['type']=="input"){
?>
  <tr class="<?php echo $total['code']; ?>">
  <td class="text-left" width="50%"><strong><?php echo $total['title']; ?>:</strong></td>
  <td class="text-right" width="50%">
    <div class="input-group">
      <span class="input-group-addon"><?php echo $total['currency']?></span>
      <input type="text" class="form-control number" name="input_payment_<?php echo $total['code']?>" id="input_payment_<?php echo $total['code']?>" value="<?php echo $input_payment_wallet && $total['code']=="wallet"? $input_payment_wallet : $total['value']?>">
      <span class="input-group-addon btn btn-default" id="button-<?php echo $total['code']?>">Apply</span>
    </div>
    <!-- <input type="text" class="form-control" value="<?php //echo $total['value']; ?>"> -->
  </td>
  </tr>

<?php
  }
  else if($total['type']=="input-disabled"){
?>
  <tr class="<?php echo $total['code']; ?>">
  <td class="text-left" width="50%"><strong><?php echo $total['title']; ?>:</strong></td>
  <td class="text-right" width="50%">
    <div class="input-group">
      <div class="input-group-addon"><?php echo $total['symbol']?> <?php echo $total['currency']?></div>
      <input type="text" class="form-control number" name="input_payment_<?php echo $total['code']?>" id="input_payment_<?php echo $total['code']?>" value="<?php echo $input_payment_wallet && $total['code']=="wallet"? $input_payment_wallet : $total['value']?>" readonly>
    </div>
    <!-- <input type="text" class="form-control" disabled value="<?php //echo $total['value']; ?>"> -->
  </td>
  </tr>

<?php
  }

  ?>
<?php } ?>
</table>


</div>

</div>
</div>         

</div>

</div>





<?php
/*
<div class="row">
  <div class="col-md-6 col-lg-6"><div class="message"></div></div>
  <div class="col-md-6 col-lg-6">
    <?php if ($logged && $is_wallet) { ?>
    <div class="pull-right">
      <table class="table table-bordered">
        <tr>
          <th>Use Wallet</th>
          <td>
          <div class="input-group">
            <div class="input-group-addon"><?php echo $currency?></div>
            <input type="text" class="form-control number" name="input_payment_wallet" id="input_payment_wallet" value="<?php echo $input_payment_wallet? $input_payment_wallet : ''?>">
          </div>
          </td>
        </tr>
        <tr>
          <th>You Pay</th>
          <td>
          <div class="input-group mb-2 mb-sm-0">
            <div class="input-group-addon"><?php echo $currency?></div>
            <input type="text" class="form-control number" name="input_payment_normal" id="input_payment_normal" value="<?php echo $input_payment_normal? $input_payment_normal : ''?>" readonly="readonly">
          </div>
          </td>
        </tr>
      </table>
      
      
    </div>
    <div class="clearfix"></div>
    <?php } ?>
  </div>
</div>
*/

?>





<p><strong><?php echo $text_coupon; ?></strong></p>
<p>
  <input type="text" name="coupon" id="input_coupon" class="form-control" value="<?php echo $coupon; ?>">
</p>

<div class="clearfix"></div>



<table style="width:100%;">
  <tr>
    <td class="pull-left" width="50%"><p><?php echo $text_modify_cart?></p></td>
    <td class="pull-right text-right" width="50%">
    <input type="image" src="<?php echo $btn_use?>" id="button-apply-coupon" data-loading-text="<?php echo $text_loading; ?>" class="" />&nbsp;<input type="image" src="<?php echo $btn_next?>" id="button-voucher" data-loading-text="<?php echo $text_loading; ?>" class="" />
    </td>
  </tr>
</table>



<div class="clearfix"></div>
<script type="text/javascript">


$(document).ready(function(){
    input_payment_wallet = <?php echo isset($input_payment_wallet) ? $input_payment_wallet : "0" ?>;
    input_payment_total = <?php echo isset($input_payment_total) ? $input_payment_total : "0" ?>;
    order_total = <?php echo isset($input_payment_wallet) ? $input_payment_wallet : "0" ?> + <?php echo isset($input_payment_total) ? $input_payment_total : "0" ?>;
    // $("#input_payment_wallet").focus(); 
    

    // $(document).delegate('#input_payment_wallet','keyup',function(e){
    //   if($(this).val()>deposit){
    //     alert('Wallet not enough');
    //     $("#input_payment_wallet").val(input_payment_wallet);  
    //     $("#input_payment_total").val(input_payment_total);  
    //     return false;
    //   }

    //   if(order_total-$(this).val()<50000){
    //     alert('Sisa minimal IDR 50000');
    //     $("#input_payment_wallet").val(input_payment_wallet);  
    //     $("#input_payment_total").val(input_payment_total);  
    //     return false;
    //   }      
    //   $("#input_payment_total").val(order_total-$(this).val());
    // });
    //$(document).delegate('#input_payment_wallet','change',function(){
      //return validateWallet(this);
      <?php
      /*
      if($(this).val() > <?php echo $deposit?>){
        err_wallet = true;
        alert('Wallet not enough');
        $("#input_payment_wallet").val(input_payment_wallet);
        $("#input_payment_wallet").focus();            
        return false;
      }

      if($(this).val()=="" || $(this).val()=="0"){
        $("#input_payment_total").val("<?php echo $order_total?>");
      }
      //tidak nol/kosong
      else{
        <?php
        if(isset($input_payment_wallet) && isset($input_payment_total)){
          ?>
          //var _order_total = input_payment_wallet+input_payment_total;
          if($(this).val()>=order_total){
              $(this).val(order_total);
              $("#input_payment_total").val("0");
          }
          else{
            //var _order_total = <?php echo $order_total?>;
            var sisa = eval(order_total - $(this).val());
            if(sisa<50000){
              err_payment = true;
              $("#input_payment_wallet").val(input_payment_wallet);
              // $("#input_payment_wallet").focus();
              alert('Sisa minimal IDR 50000');
              return false;
            }
            else{
              $("#input_payment_total").val(sisa);   
           
            }
          }
          <?php
        }
        ?>
      }
      setwallet();
      reloadCart();


      //reset interval
      // clearInterval(interval);
      // timer = false;
      // interval = null;
      // timer = true;
      // interval = setInterval(reloadCart, 10000);   
      */

      ?>
   
    //});






// $(document).delegate('#input_payment_wallet','keyup',debounce(function () {
//   if($(this).val() > <?php echo $deposit?>){
//             err_wallet = true;
//             alert('Wallet not enough');
//             $("#input_payment_wallet").val(input_payment_wallet);
//             $("#input_payment_wallet").focus();            
//             return false;
//   }

//     if($(this).val()=="" || $(this).val()=="0"){
//       $("#input_payment_total").val("<?php echo $order_total?>");
//     }
//     //tidak nol/kosong
//     else{
//       <?php
//       //session payment_wallet dan payment_total sudah ada
//       if(isset($input_payment_wallet) && isset($input_payment_total)){
//         ?>
//         // alert("Payment wallet sekarang <?php echo $input_payment_wallet?>");
//         // alert("Payment total sekarang <?php echo $input_payment_total?>");
//         var _order_total = input_payment_wallet+input_payment_total;
//         if($(this).val()>=_order_total){
//             $(this).val(_order_total);
//             $("#input_payment_total").val("0");
//         }
//         else{
//           var _order_total = <?php echo $order_total + $input_payment_wallet?>;
//           var sisa = eval(_order_total - $(this).val());
//           if(sisa<50000){
//             err_payment = true;
//             $("#input_payment_wallet").val(input_payment_wallet);
//             $("#input_payment_wallet").focus();
//             alert('Sisa minimal IDR 50000');
//             return false;
//           }
//           else{
//             $("#input_payment_total").val(sisa);   
         
//           }
//         }




//         <?php
//       }

//       ?>
//     }
//     setwallet();
//     reloadCart();


//     //reset interval
//           clearInterval(interval);
//           timer = false;
//           interval = null;

//     timer = true;
//     interval = setInterval(reloadCart, 10000);    
// },1500));

// $(document).delegate('#input_payment_wallet','change',function(e){
//   //nol atau kosong
//   if($(this).val()=="" || $(this).val()=="0"){
//     $("#input_payment_total").val("<?php echo $order_total?>");
//   }
//   //tidak nol/kosong
//   else{
//     <?php
//     //session payment_wallet dan payment_total sudah ada
//     if(isset($input_payment_wallet) && isset($input_payment_total)){
//       ?>
//       alert("<?php echo $input_payment_wallet?>");
//       alert("<?php echo $input_payment_total?>");
//       var _order_total = <?php echo ($order_total + $input_payment_wallet)?>;
//       if($(this).val()>=_order_total){
//           $(this).val("<?php echo $order_total + $input_payment_wallet?>");
//           $("#input_payment_total").val("0");
//       }
//       else{
//         var _order_total = <?php echo $order_total + $input_payment_wallet?>;
//         var sisa = eval(_order_total - $(this).val());
//         if(sisa<50000){
//           $("#input_payment_wallet").val("<?php echo $input_payment_wallet?>");
//           $("#input_payment_wallet").focus();
//           alert('Sisa minimal IDR 50000');
//           return false;
//         }
//         else{
//           $("#input_payment_total").val(sisa);   
       
//         }
//       }




//       <?php
//     }

//     ?>
//   }
//   //e.preventDefault();


// });

});
</script>


