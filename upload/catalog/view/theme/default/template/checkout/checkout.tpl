<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_option; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-checkout-option">
            <div class="panel-body"></div>
          </div>
        </div>
        <?php if (!$logged) { ?>


        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_shipping_address; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-shipping-address">
            <div class="panel-body"></div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_shipping_method; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-shipping-method">
            <div class="panel-body"></div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_coupon; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-coupon">
            <div class="panel-body"></div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_payment_method; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-payment-method">
            <div class="panel-body"></div>
          </div>
        </div>
        <?php } else { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_shipping_address; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-shipping-address">
            <div class="panel-body"></div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_shipping_method; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-shipping-method">
            <div class="panel-body"></div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_coupon; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-coupon">
            <div class="panel-body"></div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_payment_method; ?></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-payment-method">
            <div class="panel-body"></div>
          </div>
        </div>        
        <?php } ?>


      </div> 
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" media="screen" />
<script type="text/javascript" src="mobile/view/theme/default/assets/js/jquery.redirect.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/moment.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
  


<script type="text/javascript"><!--
var input_payment_total = <?php echo isset($input_payment_total) ? $input_payment_total : "0" ?>;
var input_payment_wallet = <?php echo isset($input_payment_wallet) ? $input_payment_wallet : "0" ?>;
var subtotal = 0;
var deposit = <?php echo isset($deposit) ? (int)$deposit : "0" ?>;
var payment_method = "";
var active = "collapse-payment-address";
var timer = false;
var interval = null;
var intervalCheck = null;

var err_wallet = false;
var err_payment = false;
var order_total = 0;
var err = false;

// $(document).ready(function(){

// });
var typingTimer;                //timer identifier
var doneTypingInterval = 3000;  //time in ms, 5 second for example
var $input = $('#input_payment_wallet');

//on keyup, start the countdown
// $(document).delegate('#input_payment_wallet', 'keyup', function(e) {

//   clearTimeout(typingTimer);
//   typingTimer = setTimeout(doneTyping, doneTypingInterval);
  
// });

//on keydown, clear the countdown 
// $(document).delegate('#input_payment_wallet', 'keydown', function(e) {
//   clearTimeout(typingTimer);  
//   var keyCode = e.keyCode || e.which; 
//   if (keyCode == 9) { 
//     //e.preventDefault(); 
//     //clearTimeout(typingTimer);
//     typingTimer = setTimeout(doneTyping, 1);
//     //doneTyping();
//     //alert('tab pressed');
//     // call custom function here
//   } 


// });


  $(document).delegate('#input_payment_wallet,#input_coupon','focus',function () {
        console.log("interval stopped");
        if(interval!=null){
        clearInterval(interval);  
        interval = null;            
        }
        
        
  });
  $(document).delegate('#input_payment_wallet,#input_coupon','blur',function (e) {
    
      console.log("interval running");
      console.log("blurred");
      if(interval==null){
        interval = setInterval(reloadCart, 10000);            
      }
      //clearTimeout(typingTimer);
      //typingTimer = setTimeout(doneTyping, 1); 
      //e.stopPropagation();
      
      // if(typingTimer){
      //   clearTimeout(typingTimer);
      //   if(doneTyping()==false){
      //     $("#input_payment_wallet").focus();
      //   }
      // }
        
        
  });
    
$(document).delegate('#button-wallet','click',function (e) {
    //alert($("#input_payment_wallet").val());return false;
    var sisa = eval(order_total - $("#input_payment_wallet").val());
    //alert(sisa);
    if($("#input_payment_wallet").val()>deposit){
      alert('Wallet not enough');
      $("#input_payment_wallet").val(input_payment_wallet);
      $("#input_payment_wallet").focus();
      err = true;
      return false;    
    }

    else if(sisa<50000 && sisa>0 && $("#input_payment_wallet").val()!="0"){
      alert('Sisa minimal IDR 50000');
      $("#input_payment_wallet").val(input_payment_wallet);
      $("#input_payment_wallet").focus();
      err = true;
      return false;
    }    
    else{
      if($("#input_payment_wallet").val()==""){
        $("#input_payment_wallet").val("0");
        $("#input_payment_total").val(order_total);
      }
      else if(sisa<=0 && $("#input_payment_wallet").val()>=order_total){
        $("#input_payment_wallet").val(order_total);
        $("#input_payment_total").val("0");
      } 
      else{
        $("#input_payment_total").val(sisa);        
      }


    setwallet();
    reloadCart();
    err = false;
    return true;

    } 

});



//user is "finished typing," do something
// function doneTyping () {
       

//     var sisa = eval(order_total - $("#input_payment_wallet").val());
//     if($("#input_payment_wallet").val()>deposit){
//       alert('Wallet not enough');
//       $("#input_payment_wallet").val(input_payment_wallet);  
//       $("#input_payment_total").val(input_payment_total); 
//       $("#input_payment_wallet").focus(); 
//       err = true;
//       return false;
//     }

//     else if(sisa<50000 && sisa>0){
//       alert('Sisa minimal IDR 50000');
//       $("#input_payment_wallet").val(input_payment_wallet);  
//       $("#input_payment_total").val(input_payment_total);  
//       $("#input_payment_wallet").focus(); 
//       err = true;
//       return false;
//     }     
//     else {
//       if($("#input_payment_wallet").val()==""){
//         $("#input_payment_wallet").val("0");
//         $("#input_payment_total").val(order_total);
//       }
//       else if(sisa<0 && $("#input_payment_wallet").val()>=order_total){
//         $("#input_payment_wallet").val(order_total);
//         $("#input_payment_total").val("0");

//       } 


//       else{
//         $("#input_payment_total").val(order_total-$("#input_payment_wallet").val());        
//       }
//       setwallet();
//       reloadCart(); 
//       err = false; 
//       return true;      

//     }
// }



//SUBMIT COUPON DISCOUNT
$(document).delegate('#button-voucher', 'click', function() { 
  //recheck payment wallet/normal


  //alert(validateWallet($('#input_payment_wallet')));
  // if(validateWallet($('#input_payment_wallet'))==false){
  //   return false;
  // }

// //on keyup, start the countdown
// $(document).delegate('#input_payment_wallet', 'keyup', function() {
//   clearTimeout(typingTimer);
//   typingTimer = setTimeout(doneTyping, doneTypingInterval);
// });

// //on keydown, clear the countdown 
// $(document).delegate('#input_payment_wallet', 'keydown', function() {
//   clearTimeout(typingTimer);
// });

  // typingTimer = setTimeout(doneTyping, 1);
  // clearTimeout(typingTimer);

  // if(err_wallet || err_payment){
  //   err_wallet = false;
  //   err_payment = false;
  //   return false;
  // }

// if(typingTimer){
// clearTimeout(typingTimer);
// if(doneTyping()==false){
//   return false;
// }

// }

  $('#button-wallet').trigger('click');
  if(err){
    return false; 
  }

  $.ajax({
    url:"index.php?route=checkout/coupon/setwallet",
    type:"post",
    data:{
    input_payment_wallet : $("#input_payment_wallet").val(),
    input_payment_total : $("#input_payment_total").val()
    },
    success:function(data){
      $.ajax({
          url: 'index.php?route=checkout/payment_method',
          data: $('#collapse-coupon input[type=\'text\']'),
          dataType: 'html',
          type:'post',
          complete: function() { $('#button-voucher').button('reset'); },
          success: function(html) {
              $('#collapse-payment-method .panel-body').html(html);


              $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

              $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<a href="#collapse-coupon" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-coupon"><?php echo $text_checkout_coupon; ?> <i class="fa fa-caret-down"></i></a>');


              $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-payment-method"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');


              $('a[href=\'#collapse-payment-method\']').trigger('click');
              active = "collapse-payment-method";

              if(interval!=null){
                console.log("interval stopped");
                clearInterval(interval);
                interval = null;
              } 

          }
      });

    }
  }); 



  
  // alert('xas');return false;

});






//CHOOSE CHECKOUT OPTION GUEST OR REGISTER NEW ACCOUNT OR LOGIN WITH EXISTING ACCOUNT
// Checkout (CHECKOUT OPTIONS CONTINUE) 
$(document).delegate('#button-account', 'click', function() {
    var option = $('input[name=\'account\']:checked').val();  //guest or register
    $.ajax({
        url: 'index.php?route=checkout/guest_shipping',
        dataType: 'html',
        success: function(html) {
            $('.alert, .text-danger').remove();

            $('#collapse-shipping-address .panel-body').html(html);
            $(".checkPhoneMsg .msg").hide();
      if (option == 'register') {
        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
      } else {
        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
      }

      $('a[href=\'#collapse-shipping-address\']').trigger('click');
      active = "collapse-shipping-address";
        }
    });
});


// Login (CHECKOUT OPTIONS LOGIN)
$(document).delegate('#button-login', 'click', function() {
  $.ajax({
      url: 'index.php?route=checkout/login/save',
      type: 'post',
      data: $('#collapse-checkout-option :input'),
      dataType: 'json',
      success: function(json) {
          $('.alert, .text-danger').remove();
          $('.form-group').removeClass('has-error');

          if (json['redirect']) {
              location = json['redirect'];
          } else if (json['error']) {
              $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

			// Highlight any found errors
			$('input[name=\'email\']').parent().addClass('has-error');
			$('input[name=\'password\']').parent().addClass('has-error');
	   }
      }
  });
});




// Payment Address (BILLING DETAILS BUTTON)


// Shipping Address (DELIVERY DETAILS BUTTON CONTINUE)
$(document).delegate('#button-shipping-address', 'click', function() {  
  $.ajax({
    url: 'index.php?route=checkout/shipping_address/save',
    type: 'post',
    data: $('#collapse-shipping-address input[type=\'hidden\'], #collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
    dataType: 'json',
    success: function(json) {
      $('.alert, .text-danger').remove();

      if (json['redirect']) {
        //alert('a');return false;
        location = json['redirect'];
      } else if (json['error']) {
        //alert('b');return false;
        $('#button-shipping-address').button('reset');

        if (json['error']['warning']) {
        $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        for (i in json['error']) {
          var element = $('#input-shipping-' + i.replace('_', '-'));
          if ($(element).parent().hasClass('input-group')) {
            $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
          } else {
            $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
          }
        }

        // Highlight any found errors
        $('.text-danger').parent().parent().addClass('has-error');
      } else {
         //alert('c');return false;
          // Add the shipping address
            $.ajax({
              url: 'index.php?route=checkout/shipping_address',
              dataType: 'html',
              success: function(html) {
              $('#collapse-shipping-address .panel-body').html(html);

              $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

              }
            });



        $.ajax({
          url: 'index.php?route=checkout/shipping_method',
          dataType: 'html',
          success: function(html) {
            $('#collapse-shipping-method .panel-body').html(html);

            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

            $('a[href=\'#collapse-shipping-method\']').trigger('click');
            active = "collapse-shipping-method";

            $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_coupon; ?>');
            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

            $.ajax({
              url: 'index.php?route=checkout/shipping_address',
              dataType: 'html',
              success: function(html) {
                $('#collapse-shipping-address .panel-body').html(html);
              }
            });
          }
        });


      }
    }
  });
});

// Guest


// Guest Shipping
$(document).delegate('#button-guest-shipping', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/guest_shipping/save',
        type: 'post',
        data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
        dataType: 'json',
        success: function(json) {
        $('.alert, .text-danger').remove();

        if (json['redirect']) {
          location = json['redirect'];
        } 
        else if (json['error']) {
          $('#button-guest-shipping').button('reset');
          if (json['error']['warning']) {
            $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }
          for (i in json['error']) {
            var element = $('#input-shipping-' + i.replace('_', '-'));
            if ($(element).parent().hasClass('input-group')) {
              $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
            } else {
              $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
            }
          }

          // Highlight any found errors
          $('.text-danger').parent().addClass('has-error');
        } 
        else {
          $.ajax({
            url: 'index.php?route=checkout/shipping_method',
            dataType: 'html',
            complete: function() { $('#button-guest-shipping').button('reset'); },
            success: function(html) {
            $('#collapse-shipping-method .panel-body').html(html);

            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i>');

            active = "collapse-shipping-method";
            $('a[href=\'#collapse-shipping-method\']').trigger('click');

            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
            }
          });
        }
        }
    });
});

//saving shipping method
$(document).delegate('#button-shipping-method', 'click', function() {
    // alert('rerasdas');return false;
    $.ajax({
        url: 'index.php?route=checkout/shipping_method/save',
        type: 'post',
        data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
        dataType: 'json',
        success: function(json) {
            //console.log(json);
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#button-shipping-method').button('reset');

                if (json['error']['warning']) {
                    $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
            } else {
                $.ajax({
                    url: 'index.php?route=checkout/coupon',
                    dataType: 'html',
                    success: function(html) {
                        //alert(html);
                        $('#collapse-coupon .panel-body').html(html);

                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                        $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<a href="#collapse-coupon" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-coupon"><?php echo $text_checkout_coupon; ?> <i class="fa fa-caret-down"></i></a>');



                        //$('a[href=\'#collapse-coupon\']').trigger('click');
                        active = "collapse-coupon";
                        $('a#btn-collapse-coupon').trigger('click');
                        

                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

                        if(interval==null){                          
                          console.log("interval running");
                          interval = setInterval(reloadCart, 10000);
                        }  

                        


                    }
                });
            }
        }
    });
});







$(document).delegate('#button-apply-coupon', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/coupon/save',
        type: 'post',
        data: $('#collapse-coupon input[type=\'text\']'),
        dataType: 'json',        
        success: function(json) {
            $('.alert, .text-danger').remove();
            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {                
              if (json['error']['warning']) {
                $('#collapse-coupon .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                reloadCart();
              }
            } 
            else {
              if(json['success']){                
                $('#collapse-coupon .panel-body').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> '+ json['success'] +' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');     
                reloadCart();
              }
            }
        }
    });
});












$(document).delegate('.select_option', 'blur', function() {
  $(this).removeClass('highlight');
  if(this.value==""){
    $(this).addClass('highlight');
  }
});



//save payment method
$(document).delegate('.radio-payment','click',function(){
  $.ajax({
    url: 'index.php?route=checkout/payment_method/ajax_save',
    type: 'post',
    data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
    dataType: 'json',
    success: function(json) {
      payment_method = json['payment_method'];
      if(payment_method!=""){
        $('#collapse-payment-method #button-payment-method').removeAttr('disabled');
      }
    }
  });
});




$(document).delegate('a#btn-collapse-shipping-method','click',function(){
  if(active!="collapse-shipping-method"){
    console.log('active collapse shipping method');
    $.ajax({
    url: 'index.php?route=checkout/shipping_method',
    dataType: 'html',
    complete: function() { $('#button-guest').button('reset'); },
    success: function(html) {
    $('#collapse-shipping-method .panel-body').html(html);
    active = "collapse-shipping-method";

    }
    });
  }
    if(interval!=null){
      console.log("interval stopped");
      clearInterval(interval);
      interval = null;
    } 

});


$(document).delegate('a#btn-collapse-coupon','click',function(){
  //alert('terklik');
  if(active!="collapse-coupon"){
    $.ajax({
    url: 'index.php?route=checkout/coupon',
    dataType: 'html',
    success: function(html) {
    $('#collapse-coupon .panel-body').html(html);
    active = "collapse-coupon";

    }
    });
  }
  if(interval==null){
  console.log("interval running");  
  interval = setInterval(reloadCart, 10000);
    
  }


});

$(document).delegate('a#btn-collapse-payment-method','click',function(){
  $.ajax({
  url: 'index.php?route=checkout/payment_method',
  dataType: 'html',
  success: function(html) {
  $('#collapse-payment-method .panel-body').html(html);
  active = "collapse-payment-method";
  }
  });
});


//PROCEED TO BUY (LOGIN)
$(document).delegate('#collapse-payment-method #button-payment-method','click',function(){
  // if(intervalCheck){
  // clearInterval(intervalCheck);  
  // }
  
  $.ajax({
    type: 'post',
    data: $('#collapse-payment-method input[type=\'hidden\'], #collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
    dataType: 'json',
    url: 'index.php?route=checkout/payment_method/confirm',
    cache: false,
    beforeSend: function() { $('#collapse-payment-method #button-payment-method').button('loading'); },
    complete: function() { $('#collapse-payment-method #button-payment-method').button('reset'); },
    success: function(json) {
      $('#collapse-payment-method .panel-body .alert').remove();
      if (json['redirect']) {
        location = json['redirect'];
      } 
      else if(json['error']){
        if (json['error']['warning']) {
        $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
      }
      else if(json['data'] && json['redirect_payment_url']){
        $.redirect(json['redirect_payment_url'],json['data']); 

      }
      else if(json['doku_url']){
        $.redirect(json['doku_url'],json['doku_data']); 
      }
      else if(json['success_url']){
        window.location.href = json['success_url'];
      }
    }
  });
});

//PROCEED TO BUY (NO LOGIN)
$(document).delegate('#collapse-payment-method #button-payment-method-guest','click',function(){
  $.ajax({
    type: 'post',
    data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
    dataType: 'json',
    url: 'index.php?route=checkout/payment_method/confirmguest',
    cache: false,
    beforeSend: function() { $('#collapse-payment-method #button-payment-method-guest').button('loading'); },
    complete: function() { $('#collapse-payment-method #button-payment-method-guest').button('reset'); },
    success: function(json) {
      $('#collapse-payment-method .panel-body .alert').remove();
      if (json['redirect']) {
          location = json['redirect'];
      } 
      else if(json['error']){
        if (json['error']['warning']) {
        $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
      }
      else if(json['data'] && json['redirect_payment_url']){
        $.redirect(json['redirect_payment_url'],json['data']); 

      }
      else{
        $.redirect(json['doku_url'],json['doku_data']); 
      }
    }
  });
});







//CHECK IF PURCHASE MADE BEFORE, THEN TAKE THE INFOMATION FROM DB
$(document).delegate("#btn-check-phone","click",function(){
    if($('#input-shipping-telephone').val()==""){
      return false;
    }
    $.ajax({
        url: 'index.php?route=checkout/guest/check',
        type: 'post',
        data: $('#input-shipping-telephone'),
        dataType: 'json',
        beforeSend: function() { $('#guest-check').button('loading'); },
        complete: function() { $('#guest-check').button('reset'); },
        success: function(jsonGuest) {
          var jsonEmpty = $.isEmptyObject(jsonGuest);
          if(jsonGuest && jsonEmpty==false){
              $('#input-shipping-firstname').val(jsonGuest.firstname);
              $('#input-shipping-lastname').val(jsonGuest.lastname);
              $('#input-shipping-email').val(jsonGuest.email);
              $('#input-shipping-address-1').val(jsonGuest.address);
              $('#input-shipping-postcode').val(jsonGuest.postcode);
              $('#input-shipping-country').val(jsonGuest.country);

              $('#collapse-shipping-address select[name=\'country_id\']').val(jsonGuest.country_id);

              $.ajax({
                url: 'index.php?route=checkout/checkout/country&country_id=' + jsonGuest.country_id,
                dataType: 'json',
                success: function(json) {
                  html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';

                  if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                      html += '<option value="' + json['zone'][i]['zone_id'] + '"';
                      if (json['zone'][i]['zone_id'] == jsonGuest.zone_id) {
                        html += ' selected="selected"';
                      }
                      html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                  } else {
                    html += '<option value="" selected="selected"><?php echo $text_select; ?></option>';
                  }
                  $('#collapse-shipping-address select[name=\'zone_id\']').html(html);

                  $.ajax({
                    url: 'index.php?route=account/account/zone&zone_id=' + jsonGuest.zone_id,
                    dataType: 'json',
                    success: function(json) {
                      html = '<option value="" selected="selected"><?php echo $text_select; ?></option>';
                      if (json['city'] && json['city'] != '') {
                        for (i = 0; i < json['city'].length; i++) {
                          html += '<option value="' + json['city'][i]['city_id'] + '"';
                          if (json['city'][i]['city_id'] == jsonGuest.id_city) {
                            html += ' selected="selected"';
                          }
                          html += '>'+json['city'][i]['type']+' '+ json['city'][i]['city_name'] + '</option>';
                        }
                      } else {
                        html += '<option value="" selected="selected"><?php echo $text_select; ?></option>';
                      }
                      $('#collapse-shipping-address select[name=\'id_city\']').html(html);
                      if(jsonGuest.id_city){
                        $('#collapse-shipping-address select[name=\'id_city\']').trigger('change');
                      }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                  });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
          }
          else{
            if ($(".checkPhoneMsg .msg").is(":visible")) { return; }
            $(".checkPhoneMsg .msg").show();
            setTimeout(function() {
            $(".checkPhoneMsg .msg").hide();
            }, 1500);

            
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});











$(document).delegate('.number','keydown',function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
















<?php if (!$logged) { ?>
  $(document).ready(function() {
      $(".checkPhoneMsg .msg").hide();
      $.ajax({
          url: 'index.php?route=checkout/login',
          dataType: 'html',
          success: function(html) {
             $('#collapse-checkout-option .panel-body').html(html);

        $('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_option; ?> <i class="fa fa-caret-down"></i></a>');

        $('a[href=\'#collapse-checkout-option\']').trigger('click');
        active = "collapse-checkout-option";
          }
      });
  });
<?php 
} 
else { ?>
$(document).ready(function() {
  $(".checkPhoneMsg .msg").hide();
  ///check if cart has change
  // intervalCheck = setInterval(function(){
  //   $.ajax({
  //     url:"index.php?route=checkout/checkout/check",
  //     data:{},
  //     type:"get",
  //     success:function(data){
  //       console.log(data);
  //       // if(data==1){
  //       //   alert('You have modified your cart, the page will be refreshed');     
  //       //   window.location.reload();
  //       // }
  //     }
  //   });

  // }, 15000);

  <?php 
  if($address_id!=0){
  ?>
      $.ajax({
        url: 'index.php?route=checkout/shipping_address/save',
        type: 'post',
        data: {
          shipping_address : "existing",
          address_id:"<?php echo $address_id?>",
          firstname:"<?php echo $firstname?>",
          lastname:"<?php echo $lastname?>",
          company:"<?php echo $company?>",
          address_1:"<?php echo $address_1?>",
          address_2:"<?php echo $address_2?>",
          city:"<?php echo $city?>",
          postcode:"<?php echo $postcode?>",
          country_id:"<?php echo $country_id?>",
          zone_id:"<?php echo $zone_id?>",
          id_city:"<?php echo $id_city?>"
        },
        success: function(json) {
          if (json['redirect']) {
            location = json['redirect'];
          } else if (json['error']) {
            $('#button-shipping-address').button('reset');

            if (json['error']['warning']) {
              $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

            for (i in json['error']) {
              var element = $('#input-shipping-' + i.replace('_', '-'));
              if ($(element).parent().hasClass('input-group')) {
                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
              } else {
                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
              }
            }

            // Highlight any found errors
            $('.text-danger').parent().parent().addClass('has-error');
          } else {

          }
        }
      });
      

      $.ajax({
        url: 'index.php?route=checkout/payment_address/save',
        type: 'post',
        data: {
          payment_address : "existing",
          address_id:"<?php echo $address_id?>",
          firstname:"<?php echo $firstname?>",
          lastname:"<?php echo $lastname?>",
          company:"<?php echo $company?>",
          address_1:"<?php echo $address_1?>",
          address_2:"<?php echo $address_2?>",
          city:"<?php echo $city?>",
          postcode:"<?php echo $postcode?>",
          country_id:"<?php echo $country_id?>",
          zone_id:"<?php echo $zone_id?>",
          id_city:"<?php echo $id_city?>"
        },
        success: function(json) {
          $('.alert, .text-danger').remove();

          if (json['redirect']) {
            location = json['redirect'];
          } else if (json['error']) {
            $('#button-shipping-address').button('reset');

            if (json['error']['warning']) {
            $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }

            for (i in json['error']) {
              var element = $('#input-shipping-' + i.replace('_', '-'));
              if ($(element).parent().hasClass('input-group')) {
                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
              } else {
                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
              }
            }

            // Highlight any found errors
            $('.text-danger').parent().parent().addClass('has-error');
          } else {

          }
        }
      });

      //and open shipping method tab
      $.ajax({
      url: 'index.php?route=checkout/shipping_method',
      dataType: 'html',
      complete: function() { $('#button-shipping-address').button('reset'); },
      success: function(html) {
        $('#collapse-shipping-method .panel-body').html(html);

        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle" id="btn-collapse-shipping-method"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

        $('a[href=\'#collapse-shipping-method\']').trigger('click');
        active = "collapse-shipping-method";

        $('#collapse-coupon').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_coupon; ?>');
        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');

        $.ajax({
          url: 'index.php?route=checkout/shipping_address',
          dataType: 'html',
          success: function(html) {
          $('#collapse-shipping-address .panel-body').html(html);
          }
        });
      }
      });
    <?php
  }
  //no default address
  else{ 
  ?>
    $.ajax({
      url: 'index.php?route=checkout/shipping_address',
      dataType: 'html',
      success: function(html) {
      $('#collapse-shipping-address .panel-body').html(html);

      $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

      $('a[href=\'#collapse-shipping-address\']').trigger('click');
      active = "collapse-shipping-address";

      $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
      $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
      }
    });

  <?php    
  }?> 


});
<?php 
} 
?>



function validateWallet(obj){
  if($(obj).val() > deposit){
    return false;
  }

  if($(obj).val()=="" || $(obj).val()=="0"){
    return true;
  }
  else{
    <?php
    if(isset($input_payment_wallet) && isset($input_payment_total)){
      ?>
      if($(obj).val()>=order_total){
          return true;
      }
      else{
        var sisa = eval(order_total - $(obj).val());
        if(sisa<50000){
          return false;
        }
        else{
          return true;
       
        }
      }
      <?php
    }
    else{
      ?>
      return true;
      <?php
    }
    ?>
  }
  return true;
}


//other functions
function setwallet(){
  $.ajax({
  url:"index.php?route=checkout/coupon/setwallet",
  type:"post",
  data:{
  input_payment_wallet : $("#input_payment_wallet").val(),
  input_payment_total : $("#input_payment_total").val()
  },
  success:function(data){ }
  }); 
}

function reloadCart(){   
    $.ajax({
      url: 'index.php?route=checkout/shipping_method',
      dataType: 'html',
      success: function(html) {
        $('#collapse-shipping-method .panel-body').html(html);
        $.ajax({
            url: 'index.php?route=checkout/shipping_method/save',
            type: 'post',
            data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
            dataType: 'json',
            success: function(json) {
                //console.log(json);
                $.ajax({
                  url: 'index.php?route=checkout/coupon/reload',
                  dataType: 'html',
                  success: function(html) {
                    $('#collapse-coupon .data-cart').html(html);                    
                  },
                });

            }
        });
      }
    });
}

function removeCart(key){
    $.ajax({
      url: 'index.php?route=checkout/cart/remove',
      type: 'post',
      data: 'key=' + key,
      dataType: 'json',
      success: function(json) {
        reloadCart();
      }
    });  
}

//--></script>

<?php echo $footer; ?>
<style type="text/css">
.highlight{
  border: #FF0000 1px solid;
}





</style>