<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($products) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-center"><?php echo $column_image; ?></td>
              <td class="text-left"><?php echo $column_name; ?></td>
              <?php /*<td class="text-left"><?php echo $column_model; ?></td>*/?>
              <td class="text-right"><?php echo $column_stock; ?></td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right" width="100px"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="text-center" ><?php if ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                <?php } ?>

                
              </td>
              <td class="text-left">
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <div class="alert-wishlist"></div>
              </td>
              <?php /*<td class="text-left"><?php echo $product['model']; ?></td>*/?>
              <td class="text-right"><?php echo $product['stock']; ?></td>
              <td class="text-right">
                <?php /*if ($product['price']) { ?>
                <div class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s>
                  <?php } ?>
                </div>
                <?php }*/ ?>






<?php if ($product['price']) { ?>
<div class="harga" style="text-align:left;">
<?php
if($product['discount']){
  ?>
  <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
  <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $product['price_before']; ?></span>
  </span>
  <br/>                                    
  <span class="final-price"><?php echo $product['price']; ?></span>
  <?php
}
else{
  ?>
  <span class="final-price"><?php echo $product['price']; ?></span>
  <?php
}
?>
</div>
<div class="clearfix"></div>

<div class="clearfix"></div>
<?php } ?>   


              </td>
              <td class="text-right">
                <input type="image" src="<?php echo $btn_cart; ?>" alt="" class="img-responsive" onclick="cart.add('<?php echo $product['product_id']; ?>', '1','wishlist',this);" style="margin-bottom:3px;" />  

                <a href="<?php echo $product['remove']; ?>" title="<?php echo $button_remove; ?>" >
                  <img src="<?php echo $btn_remove?>">
                </a>
                <?php
                /*
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '1','wishlist',this);" title="<?php echo $button_cart; ?>" class="btn btn-cart"><i class="fa fa-shopping-cart"></i></button>
                <a href="<?php echo $product['remove']; ?>" title="<?php echo $button_remove; ?>" class="btn btn-remove"><i class="fa fa-times"></i></a>

                */

                ?>   
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<style type="text/css">
.btn-cart, .btn-remove{
  background: #FF3399;
  border: none;
  color:white;
}
.btn-cart:hover, .btn-remove:hover{
  background: #FF3399;
  color: white;

}
.alert-wishlist{
  height: 22px;
font-size: smaller;

}
.alert-wishlist div{
background: #ffc2e3;

}
.alert-wishlist .alert{
  padding: 0;
  margin:0;
  padding: 1px 2px;

}
a:hover{
  color: #FF3399;
}

.alert-success{
  background: #ffc2e3;color:#fc3a99;
}
</style>
<?php echo $footer; ?>
