<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_password; ?></legend>



          <input type="hidden" name="verified" id="verified" value="">
          <div class="form-group required">
            
            <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
            <div class="col-sm-10">
<div class="input-group">
  <input type="tel" name="telephone" class="form-control numeric" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" value="<?php echo $telephone; ?>" readonly>



  <span class="input-group-btn">
    <button class="btn btn-default btn-verify" type="button">
        Verify
    </button>
  </span>

</div>
<div class="verify_phone" style="margin-top:5px;display:none;">
  <div class="verify-wrapper">
              <span class="prenumber"></span>&nbsp;
              <input type="text" size="1" maxlength="1" name="token1" value="" placeholder="" id="input-token1" class="text-center autotab numeric"  />
              <input type="text" size="1" maxlength="1" name="token2" value="" placeholder="" id="input-token2" class="text-center autotab numeric"  />
              <input type="text" size="1" maxlength="1" name="token3" value="" placeholder="" id="input-token3" class="text-center autotab numeric" />
              <input type="text" size="1" maxlength="1" name="token4" value="" placeholder="" id="input-token4" class="text-center autotab numeric"  />
              <input type="text" size="1" maxlength="1" name="token5" value="" placeholder="" id="input-token5" class="text-center autotab numeric"  />&nbsp;  
              <button type="button" class="btn btn-default btn-xs btn-submit-token">SUBMIT</button>
  </div>
  
  <div class="verify_message"></div>
</div>
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>
            </div>
          </div>


          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-10">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <?php /*<div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>*/?>
          <div class="pull-right">
            <input type="image" src="<?php echo $btn_save; ?>" alt="Submit Form" class="img-responsive" />  
            
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).delegate(".autotab", "keyup", function(e){
//$(".autotab").keyup(function(e){
  // alert(e.which);
  var val = $(this).val();
  var id = $(this).attr('id');
  var num = id.substring(11, 12);
  var next = (parseInt(num)+1);
  var keyCode = e.keyCode || e.which;    


    if(val!=""){

      if((e.shiftKey && e.keyCode == 9)===false){
        if(num<$('.autotab').length){
          if($('#input-token'+next).val()==""){
            //alert('maju');
            $('#input-token'+next).focus();
          }
        }
      }

    }

  return false;

  //alert('x');
});




$(document).delegate(".numeric", "keydown", function(e){

    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});




$(document).delegate(".btn-verify", "click", function(e){
  if($("#input-telephone").val()!=""){
  //$('.btn-verify').click(function(){
    if($(".verify_phone").is(":visible")){
      $('.verify_phone').fadeOut();
    }

  $.ajax({
  url:"<?php echo $action_request?>",
  data:{telephone:$("#input-telephone").val()},
  type:"post",
  dataType:"json",
  beforeSend: function() { 
    $('button.btn-verify').button('loading'); 
  },
  complete: function() { $('button.btn-verify').button('reset'); },
  success:function(data){
  //alert(data.status);
  if(data.status=="1"){ 
    //sukses
var res = data.prenumber.split("");
var html = "";
for (var i=0; i<res.length; i++) {
html += '<input type="text" size="1" maxlength="1" value="'+res[i]+'" class="text-center autotab numeric" disabled />';
}
$(".prenumber").html(html);
    // $(".prenumber").text(data.prenumber);
    $('.verify_phone').fadeIn();
    e.preventDefault();
  }
  else if(data.status=="0"){
    //error / failed
    $(".prenumber").text(data.prenumber);
    $('.verify_phone').html('<div class="alert alert-warning">'+data.message+'</div>').fadeIn();

  }
  else if(data.status=="2"){
    //phone used by another account
    $(".prenumber").text(data.prenumber);
    $('.verify_phone').html('<div class="alert alert-warning">'+data.message+'</div>').fadeIn();

  }
  }
  });


  

  }
  return false;



});


$(document).delegate(".btn-submit-token", "click", function(){
  var token = $("#input-token1").val()+$("#input-token2").val()+$("#input-token3").val()+$("#input-token4").val()+$("#input-token5").val();
  $('.verify_message').html('');
  $.ajax({
  url:"<?php echo $action_verify?>",
  data:{token:token},
  type:"post",
  dataType:"json",
  // beforeSend: function() { $('button.btn-submit-token').button('loading'); },
  // complete: function() { $('button.btn-submit-token').button('reset'); },
  success:function(data){
  if(data.status=="1"){ 
      $("#input-telephone").attr('readonly','readonly');
      $("button.btn-verify").text('Verified');
      $("button.btn-verify").prop("disabled", true);
      // $("button.btn-submit-token").prop("disabled", true);
      $("#verified").val("1");
      $('.verify-wrapper').fadeOut();

      //verified

  }
  else{
    $('.verify_message').html(data.message).fadeIn();

  }
  }
  });
  return false;

});

</script>