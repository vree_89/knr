<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $text_address_book; ?></h2>
      <?php if ($addresses) { ?>
      <div class="table-responsive">
        
        <table class="table">
          <?php foreach ($addresses as $result) { 
            $default = 0;
            if($result['address_id'] == $default_addres_id){
              $default = 1;
            }

            ?>
          <tr <?php echo $result['class']?>>
            <td class="text-left"><?php echo $result['address']; ?></td>
            <td class="text-right" style="vertical-align:middle;">
              <a href="<?php echo $result['default']; ?>"><img src="<?php echo $btn_setdefault?>"></a> &nbsp;
              <a href="<?php echo $result['update']; ?>" ><img src="<?php echo $btn_update?>"></a> &nbsp; 
              <a href="<?php echo $result['delete']; ?>" <?php echo $default ? 'onclick="alert(\''.$text_warning_delete.'\');return false;"' : 'onclick="return confirm(\'Delete this address?\')"' ?>><img src="<?php echo $btn_delete?>"></a>
            </td>
          </tr>
          <?php } ?>
        </table>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <?php /*<div class="pull-left"><a href="<?php echo $back; ?>"><img src="<?php echo $btn_back?>"></a></div>*/?>
        <div class="pull-right"><a href="<?php echo $add; ?>"><img src="<?php echo $btn_add?>"></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<style type="text/css">
.default-address{
  background: #f3f3f3;  
  font-weight: bold;
}
.alert-success{
  background: #ffc2e3;color:#fc3a99;
} 
</style>