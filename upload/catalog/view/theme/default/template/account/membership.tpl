<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

  <?php
  /*Membership Information*/
  if($membership_info){
    ?>
    <div class="row">
      <div class="col-md-12 pull-right text-right" style="margin-bottom:15px;">
        
        <div><b style="font-size:larger;">Your Membership</b> : <?php echo $membership_info['membership_name']?> (expired : <?php echo $membership_info['membership_expired']?>)</div>
      </div> 
    </div>
    <?php
  }
  ?>
    <div id="content" class="<?php echo $class?>">
      <h3>Membership History</h3>
      <?php
      foreach ($memberships as $result) {
        ?>
      <div class="membership_history_list <?php echo $result['status']=="1" ? "paid" : "unpaid"?>">
        <h3>
          #MBR-<?php echo $result['customer_membership_id']?>
          <?php echo $result['name']?></h3>
        <div class="request_date">Order Date : <?php echo $result['date_added']?></div>        
        <div class="payment_method">Payment Method : <?php echo $result['payment_method']?></div>
        <div class="payment_total">Payment Total : <?php echo $result['payment_total']?></div>
        <div class="payment_date">Payment Date : <?php echo $result['payment_date']=="" ? '-' : $result['payment_date']  ?></div>
        <div class="payment_code">Payment Code : <?php echo str_replace("_"," ",$result['payment_code'])?></div>
          <div class="status"><?php echo $result['status']=="1" ? "PAID" : "UNPAID"?></div>
      </div>
        <?php
        # code...
      }
      if(count($memberships)<=0){ 
        ?>
        No membership request history
        <?php

      }
      else{
        ?>
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        <?php
      }
      ?>


    </div>



    <?php echo $column_right; ?>
    </div>
</div>
<style type="text/css">
.paid{
background: #fc9fd1;

}
.unpaid{
background: #FFE3DF;
}


.membership_history_list{
  min-height: 100px;
  margin-bottom: 10px;
  border-bottom: 1px dotted #c0c0c0;
  //background: #FFF3FF;
  position: relative;
  padding: 10px;
  padding: 5px;
  background: #fedff0;
}
.membership_history_list div{
  line-height: 12px;
}
.membership_history_list .request_date{
  font-size: smaller;
}

.membership_history_list .status{      
  position: absolute;
  right:10px;
  top:10px;
  float: left;
  background: rgba(255,020,147,.5);
  background: rgba(255,0,0,0.3);
  background: #ff7ec4;
  padding: 15px 40px;
  text-align: center;
  font-size: 100%;
  color:white;
  font-weight: bold;
  border-radius:5px;
  font-size: smaller;
}
.disc{
  position: absolute;
  right: 0;
  top:0;
  float: left;
  background: rgba(255,020,147,.5);
  background: rgba(255,0,0,0.3);
  padding: 15px 0px;
  text-align: center;
  font-size: 100%;
  color:white;
  font-weight: bold;
  width: 50px;
  height: 50px;
}   

.package-list{
  //background: red;
  margin-bottom: 20px;
}
.package-list .package-inner{
  padding: 5px 10px;
  background: #FAEBD7;
  box-shadow: 10px 10px 5px #888888;
}
.package-list .package-inner h3{
  font-weight: bold;
  }
</style>
<?php echo $footer; ?>