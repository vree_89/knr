<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>

  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <div id="content" class="<?php echo $class; ?>">
        <?php //echo $content_top; ?>

        <div class="row">
          <div class="col-sm-12 account_summary">
            <h2><?php echo $text_my_account?></h2>

            <p class="heading"><?php echo $text_category_account?></p>
            <p><small class="text-muted contact">Name</small> <?php echo $customer_info['firstname'] .' '. $customer_info['lastname']; ?></p>
            <p><small class="text-muted contact">Email</small> <?php echo $customer_info['email']; ?></p>
            <p><small class="text-muted contact">Phone</small> <?php echo $customer_info['telephone']; ?></p>
            <p><small class="text-muted contact">Point</small> <?php echo $reward_total; ?> <!-- <a href="redeem.html"><small>(Redeem Now)</small></a> --></p>
            <p><small class="text-muted contact">Wallet</small> <?php echo number_format($customer_info['deposit']); ?></p>

            <br/>

            <p class="heading"><?php echo $text_category_membership?></p>
            <?php 
            $membership_name = isset($membership_info['membership_name']) ? $membership_info['membership_name'] : 'Non-Membership';
            $membership_expired = !isset($membership_info['membership_name']) ? '-' : date('d F Y - H:i:s', strtotime($customer_info['membership_expired']));
            ?>
            <p><small class="text-muted contact">Name</small> <?php echo $membership_name; ?></p>
            <p><small class="text-muted contact">Expired</small> <?php echo $membership_expired; ?></p>

            <br/>

            <p class="heading"><?php echo $text_category_address?></p>
            <?php if ($address_info) { ?>
            <p>
            <?php echo $address_info['address_1'].', '.$address_info['city'].', '.$address_info['zone'].', '.$address_info['country'] ; ?> 
            <br> 
            <?php echo $address_info['postcode']; ?>
            </p>
            <?php } else { ?>
            <p>You have no addresses in your account. <a href="<?php echo $address; ?>">Click here to add address</a>.</p>
            <?php } ?>

            <br/>

          <p class="heading"><?php echo $text_category_referal?></p>
          <div class="input-group">
            <input type="text" id="referal_link" class="form-control" value="<?php echo $referal_link; ?>" readonly>
            <span class="input-group-btn">
              <button id="copy_btn" class="btn btn-default" type="button">Copy</button>
            </span>
          </div><br>
          <!-- <div class="share">
            <div class="sharethis-inline-share-buttons" data-url="<?php //echo $referal_link; ?>"></div>
          </div> -->

          </div>

        </div>


      <?php //echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?> 
  <script>
    $(document).ready(function() {
      var copyBtn = document.querySelector('#copy_btn');

      copyBtn.addEventListener('click', function(event) {
        var referalLink = document.querySelector('#referal_link');
        referalLink.select();

        try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Copying text command was ' + msg);
          $("#referal_link").parent().after('<div class="alert alert-success col-md-4" style="margin-top:5px;background: #ffc2e3;color:#fc3a99;"><i class="fa fa-check-circle"></i> Copied to your clipboard</div>');
          $('.alert').delay(1250).fadeOut();
        } catch (err) {
          console.log('Oops, unable to copy');
        }
      });
    });

  </script>
  <style type="text/css">
.alert-success{
  background: #ffc2e3;color:#fc3a99;
} 
  </style>
  <?php
  /*
          <div class="col-sm-6">
            <h2><?php echo $text_my_account; ?></h2>
            <ul class="list-unstyled">
            <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
            <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
            <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            <li><a href="<?php echo $membership; ?>"><?php echo $text_membership; ?></a></li>
            </ul>

            <?php if ($credit_cards) { ?>
            <h2><?php echo $text_credit_card; ?></h2>
            <ul class="list-unstyled">
            <?php foreach ($credit_cards as $credit_card) { ?>
            <li><a href="<?php echo $credit_card['href']; ?>"><?php echo $credit_card['name']; ?></a></li>
            <?php } ?>
            </ul>
            <?php } ?>

            <h2><?php echo $text_my_orders; ?></h2>
            <ul class="list-unstyled">
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <?php if ($reward) { ?>
            <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
            <?php } ?>
            <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
            </ul>

            <h2><?php echo $text_my_newsletter; ?></h2>
            <ul class="list-unstyled">
            <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
            </ul>
          </div>
  */
  ?>