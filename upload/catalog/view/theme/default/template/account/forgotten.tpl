<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <?php if ($success_miscall) { ?>
  <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success_miscall; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_phone; ?></p>
      <?php
        ?>
        <div class="forgotten_message"></div>
      <div class="form-request">
      <!-- <form action="<?php //echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal"> -->






























































      <form class="form1 form-horizontal" onsubmit="return submitRequest();">
        <?php
        /*
        <fieldset>
          <legend><?php echo $text_your_email; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
        </fieldset>
        */
        ?>
        <fieldset>
          <legend><?php echo $text_your_phone; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_phone; ?></label>
            <div class="col-sm-10">
              <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_phone; ?>" id="input-telephone" class="form-control numeric" required="required"/>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"></div>
          <div class="pull-right">
            <input type="image" src="<?php echo $btn_continue?>" alt="Continue" class="btn-request">
            
          </div>
        </div>
      </form>

      </div>

      <div class="form-verify"  style="display:none;" >
      <form class="form2 form-horizontal">
        <fieldset>
          <legend>Phone Number</legend>
          <div class="form-group">
            
            <div class="col-sm-12 input-token" >              
              <span class="prenumber"></span>&nbsp;&nbsp;<input type="text" size="1" maxlength="1" name="token1" value="" placeholder="" id="input-token1" class="text-center autotab numeric" required="required" />
              <input type="text" size="1" maxlength="1" name="token2" value="" placeholder="" id="input-token2" class="text-center autotab numeric" required="required" />
              <input type="text" size="1" maxlength="1" name="token3" value="" placeholder="" id="input-token3" class="text-center autotab numeric" required="required" />
              <input type="text" size="1" maxlength="1" name="token4" value="" placeholder="" id="input-token4" class="text-center autotab numeric" required="required" />
              <input type="text" size="1" maxlength="1" name="token5" value="" placeholder="" id="input-token5" class="text-center autotab numeric" required="required" />
            </div>
          </div>
        </fieldset>

        <div class="buttons clearfix">
          <div class="pull-left"></div>
          <div class="pull-right">
            <input type="image" src="<?php echo $btn_continue?>" alt="Continue" class="btn-verify">
            
          </div>
        </div>
      </form>



      </div>

        <?php
      
      ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script type="text/javascript" src="mobile/view/theme/default/assets/js/jquery.redirect.js"></script>
    

<script type="text/javascript">
$(".numeric").keydown(function (e) {
  
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

$(".autotab").keyup(function(e){
  // alert(e.which);
  var val = $(this).val();
  var id = $(this).attr('id');
  var num = id.substring(11, 12);
  var next = (parseInt(num)+1);
  var keyCode = e.keyCode || e.which;    


    if(val!=""){

      if((e.shiftKey && e.keyCode == 9)===false){
        if(num<$('.autotab').length){
          if($('#input-token'+next).val()==""){
            //alert('maju');
            $('#input-token'+next).focus();
          }
        }
      }

    }

  return false;

  //alert('x');
});
function submitRequest(){

$.ajax({
url:"<?php echo $action_request?>",
data:{telephone:$("#input-telephone").val()},
type:"post",
dataType:"json",
beforeSend: function() { $('.btn-request').button('loading'); },
complete: function() { $('.btn-request').button('reset'); },
success:function(data){
//alert(data.status);
if(data.status=="1"){ 
$(".forgotten_message").html('<div class="alert alert-success">'+data.message+'</div>');

var res = data.prenumber.split("");
var html = "";
for (var i=0; i<res.length; i++) {
html += '<input type="text" size="1" maxlength="1" value="'+res[i]+'" class="text-center autotab numeric" disabled />';
}
$(".prenumber").html(html);

$(".form-request").hide();
$(".form-verify").show();
$(".form2").attr('onsubmit','return submitVerify();');



}
else if(data.status=="0"){
$(".prenumber").text(data.prenumber);  
$(".forgotten_message").html('<div class="alert alert-warning">'+data.message+'</div>');

}
else if(data.status=="2"){
$(".prenumber").text(data.prenumber);  
$(".forgotten_message").html('<div class="alert alert-warning">'+data.message+'</div>');

}

else if(data.status=="3"){
$(".prenumber").text(data.prenumber);  
$(".forgotten_message").html('<div class="alert alert-warning">'+data.message+'</div>');

}


}
});


return false;


}


function submitVerify(){
  var token = $("#input-token1").val()+$("#input-token2").val()+$("#input-token3").val()+$("#input-token4").val()+$("#input-token5").val();
  $(".forgotten_message").html('');
  $.ajax({
  url:"<?php echo $action_verify?>",
  //data:{token:$("#input-token").val()},
  data:{token:token},
  type:"post",
  dataType:"json",
beforeSend: function() { $('.btn-verify').button('loading'); },
complete: function() { $('.btn-verify').button('reset'); },
  success:function(data){
  // alert(data.redirect);return false;
  if(data.status=="1"){ 
    //$.redirect(data.redirect); 
    window.location.href = data.redirect;
    //$(".forgotten_message").html('<div class="alert alert-success">'+data.message+'</div>');


  $(".form-request").hide();
  $(".form-verify").show();
  }
  else{
  $(".forgotten_message").html('<div class="alert alert-warning">'+data.message+'</div>');

  }
  }
  });
  return false;

}
</script>
<style type="text/css">
.prenumber input{
  font-size: small;
  padding:5px 1px;

}

.input-token{
  font-size: x-large;
}
.input-token input{
  font-size: small;
  padding:5px 1px;

}
.prenumber{
  font-size: x-large;
}
.alert-success{
  background: #ffc2e3;
}

</style>