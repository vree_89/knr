<?php echo $header; ?>
<div class="container best-seller-area another">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($thumb || $description) { ?>
      <div class="row">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <hr>
      <?php } ?>

      <?php if ($products){ ?>
      <div class="row">
        <div class="col-md-5 col-sm-12"></div>
        <div class="col-md-4 col-xs-12">
          <div class="form-group input-group input-group-sm">
            <label class="input-group-addon" for="input-sort"><?php echo $text_sort; ?></label>
            <select id="input-sort" class="form-control" onchange="location = this.value;">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="form-group input-group input-group-sm">
            <label class="input-group-addon" for="input-limit"><?php echo $text_limit; ?></label>
            <select id="input-limit" class="form-control" onchange="location = this.value;">
              <?php foreach ($limits as $limits) { ?>
              <?php if ($limits['value'] == $limit) { ?>
              <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
      </div> 
      <style type="text/css">
      .out-of-stock{
        position: absolute;
        left: 0;
        top:50%;
        float: left;
        width: 100%;
        //background: rgba(0,0,0,.5);
        background: rgba(255,020,147,.5);
        padding: 15px 0px;
        text-align: center;
        font-size: 150%;
        color:white;
        font-weight: bold;

      }
      .disc{
        position: absolute;
        right: 0;
        top:0;
        float: left;
        background: rgba(255,020,147,.5);
        background: rgba(255,0,0,0.3);
        padding: 15px 0px;
        text-align: center;
        font-size: 150%;
        color:white;
        font-weight: bold;
        width: 50px;
        height: 50px;
        text-align: center; 

      }       
      </style>  

                <div class="row">
                    <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="best-seller">                            
                        <div class="">
                            <?php foreach ($products as $product) { ?>              

                                <div class="col-md-3" style="margin-bottom:15px;border:none;">
                                  
                                <div class="single-product" style="border:none;">
                                  
<div class="product-img" style="border:none;">
<a class="" href="<?php echo $product['href']; ?>">
<img class="primary-img" src="<?php echo $product['thumb']; ?>" alt="">
<?php
if($product['special']){
?>
<div class="special"></div>            
<?php  
}
if($product['quantity']<=0){
?>
<div class="out-of-stock"><?php echo $product['stock_status']?></div>            
<?php
}
else if($product['is_preorder']){
?>
<div class="out-of-stock"><span style="font-size:smaller;">Date Available:<?php echo $product['date_available']?></span></div>
<?php
}?>                                
</a>
</div>
                                <div class="product-block-text">
<div style="height:37px;width:100%;padding:0 2%;display:table;text-align:left;" >
  <div style="line-height:10px;vertical-align:middle;display: table-cell;" >
    <div style="padding-bottom:2px;" class="product-manufacturer"><b ><?php echo $product['manufacturer']; ?></b></div>
    <div class="product-name"><?php echo ucwords(strtolower($product['name'])); ?></div>
  </div>
</div>
                                <?php if ($product['rating']) { ?>
                                <div class="rating" style="display:inline-block;margin-bottom:0;">
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } else { ?>
                                <span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star fa-stack-2x" style="color:#FF3399 ;"></i><i class="fa fa-star-o fa-stack-2x" style="color:#FF3399 ;"></i></span>
                                <?php } ?>
                                <?php } ?>
                                </div>
                                <?php 
                                }
                                else{
                                  ?>
                                <div class="rating" style="display:inline-block;margin-bottom:0;">
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star-o fa-stack-2x"></i></span>

                                <?php } ?>
                                </div>
                                  <?php
                                }
                                ?>
                                <div class="clearfix"></div>


                               <!--  <h3 style="height:40px;width:100%;padding:2%;line-height:13px;display:table;text-align:center;"><a href="#" style="font-size:70%;vertical-align:middle;display: table-cell;text-align:center;font-weight:bold;" ><?php //echo $product['name']; ?> <?php //echo $product['manufacturer']; ?></a></h3> -->
                                <div class="clearfix"></div>
                                <?php if ($product['price']) { ?>


  <div style="min-height:40px;">
    <?php
    if($product['special']){
      ?>
      <div class="potongan_persen">
      <?php
      if($product['discount']){
      ?><?php echo $product['discount'];?>%<?php
      }
      ?>
      </div>
      <div class="harga" style="text-align:left;">
      <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
      <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $product['price_before']; ?></span>
      </span>                                    
      <span class="final-price"><?php echo $product['special']; ?></span>

      </div>
      <?php
    }
    else{
      ?>
      <div class="potongan_persen">
      <?php
      if($product['discount']){
      ?><?php echo $product['discount'];?>%<?php
      }
      ?>
      </div>
      <div class="harga" style="text-align:left;">
      <?php
      if($product['discount']){
      ?>
      <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
      <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $product['price_before']; ?></span>
      </span>                                    
      <span class="final-price"><?php echo $product['price']; ?></span>
      <?php
      }
      else{
      ?>
      <span class="final-price"><?php echo $product['price']; ?></span>
      <?php
      }
      ?>
      </div>
      <div class="clearfix"></div>
      <?php

    }

    ?>

  </div>
                                
                                <?php } ?>   
                                <div class="clearfix"></div>



<?php

if($product['quantity']<=0){
  ?>
<a class="p-b-t-a-c" style="cursor:pointer;" style="font-size:80%;padding:0;margin:0;padding:5px 10px;margin-bottom:10px;margin-top:0;" onclick="return false;">add to cart</a>
  <?php 
  

  
}
else if($product['is_preorder']){
  ?>

<a class="p-b-t-a-c" style="cursor:pointer;" style="font-size:80%;padding:0;margin:0;padding:5px 10px;margin-bottom:10px;margin-top:0;" onclick="cart.add('<?php echo $product['product_id']; ?>', 1,'home',this);">Pre Order</a>

  <?php
}
else{
?>
<a class="p-b-t-a-c" style="cursor:pointer;" style="font-size:80%;padding:0;margin:0;padding:5px 10px;margin-bottom:10px;margin-top:0;" onclick="cart.add('<?php echo $product['product_id']; ?>', 1,'home',this);">add to cart</a>
<?php  
}
?>

                                </div>
                                </div>   
                                </div>                                
                            <?php } ?>
                        </div>
                    </div>
                    </div>           
                </div>



      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if($categories && !$products){ ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php }  ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

  

<style type="text/css">
.discount-percentage{
  font-size: 250%;
  font-weight: 200;
  height: 100%;
  padding-top: 10px;
}
.discount-amount{
  text-align: left;
  height: 100%;
}
.price-new{
  font-size: 150%;
}
</style>
        <style type="text/css">


.alert {
  position: absolute;
  bottom: 0px;
  left: 20px;
  right: 20px;
}
        .potongan_persen{
          //background: red;
          text-align: center;
          padding: 10px 5px;
          min-height: 40px;
          float: left;
          width: 30%;
          color: #ffcce6;
          color: #FF3399;
          font-weight: bold;
          font-size: 245%;

        }
        .harga{
          //background: green;
          text-align: center;
          padding: 10px 5px;
          min-height: 40px;
          float: left;
          width: 70%;
          line-height: 10px;
          padding: 0;
          margin:0;
        }
        .final-price{
          font-weight: bold;
          font-size: 150%;
          color: black;
        }
        </style>

          <?php
          /*
          <p class="price" style="margin-bottom:0;">
          <?php if (!$product['special']) { ?>
          <b ><?php echo $product['price']; ?></b>
          <?php } else { ?>
          <b ><?php echo $product['special']; ?></b>
          <strike><?php echo $product['price']; ?></strike>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax">c<?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
          </p>
          */
          ?>
          <?php
          /*
          <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
          <p><?php echo $product['description']; ?></p>
          <?php if ($product['price']) { ?>
          <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <?php
          $discount = number_format((($product['pricenum']-$product['specialnum'])/$product['pricenum'])*100);
          ?>
          <div class="row" style="height:50px;">
          <div class="col-sm-4 discount-percentage" ><?php echo $discount."%"?></div>
          <div class="col-sm-8 discount-amount" ><span class="price-new"><?php echo $product['special']; ?></span>
          <br/><span class="price-old" style="font-size:smaller;"><?php echo $product['price']; ?></span></div>  

          </div>
          <!-- <span class="price-new"><?php //echo $product['special']; ?></span>  -->
          <!-- <span class="price-old"><?php //echo $product['price']; ?></span> -->
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
          </p>
          <?php } ?>
          <?php if ($product['rating']) { ?>
          <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
          </div>
          <?php } ?>
          */

          ?>

<?php echo $footer; ?>
