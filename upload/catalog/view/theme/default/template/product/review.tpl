<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<table class="table table-borderless review-item">
  <tr>
    <td style="width: 50%;">
      <strong><?php echo $review['author']; ?></strong>
      <br/>
      <div class="rating" style="display:inline-block;margin-bottom:0;">
      <?php for ($i = 1; $i <= 5; $i++) { ?>
      <?php if ($review['rating'] < $i) { ?>
      <span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star-o fa-stack-2x"></i></span>
      <?php } else { ?>
      <span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star fa-stack-2x" style="color:#FF3399 ;"></i><i class="fa fa-star-o fa-stack-2x" style="color:#FF3399 ;"></i></span>
      <?php } ?>
      <?php } ?>
      </div>
    </td>
    <td class="text-right"><?php echo $review['date_added']; ?></td>
  </tr>
  <tr>
    <td colspan="2">
      <p><?php echo $review['text']; ?></p>
    </td>
  </tr>
</table>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="well text-center no-reviews"><?php echo $text_no_reviews; ?></div>
<p style="padding:0;margin:0;"><b>Keterangan:</b></p>
<ol style="padding:12px;margin:0;">
<li>Ulasan hanya bisa diberikan oleh pembeli yang transaksinya sudah berhasil</li>
<li>Berikanlah ulasan yang sejujur-jujurnya, karena komunitas akan menilai sendiri nantinya seberapa jujur setiap pembeli dan penjual yang ada di KR House Beauty.</li>
<li>Ulasan <b>Kualitas Produk</b> menandakan kualitas dari barang yang telah diterima oleh pembeli</li>
</ol>
<style type="text/css">.no-reviews{font-weight: bold;text-transform: capitalize;font-size: normal;}</style>
<!-- <p><?php //echo $text_no_reviews; ?></p> -->
<?php } ?>
<style type="text/css">
.table-borderless > tbody > tr > td,
.table-borderless > tbody > tr > th,
.table-borderless > tfoot > tr > td,
.table-borderless > tfoot > tr > th,
.table-borderless > thead > tr > td,
.table-borderless > thead > tr > th {
    border: none;
}
.review-item{
  border-top:1px dashed #C0C0C0;
}
</style>

