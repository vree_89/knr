<?php echo $header; ?>

<div class="container best-seller-area another">

  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <!-- <div class="product-header-area">
  <div class="container">
  <div class="row">
  <div class="col-md-12">
  <div class="product-menu">
    <ul class="breadcrumb">
      <?php //foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php //echo $breadcrumb['href']; ?>"><?php //echo $breadcrumb['text']; ?></a></li>
      <?php //} ?>
    </ul>
  </div>
  </div>
  </div>
  </div>
  </div> -->

  <div class="product-simple-area">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="single-product-image">
            <div class="single-product-tab">
            <!-- Nav tabs -->
            <?php if ($thumb || $images) { ?>
              <?php
              $i=1;
              ?>
              <ul role="tablist" class="nav nav-tabs">
                <?php if ($thumb) { ?>
                  <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#img<?php echo $i?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"  title="<?php echo $heading_title; ?>">   
                               
                  </a></li>
                <?php } ?>
                <?php if ($images) { ?>
                  <?php foreach ($images as $image) { ?>
                    <?php $i++;?>
                    <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#img<?php echo $i?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>"  title="<?php echo $heading_title; ?>">

                    </a></li>
                  <?php } ?>
                <?php } ?>
              </ul>
            <?php } ?>
            <?php if ($thumb || $images) { ?>
              <?php
              $i=1;
              ?>
              <div class="tab-content" style="position:relative;">
                <?php if ($thumb) { ?>
                   
                  <div id="img<?php echo $i?>" class="tab-pane active" role="tabpanel" style="position:relative;"><img src="<?php echo $thumb; ?>" alt="">
                                                         
                                
                  </div>
                <?php } ?>
                <?php if ($images) { ?>
                  <?php foreach ($images as $image) { ?>
                    <?php $i++;?>
                    <div id="img<?php echo $i?>" class="tab-pane" role="tabpanel"><img src="<?php echo $image['thumb']; ?>" alt="">

                    </div>
                  <?php } ?>
                <?php } ?>
              </div>
            <?php } ?>
            <!-- Tab panes -->
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="single-product-info">
            <!--div class="product-nav">
              <a href="#"><i class="fa fa-angle-right"></i></a>
            </div-->
            <h1 class="product_title"><?php echo $heading_title; ?></h1>
            <div class="clearfix"></div>

            <ul class="list-unstyled">
              <?php if ($manufacturer) { ?>
              <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
              <?php } ?>
              <li><?php echo $text_model; ?> <?php echo $model; ?></li>
              <?php if ($reward) { ?>
              <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
              <?php } ?>
            </ul>

            <?php if ($price) { ?>
  <div style="min-height:40px;">
    <?php
    if($special){
      ?>
      <div class="potongan_persen">
      <?php
      if($discount){
      ?><?php echo $discount;?>%<?php
      }
      ?>
      </div>
      <div class="harga" style="text-align:left;">
              <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
              <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $price_before; ?></span>
              </span>     
              <br/><br/>                               
              <span class="final-price"><?php echo $special; ?></span>      

      </div>
      <?php
    }
    else{
      ?>
      <div class="potongan_persen">
      <?php
      if($discount){
      ?><?php echo $discount;?>%<?php
      }
      ?>
      </div>
      <div class="harga" style="text-align:left;">
      <?php
      if($discount){
      ?>
              <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
              <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $price_before; ?></span>
              </span>     
              <br/><br/>                               
              <span class="final-price"><?php echo $price; ?></span>      
      <?php
      }
      else{
      ?>
      <span class="final-price"><?php echo $price; ?></span>
      <?php
      }
      ?>
      </div>
      <div class="clearfix"></div>
      <?php

    }

    ?>

  </div>
            <?php
            /*
              <div style="min-height:40px;">
              <div class="potongan_persen">
              <?php
              if($discount){
              ?><?php echo $discount;?>%<?php
              }
              ?>
              </div>
              <div class="harga" style="text-align:left;">
              <?php
              if($discount){
              ?>
              <span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
              <span style='color:black;font-size:smaller;margin:0;padding:0;padding-top:5px;'><?php echo $price_before; ?></span>
              </span>     
              <br/><br/>                               
              <span class="final-price"><?php echo $price; ?></span>              
              <?php

              }
              else{
              ?>
              <span class="final-price"><?php echo $price; ?></span>
              <?php
              }
              ?>
              </div>
              <div class="clearfix"></div>
              </div>
            */

            ?>

              <style type="text/css">
        .potongan_persen{
          //background: red;
          text-align: center;
          padding: 10px 5px;
          min-height: 40px;
          float: left;
          width: 30%;
          color: #ffcce6;
          color: #FF3399;
          font-weight: bold;
          font-size: 245%;

        }
        .harga{
          //background: green;
          text-align: center;
          padding: 10px 5px;
          min-height: 40px;
          float: left;
          width: 70%;
          line-height: 10px;
          padding: 0;
          margin:0;
        }
        .final-price{
          font-weight: bold;
          font-size: 150%;
          color: black;
        }
        .special-price{
          font-weight: bold;
          font-size: 150%;
          color: red;
        }
        .stock-status label,.stock-status strong{
          color: #666;
        }

      .disc{
        position: absolute;
        left: 10%;
        top:10%;
        float: left;
        background: rgba(255,020,147,.5);
        background: rgba(255,0,0,0.3);
        padding: 15px 0px;
        text-align: center;
        font-size: 150%;
        color:white;
        font-weight: bold;
        width: 50px;
        height: 50px;
        text-align: center; 
        z-index: 1;

      }    
              </style>

              <?php
              /*
              <div class="price-box" style="margin-bottom:15px;height:27px;">
                <div class="potongan_persen" >
                  <?php
                  if($potongan_retail){
                    ?><div style="font-size:125%;color:#FF3399;font-weight:bold;"><?php echo $potongan_retail;?></div><?php
                  }
                  ?>

                </div>
                
                <div class="harga" style="height:100%;">
                  <?php
                  if($potongan_retail){
                    ?>
                    <span class="new-price" style="font-size:150%;margin:0;padding:0;color:black; "><?php echo $price; ?></span>
                    <span class="old-price" style="font-size:70%;margin:0;padding:0;font-weight:normal;vertical-align:top;"><?php echo $price_retail; ?></span>


                    <?php
                  }
                  else{
                    ?>
                  <span class="new-price"><?php echo $price; ?></span>
                    <?php
                  }
                  ?>
                </div>
                 <div class="clearfix"></div>
              </div>
              */
              ?>


              <?php /*if (!$special) { ?>
                <div class="price-box">
                <span class="new-price"><?php echo $price; ?></span>
                </div>
              <?php } else { ?>
                <div class="price-box">
                <span class="new-price"><?php echo $special; ?></span>
                <span class="old-price"><?php echo $price; ?></span>
                </div>
              <?php } ?>
              <?php if ($tax) { ?>
                <div><?php echo $text_tax; ?> <?php echo $tax; ?></div>
              <?php } ?>
              <?php if ($points) { ?>
                <div><?php echo $text_points; ?> <?php echo $points; ?></div>
              <?php } ?>
              <?php if ($discounts) { ?>
                <hr>
                <?php foreach ($discounts as $discount) { ?>
                  <div><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></div>
                <?php } ?>
              <?php }*/ ?>
            <?php } ?>


            <div id="product">
              <?php
              if(strtolower($stock)!="out of stock"){
                if ($options){ 
                ?>
                <hr>
                <h3><?php echo $text_option; ?></h3>
                <?php foreach ($options as $option){ ?>
                <?php if ($option['type'] == 'select'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
                </select>
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'radio'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label"><?php echo $option['name']; ?></label>
                <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                <?php if ($option_value['image']) { ?>
                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                <?php } ?>                    
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </label>
                </div>
                <?php } ?>
                </div>
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'checkbox'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label"><?php echo $option['name']; ?></label>
                <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                <label>
                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                <?php if ($option_value['image']) { ?>
                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                <?php } ?>
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </label>
                </div>
                <?php } ?>
                </div>
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'text'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'textarea'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'file'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label"><?php echo $option['name']; ?></label>
                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'date'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'datetime'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                </div>
                <?php } ?>
                <?php if ($option['type'] == 'time'){ ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                </div>
                <?php } ?>
                <?php } ?>
                <?php 
                } 
              }
              ?>



            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
              <?php if ($recurrings) { ?>
                <hr>
                <h3><?php echo $text_payment_recurring; ?></h3>
                <div class="form-group required">
                  <select name="recurring_id" class="form-control">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($recurrings as $recurring) { ?>
                    <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                    <?php } ?>
                  </select>
                  <div class="help-block" id="recurring-description"></div>
                </div>
              <?php } ?>


              <div class="clearfix"></div>

              <?php
              if(strtolower($stock)!="out of stock"){
                ?>
<div>
  <div class="clearfix"></div>
  <div class="quantity" style="float:left;">
  <input type="number" id="qty" name="quantity" value="<?php echo $minimum; ?>" min="1">
  <?php
  if($is_preorder){
    ?>
<button style="background:#FF3399" type="button" id="button-cart" <?php /*onclick="cart.add('<?php echo $product_id; ?>', $('#qty').val(),'',$('.cart-message-wrapper'));" */?> data-loading-text="<?php echo $text_loading; ?>">Pre Order</button>
    <?php

  }
  else{
    ?>
<button style="background:#FF3399" type="button" id="button-cart" <?php /*onclick="cart.add('<?php echo $product_id; ?>', $('#qty').val(),'',$('.cart-message-wrapper'));" */?> data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_cart; ?></button>
    <?php

  }

  ?>
  
  <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
  </div>

  <div class="add-to-wishlist-p" style="float:left;">
  <a title="Add to Wishlist" style="cursor:pointer;background:#FF3399" data-toggle="tooltip" data-original-title="Add to Wishlist" onclick="wishlist.add('<?php echo $product_id; ?>',$('.wishlist-message-wrapper'));"><i class="fa fa-star fa-2x"></i></a>
  <!-- <a title="" data-toggle="tooltip" href="#" data-original-title="Compare"><i class="fa fa-exchange fa-2x"></i></a> -->
  </div>
  
  <div class="clearfix"></div>

</div>
                <?php

              }
              ?>

              <div class="clearfix"></div>

            <div class="cart-message-wrapper" ></div>
            <div class="wishlist-message-wrapper" ></div>


              <?php if ($minimum > 1) { ?>
                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
              <?php } ?>            </div>

            <?php if ($review_status) { ?>
              <div class="rating">
                <p>
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($rating < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x" style="color:#FF3399 ;"></i><i class="fa fa-star-o fa-stack-1x" style="color:#FF3399 ;"></i></span>
                    <?php } ?>
                  <?php } ?>
                  <?php /*<a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>*/ ?>
                </p>
                <hr>
                <?php
                $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                ?>

                <!-- <a href="http://www.facebook.com/sharer.php?u=<?php //echo urlencode($url)?>" target="_blank">Share to FaceBook TEST</a> -->
                <!-- AddThis Button BEGIN -->
                <script type="text/javascript">
                // (function(document) {
                //    var shareButtons = document.querySelectorAll(".st-custom-button[data-network]");
                //    for(var i = 0; i < shareButtons.length; i++) {
                //       var shareButton = shareButtons[i];

                //       shareButton.addEventListener("click", function(e) {
                //          var elm = e.target;
                //          var network = elm.dataset.network;

                //          console.log("share click: " + network);
                //       });
                //    }
                // })(document);
                </script>
<style type="text/css">
.st-custom-button[data-network] {
   //background-color: #FF3399;
   display: inline-block;
   padding: 5px 0px;
   text-align: center;
   cursor: pointer;
   font-weight: bold;
   color: #fff;
   //width: 30px;
   //height: 30px;
   border-radius: 20px;
   margin-right: 15px;
   }
   /*.st-custom-button>i{
    color: #FF3399;
   }*/
   .st-custom-button i{
    font-size: 200%;
   }
   .st-custom-button>i.fa-facebook{
    color:#3c5a9a;
   }
   .st-custom-button>i.fa-twitter{
    color:#00abf0;
   }
   .st-custom-button>i.fa-pinterest{
    color:#cc2127;
   }
   .st-custom-button:hover{
    //background-color: black;
   }
   .st-custom-button:hover>i{
    color: #FF3399;
   }
</style>
Share : 
<div data-network="facebook" class="st-custom-button"><i class="fa fa-facebook fa-5x" aria-hidden="true"></i></a></div> 
<div data-network="twitter" class="st-custom-button"><i class="fa fa-twitter fa-5x" aria-hidden="true"></i></div>
<div data-network="pinterest" class="st-custom-button"><i class="fa fa-pinterest fa-5x" aria-hidden="true"></i></div> 
                  <!--div class="share">
                    <div class="sharethis-inline-share-buttons"></div>
                  </div-->
                <!--div class="addthis_toolbox addthis_default_style" data-url="<?php //echo $share; ?>">
                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> 
                <a class="addthis_button_tweet"></a> 
                <a class="addthis_counter addthis_pill_style"></a>
                </div-->
                

                <?php
                /*
                <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>">
                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> 
                <a class="addthis_button_tweet"></a> 
                <a class="addthis_button_pinterest_pinit"></a> 
                <a class="addthis_counter addthis_pill_style"></a>
                </div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                */
                ?>
                <!-- AddThis Button END -->
              </div>
            <?php } ?>

            <div class="stock-status">
              <label><?php echo $text_stock; ?></label>: 
              <?php
              if($is_preorder){
                ?>
                <strong>Pre-Order</strong>
                <br/>
                Date Available : <?php echo $date_available?>

                <?php
              }
              else{
                ?>
                <strong><?php echo $stock; ?></strong>
                <?php  
              }
              ?>
              
            </div>     

        <?php if ($discounts) { ?>
        <div class="row">
          <div class="col-md-12"><b>Special Price for Membership</b></div>
            <?php foreach ($discounts as $discount) { ?>
          <div class="col-md-12"><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>

        <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </div>





  <div class="product-tab-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9">
          <div class="product-tabs">
            <div>
              <!-- Nav tabs -->
              <ul role="tablist" class="nav nav-tabs">
              <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="tab-desc" href="#tab-desc" aria-expanded="true"><?php echo $tab_description; ?></a></li>
              <!-- <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="page-comments" href="#page-comments" aria-expanded="false">Reviews (1)</a></li> -->

              <?php if ($attribute_groups) { ?>
              <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="tab-desc" href="#tab-specification" aria-expanded="true"><?php echo $tab_attribute; ?></a></li>
              <?php } ?>
              <?php if ($review_status) { ?>
              <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="tab-desc" href="#tab-review" aria-expanded="true"><?php echo $tab_review; ?></a></li>
              <?php } ?>

              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div id="tab-desc" class="tab-pane active" role="tabpanel">
                <div class="product-tab-desc">
                <?php echo $description; ?>
                </div>
                </div>

                <?php if ($attribute_groups) { ?>
                <div class="tab-pane" id="tab-specification">
                  <table class="table table-bordered">
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                    <thead>
                    <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                    <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                    <?php } ?>
                  </table>
                </div>
                <?php } ?>

                <?php if ($review_status) { ?>
                  <div class="tab-pane" id="tab-review">
                    <div id="review"></div>
                  </div>
                <?php } ?>
              </div>
            </div>            
          </div>
        <div class="clear"></div>

        <?php if($products){ ?>
          <div class="upsells_products_widget">
            <div class="section-heading">
              <h3><?php echo $text_related; ?></h3>
            </div>
            <div class="row">
              <?php foreach ($products as $product) { ?>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <div class="f-single-product" style="height:370px;border:1px solid #2b2b2b;">
                    <div class="f-product-img">
                      <a class="f-p-img-p" href="<?php echo $product['href']; ?>">
                        <img class="primary-img" src="<?php echo $product['thumb']; ?>" alt="">
                        <img class="hover-img" src="<?php echo $product['thumb']; ?>" alt="">
                      </a>

                    </div>
                    <div>

                    </div>
                    <div class="related-cart-message-wrapper" style="text-align:center;"></div>
                    <div class="feature-product-block" style="margin:0;padding:0;">
                        <h3 style="line-height:12px;min-height:40px;padding:0;margin:0;">
                          <a href="<?php echo $product['href']; ?>" style="font-size:small;"><?php echo $product['name']; ?></a>
                        </h3>


<?php
if($product['price']){
if($product['discount']){
?>
<div class="" style="margin:0;padding:0;height:32px;">
<span style="height:16px;font-size:larger;"><?php echo $product['price']; ?></span> 

<span style='color:red;text-decoration:line-through;margin:0;padding:0;'>
<span style='color:black;font-size:smaller;margin:0;padding:0;height:16px;'><?php echo $product['price_before']; ?></span>
</span>                            
</div>

<?php

}
else{
?>
<div class="" style="margin:0;padding:0;height:16px;">
<span ><?php echo $product['price']; ?></span>
</div>

<?php

}
?>
<?php if ($product['tax']) { ?>
<span class="" style="margin:0;padding:0;"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
<?php } ?>
<?php                          
}
?>
                      
<?php if ($product['rating']) { ?>
<div class="rating" style="display:inline-block;margin-bottom:0;height:20px;padding:0;margin:0;">
<?php for ($i = 1; $i <= 5; $i++) { ?>
<?php if ($product['rating'] < $i) { ?>
<span class="fa fa-stack" style="display:inline-block;padding:0;margin:0;"><i class="fa fa-star-o fa-stack-2x"></i></span>
<?php } else { ?>
<span class="fa fa-stack" style="display:inline-block;padding:0;margin:0;"><i class="fa fa-star fa-stack-2x" style="color:#FF3399 ;"></i><i class="fa fa-star-o fa-stack-2x" style="color:#FF3399 ;"></i></span>
<?php } ?>
<?php } ?>
</div>
<?php 
}
else{
?>
<div class="rating" style="display:inline-block;margin-bottom:0;height:20px;padding:0;margin:0;">
<?php for ($i = 1; $i <= 5; $i++) { ?>
<span class="fa fa-stack" style="display:inline-block;padding:0;margin:0;"><i class="fa fa-star-o fa-stack-2x"></i></span>

<?php } ?>
</div>
<?php
}
?>  
<div class="clearfix"></div>

                                          
                    </div>
                    <div class="product-block-text"> <a class="p-b-t-a-c" style="cursor:pointer;border:1px solid #2b2b2b;" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','related',this);">add to cart</a></div>
                  </div>   
                </div>
              <?php } ?>
            </div>
          </div>
        <?php } ?>

        </div>


      <div class="clearfix"></div>
      </div>
    </div>
  </div>          
        <style type="text/css">

        .potongan_persen{
          text-align: center;
          float: left;
          width: 15%;
          font-weight: bold;
          text-align: center;
          color: #ffcce6;
          color: #FF3399;
        }
        .harga{
          text-align: center;
          float: left;
          width: 85%;
          text-align: left;
        }
        .final-price{
          font-weight: bold;
          font-size: 150%;
        }

        
.alert-wishlist div{
background: #ffc2e3;

}
.alert-wishlist .alert{
  padding: 0;
  margin:0;
  padding: 1px 2px;

}




        </style>

<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/magnific/magnific-popup.css" media="screen" />
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" media="screen" />
<script type="text/javascript" src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/moment.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<!--script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#async=1"></script-->


<script type="text/javascript"><!--
 // function initAddThis(){
 //      addthis.init()
 // }
 //  initAddThis();
$(document).delegate('input.input-rating','change',function(){
  //alert(this.value);

});
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
  $.ajax({
    url: 'index.php?route=product/product/getRecurringDescription',
    type: 'post',
    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#recurring-description').html('');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();

      if (json['success']) {
        $('#recurring-description').html(json['success']);
      }
    }
  });
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
  $.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea,#product input[type=\'number\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-cart').button('loading');
    },
    complete: function() {
      $('#button-cart').button('reset');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            var element = $('#input-option' + i.replace('_', '-'));

            if (element.parent().hasClass('input-group')) {
              element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            } else {
              element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            }
          }
        }

        if (json['error']['recurring']) {
          $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
        }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
      }

      if (json['success']) {
          $('.alert, .text-danger').remove();

          if (json['redirect']) {
            location = json['redirect'];
          }

          if (json['success']) {
            //alert(elem);return false;
            $(".cart-message-wrapper").html('<br/><div class="alert alert-success"><?php echo $text_added_cart?> <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          }

        // $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        // $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

        // $('html, body').animate({ scrollTop: 0 }, 'slow');

        // $('#cart > ul').load('index.php?route=common/cart/info ul li');
      }
    },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});

$('.time').datetimepicker({
  pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').val(json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');



//$(document).ready(function() {
  // $('.thumbnails').magnificPopup({
  //   type:'image',
  //   delegate: 'a',
  //   gallery: {
  //     enabled:true
  //   }
  // });
//});
//--></script>     
</div>   
<?php echo $footer; ?>
<style type="text/css">
.alert-related {
  position: absolute;
  bottom: 25px;
  left: 0px;
  right: 0px;
  width: 80%;
  margin: 0 auto;
}
#st-1 .st-btn:hover {
  opacity: 1;
}


.alert-wishlist div{
background: #ffc2e3;

}
.alert-wishlist .alert{
  padding: 0;
  margin:0;
  padding: 1px 2px;

}

.alert-success{
background: #ffc2e3;
color:#fc3a99;

}
.alert-success .alert{

}
</style>
