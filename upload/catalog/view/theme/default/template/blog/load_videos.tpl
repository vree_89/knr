      <?php if ($videos) { ?>
                  <div class="row" style="padding:5px 0px;">
                          <?php foreach ($videos as $video) { ?>
                          <div class="col-md-4" style="margin-bottom:15px;">
                              <div class="single-blog-from">
                                  <div class="blog-from-block">
                                      <a href="<?php echo $video['href']; ?>"><img src="<?php echo $video['image']; ?>" alt=""></a>
                                      <div class="blog-img-block">
                                          <h3><a href="#"><?php echo $video['name']; ?></a></h3>
                                         <div class="blog-img-info">
                                            <a href="<?php echo $video['href']; ?>"><i class="fa fa-calendar"></i><?php echo $video['date_modified']; ?></a>
                                             <!-- <a href="#"><i class="fa fa-comment-o"></i>0 comment</a> -->
                                         </div>
                                      </div>
                                  </div>    
                              </div>
                          </div>
                          <?php } ?>          
                  </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$videos) { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <br/>

      
<script type="text/javascript">


</script>
