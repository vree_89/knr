<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>
<h2><?php echo $name; ?></h2>
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-reviews" data-toggle="tab">Reviews</a></li>
            <li ><a href="#tab-videos" data-toggle="tab">Videos</a></li>
          </ul>
<div class="tab-content">
  <div class="tab-pane active" id="tab-reviews">

  </div>
  <div class="tab-pane" id="tab-videos">



  </div>
</div>


      






      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function(){
$('#tab-reviews').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

  $('#tab-reviews').load(this.href);
});

$('#tab-videos').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

  $('#tab-videos').load(this.href);
});
$('#tab-reviews').load('index.php?route=blog/blog/load_reviews');

$('#tab-videos').load('index.php?route=blog/blog/load_videos');
});

</script>
