<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>


    <div id="content" class="col-sm-12">
      <h3><?php echo $name?></h3>
      <div class="row" style="min-height:175px;margin-bottom:25px;">
        <div class="col-sm-3 package-detail-list text-center" >
          <div class="inner" style="padding:50px 0px;"><h3><?php echo $price?></h3><span>deposit</span></div>
        </div>
        <div class="col-sm-3 package-detail-list text-center" >
          <div class="inner" style="padding:50px 0px;"><h3><?php echo $period?></h3><span>period</span></div>
        </div>
        <div class="col-sm-3 package-detail-list text-center" >
          <div class="inner" style="padding:50px 0px;"><h3><?php echo $discount?></h3><span>product discount</span></div>
        </div>
        <div class="col-sm-3 package-detail-list text-center" >
          <div class="inner" style="padding:50px 0px;"><h3><?php echo $loyalty_point?></h3><span>reward</span></div>
        </div>
        
        <div class="clearfix"></div>

      </div>
      <div class="row">
        <div class="col-sm-12 text-center"><a href="<?php echo $href?>"><button class="btn btn-lg btn-primary">JOIN NOW</button></a></div>
      </div>
      <div class="clearfix"></div>

      
    </div>





    </div>
</div>
<style type="text/css">
.package-detail-list{
  height: 100px;
  //background: red;
  
}
.package-detail-list div.inner{
  background: #FF3399;
  //padding: 50px 0;
  color: white;
}
.package-detail-list div.inner h3{
  font-size: 250%;
  font-weight: bold;
  color: white;
}
.disc{
position: absolute;
right: 10%;
top:10%;
float: left;
background: rgba(255,020,147,.5);
background: rgba(255,0,0,0.3);
background-color: rgba(184, 134, 11, 0.8);
padding: 15px 0px;
text-align: center;
font-size: 125%;
color:white;
font-weight: bold;
width: 65px;
height: 65px;
border-radius:50px;
padding: 10px;
color: #493f3f;

}   

.package-list{
  //background: red;
  margin-bottom: 20px;
}
.package-list .package-inner{
  padding: 5px 10px;
  background: #FAEBD7;
  box-shadow: 10px 10px 5px #888888;
}
.package-list .package-inner h3{
  font-weight: bold;
  }
</style>
<?php echo $footer; ?>