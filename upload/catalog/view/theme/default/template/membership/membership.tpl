<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>


    <div id="content" class="col-sm-12">
      <h3><?php echo $text_membership_title;?></h3>
    <p><?php echo $text_membership_info1?></p>
    <p><?php echo $text_membership_info2?></p>
    <div class="row">
      <?php
      $i=0;
      foreach ($memberships as $result) {
        $i++;
        ?>
        

        <div class="col-sm-4 membership_list" title="<?php echo $result['name']; ?>" >
          <div class="membership_list_inner">
            <div class="ribbon" ><img src="<?php echo $result['image']?>" class="img-responsive"></div>
            <h2 style="color:<?php echo $result['text_color']?>;"><b><i><?php echo $result['name']; ?></i></b></h2>
            <ul>
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $result['price']?> deposit</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $result['name']?> price</li>
            <!--<li><i class="fa fa-check" aria-hidden="true"></i> <?php //echo $result['discount']?>% discount</li>-->
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $result['loyalty_point']?>% reward</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $result['period']?> days period</li>
            </ul>

          <div class="membership_buy text-center" onclick="javascript:window.location='<?php echo $result['href']; ?>'"><b style="font-size:x-large;">BUY PACKAGE</b></div>

          </div>
        </div>

        <?php
        if($i%3==0){
          echo '<div class="clearfix"></div>';
        }
        ?>



        <?php
        # code...
      }

      ?>

      <div class="clearfix"></div>
    </div>
<div class="clearfix"></div>
    </div>


    </div>
</div>
<style type="text/css">
.membership_list{
  cursor:pointer;padding:0px 8px;margin-bottom:25px;
}
.membership_list_inner{
  padding:0;background:#ecf0f1;
}
.membership_list h2{
  text-shadow:0px 1px #ffffff;margin:0;padding:0;padding-left:15px;padding-top:15px;padding-bottom:10px;
}
.membership_list ul{
  padding-left:15px;
}
.membership_buy{
  background:#ff3399;padding:7px 0px;color:white;margin-top:50px;
}
.ribbon{
  position: absolute;
  width: 54px;
  height: 86px;
  right: 17px;
  top:-7px;
}
</style>
<?php echo $footer; ?>