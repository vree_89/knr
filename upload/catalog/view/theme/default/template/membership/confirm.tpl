<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
  </ul>

  <div class="row">
    <?php echo $column_left; ?>

    <div id="content" class="col-sm-12" style="margin:10px 0px;">
      <h3><?php echo $name?></h3>

      <p><?php echo $text_membership_confirm_info?></p>

      <table class="table">
      <tr>
      <td>Package</td>
      <td><?php echo $name?></td>
      </tr>
      <tr>
      <td>Deposit</td>
      <td><?php echo $price?></td>
      </tr>
      <tr>
      <td>Period</td>
      <td><?php echo $period?></td>
      </tr>
      <tr>
      <td>Discount</td>
      <td><?php echo $discount?></td>
      </tr>
      <tr>
      <td>Loyalty Point</td>
      <td><?php echo $loyalty_point?></td>
      </tr>
      <tr>
      <td>Bonus Point</td>
      <td><?php echo $bonus_point?></td>
      </tr>
      </table>

      <div class="clearfix"></div>

      <h4 class="checkout-subtitle"><?php echo $text_payment_method?></h4>

      <div class="payment-selector">
        <?php 
        if ($payment_methods) {  
          foreach ($payment_methods as $payment_method) { 
            ?>     
            <div class="col-md-12">

<input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>"  />
    <label class="drinkcard-payment" for="<?php echo $payment_method['code']; ?>" style="background-image:url(<?php echo $payment_method['image']; ?>)" title="<?php echo $payment_method['title']; ?>"></label>
              <?php
              /*
            <label for="<?php echo $payment_method['code']; ?>">
            <input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" />
            <?php echo $payment_method['title']?>
            </label>
              */
              ?>
            </div>
            <?php 
          }
        } 
        ?>      
      </div>  

      <div class="row">
        <div class="col-md-6 alert-wrapper"></div>
        <div class="col-md-6">
          <input type="hidden" name="membership_id" value="<?php echo $membership_id?>">
          <div class="pull-right">
            <?php echo $text_agree; ?>
            <?php if ($agree) { ?>
            <input type="checkbox" name="agree" value="1" checked="checked" required="required" />
            <?php } else { ?>
            <input type="checkbox" name="agree" value="1" required="required" />
            <?php } ?>
            &nbsp;
            <input type="image" src="<?php echo $btn_next; ?>" value="<?php echo $button_confirm; ?>" id="button-confirm" data-loading-text="<?php echo $text_loading; ?>" class="img-responsive" />
            <!-- <input type="button" value="<?php //echo $button_confirm; ?>" id="button-confirm" data-loading-text="<?php //echo $text_loading; ?>" class="btn btn-primary" /> -->
          </div>
        </div>
      </div>

    </div>





  </div>
</div>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" media="screen" />
<script type="text/javascript" src="mobile/view/theme/default/assets/js/jquery.redirect.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/moment.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
  
<script type="text/javascript">
$(document).delegate('#button-confirm','click',function(){
  $.ajax({
    type: 'post',
    data: $('input[type=\'hidden\'], input[type=\'text\'], input[type=\'checkbox\']:checked,input[type=\'radio\']:checked'),
    dataType: 'json',
    url: 'index.php?route=membership/membership/save',
    cache: false,
    // beforeSend: function() { $('#button-confirm').button('loading'); },
    // complete: function() { $('#button-confirm').button('reset'); },
    success: function(json) {
      $('.alert').remove();

      if (json['redirect']) {
          location = json['redirect'];
      } 
      else if(json['error']){
          $('.alert-wrapper').prepend('<div class="alert alert-danger">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      else if(json['data'] && json['redirect_payment_url']){
        $.redirect(json['redirect_payment_url'],json['data']); 

      }
      else{
        $.redirect(json['doku_url'],json['doku_data']); 
      }
    }
  });


});
</script>
<style type="text/css">
a:hover{
  color: #FF3399;
}
.alert-wrapper{
  height: 38px;  
}
.package-detail-list{
  height: 100px;
  //background: red;
  
}
.package-detail-list div.inner{
  background: #FF3399;
  //padding: 50px 0;
  color: white;
}
.package-detail-list div.inner h3{
  font-size: 250%;
  font-weight: bold;
  color: white;
}
.disc{
position: absolute;
right: 10%;
top:10%;
float: left;
background: rgba(255,020,147,.5);
background: rgba(255,0,0,0.3);
background-color: rgba(184, 134, 11, 0.8);
padding: 15px 0px;
text-align: center;
font-size: 125%;
color:white;
font-weight: bold;
width: 65px;
height: 65px;
border-radius:50px;
padding: 10px;
color: #493f3f;

}   

.package-list{
  //background: red;
  margin-bottom: 20px;
}
.package-list .package-inner{
  padding: 5px 10px;
  background: #FAEBD7;
  box-shadow: 10px 10px 5px #888888;
}
.package-list .package-inner h3{
  font-weight: bold;
}

.payment-selector input{
    margin:0;
    padding:0;
    display: none;
}
.payment-selector input:active +.drinkcard-payment{opacity: .9;}
.payment-selector input:checked +.drinkcard-payment{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
}
.drinkcard-payment{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    width: 100%;
    height: 80px;
    -webkit-transition: all 100ms ease-in;
       -moz-transition: all 100ms ease-in;
            transition: all 100ms ease-in;
    -webkit-filter: brightness(1.8) grayscale(1) opacity(.7);
       -moz-filter: brightness(1.8) grayscale(1) opacity(.7);
            filter: brightness(1.8) grayscale(1) opacity(.7);
}
.drinkcard-payment:hover{
    -webkit-filter: brightness(1.2) grayscale(.5) opacity(.9);
       -moz-filter: brightness(1.2) grayscale(.5) opacity(.9);
            filter: brightness(1.2) grayscale(.5) opacity(.9);
}

</style>
<?php echo $footer; ?>