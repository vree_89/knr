$(document).ready(function() {
	$('a[href="#navbar-more-show"], .navbar-more-overlay').on('click', function(event) {
		event.preventDefault();
		$('body').toggleClass('navbar-more-show');
		if ($('body').hasClass('navbar-more-show'))	{
			$('a[href="#navbar-more-show"]').closest('li').addClass('active');
		} else {
			$('a[href="#navbar-more-show"]').closest('li').removeClass('active');
		}
		return false;
	});

	//Enable swiping...
	$('.carousel-inner').swipe( {
		//Generic swipe handler for all directions
		swipeLeft:function(event, direction, distance, duration, fingerCount) {
			$(this).parent().carousel('next'); 
		},
		swipeRight: function() {
			$(this).parent().carousel('prev'); 
		},
		//Default is 75px, set to 0 for demo so any distance triggers swipe
		threshold:0
	});

	$(document).on('click','.value-control',function(){
	    var action = $(this).attr('data-action')
	    var target = $(this).attr('data-target')
	    var value  = parseFloat($('[id="'+target+'"]').val());
	    if ( action == "plus" ) {
	      value++;
	    }
	    if ( action == "minus" ) {
	    	if ( value > 1) {
	      		value--;
	      	}
	    }
	    $('[id="'+target+'"]').val(value)
	})
});