        <div class="">
            <div class="container best-seller-area another">
                <div class="row">
                    <div class="col-md-12">
                        <div class="the-blog-title">
                           <h3>FEATURED</h3>
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="best-seller">                            
                        <div class="">
                            <?php foreach ($products as $product) { ?>

                                <div class="col-md-3" style="margin-bottom:15px;">
                                <div class="single-product">
                                <div class="product-img">
                                <a class="b-s-p-img" href="<?php echo $product['href']; ?>">
                                <img class="primary-img" src="<?php echo $product['thumb']; ?>" alt="">
                                <img class="hover-img" src="<?php echo $product['thumb']; ?>" alt="">
                                </a>
                                <div class="img-block">
                                <div class="primary-icon">
                                <a href="#"><i class="fa fa-random"></i></a>
                                <a href="#"><i class="fa fa-eye"></i></a>
                                </div>
                                <div class="hover-icon">
                                <span class="random"><a href="#"><i class="fa fa-random"></i></a></span>
                                <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                                </div>    
                                </div>
                                </div>
                                <div class="product-block-text">
                                <?php if ($product['price']) { ?>
                                <p class="price">
                                  <?php if (!$product['special']) { ?>
                                    <b style="font-size:x-large;"><?php echo $product['price']; ?></b>
                                  <?php } else { ?>
                                    <b style="font-size:x-large;"><?php echo $product['special']; ?></b>
                                    <strike><?php echo $product['price']; ?></strike>
                                  <?php } ?>
                                  <?php if ($product['tax']) { ?>
                                    <span class="price-tax">c<?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                  <?php } ?>
                                </p>
                                <?php } ?>   

                                <?php if ($product['rating']) { ?>
                                <div class="rating" style="display:inline-block;">
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } else { ?>
                                <span class="fa fa-stack" style="display:inline-block;"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } ?>
                                <?php } ?>
                                </div>
                                <?php } ?>

                                <h3><a href="#" style="font-size:small;"><?php echo $product['name']; ?></a></h3>

                                <a class="p-b-t-a-c" href="#" style="font-size:80%;padding:0;margin:0;padding:5px 10px;margin-bottom:10px;">add to cart</a>
                                </div>
                                </div>   
                                </div>

                                
                            <?php } ?>









                        </div>
                    </div>
                    </div>           
                </div>
            </div>
        </div>







<?php
/*
        <div class="best-seller-area another owl-indicator">
            <div class="container" >
                <div class="row">
                    <div class="col-md-12">
                        <div class="the-blog-title">
                           <h3>FEATURED</h3>
                        </div>
                    </div>
                </div>    
                <div class="row" >
                    <div class="tab-content" >
                    <div role="tabpanel" class="tab-pane active" id="best-seller">                            
                        <div class="best-seller-product">
                          <?php foreach ($products as $product) { ?>
                            <div class="col-md-3">
                            <div class="single-product">
                            <div class="product-img">
                            <a class="b-s-p-img" href="<?php echo $product['href']; ?>">
                            <img class="primary-img" src="<?php echo $product['thumb']; ?>" alt="">
                            <img class="hover-img" src="<?php echo $product['thumb']; ?>" alt="">
                            </a>
                            <div class="img-block">

                            <!--<div class="primary-icon">
                            <a href="#"><i class="fa fa-random"></i></a>
                            <a href="#"><i class="fa fa-eye"></i></a>
                            </div>-->

                            <div class="hover-icon">
                            <span class="random"><a href="#" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-random"></i></a></span>
                            <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                            </div>    
                            </div>
                            </div>
                            <div class="product-block-text">

                            <h3 ><a href="<?php echo $product['href']; ?>" style="font-size:small;"><?php echo $product['name']; ?></a></h3>
                            <?php if ($product['price']) { ?>
                            <p class="price">
                              <?php if (!$product['special']) { ?>
                                <b><?php echo $product['price']; ?></b>
                              <?php } else { ?>
                                <b><?php echo $product['special']; ?></b>
                                <strike><?php echo $product['price']; ?></strike>
                              <?php } ?>
                              <?php if ($product['tax']) { ?>
                                <span class="price-tax">c<?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                              <?php } ?>
                            </p>
                            <?php } ?>           




                            <a class="p-b-t-a-c" style="cursor:pointer;" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','home',this);">add to cart</a>
                            </div>
                            </div>   
                            </div>                          
                          <?php } ?>
                        </div>
                    </div>
                    </div>           
                </div>
            </div>
        </div>










<h3><?php echo $heading_title; ?></h3>
<div class="row">
  <?php foreach ($products as $product) { ?>
  <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <p><?php echo $product['description']; ?></p>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group">
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
      </div>
    </div>
  </div>
  <?php } ?>
</div>*/
?>