
<?php
/*
<div class="container">
<div id="carousel1" class="owl-carousel">
  <?php foreach ($banners as $banner) { ?>
  <div class="item text-center">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
</div>

*/

?>

        <div class="our-brand-area owl-indicator">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="brand-title">
                           <h3>Brand</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="brand-list">

                        <?php 
                        // var_dump($banners);
                        // exit;
                        ?>
                        <?php foreach ($banners as $banner) { ?>
                        <div class="col-md-12">
                            <div class="single-brand">
                                <a href=""><img src="<?php echo $banner['image']; ?>" alt=""></a>
                            </div>    
                        </div> 
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

<!--         <div class="best-seller-area another owl-indicator">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="the-blog-title">
                           <h3>BEST SELLER</h3>
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="best-seller">                            
                        <div class="best-seller-product">
                            <div class="col-md-3">
                            <div class="single-product">
                            <div class="product-img">
                            <a class="b-s-p-img" href="#">
                            <img class="primary-img" src="img/product/prod11.jpg" alt="">
                            <img class="hover-img" src="img/product/prod12.jpg" alt="">
                            </a>
                            <div class="img-block">
                            <div class="primary-icon">
                            <a href="#"><i class="fa fa-random"></i></a>
                            <a href="#"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="hover-icon">
                            <span class="random"><a href="#"><i class="fa fa-random"></i></a></span>
                            <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                            </div>    
                            </div>
                            </div>
                            <div class="product-block-text">
                            <h3><a href="#">Bag<span>$60.23</span></a></h3>
                            <a class="p-b-t-a-c" href="#">add to cart</a>
                            </div>
                            </div>   
                            </div>
                            <div class="col-md-3">
                            <div class="single-product">
                            <div class="product-img">
                            <a class="b-s-p-img" href="#">
                            <img class="primary-img" src="img/product/prod21.jpg" alt="">
                            <img class="hover-img" src="img//product/prod22.png" alt="">
                            </a>
                            <div class="img-block">
                            <div class="primary-icon">
                            <a href="#"><i class="fa fa-random"></i></a>
                            <a href="#"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="hover-icon">
                            <span class="random"><a href="#"><i class="fa fa-random"></i></a></span>
                            <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                            </div>    
                            </div>
                            </div>
                            <div class="product-block-text">
                            <h3><a href="#">Bag<span>$60.23</span></a></h3>
                            <a class="p-b-t-a-c" href="#">add to cart</a>
                            </div>
                            </div>   
                            </div>
                            <div class="col-md-3">
                            <div class="single-product">
                            <div class="product-img">
                            <a class="b-s-p-img" href="#">
                            <img class="primary-img" src="img/product/prod31.JPG" alt="">
                            <img class="hover-img" src="img/product/prod32.JPG" alt="">
                            </a>
                            <div class="img-block">
                            <div class="primary-icon">
                            <a href="#"><i class="fa fa-random"></i></a>
                            <a href="#"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="hover-icon">
                            <span class="random"><a href="#"><i class="fa fa-random"></i></a></span>
                            <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                            </div>    
                            </div>
                            </div>
                            <div class="product-block-text">
                            <h3><a href="#">Bag<span>$60.23</span></a></h3>
                            <a class="p-b-t-a-c" href="#">add to cart</a>
                            </div>
                            </div>   
                            </div>
                            <div class="col-md-3">
                            <div class="single-product">
                            <div class="product-img">
                            <a class="b-s-p-img" href="#">
                            <img class="primary-img" src="img/product/prod41.jpg" alt="">
                            <img class="hover-img" src="img/product/prod42.jpg" alt="">
                            </a>
                            <div class="img-block">
                            <div class="primary-icon">
                            <a href="#"><i class="fa fa-random"></i></a>
                            <a href="#"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="hover-icon">
                            <span class="random"><a href="#"><i class="fa fa-random"></i></a></span>
                            <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                            </div>    
                            </div>
                            </div>
                            <div class="product-block-text">
                            <h3><a href="#">Bag<span>$60.23</span></a></h3>
                            <a class="p-b-t-a-c" href="#">add to cart</a>
                            </div>
                            </div>   
                            </div>
                            <div class="col-md-3">
                            <div class="single-product">
                            <div class="product-img">
                            <a class="b-s-p-img" href="#">
                            <img class="primary-img" src="img/product/prod51.jpg" alt="">
                            <img class="hover-img" src="img/product/prod52.jpg" alt="">
                            </a>
                            <div class="img-block">
                            <div class="primary-icon">
                            <a href="#"><i class="fa fa-random"></i></a>
                            <a href="#"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="hover-icon">
                            <span class="random"><a href="#"><i class="fa fa-random"></i></a></span>
                            <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                            </div>    
                            </div>
                            </div>
                            <div class="product-block-text">
                            <h3><a href="#">Bag<span>$60.23</span></a></h3>
                            <a class="p-b-t-a-c" href="#">add to cart</a>
                            </div>
                            </div>   
                            </div>
                            <div class="col-md-3">
                            <div class="single-product">
                            <div class="product-img">
                            <a class="b-s-p-img" href="#">
                            <img class="primary-img" src="img/product/prod61.JPG" alt="">
                            <img class="hover-img" src="img/product/prod62.JPG" alt="">
                            </a>
                            <div class="img-block">
                            <div class="primary-icon">
                            <a href="#"><i class="fa fa-random"></i></a>
                            <a href="#"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="hover-icon">
                            <span class="random"><a href="#"><i class="fa fa-random"></i></a></span>
                            <span class="eye"><a href="#"><i class="fa fa-eye"></i>Quick view</a></span>
                            </div>    
                            </div>
                            </div>
                            <div class="product-block-text">
                            <h3><a href="#">Bag<span>$60.23</span></a></h3>
                            <a class="p-b-t-a-c" href="#">add to cart</a>
                            </div>
                            </div>   
                            </div>


                        </div>
                    </div>
                    </div>           
                </div>
            </div>
        </div> -->