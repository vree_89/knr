<!DOCTYPE html>
<!--
  Name: Cosmetic Mobile Version
  Version: 1.0.0
  Author: Yudhistira EP <yudhistira.ep@gmail.com>
  Copyright 2017.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cosmetic Mobile Version | Home | Brand</title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Icon -->
  <link rel="icon" type="image/png" href="assets/img/icon.png">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css" href="catalog/view/mobile/default/plugins/bootstrap/css/bootstrap.min.css" />

  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="catalog/view/mobile/default/plugins/font-awesome/css/font-awesome.min.css" />

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="catalog/view/mobile/default/assets/css/style.css" />
</head>

<body>
  <!-- Navbar -->
  <div class="navbar-more-overlay"></div>
  <nav class="navbar navbar-inverse navbar-fixed-top animate">
    <div class="container navbar-more">
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="submit">Find</button>
            </span>
          </div>
        </div>
      </form>
      <ul class="nav navbar-nav">
        <li>
          <a href="faq.html">
            <span class="menu-icon fa fa-question"></span>
            FAQ
          </a>
        </li>
        <li>
          <a href="contact.html">
            <span class="menu-icon fa fa-envelope"></span>
            Contact
          </a>
        </li>
      </ul>
    </div>
    <div class="container">
      <ul class="nav navbar-nav mobile-bar">
        <li>
          <a href="index.html">
            <span class="menu-icon fa fa-home"></span>
            Home
          </a>
        </li>
        <li>
          <a href="product.html">
            <span class="menu-icon fa fa-cubes"></span>
            Product
          </a>
        </li>
        <li>
          <a href="cart.html">
            <span class="menu-icon fa fa-shopping-bag">
              <span class="badge">99</span>
            </span>
            Cart
          </a>
        </li>
        <li>
          <a href="about.html">
            <span class="menu-icon fa fa fa-info"></span>
            About
          </a>
        </li>
        <li>
          <a href="#navbar-more-show">
            <span class="menu-icon fa fa-bars"></span>
            More
          </a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /Navbar -->