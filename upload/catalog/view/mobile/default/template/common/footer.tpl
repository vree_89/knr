  <!-- Main Banner -->
  <section class="container-fluid" id="main-banner">
    <div id="carousel-main-banner" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-main-banner" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-main-banner" data-slide-to="1"></li>
        <li data-target="#carousel-main-banner" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <!-- Images size 800 x 400 -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="catalog/view/mobile/default/assets/img/main_banner_1.jpg" alt="Main Banner 1">
          <div class="carousel-caption">
            <a href="#">Main Banner 1</a>
          </div>
        </div>
        <div class="item">
          <img src="catalog/view/mobile/default/assets/img/main_banner_2.jpg" alt="Main Banner 2">
          <div class="carousel-caption">
            <a href="#">Main Banner 2</a>
          </div>
        </div>
        <div class="item">
          <img src="catalog/view/mobile/default/assets/img/main_banner_3.jpg" alt="Main Banner 3">
          <div class="carousel-caption">
            <a href="#">Main Banner 3</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /Main Banner -->

  <!-- Product -->
  <!-- Images size 300 x 200 -->
  <section class="container-fluid">
    <div class="brand">
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_1.jpg" alt="Product 1">
            <div class="caption">
              <p>Brand 1</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_2.jpg" alt="Product 2">
            <div class="caption">
              <p>Brand 2</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_1.jpg" alt="Product 1">
            <div class="caption">
              <p>Brand 1</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_2.jpg" alt="Product 2">
            <div class="caption">
              <p>Brand 2</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_1.jpg" alt="Product 1">
            <div class="caption">
              <p>Brand 1</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_2.jpg" alt="Product 2">
            <div class="caption">
              <p>Brand 2</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_1.jpg" alt="Product 1">
            <div class="caption">
              <p>Brand 1</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_2.jpg" alt="Product 2">
            <div class="caption">
              <p>Brand 2</p>
            </div>
          </div>
        </a>
      </div> 
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_1.jpg" alt="Product 1">
            <div class="caption">
              <p>Brand 1</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_2.jpg" alt="Product 2">
            <div class="caption">
              <p>Brand 2</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_1.jpg" alt="Product 1">
            <div class="caption">
              <p>Brand 1</p>
            </div>
          </div>
        </a>
      </div>
      <div class="item">
        <a href="product.html">
          <div class="thumbnail">
            <img src="catalog/view/mobile/default/assets/img/brand_2.jpg" alt="Product 2">
            <div class="caption">
              <p>Brand 2</p>
            </div>
          </div>
        </a>
      </div>
    </div>
  </section>
  <!-- /Product -->

  <!-- jQuery -->
  <script type="text/javascript" src="catalog/view/mobile/default/assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="catalog/view/mobile/default/assets/js/jquery.touchSwipe.min.js"></script>

  <!-- Bootstrap -->
  <script type="text/javascript" src="catalog/view/mobile/default/plugins/bootstrap/js/bootstrap.min.js"></script>

  <!-- Styles -->
  <script type="text/javascript" src="catalog/view/mobile/default/assets/js/style.js"></script>
</body>
</html>