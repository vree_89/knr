<?php echo $header; ?>
<br/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>

      <?php if ($categories) { ?>

      <!--Brand Index-->
      <?php
      /*
      <p><strong><?php echo $text_index; ?></strong>
        <?php foreach ($categories as $category) { ?>
        &nbsp;&nbsp;&nbsp;<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
        <?php } ?>
      </p>
      */
      ?>
  
      <div class="row manufacturer-list">
      <?php foreach ($categories as $category) { ?>
      <!-- <h2 id="<?php //echo $category['name']; ?>"><?php //echo $category['name']; ?></h2> -->
      <?php if ($category['manufacturer']) { ?>
      <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
        
        <?php foreach ($manufacturers as $manufacturer) { ?>
        <div class="col-sm-2 text-center;">
        <a href="<?php echo $manufacturer['href']; ?>" title="<?php echo $manufacturer['name']; ?>">
        <img src="<?php echo $manufacturer['image']; ?>" alt="" class="img-responsive text-center" >
        <!-- <div><?php //echo $manufacturer['name']; ?></div> -->
        </a>
        </div>
        <?php } ?>
     
      <?php } ?>
      <?php } ?>
      <?php } ?>
      <div class="clearfix"></div>
    </div>

      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<style type="text/css">
.manufacturer-list{
  margin-bottom: 20px;
}
.manufacturer-list img.img-responsive{
   margin: 0 auto;
}
 .manufacturer-list div{
  text-align: center;
  margin: 0 auto;
  }
</style>