<?php
class ControllerCommonLocation extends Controller {
	public function index() {
		$data = array();
		$this->load->language('common/header');
		$data['text_location'] = $this->language->get('text_location');
		$data['text_change_location'] = $this->language->get('text_change_location');
		$data['setlocation'] = $this->url->link('common/home/setlocation');
		if(isset($this->session->data['location'])){
			$data['location'] = $this->session->data['location'];
		}
		else{
			$data['href_id'] = $this->url->link('common/home/setlocation','location=id');
			$data['href_my'] = $this->url->link('common/home/setlocation','location=my');
			$data['href_sg'] = $this->url->link('common/home/setlocation','location=sg');

		}
		$data['change']  = $this->url->link('common/home/changelocation');

		$data['text_country'] = "";
		if(isset($this->session->data['location'])){
			$location = $this->session->data['location'];
			if($location=="id"){
				$data['text_country'] = "Indonesia";
			}
			if($location=="my"){
				$data['text_country'] = "Malaysia";
			}
			if($location=="sg"){
				$data['text_country'] = "Singapore";
			}
		}


		return $this->load->view('common/location', $data);
	}

}