<?php
class ControllerCommonContentTop extends Controller {
	public function index() {
		//die('content top');
		$this->load->model('design/layout');

		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}

		$layout_id = 0;

		if ($route == 'product/category' && isset($this->request->get['path'])) {
			$this->load->model('catalog/category');

			$path = explode('_', (string)$this->request->get['path']);

			$layout_id = $this->model_catalog_category->getCategoryLayoutId(end($path));
		}

		if ($route == 'product/product' && isset($this->request->get['product_id'])) {
			$this->load->model('catalog/product');

			$layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
		}

		if ($route == 'information/information' && isset($this->request->get['information_id'])) {
			$this->load->model('catalog/information');

			$layout_id = $this->model_catalog_information->getInformationLayoutId($this->request->get['information_id']);
		}

		if (!$layout_id) {
			$layout_id = $this->model_design_layout->getLayout($route);
		}

		if (!$layout_id) {
			$layout_id = $this->config->get('config_layout_id');
		}
		

		$this->load->model('extension/module');

		$data['modules'] = array();

		$modules = $this->model_design_layout->getLayoutModules($layout_id, 'content_top');
		//var_dump($modules);

		/*
		array(3) { 
		array(5) { 
			[0]=> array(5) { ["layout_module_id"]=> string(3) "100" ["layout_id"]=> string(1) "1" ["code"]=> string(12) "slideshow.27" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "0" } 
			[1]=> array(5) { ["layout_module_id"]=> string(3) "101" ["layout_id"]=> string(1) "1" ["code"]=> string(9) "banner.31" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "1" } 
			[2]=> array(5) { ["layout_module_id"]=> string(3) "102" ["layout_id"]=> string(1) "1" ["code"]=> string(11) "featured.28" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "2" } 
			[3]=> array(5) { ["layout_module_id"]=> string(3) "103" ["layout_id"]=> string(1) "1" ["code"]=> string(11) "featured.28" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "3" } 
			[4]=> array(5) { ["layout_module_id"]=> string(3) "104" ["layout_id"]=> string(1) "1" ["code"]=> string(11) "carousel.29" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "4" } 
		}

			array(3) { 
			[0]=> array(5) { ["layout_module_id"]=> string(3) "134" ["layout_id"]=> string(1) "1" ["code"]=> string(12) "slideshow.27" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "0" } 
			[1]=> array(5) { ["layout_module_id"]=> string(3) "135" ["layout_id"]=> string(1) "1" ["code"]=> string(11) "carousel.29" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "3" } 
			[2]=> array(5) { ["layout_module_id"]=> string(3) "136" ["layout_id"]=> string(1) "1" ["code"]=> string(11) "featured.28" ["position"]=> string(11) "content_top" ["sort_order"]=> string(1) "5" }
			 }		
		*/

		foreach ($modules as $module) {
			$part = explode('.', $module['code']);
			if (isset($part[0]) && $this->config->get($part[0] . '_status')) {

				$module_data = $this->load->controller('extension/module/' . $part[0]);

				if ($module_data) {
					$data['modules'][] = $module_data;
				}
			}

			if (isset($part[1])) {
				$setting_info = $this->model_extension_module->getModule($part[1]);
				if ($setting_info && $setting_info['status']) {
					$output = $this->load->controller('extension/module/' . $part[0], $setting_info);

					if ($output) {
						$data['modules'][] = $output;
					}
				}
			}
		}

		// var_dump($data['modules']);
		// exit;

		return $this->load->view('common/content_top', $data);
	}
}
