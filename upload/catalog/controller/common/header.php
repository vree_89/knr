<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		$this->document->addStyle('catalog/view/theme/default/stylesheet/bootstrap.min.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/font-awesome.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.theme.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.transitions.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/lib/css/nivo-slider.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/lib/css/preview.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/animate.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/meanmenu.min.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/jQuery-ui.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/normalize.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/main.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/style.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/responsive.css');


		$this->document->addScript('catalog/view/javascript/vendor/modernizr-2.8.3.min.js');
		//$this->document->addScript('catalog/view/javascript/vendor/jquery-1.11.3.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/jquery-2.1.1.min.js');
		//$this->document->addScript('catalog/view/javascript/jquery-price-slider.js');
		$this->document->addScript('catalog/view/javascript/bootstrap.min.js');
		//$this->document->addScript('catalog/view/javascript/wow.min.js');
		$this->document->addScript('catalog/view/theme/default/stylesheet/lib/js/jquery.nivo.slider.js');
		//$this->document->addScript('catalog/view/theme/default/stylesheet/lib/home.js');
		$this->document->addScript('catalog/view/javascript/jquery.meanmenu.js');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');
		//$this->document->addScript('catalog/view/javascript/owl.carousel.min.js');
		$this->document->addScript('catalog/view/javascript/jquery.mixitup.min.js');
		//$this->document->addScript('catalog/view/javascript/jquery.collapse.js');
		//$this->document->addScript('catalog/view/javascript/jquery.scrollUp.min.js');
		//$this->document->addScript('catalog/view/javascript/plugins.js');
		$this->document->addScript('catalog/view/javascript/main.js');
		$this->document->addScript('catalog/view/javascript/common.js');



		
		
	

		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['text_free_shipping'] = $this->language->get('text_free_shipping');
		$data['text_original'] = $this->language->get('text_original');
		$data['text_return'] = $this->language->get('text_return');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');

		

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();
				
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					// $children_data[] = array(
					// 	'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					// 	'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					// );
					$children_lv3 = $this->model_catalog_category->getCategories($child['category_id']);
					if($children_lv3)
					{    

					    foreach ($children_lv3 as $child_lv3) 
					    {
					    	// var_dump($children_lv3);
					    	// echo "<br/><br/>";
					        $filter_data_lv3 = array(
					        'filter_category_id'  => $child_lv3['category_id'],
					        'filter_sub_category' => true
					        );

					        $children_lv3_data[] = array(
					        'category_id' => $child_lv3['category_id'],
					        //'name'  => $child_lv3['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data_lv3) . ')' : ''),
					        'name'  => $child_lv3['name'] ,
					        'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child_lv3['category_id'])
					        );
					    }

					    $children_data[] = array(
					    'children_lv3' => $children_lv3_data,
					    //'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					    'name'  => $child['name'] ,
					    'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					    );
					    $children_lv3_data = array();

					}

					else
					{

					    $children_data[] = array(
					'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					    );
					}

				}

				// Level 1
				$data['categories'][] = array(
					'name'     => strtoupper($category['name']),
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}




		/*manufacturer list*/
		$children_data = array();
		$this->load->model('catalog/manufacturer');
		$results = $this->model_catalog_manufacturer->getManufacturers();
		foreach ($results as $result) {
			$children_data[] = array(
			'name'  => $result['name'],
			'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']),
			'image' => HTTP_SERVER.'image/'.$result['image']
			);

		}
		$data['categories'][] = array(
			'name'     => 'BRAND',
			'children' => $children_data,
			'column'   => 1,
			'href'     => $this->url->link('product/manufacturer')
		);


		// sale
		// $data['categories'][] = array(
		// 	'name'     => 'SALE',
		// 	'children' => array(),
		// 	'column'   => 1,
		// 	'href'     => $this->url->link('product/sale','path=sale')
		// );

		// bestseller
		$data['categories'][] = array(
			'name'     => 'BEST SELLER',
			'children' => array(),
			'column'   => 1,
			'href'     => $this->url->link('product/bestseller', 'path=bestseller')
		);

		// blog
		$data['categories'][] = array(
			'name'     => 'BLOG',
			'children' => array(),
			'column'   => 6,
			'href'     => $this->url->link('blog/blog')
		);

		// membership
		$data['categories'][] = array(
			'name'     => 'MEMBERSHIP',
			'children' => array(),
			'column'   => 7,
			'href'     => $this->url->link('membership/membership')
		);

		// bestseller
		// $data['categories'][] = array(
		// 	'name'     => 'REQUEST',
		// 	'children' => array(),
		// 	'column'   => 7,
		// 	'href'     => $this->url->link('product/bestseller', 'path=best_seller')
		// );






		//var_dump($data['categories']);exit;

		$data['location'] = $this->load->controller('common/location');
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		$data['topimage'] = HTTP_SERVER.'image/index.jpg';
		$data['total_items'] = $this->cart->countProducts();

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		//var_dump($data);exit;



		//location, language and currency
		$data['text_location'] = $this->language->get('text_location');
		$data['text_change_location'] = $this->language->get('text_change_location');
		$data['setlocation'] = $this->url->link('common/home/setlocation');
		$data['code_location'] = @$this->session->data['location'];
		$data['href_id'] = $this->url->link('common/home/setlocation','location=id');
		$data['href_my'] = $this->url->link('common/home/setlocation','location=my');
		$data['href_sg'] = $this->url->link('common/home/setlocation','location=sg');

		$this->load->model('localisation/language');
		$data['text_language'] = $this->language->get('text_language');
		$data['code_language'] = $this->session->data['language'];
		$data['languages'] = array();
		$results = $this->model_localisation_language->getLanguages();
		foreach ($results as $result) {
			if ($result['status']) {
				$data['languages'][] = array(
					'name' => $result['name'],
					'code' => $result['code']
				);
			}
		}


		$data['text_currency'] = $this->language->get('text_currency');
		$data['code_currency'] = $this->session->data['currency'];
		$this->load->model('localisation/currency');
		$data['currencies'] = array();
		$results = $this->model_localisation_currency->getCurrencies();
		foreach ($results as $result) {
			if ($result['status']) {
				$data['currencies'][] = array(
					'title'        => $result['title'],
					'code'         => $result['code'],
					'symbol_left'  => $result['symbol_left'],
					'symbol_right' => $result['symbol_right']
				);
			}
		}

		$data['change']  = $this->url->link('common/home/changelocation');

		$data['text_country'] = "";
		if(isset($this->session->data['location'])){
			$location = $this->session->data['location'];
			if($location=="id"){
				$data['text_country'] = "Indonesia";
			}
			if($location=="my"){
				$data['text_country'] = "Malaysia";
			}
			if($location=="sg"){
				$data['text_country'] = "Singapore";
			}
		}



		//reload membership (if user by a moment ago)
		if($this->customer->isLogged()){
			$this->load->model('account/customer');
			$membership_info = $this->model_account_customer->getCustomerMembership($this->session->data['customer_id']);
			if($membership_info){
				$this->session->data['membership']['membership_name'] = $membership_info['membership_name'];
				$this->session->data['membership']['membership_deposit'] = $membership_info['membership_deposit'];
				$this->session->data['membership']['membership_expired'] = $membership_info['membership_expired'];
				$this->session->data['membership']['membership_discount'] = $membership_info['membership_discount'];
				$this->session->data['membership']['membership_loyalty_point'] = $membership_info['membership_loyalty_point'];
				$this->session->data['membership']['membership_bonus_point'] = $membership_info['membership_bonus_point'];
			}					


			//get customer deposit
			$deposit = $this->model_account_customer->getCustomerDeposit($this->session->data['customer_id']);
			$this->session->data['customer_deposit'] = $deposit;




		}








		return $this->load->view('common/header', $data);
	}


/*
  1 => 
  array (
    'name' => 'RAMBUT & BADAN',
    'children' => 
    array (
      0 => 
      array (
        'name' => 'Badan (0)',
        'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=82_86',
      ),
      1 => 
      array (
        'name' => 'Perawatan Badan (0)',
        'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=82_84',
      ),
      2 => 
      array (
        'name' => 'Perawatan Rambut (0)',
        'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=82_83',
      ),
      3 => 
      array (
        'name' => 'Rambut (0)',
        'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=82_85',
      ),
    ),
    'column' => '1',
    'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=82',
  ),
    0 => 
  array (
    'name' => 'MAKE UP',
    'children' => 
    array (0 => 
      array ('children_lv3' => 
        array (
          0 => array (
            'category_id' => '81',
            'name' => 'Lipstick',
            'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=77_80_81',
          ),
        ),
        'name' => 'Make Up Bibir',
        'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=77_80',
      ),
      1 => 
      array (
        'children_lv3' => 
        array (
          0 => 
          array (
            'category_id' => '79',
            'name' => 'Krim BB/CC',
            'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=77_78_79',
          ),
        ),
        'name' => 'Make Up Wajah',
        'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=77_78',
      ),
    ),
    'column' => '1',
    'href' => 'http://localhost/opencart/upload/index.php?route=product/category&path=77',
  ),


*/

	// public function index() {	
	// 	// var_dump($this->config->get('config_template'));
	// 	// exit;
	// 	//  die((DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/main.css' ));
 //  // //   if(file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/' . $css_file)) {
 //  // //       $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template'). '/stylesheet/' . $css_file);
 //  // //   }		




	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/bootstrap.min.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/font-awesome.min.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/owl.carousel.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/owl.theme.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/owl.transitions.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/lib/css/nivo-slider.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/lib/css/preview.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/animate.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/meanmenu.min.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/jQuery-ui.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/normalize.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/main.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/style.css');
	// 	$this->document->addStyle('catalog/view/theme/default/stylesheet/responsive.css');


	// 	$this->document->addScript('catalog/view/javascript/vendor/modernizr-2.8.3.min.js');
	// 	$this->document->addScript('catalog/view/javascript/vendor/jquery-1.11.3.min.js');
	// 	$this->document->addScript('catalog/view/javascript/jquery-price-slider.js');
	// 	$this->document->addScript('catalog/view/javascript/bootstrap.min.js');
	// 	$this->document->addScript('catalog/view/javascript/wow.min.js');
	// 	$this->document->addScript('catalog/view/theme/default/stylesheet/lib/js/jquery.nivo.slider.js');
	// 	$this->document->addScript('catalog/view/theme/default/stylesheet/lib/home.js');
	// 	$this->document->addScript('catalog/view/javascript/jquery.meanmenu.js');
	// 	$this->document->addScript('catalog/view/javascript/owl.carousel.min.js');
	// 	$this->document->addScript('catalog/view/javascript/jquery.mixitup.min.js');
	// 	$this->document->addScript('catalog/view/javascript/jquery.collapse.js');
	// 	$this->document->addScript('catalog/view/javascript/jquery.scrollUp.min.js');
	// 	$this->document->addScript('catalog/view/javascript/plugins.js');
	// 	$this->document->addScript('catalog/view/javascript/main.js');


        
        
		
        	
        










	// 	// Analytics
	// 	$this->load->model('extension/extension');

	// 	$data['analytics'] = array();

	// 	$analytics = $this->model_extension_extension->getExtensions('analytics');

	// 	foreach ($analytics as $analytic) {
	// 		if ($this->config->get($analytic['code'] . '_status')) {
	// 			$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
	// 		}
	// 	}

	// 	if ($this->request->server['HTTPS']) {
	// 		$server = $this->config->get('config_ssl');
	// 	} else {
	// 		$server = $this->config->get('config_url');
	// 	}

	// 	if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
	// 		$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
	// 	}

	// 	$data['title'] = $this->document->getTitle();

	// 	$data['base'] = $server;
	// 	$data['description'] = $this->document->getDescription();
	// 	$data['keywords'] = $this->document->getKeywords();
	// 	$data['links'] = $this->document->getLinks();
	// 	$data['styles'] = $this->document->getStyles();
	// 	$data['scripts'] = $this->document->getScripts();
	// 	$data['lang'] = $this->language->get('code');
	// 	$data['direction'] = $this->language->get('direction');

	// 	$data['name'] = $this->config->get('config_name');

	// 	if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
	// 		$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
	// 	} else {
	// 		$data['logo'] = '';
	// 	}

	// 	$this->load->language('common/header');

	// 	$data['text_home'] = $this->language->get('text_home');

	// 	// Wishlist
	// 	if ($this->customer->isLogged()) {
	// 		$this->load->model('account/wishlist');

	// 		$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
	// 	} else {
	// 		$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
	// 	}

	// 	$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
	// 	$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

	// 	$data['text_account'] = $this->language->get('text_account');
	// 	$data['text_register'] = $this->language->get('text_register');
	// 	$data['text_login'] = $this->language->get('text_login');
	// 	$data['text_order'] = $this->language->get('text_order');
	// 	$data['text_transaction'] = $this->language->get('text_transaction');
	// 	$data['text_download'] = $this->language->get('text_download');
	// 	$data['text_logout'] = $this->language->get('text_logout');
	// 	$data['text_checkout'] = $this->language->get('text_checkout');
	// 	$data['text_category'] = $this->language->get('text_category');
	// 	$data['text_all'] = $this->language->get('text_all');

	// 	$data['home'] = $this->url->link('common/home');
	// 	$data['wishlist'] = $this->url->link('account/wishlist', '', true);
	// 	$data['logged'] = $this->customer->isLogged();
	// 	$data['account'] = $this->url->link('account/account', '', true);
	// 	$data['register'] = $this->url->link('account/register', '', true);
	// 	$data['login'] = $this->url->link('account/login', '', true);
	// 	$data['order'] = $this->url->link('account/order', '', true);
	// 	$data['transaction'] = $this->url->link('account/transaction', '', true);
	// 	$data['download'] = $this->url->link('account/download', '', true);
	// 	$data['logout'] = $this->url->link('account/logout', '', true);
	// 	$data['shopping_cart'] = $this->url->link('checkout/cart');
	// 	$data['checkout'] = $this->url->link('checkout/checkout', '', true);
	// 	$data['contact'] = $this->url->link('information/contact');
	// 	$data['telephone'] = $this->config->get('config_telephone');












	// 	$data['text_register'] = $this->language->get('text_register');
	// 	$data['text_login'] = $this->language->get('text_login');
	// 	$data['text_logout'] = $this->language->get('text_logout');
	// 	$data['text_forgotten'] = $this->language->get('text_forgotten');
	// 	$data['text_account'] = $this->language->get('text_account');
	// 	$data['text_edit'] = $this->language->get('text_edit');
	// 	$data['text_password'] = $this->language->get('text_password');
	// 	$data['text_address'] = $this->language->get('text_address');
	// 	$data['text_wishlist'] = $this->language->get('text_wishlist');
	// 	$data['text_order'] = $this->language->get('text_order');
	// 	$data['text_download'] = $this->language->get('text_download');
	// 	$data['text_reward'] = $this->language->get('text_reward');
	// 	$data['text_return'] = $this->language->get('text_return');
	// 	$data['text_transaction'] = $this->language->get('text_transaction');
	// 	$data['text_newsletter'] = $this->language->get('text_newsletter');
	// 	$data['text_recurring'] = $this->language->get('text_recurring');

	// 	$data['logged'] = $this->customer->isLogged();
	// 	$data['register'] = $this->url->link('account/register', '', true);
	// 	$data['login'] = $this->url->link('account/login', '', true);
	// 	$data['logout'] = $this->url->link('account/logout', '', true);
	// 	$data['forgotten'] = $this->url->link('account/forgotten', '', true);
	// 	$data['account'] = $this->url->link('account/account', '', true);
	// 	$data['edit'] = $this->url->link('account/edit', '', true);
	// 	$data['password'] = $this->url->link('account/password', '', true);
	// 	$data['address'] = $this->url->link('account/address', '', true);
	// 	$data['wishlist'] = $this->url->link('account/wishlist');
	// 	$data['order'] = $this->url->link('account/order', '', true);
	// 	$data['download'] = $this->url->link('account/download', '', true);
	// 	$data['reward'] = $this->url->link('account/reward', '', true);
	// 	$data['return'] = $this->url->link('account/return', '', true);
	// 	$data['transaction'] = $this->url->link('account/transaction', '', true);
	// 	$data['newsletter'] = $this->url->link('account/newsletter', '', true);
	// 	$data['recurring'] = $this->url->link('account/recurring', '', true);













	// 	// Menu
	// 	$this->load->model('catalog/category');

	// 	$this->load->model('catalog/product');

	// 	$data['categories'] = array();

	// 	$categories = $this->model_catalog_category->getCategories(0);

	// 	foreach ($categories as $category) {
	// 		if ($category['top']) {
	// 			// Level 2
	// 			$children_data = array();

	// 			$children = $this->model_catalog_category->getCategories($category['category_id']);

	// 			foreach ($children as $child) {
	// 				$filter_data = array(
	// 					'filter_category_id'  => $child['category_id'],
	// 					'filter_sub_category' => true
	// 				);

	// 				$children_data[] = array(
	// 					'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
	// 					'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
	// 				);
	// 			}

	// 			// Level 1
	// 			$data['categories'][] = array(
	// 				'name'     => $category['name'],
	// 				'children' => $children_data,
	// 				'column'   => $category['column'] ? $category['column'] : 1,
	// 				'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
	// 			);
	// 		}
	// 	}

	// 	$data['language'] = $this->load->controller('common/language');
	// 	$data['currency'] = $this->load->controller('common/currency');
	// 	$data['search'] = $this->load->controller('common/search');
	// 	$data['cart'] = $this->load->controller('common/cart');

	// 	// For page specific css
	// 	if (isset($this->request->get['route'])) {
	// 		if (isset($this->request->get['product_id'])) {
	// 			$class = '-' . $this->request->get['product_id'];
	// 		} elseif (isset($this->request->get['path'])) {
	// 			$class = '-' . $this->request->get['path'];
	// 		} elseif (isset($this->request->get['manufacturer_id'])) {
	// 			$class = '-' . $this->request->get['manufacturer_id'];
	// 		} elseif (isset($this->request->get['information_id'])) {
	// 			$class = '-' . $this->request->get['information_id'];
	// 		} else {
	// 			$class = '';
	// 		}

	// 		$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
	// 	} else {
	// 		$data['class'] = 'common-home';
	// 	}

	// 	return $this->load->view('common/header', $data);
	// }
}
