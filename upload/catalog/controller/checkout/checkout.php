<?php
class ControllerCheckoutCheckout extends Controller {

	public function index() {
		if($this->customer->getAddressId()==0 & $this->customer->isLogged()){
			unset($this->session->data['payment_address']);
			unset($this->session->data['shipping_address']);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$this->response->redirect($this->url->link('checkout/cart'));
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$this->response->redirect($this->url->link('checkout/cart'));
			}
		}

		$this->load->language('checkout/checkout');

		$this->document->setTitle($this->language->get('heading_title'));

		// Required by klarna
		if ($this->config->get('klarna_account') || $this->config->get('klarna_invoice')) {
			$this->document->addScript('http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_cart'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_checkout_option'] = sprintf($this->language->get('text_checkout_option'), 1);
		$data['text_checkout_account'] = sprintf($this->language->get('text_checkout_account'), 2);
		$data['text_checkout_payment_address'] = sprintf($this->language->get('text_checkout_payment_address'), 2);
		$data['text_checkout_shipping_address'] = sprintf($this->language->get('text_checkout_shipping_address'), 2);
		$data['text_checkout_shipping_method'] = sprintf($this->language->get('text_checkout_shipping_method'), 3);
		$data['text_checkout_coupon'] = sprintf($this->language->get('text_checkout_coupon'), 4);

		
		//SHIPPING IS REQUIRED BY DEFAULT
		// if ($this->cart->hasShipping()) {
		// 	$data['text_checkout_coupon'] = sprintf($this->language->get('text_checkout_coupon'), 4);
		// 	$data['text_checkout_payment_method'] = sprintf($this->language->get('text_checkout_payment_method'), 5);
		// 	//$data['text_checkout_confirm'] = sprintf($this->language->get('text_checkout_confirm'), 7);
		// } 
		// else {
		// 	$data['text_checkout_coupon'] = sprintf($this->language->get('text_checkout_coupon'), 2);
		// 	$data['text_checkout_payment_method'] = sprintf($this->language->get('text_checkout_payment_method'), 3);
		// 	//$data['text_checkout_confirm'] = sprintf($this->language->get('text_checkout_confirm'), 5);	
		// }
		$data['text_checkout_coupon'] = sprintf($this->language->get('text_checkout_coupon'), 4);
		$data['text_checkout_payment_method'] = sprintf($this->language->get('text_checkout_payment_method'), 5);
		//$data['text_checkout_confirm'] = sprintf($this->language->get('text_checkout_confirm'), 7);


		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error_warning'] = '';
		}

		$data['logged'] = $this->customer->isLogged();

		if (isset($this->session->data['account'])) {
			$data['account'] = $this->session->data['account'];
		} else {
			$data['account'] = '';
		}

		//$data['shipping_required'] = $this->cart->hasShipping();
		$data['shipping_required'] = true; //SHIPPING REQUIRED BY DEFAULT
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['shipping_method'] = @$this->session->data['shipping_method'];


		//var_dump($this->session->data['payment_address']);exit;
		//var_dump($this->session->data['payment_address']);
		if (isset($this->session->data['payment_address']['address_id'])) {
			//die("payment_address ".$this->session->data['payment_address']['address_id']);
			$data['address_id'] = $this->session->data['payment_address']['address_id'];
			$data['firstname'] = $this->session->data['payment_address']['firstname'];
			$data['lastname'] = $this->session->data['payment_address']['lastname'];
			$data['company'] = $this->session->data['payment_address']['company'];
			$data['address_1'] = $this->session->data['payment_address']['address_1'];
			$data['address_2'] = $this->session->data['payment_address']['address_2'];
			$data['city'] = $this->session->data['payment_address']['city'];
			$data['postcode'] = $this->session->data['payment_address']['postcode'];
			$data['country'] = $this->session->data['payment_address']['country'];
			$data['country_id'] = $this->session->data['payment_address']['country_id'];
			$data['zone_id'] = $this->session->data['payment_address']['zone_id'];
			$data['id_city'] = $this->session->data['payment_address']['id_city'];
		} else if($this->customer->isLogged() && $this->customer->getAddressId()!=0) {
			//die("logged address".$this->customer->getAddressId());
			$data['address_id'] = $this->customer->getAddressId();
			$this->load->model('account/address');
			$address = $this->model_account_address->getAddress($this->customer->getAddressId());
			$data['firstname'] = $address['firstname'];
			$data['lastname'] = $address['lastname'];
			$data['company'] = $address['company'];
			$data['address_1'] = $address['address_1'];
			$data['address_2'] = $address['address_2'];
			$data['city'] = $address['city'];
			$data['postcode'] = $address['postcode'];
			$data['country'] = $address['country'];
			$data['country_id'] = $address['country_id'];
			$data['zone_id'] = $address['zone_id'];
			$data['id_city'] = $address['id_city'];
		}
		else{
			$data['address_id'] = 0;
		}


		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		

		$data['input_payment_wallet'] = 0;
		$data['input_payment_total'] = 0;
		$data['deposit'] = 0;
		if(isset($this->session->data['customer_deposit'])){
			$data['deposit'] = $this->session->data['customer_deposit'];
		}
		
		$this->response->setOutput($this->load->view('checkout/checkout', $data));
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}



	//pos indonesia courier
	public function getPos($destination,$weight,$origin=151){ 
		//die($weight);
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  origin* - id kota asal (POST)
		  destination* - id kota tujuan (POST)
		  weight* - berat dalam gram (POST)
		  courier* - kode courer:jne, pos, tiki (POST)
		*/ 

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=pos",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);		
		curl_close($curl);
		
		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		  //echo '<pre>' . var_export(json_decode($response,true), true) . '</pre>';
		  //exit;
		}


	}

	public function check(){
		if(!isset($this->session->data['shipping_method']) && !isset($this->session->data['shipping_methods']) && !isset($this->session->data['payment_method']) && !isset($this->session->data['payment_methods']) && !isset($this->session->data['reward'])){
			echo "1";
			return;

		}
		echo "0";

	}
	
}
