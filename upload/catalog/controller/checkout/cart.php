<?php
class ControllerCheckoutCart extends Controller {
	public function index() {
		if (!isset($this->session->data['location'])) {
			if(isset($this->request->get['path'])){
			$this->session->data['redirect'] = $this->url->link('checkout/cart', 'path='.$this->request->get['path']);
			}
			else{
			$this->session->data['redirect'] = $this->url->link('checkout/cart');
			}
			$this->response->redirect($this->url->link('common/home', '', true));
		}

		if(!isset($this->session->data['input_payment_wallet'])){
			$this->session->data['input_payment_wallet'] = 0;
		}
		if(!isset($this->session->data['input_payment_total'])){
			$this->session->data['input_payment_total'] = 0;
		}

		$this->load->language('checkout/cart');
		// $this->load->language('product/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('checkout/cart'),
			'text' => $this->language->get('heading_title')
		);


		$data['text_address_warning'] = "";
		$data['enable_checkout']=1;


		$reward_percentage = null;
		if($this->customer->isLogged()){
			if(isset($this->session->data['membership']['membership_loyalty_point']) && isset($this->session->data['membership']['membership_bonus_point'])){
				$reward_percentage = $this->session->data['membership']['membership_loyalty_point'];
			}
			else{
				$reward_percentage = $this->config->get('config_global_reward') ;
			}
		}
		// echo @$this->session->data['membership']['membership_loyalty_point']."<br/>";
		// echo @$this->session->data['membership']['membership_bonus_point']."<br/>";



		if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_recurring_item'] = $this->language->get('text_recurring_item');
			$data['text_next'] = $this->language->get('text_next');
			$data['text_next_choice'] = $this->language->get('text_next_choice');
			$data['text_select'] = $this->language->get('text_select');
			$data['text_option'] = $this->language->get('text_option');

			$data['column_image'] = $this->language->get('column_image');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');

			$data['button_update'] = $this->language->get('button_update');
			$data['button_remove'] = $this->language->get('button_remove');
			$data['button_shopping'] = $this->language->get('button_shopping');
			$data['button_checkout'] = $this->language->get('button_checkout');

			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$data['error_warning'] = $this->language->get('error_stock');
			} elseif (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$data['attention'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			$data['action'] = $this->url->link('checkout/cart/edit', '', true);
			if ($this->config->get('config_cart_weight')) {
				$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));

				if($this->cart->getWeight()<=0){
					$data['weight'] = $this->weight->format(number_format(1,3), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
				}
				else{
					$data['weight']	 = $this->weight->format(number_format(ceil($this->cart->getWeight()),3), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
				}
			} else {
				$data['weight'] = '';
			}


			$this->load->model('tool/image');
			$this->load->model('tool/upload');


			$this->load->model('catalog/product');
			$data['products'] = array();

			$products = $this->cart->getProducts();

			foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {					
					$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					if($image==""){
						$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					}
				} else {
					$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}



				$option_data = array();

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));					
					$price = $this->currency->format($unit_price, $this->session->data['currency']);
					$price_num = $unit_price;
					$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
				} else {
					$price = false;
					$price_num = false;
					$total = false;
				}

				



				
				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}


				$reward = 0;

				if($reward_percentage && $price_num){
					$reward_num = floor($reward_percentage*$price_num)*$product['quantity'];
					$reward = sprintf($this->language->get('text_points'), $reward_num);
				}
				// echo $reward_percentage."<br/>";
				// echo $price_num."<br/>";
				// echo $product['quantity']."<br/>";
				// echo $this->language->get('text_points')."<br/>";
				// echo $reward_num."<br/>";
				// echo $reward."<br/>";

				$data['products'][] = array(
					//'options'=>$_options,
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'recurring' => $recurring,
					'quantity'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					//'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					'reward' => $reward,
					'price'     => $price,
					'total'     => $total,
					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'is_preorder'      => $product['is_preorder'],
					'date_available'      => $product['date_available'],
				);
			}

			// Gift Voucher
			$data['vouchers'] = array();
			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
					);
				}
			}

			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();
				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}
				array_multisort($sort_order, SORT_ASC, $results);				

				foreach ($results as $result) {		
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
				$data['order_total'] = $total_data['total'];
			}
			

			

			$data['totals'] = array();

			foreach ($totals as $total) {
				if($total['title'] == 'Use Wallet' && $this->customer->isLogged()){
					$data['totals'][] = array(
					'currency'=>$this->session->data['currency'],
					'type' => $total['type'],
					'code' => $total['code'],
					'title' => $total['title'].'<br/>(Your Wallet : '.number_format((int) $this->session->data['customer_deposit']).')',
					'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
					'value'=>abs($total['value']),
					'symbol'=> $total['value']>=0 ? '' : '-',
					'button' => $total['button'],
					);

				}
				else {
					$data['totals'][] = array(
					'currency'=>$this->session->data['currency'],
					'type' => $total['type'],
					'code' => $total['code'],
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
					'value'=>abs($total['value']),
					'symbol'=> $total['value']>=0 ? '' : '-',
					'button' => $total['button'],
					);
				}			
			}


			
			if (isset($this->session->data['input_payment_wallet'])) {
				$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
			} else {
				$data['input_payment_wallet'] = 0;
			}

			$this->session->data['input_payment_total'] = $data['order_total'];
			$data['input_payment_total'] = $data['order_total'];	
	

			if(isset($this->session->data['coupon'])){	
				$this->load->model('extension/total/coupon');
				$coupon_info = $this->model_extension_total_coupon->getCoupon($this->session->data['coupon']);
				$subtotal = $this->cart->getSubTotal();
				if($coupon_info['status'] && $coupon_info['total']){
					//do nothing, keep session
				}
				else{
					unset($this->session->data['coupon']);
				}
			}
			
			if($this->customer->isLogged()){
				if($data['input_payment_total']<0 || $data['order_total']<0 || ($data['input_payment_total']>0 &&  $data['order_total']<50000 && $data['input_payment_wallet']>0)){
					//set payment wallet to 0
					$this->session->data['input_payment_wallet'] = 0;
					//echo "<p>Error : minus/salah perhitungan</p>";


					// Totals
					$this->load->model('extension/extension');

					$totals = array();
					$taxes = $this->cart->getTaxes();
					$total = 0;

					// Because __call can not keep var references so we put them into an array. 			
					$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
					);		

					// Display prices
					//if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);


					foreach ($results as $result) {		
					if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
					//var_dump($totals);exit;
					//}

					$data['order_total'] = $total_data['total'];
					$data['currency'] = $this->session->data['currency'];
					$data['logged'] = $this->customer->isLogged();
					//die('x');


					$data['totals'] = array();

					foreach ($totals as $total) {
						if($total['title'] == 'Use Wallet' && $this->customer->isLogged()){

							$data['totals'][] = array(
								'currency'=>$this->session->data['currency'],
								'type' => $total['type'],
								'code' => $total['code'],
								'title' => $total['title'].'<br/>(Your Wallet : '.number_format((int) $this->session->data['customer_deposit']).')',
								'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
								'value'=>abs($total['value']),
								'symbol'=> $total['value']>=0 ? '' : '-',
								'button' => $total['button'],
							);

						}
						else {
							$data['totals'][] = array(
								'currency'=>$this->session->data['currency'],
								'type' => $total['type'],
								'code' => $total['code'],
								'title' => $total['title'],
								'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
								'value'=>abs($total['value']),
								'symbol'=> $total['value']>=0 ? '' : '-',
								'button' => $total['button'],
							);
						}
					}

					$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
					$this->session->data['input_payment_total'] = $data['order_total'];
					$data['input_payment_total'] = $this->session->data['input_payment_total'];
				}			
			}

			$data['continue'] = $this->url->link('common/home');
			$data['checkout'] = $this->url->link('checkout/checkout', '', true);
			$data['btn_delete'] = HTTP_SERVER.'image/cart/recycle-icon.png';
			//$data['btn_continue'] = HTTP_SERVER.'image/cart/btn_continue.png';
			$data['btn_continue'] = HTTP_SERVER.'image/cart/btn_continue_'.$this->session->data['language'].'.png';		
			$data['btn_checkout'] = HTTP_SERVER.'image/cart/btn_checkout.png';


			$this->load->model('extension/extension');
			$data['modules'] = array();			
			$files = glob(DIR_APPLICATION . '/controller/extension/total/*.php');	
			if ($files) {
				foreach ($files as $file) {
					$result = $this->load->controller('extension/total/' . basename($file, '.php'));					
					if ($result) {
						$data['modules'][] = $result;
					}
				}
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			//var_dump($data['totals']);
			$this->response->setOutput($this->load->view('checkout/cart', $data));
		} else {
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_error'] = $this->language->get('text_empty');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
			$data['btn_continue'] = HTTP_SERVER.'image/cart/empty/btn_continue_'.$this->session->data['language'].'.png';		

			unset($this->session->data['success']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function add() {
		$this->load->language('checkout/cart');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = $product_info['minimum'] ? $product_info['minimum'] : 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}


			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}
			//var_dump($json); exit;

		if (!$json) {
				//echo "a";
				// echo $quantity."<br/>";
				// var_dump($option);
				// echo "<br/>";
				// echo $recurring_id."<br/>";
				$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);
				//var_dump($this->cart->getProducts());exit;

				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

				// Unset all shipping and payment methods
				//unset($this->session->data['shipping_method']);
				//unset($this->session->data['shipping_methods']);
				//unset($this->session->data['payment_method']);
				//unset($this->session->data['payment_methods']);

				// if($shipping_method!=""){				
				// 	$this->resetShippingMethod($shipping_method);
				// }	

				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
		
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('extension/total/' . $result['code']);

							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
				}

				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
			} else {
				//echo "b";
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}








	public function edit() {

		//var_dump(@$this->session->data['shipping_method']['code']);exit;
		// $shipping_method = "";
		// if($this->session->data['shipping_method']['code']){
		// 	$shipping_method = $this->session->data['shipping_method']['code'];
		// }


		$this->load->language('checkout/cart');

		$json = array();

		if (!empty($this->request->post['quantity'])) {
			//var_dump($this->request->post['quantity']);exit;
			foreach ($this->request->post['quantity'] as $key => $value) {
				if(isset($this->request->post['option'][$key])){
					$this->cart->update($key, $value, array_filter($this->request->post['option'][$key]));

				}
				else{
					$this->cart->update($key, $value, array());
				}
				// $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
				// foreach ($product_options as $product_option) {
				// 	if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
				// 		$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				// 	}
				// }				
				// $this->cart->update($key, $value, $option);
				//echo $value."<br/>";
				// $this->cart->update($key, $value);
			}
			//exit;

			$this->session->data['success'] = $this->language->get('text_remove');

			// unset($this->session->data['shipping_method']);
			// unset($this->session->data['shipping_methods']);
			//unset($this->session->data['payment_method']);
			//unset($this->session->data['payment_methods']);
			//unset($this->session->data['reward']);
			//unset($this->session->data['coupon']);

			// if($shipping_method!=""){
			// 	$this->resetShippingMethod($shipping_method);
			// }	
			$this->recalculateShipping();
			//var_dump($this->session->data['shipping_method']);exit;

			$this->response->redirect($this->url->link('checkout/cart'));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove() {
		//var_dump($this->session->data['coupon']);exit;

		// $shipping_method = "";
		// if(@$this->session->data['shipping_method']['code']){
		// 	$shipping_method = @$this->session->data['shipping_method']['code'];
		// }

		$this->load->language('checkout/cart');

		$json = array();

		// Remove
		if (isset($this->request->post['key'])) {
			$this->cart->remove($this->request->post['key']);

			unset($this->session->data['vouchers'][$this->request->post['key']]);

			$json['success'] = $this->language->get('text_remove');

			// unset($this->session->data['shipping_method']);
			// unset($this->session->data['shipping_methods']);
			//unset($this->session->data['payment_method']);
			//unset($this->session->data['payment_methods']);
			//unset($this->session->data['reward']);
			//unset($this->session->data['coupon']);



			// if($shipping_method!=""){				
			// 	$this->resetShippingMethod($shipping_method);
			// }	



			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);

						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		}

		//var_dump($total_data);exit;

		//unset coupon, if the new total is less than its required
		if(isset($this->session->data['coupon'])){	

			$this->load->model('extension/total/coupon');
			$coupon_info = $this->model_extension_total_coupon->getCoupon($this->session->data['coupon']);
			$subtotal = $this->cart->getSubTotal();
			if($coupon_info['status'] && $coupon_info['total']){
				//do nothing, keep session
			}
			else{
				unset($this->session->data['coupon']);
			}
		}
		$this->recalculateShipping();

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}




	public function recalculateShipping(){
		$method_data = array();
		$this->load->model('extension/extension');

		$results = $this->model_extension_extension->getExtensions('shipping');
		$id_city = $this->session->data['shipping_address']['id_city'];
		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('extension/shipping/' . $result['code']);
				$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address'],$id_city);

				if ($quote) {			
					foreach ($quote as $key => $value) {
						$method_data[$key] = array(
							'title'      => $quote[$key]['title'],
							'quote'      => $quote[$key]['quote'],
							'sort_order' => $quote[$key]['sort_order'],
							'error'      => $quote[$key]['error']
						);
					}
				}			
			}
		}

		$sort_order = array();

		foreach ($method_data as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $method_data);
		$this->session->data['shipping_methods'] = $method_data;







		//var_dump($this->session->data['shipping_method']);exit;
		$this->load->language('checkout/checkout');

		$json = array();



		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}


		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}
		$_shipping_methods = array();
		if (!isset($this->session->data['shipping_method']['code'])) {
			$json['error']['warning'] = $this->language->get('error_shipping');
			//echo "1";exit;
		} else {
			$shipping = explode('.', $this->session->data['shipping_method']['code']);

			if (!isset($shipping[0]) || !isset($shipping[1])) {
				//echo "2";exit;
				$json['error']['warning'] = $this->language->get('error_shipping');
			}
			
		}
		if (!$json && isset($shipping[0]) && isset($shipping[1])) {
			//echo "11";exit;

			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][($shipping[0])]['quote'][$shipping[1]];	
			
		}		
	}










	public function resetShippingMethod($shipping_method){
		if (isset($this->session->data['shipping_address'])) {
			// Shipping Methods
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('shipping');
			$id_city = $this->session->data['shipping_address']['id_city'];
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/shipping/' . $result['code']);
					$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address'],$id_city);

					if ($quote) {			
						foreach ($quote as $key => $value) {
							# code...
							// echo $key;
							$method_data[$key] = array(
								'title'      => $quote[$key]['title'],
								'quote'      => $quote[$key]['quote'],
								'sort_order' => $quote[$key]['sort_order'],
								'error'      => $quote[$key]['error']
							);

						}
						// echo '<pre>' . var_export($quote, true) . '</pre>';
						// $method_data[$result['code']] = array(
						// 	'title'      => $quote['title'],
						// 	'quote'      => $quote['quote'],
						// 	'sort_order' => $quote['sort_order'],
						// 	'error'      => $quote['error']
						// );
					}			
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);


			//POS SHIPPING
			$weight = round($this->cart->getWeight()*1000);
			if($weight<1000){
				$weight = 1000; //in gr
			}
			$destination = $this->session->data['shipping_address']['id_city'];
			$pos = $this->getPos($destination,$weight);
			$arr_service_pos = array('Paket Kilat Khusus','Express Sameday Barang','Express Next Day Barang');
			$status = $pos['rajaongkir']['status']['code']; //200 = OK
			if($status=="200"){
				$method_data['pos'] = array(
					'title' => 'POS',
					'quote' => array(),
					'sort_order' => '2',
					'error' => false,
					);
				$res = $pos['rajaongkir']['results'];
				foreach ($res as $_res) {
					foreach($_res['costs'] as $costs){
						foreach($costs['cost'] as $_cost){
							if(in_array($costs['service'], $arr_service_pos) && !isset($method_data['quote'][strtolower($costs['service'])])){
							$method_data['pos']['quote'][strtolower($costs['service'])]['code'] = $_res['code'].".".strtolower($costs['service']);
							$method_data['pos']['quote'][strtolower($costs['service'])]['title'] = 'POS '.$costs['service'];
							$method_data['pos']['quote'][strtolower($costs['service'])]['cost'] = $_cost['value'];
							$method_data['pos']['quote'][strtolower($costs['service'])]['tax_class_id'] = $this->config->get('pos_tax_class_id');
							$method_data['pos']['quote'][strtolower($costs['service'])]['text'] = $this->currency->format($this->tax->calculate($_cost['value'], $this->config->get('pos_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']);


							}
						}
					}
				}
			}
			$this->session->data['shipping_methods'] = $method_data;
		}

		$this->load->language('checkout/checkout');

		// Validate if shipping is required. If not the customer should not have reached this page.
		// if (!$this->cart->hasShipping()) {
		// 	return false;			
		// }

		// Validate if shipping address has been set.
		if (!isset($this->session->data['shipping_address'])) {
			return false;			
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			return false;			
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				return false;
				break;
			}
		}

		$_shipping_methods = array();
		if (!isset($shipping_method)) {
			return false;			
		} else {
			$shipping = explode('.', $shipping_method);
			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
				return false;				
			}
		}

		$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];	

	}






	//pos indonesia courier
	public function getPos($destination,$weight,$origin=151){ 
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=pos",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);		
		curl_close($curl);
		
		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		}


	}



	// public function ajax_edit() {
	// 	// $shipping_method = "";
	// 	// if($this->session->data['shipping_method']['code']){
	// 	// 	$shipping_method = $this->session->data['shipping_method']['code'];
	// 	// }
	// 	$this->load->language('checkout/cart');

	// 	$json = array();

	// 	if (!isset($this->request->post['quantity'])) {
	// 		$json['error']['warning'] = $this->language->get('error_cart');
	// 	} 

	// 	if (!empty($this->request->post['quantity'])) {
	// 		foreach ($this->request->post['quantity'] as $key => $value) {
	// 			if(isset($this->request->post['option'][$key])){
	// 				$this->cart->update($key, $value, array_filter($this->request->post['option'][$key]));
	// 			}
	// 			else{
	// 				$this->cart->update($key, $value, array());
	// 			}


	// 		}

	// 		$this->session->data['success'] = $this->language->get('text_update_cart');


	// 		unset($this->session->data['shipping_method']);
	// 		unset($this->session->data['shipping_methods']);
	// 		unset($this->session->data['payment_method']);
	// 		unset($this->session->data['payment_methods']);
	// 		unset($this->session->data['reward']);
	// 		unset($this->session->data['coupon']);


	// 		// if($shipping_method!=""){				
	// 		// 	$this->resetShippingMethod($shipping_method);
	// 		// }	




			


	// 		// $this->response->redirect($this->url->link('checkout/cart'));
	// 		$json['success'] = $this->language->get('text_update_cart');
	// 	}

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }




}
