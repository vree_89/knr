<?php
//http://128.199.70.89/index.php?route=checkout/callbackdoku/identify
//http://128.199.70.89/index.php?route=checkout/callbackdoku/notify
class ControllerCheckoutCallbackdoku extends Controller {
	public function identify(){
		/*Post Params{TRANSIDMERCHANT=IKzVlEVG3w2f, PURCHASECURRENCY=360, CURRENCY=360, PAYMENTCHANNEL=36, PAYMENTCODE=8856006200000078, AMOUNT=10000.00, SESSIONID=W6NxJEhNrZqRDPl0qhK1}*/
		$transidmerchant = @$this->request->post['TRANSIDMERCHANT'];
		$paymentchannel = @$this->request->post['PAYMENTCHANNEL'];
		$amount = @$this->request->post['AMOUNT'];
		$sessionid = @$this->request->post['SESSIONID'];
		$paymentcode = @$this->request->post['PAYMENTCODE'];

		//add order history
		$this->load->model('checkout/order');
		if(isset($transidmerchant)){
			$order_info = $this->model_checkout_order->getDoku($transidmerchant);
			if($order_info){
				$trans_type = explode('-',$transidmerchant)[0];
				$order_id = explode('-',$transidmerchant)[1];
				if ($trans_type == 'PRD') {
					$payment_info = [
						'36' => 'How to Pay at the ATM
							1. Enter PIN
							2. Choose "Transfer". If using ATM BCA, choose "Others" then "Transfer"
							3. Choose "Other Bank Account"
							4. Enter the bank code (Permata is 013) followed by the 16 digit payment code {paymentcode} as the destination account, then choose "Correct"
							5. Enter the exact amount as your transaction value. Incorrect transfer amount will result in failed payment
							6. Confirm that the bank code, payment code, and transaction amount is correct, then choose "Correct"
							7. Done
							How to Pay Using Internet Banking
							Note: Payment cannot be done using BCA Internet Banking (KlikBCA)
							1. Login to your internet banking account
							2. Choose "Transfer" and choose "Other Bank Account". Enter the bank code (Permata is 013) as the destination account
							3. Enter the exact amount as your transaction value
							4. Enter the destination amount using your 16 digit payment code {paymentcode}
							5. Confirm that the bank code, payment code, and transaction amount is correct, then choose "Correct"
							6. Done',
						'35' => 'How to Pay at AlfaMart
								1. Take note of your payment code and go to your nearest Alfamart, Alfa Midi, Alfa Express, Lawson, Indomaret or DAN+DAN store
								2. Tell the cashier that you wish to make a DOKU payment
								3. If the cashier is unaware of DOKU, provide the instruction to open the e-transaction terminal, choose "2", then "2", then "2" which will then display DOKU option
								4. The cashier will request for the payment code which you will provide {paymentcode}
								5. Make a payment to the cashier according to your transaction amount
								6. Get your receipt as a proof of payment. Your merchant will be directly notified of the payment status
								7. Done',
						'04' => 'Payment has been received, please wait for the next process'
					];
					$comment = nl2br(str_replace('{paymentcode}', $paymentcode, $payment_info[$paymentchannel]));
					$order_id = explode('-',$transidmerchant)[1];
					$this->model_checkout_order->addOrderHistory($order_id, 1, $comment, true,false,$order_info['expired_date']);



					//and update doku table
					if(isset($paymentcode)){
						$this->model_checkout_order->updateDokuHistory($paymentcode, $transidmerchant);
					}

				}
				else if($trans_type == 'MBR'){
					$this->load->model('membership/membership');
					$payment_info = [
						'36' => 'How to Pay at the ATM
							1. Enter PIN
							2. Choose "Transfer". If using ATM BCA, choose "Others" then "Transfer"
							3. Choose "Other Bank Account"
							4. Enter the bank code (Permata is 013) followed by the 16 digit payment code {paymentcode} as the destination account, then choose "Correct"
							5. Enter the exact amount as your transaction value. Incorrect transfer amount will result in failed payment
							6. Confirm that the bank code, payment code, and transaction amount is correct, then choose "Correct"
							7. Done
							How to Pay Using Internet Banking
							Note: Payment cannot be done using BCA Internet Banking (KlikBCA)
							1. Login to your internet banking account
							2. Choose "Transfer" and choose "Other Bank Account". Enter the bank code (Permata is 013) as the destination account
							3. Enter the exact amount as your transaction value
							4. Enter the destination amount using your 16 digit payment code {paymentcode}
							5. Confirm that the bank code, payment code, and transaction amount is correct, then choose "Correct"
							6. Done',
						'35' => 'How to Pay at AlfaMart
								1. Take note of your payment code and go to your nearest Alfamart, Alfa Midi, Alfa Express, Lawson, Indomaret or DAN+DAN store
								2. Tell the cashier that you wish to make a DOKU payment
								3. If the cashier is unaware of DOKU, provide the instruction to open the e-transaction terminal, choose "2", then "2", then "2" which will then display DOKU option
								4. The cashier will request for the payment code which you will provide {paymentcode}
								5. Make a payment to the cashier according to your transaction amount
								6. Get your receipt as a proof of payment. Your merchant will be directly notified of the payment status
								7. Done',
						'04' => 'Payment has been received, please wait for the next process'
					];
					$comment = nl2br(str_replace('{paymentcode}', $paymentcode, $payment_info[$paymentchannel]));
					$order_id = explode('-',$transidmerchant)[1];
					$this->model_membership_membership->addOrderHistory($order_id, $comment,$order_info['expired_date']);



					//and update doku table
					if(isset($paymentcode)){
						$this->model_membership_membership->updateDokuHistory($paymentcode, $transidmerchant);
					}
				}


			}
			else{
				die('order not found');
			}
		}
		else{
			die('order not found');
		}
	}


	public function notify(){		
		$amount = @$this->request->post['AMOUNT'];
		$transidmerchant = @$this->request->post['TRANSIDMERCHANT'];
		$words = @$this->request->post['WORDS'];
		$statustype = @$this->request->post['STATUSTYPE'];
		$responsecode = @$this->request->post['RESPONSECODE'];
		$approvalcode = @$this->request->post['APPROVALCODE'];
		$resultmsg = @$this->request->post['RESULTMSG'];
		$paymentchannel = @$this->request->post['PAYMENTCHANNEL'];
		$paymentcode = @$this->request->post['PAYMENTCODE'];
		$sessionid = @$this->request->post['SESSIONID'];
		$bank = @$this->request->post['BANK'];
		$mcn = @$this->request->post['MCN'];
		$paymentdatetime = @$this->request->post['PAYMENTDATETIME'];
		$verifyid = @$this->request->post['VERIFYID'];
		$verifyscore = @$this->request->post['VERIFYSCORE'];
		$verifystatus = @$this->request->post['VERIFYSTATUS'];
		$currency = @$this->request->post['CURRENCY'];
		$purchasecurrency = @$this->request->post['PURCHASECURRENCY'];
		$brand = @$this->request->post['BRAND'];
		$chname = @$this->request->post['CHNAME'];
		$threedsecurestatus = @$this->request->post['THREEDSECURESTATUS'];
		$liability = @$this->request->post['LIABILITY'];
		$edustatus = @$this->request->post['EDUSTATUS'];

			// Validasi Wors
			$MALLID ='4565'; //input mallid
			$SHAREDKEY ='RzN4pKta12R9'; //input sharedkey
			$WORDS_GENERATED = sha1($amount.$MALLID.$SHAREDKEY.$transidmerchant.$resultmsg.$verifystatus);    
			//$WORDS_GENERATED = sha1($amount.$MALLID.$SHAREDKEY.$transidmerchant);
			// echo $WORDS_GENERATED."<br/>";
			// echo $words."<br/>";
			if ( $words == $WORDS_GENERATED ) {
				// echo "sama<br/>";
				if(isset($transidmerchant)){
					$this->load->model('checkout/order');
					$doku_info = $this->model_checkout_order->getDoku($transidmerchant);
					if(!$doku_info){
						echo 'Stop | Transaction not found';
					}
					else{
						$order_id = explode('-',$transidmerchant)[1];
						if ($resultmsg=="SUCCESS") {
							$trans_type = explode('-',$transidmerchant)[0];
							
							//IF TRANSACTION MEMBERSHIP
							if ($trans_type == 'MBR') {
								if($doku_info['trxstatus']!="SUCCESS"){
									//update doku
									$this->load->model('checkout/doku');
									$data = array(
										'trxstatus'=>$resultmsg,
										'words'=>$words,
										'statustype'=>$statustype,
										'response_code'=>$responsecode,
										'approvalcode'=>$approvalcode,
										'payment_channel'=>$paymentchannel,
										'paymentcode'=>$paymentcode,
										'session_id'=>$sessionid,
										'bank_issuer'=>$bank,
										'creditcard'=>$mcn,
										'payment_date_time'=>$paymentdatetime,
										'verifyid'=>$verifyid,
										'verifyscore'=>$verifyscore,
										'verifystatus'=>$verifystatus,
									);

									$this->model_checkout_doku->update($transidmerchant, $data);



									//update customer membership
									$this->load->model('membership/membership');
									$this->load->model('account/customer');

									$order_info = $this->model_membership_membership->getOrder($order_id);
									// echo "Order id : ".$order_id."<br/><br/>";
									if($order_info){
										// var_dump($order_info);echo "<br/><br/>";
										$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
										if($customer_info){
											// var_dump($customer_info);echo "<br/><br/>";
											$dataMembership['customer'] = $customer_info;
											$dataMembership['status'] = 1;
											$dataMembership['payment_date'] = $this->formatDate($paymentdatetime);
											$dataMembership['amount'] = $amount;
											$dataMembership['membership_period'] = $order_info['membership_period'];
											$dataMembership['membership_name'] = $order_info['membership_name'];
											$dataMembership['membership_discount'] = $order_info['membership_discount'];
											$this->model_membership_membership->editOrder($order_id, $dataMembership,$order_info['customer_id']);
											echo "CONTINUE"; //DOKU NEED WE REPLY WITH THIS									

										}
										else{
											echo "Customer ID not found";
										}

									}
									else{
										echo "Order not found";
									}



								}
								else{
									echo "Transaction already success!";
								}

							}
							//IF TRANSACTION PRODUCT								
							elseif ($trans_type == 'PRD') {
								$order_info = $this->model_checkout_order->getOrder($order_id);
								if($order_info && $doku_info['trxstatus']!="SUCCESS"){
									$this->load->model('checkout/doku');
									$this->load->model('account/customer');
									// var_dump($order_info);exit;

									//$expired_date = $this->model_checkout_doku->getDoku($transidmerchant)['expired_date'];
									//echo $expired_date;exit;

									//update status order
									$_sql_order_status = "SELECT * FROM `" . DB_PREFIX . "order_status` WHERE upper(name) = 'ON PROCESS' limit 1"; //many language but still same id
									$order_status_id = $this->db->query($_sql_order_status)->row['order_status_id'];
									//$order_id, $order_status_id, $comment = '', $notify = false, $override = false,$expired_date=null
									$this->model_checkout_order->addOrderHistory($order_id, $order_status_id,'',true,false,$doku_info['expired_date']);


									//if refferal, send reward to upline									
									$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
									if($customer_info && $customer_info['reference_id']!=NULL){
										//find reference
										$reference_info = $this->model_account_customer->getCustomer($customer_info['reference_id']);
										if($reference_info){
											//find if membership or not
											$membership_info = $this->model_account_customer->getCustomerMembership($customer_info['reference_id']);
											if($membership_info){ //IF REFERAL IS MEMBERSHIP					
												$bonus_point = $membership_info['membership_bonus_point'];
												$results = $this->db->query("SELECT * FROM ".DB_PREFIX."order_product WHERE order_id = ".$order_id)->rows;
												$_reward = 0;
												foreach ($results as $result) {
													$_reward  = (int) $_reward + floor($bonus_point*$result['price']*$result['quantity']);
												}

												$sql_reward = "UPDATE ".DB_PREFIX."customer SET reward = reward + ".$_reward." WHERE customer_id  = ".$customer_info['reference_id']."";
												$this->db->query($sql_reward);
											}
											else{ //IF REFERAL IS NOT MEMBERSHIP
												$bonus_point = $this->config->get('config_referral_reward');
												$results = $this->db->query("SELECT * FROM ".DB_PREFIX."order_product WHERE order_id = ".$order_id)->rows;
												$_reward = 0;
												foreach ($results as $result) {
													$_reward  = (int) $_reward + floor($bonus_point*$result['price']*$result['quantity']);
												}

												$sql_reward = "UPDATE ".DB_PREFIX."customer SET reward = reward + ".$_reward." WHERE customer_id  = ".$customer_info['reference_id']."";
												$this->db->query($sql_reward);

											}											

										}

									}


									//update doku
									
									$data = array(
										'trxstatus'=>$resultmsg,
										'words'=>$words,
										'statustype'=>$statustype,
										'response_code'=>$responsecode,
										'approvalcode'=>$approvalcode,
										'payment_channel'=>$paymentchannel,
										'paymentcode'=>$paymentcode,
										'session_id'=>$sessionid,
										'bank_issuer'=>$bank,
										'creditcard'=>$mcn,
										'payment_date_time'=>$paymentdatetime,
										'verifyid'=>$verifyid,
										'verifyscore'=>$verifyscore,
										'verifystatus'=>$verifystatus,
									);

									$this->model_checkout_doku->update($transidmerchant, $data);
									echo "CONTINUE"; //DOKU NEED WE REPLY WITH THIS
								}
								else{
									echo "Transaction already success!";
								}


							}	


						} else {
							//update doku
							$this->load->model('checkout/doku');
							$data = array(
								'trxstatus'=>'FAILED',
							);

							$this->model_checkout_doku->update($transidmerchant, $data);
							echo "CONTINUE"; //DOKU NEED WE REPLY WITH THIS

						}					
					}					
				}

			}
			else{
				echo "Stop | Words not match";	
			}
			

		
	}

	public function formatDate($_date){
		//20170707105735
		$year = substr($_date,0,4);
		$month = substr($_date,4,2);
		$date = substr($_date,6,2);
		$hour = substr($_date, 8,2);
		$min = substr($_date, 10,2);
		$sec = substr($_date, 12,2);
		return $year."-".$month."-".$date." ".$hour.":".$min.":".$sec;

	}

	public function index() {
		// $status_id = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_status` WHERE upper(name) = 'PROCESSED'")->row['order_status_id'];
		// echo $status_id;exit;
		$this->load->model('checkout/order');

		$ip_range = "103.10.129.16";
		if ( $_SERVER['REMOTE_ADDR'] != '103.10.129.16' && (substr($_SERVER['REMOTE_ADDR'],0,strlen($ip_range)) !== $ip_range) ){
			if(isset($_POST['TRANSIDMERCHANT'])) {
				$order_id = $_POST['TRANSIDMERCHANT'];
			} 
			else {
				$order_id = 0;
			}


			$totalamount = @$_POST['AMOUNT'];
			$words    = @$_POST['WORDS'];
			$statustype = @$_POST['STATUSTYPE'];
			$response_code = @$_POST['RESPONSECODE'];
			$approvalcode   = @$_POST['APPROVALCODE'];
			$status         = @$_POST['RESULTMSG'];
			$paymentchannel = @$_POST['PAYMENTCHANNEL'];
			$paymentcode = @$_POST['PAYMENTCODE'];
			$session_id = @$_POST['SESSIONID'];
			$bank_issuer = @$_POST['BANK'];
			$cardnumber = @$_POST['MCN'];
			$payment_date_time = @$_POST['PAYMENTDATETIME'];
			$verifyid = @$_POST['VERIFYID'];
			$verifyscore = @$_POST['VERIFYSCORE'];
			$verifystatus = @$_POST['VERIFYSTATUS'];

			// Validasi Wors
			$MALLID =''; //input mallid
			$SHAREDKEY =''; //input sharedkey

			$WORDS_GENERATED = sha1($totalamount.$MALLID.$SHAREDKEY.$order_id.$status.$verifystatus);    
			if ( $words == $WORDS_GENERATED ) {
				$order_info = $this->model_checkout_order->getOrder($order_id);
				if(!$order_info){
					echo 'Stop1';
				}
				else{
					if ($status=="SUCCESS") {
						//update status order
						$order_status_id = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_status` WHERE upper(name) = 'PROCESSED'")->row['order_status_id'];
						$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);


						//update doku
						$this->load->model('checkout/doku');
						$data = array(
							'trxstatus'=>'$status',
							'words'=>'$words',
							'statustype'=>'$statustype',
							'response_code'=>'$response_code',
							'approvalcode'=>'$approvalcode',
							'payment_channel'=>'$paymentchannel',
							'paymentcode'=>'$paymentcode',
							'session_id'=>'$session_id',
							'bank_issuer'=>'$bank_issuer',
							'creditcard'=>'$cardnumber',
							'payment_date_time'=>'$payment_date_time',
							'verifyid'=>'$verifyid',
							'verifyscore'=>'$verifyscore',
							'verifystatus'=>'$verifystatus',
						);

						$this->model_checkout_doku->update($order_id, $data);

					} else {
						$sql = "UPDATE doku set trxstatus='Failed' where transidmerchant='".$order_number."'";
						$result = mysql_query($sql) or die ("Stop3");
					}
					echo 'Continue';		
					exit;		
				}



			} else {
			echo "Stop | Words not match";
			exit;
			}

		}
		echo 'Unknown Error!';

	}






























	




}
