<?php
class ControllerCheckoutShippingMethod extends Controller {
	public function index() {
		// var_dump($this->session->data['shipping_address']);
		// exit;
		$this->load->language('checkout/checkout');

		if (isset($this->session->data['shipping_address'])) {
			//var_dump($this->session->data['shipping_address']);exit;
			//echo "1";
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('shipping');
			$id_city = $this->session->data['shipping_address']['id_city'];
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/shipping/' . $result['code']);
					$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address'],$id_city);

					if ($quote) {			
						foreach ($quote as $key => $value) {
							# code...
							// echo $key;
							$method_data[$key] = array(
								'title'      => $quote[$key]['title'],
								'quote'      => $quote[$key]['quote'],
								'sort_order' => $quote[$key]['sort_order'],
								'error'      => $quote[$key]['error']
							);

						}
						// echo '<pre>' . var_export($quote, true) . '</pre>';
						// $method_data[$result['code']] = array(
						// 	'title'      => $quote['title'],
						// 	'quote'      => $quote['quote'],
						// 	'sort_order' => $quote['sort_order'],
						// 	'error'      => $quote['error']
						// );
					}			
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);
			//exit;







			//POS SHIPPING
			// $weight = round($this->cart->getWeight()*1000);
			// if($weight<1000){
			// 	$weight = 1000; //in gr
			// }
			// $destination = $this->session->data['shipping_address']['id_city'];
			// $pos = $this->getPos($destination,$weight);
			// //echo '<pre>' . var_export($pos, true) . '</pre>';
			// //var_dump($pos);EXIT;
			// //exit;
			// $arr_service_pos = array('Paket Kilat Khusus','Express Sameday Barang','Express Next Day Barang');
			// $status = $pos['rajaongkir']['status']['code']; //200 = OK
			// if($status=="200"){
			// 	$method_data['posx'] = array(
			// 		'title' => 'POS',
			// 		'quote' => array(),
			// 		'sort_order' => '2',
			// 		'error' => false,
			// 		);
			// 	$res = $pos['rajaongkir']['results'];
			// 	foreach ($res as $_res) {
			// 		foreach($_res['costs'] as $costs){
			// 			foreach($costs['cost'] as $_cost){
			// 				if(in_array($costs['service'], $arr_service_pos) && !isset($method_data['posx']['quote'][strtolower($costs['service'])])){
			// 				$method_data['posx']['quote'][strtolower($costs['service'])]['code'] = $_res['code'].".".strtolower($costs['service']);
			// 				$method_data['posx']['quote'][strtolower($costs['service'])]['title'] = 'POS '.$costs['service'];
			// 				$method_data['posx']['quote'][strtolower($costs['service'])]['cost'] = $_cost['value'];
			// 				$method_data['posx']['quote'][strtolower($costs['service'])]['tax_class_id'] = $this->config->get('pos_tax_class_id');
			// 				$method_data['posx']['quote'][strtolower($costs['service'])]['text'] = $this->currency->format($this->tax->calculate($_cost['value'], $this->config->get('pos_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']);


			// 				}
			// 			}
			// 		}
			// 	}
			// }
			//echo '<pre>' . var_export($method_data, true) . '</pre>';



			$this->session->data['shipping_methods'] = $method_data;
			//var_dump($this->session->data['shipping_methods']);




		}
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['btn_next'] = HTTP_SERVER.'image/checkout/btn_next_'.$this->session->data['language'].'.png';

		if (empty($this->session->data['shipping_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['shipping_methods'])) {
			$data['shipping_methods'] = $this->session->data['shipping_methods'];
		} else {
			$data['shipping_methods'] = array();
		}


		if (isset($this->session->data['shipping_method']['code'])) {
			$data['code'] = $this->session->data['shipping_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		// var_dump($this->session->data['shipping_address']);
		$_shipping_address = $this->session->data['shipping_address'];
		$data['destination'] = '<b>'.$_shipping_address['firstname'].' '.$_shipping_address['lastname'].'</b><br/>'.$_shipping_address['address_1'].' '.$_shipping_address['address_2'].'<br/>'.$_shipping_address['city'].', '.$_shipping_address['zone'].'<br/>'.$_shipping_address['country'].' - '.$_shipping_address['postcode'];
		//echo $destination;exit;



		//var_dump($data['shipping_methods']);
		//var_dump($this->session->data['shipping_methods']);
		$this->response->setOutput($this->load->view('checkout/shipping_method', $data));
	}



	///pos indonesia courier
	public function getPos($destination,$weight,$origin=151){ 
		//die($weight);
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  origin* - id kota asal (POST)
		  destination* - id kota tujuan (POST)
		  weight* - berat dalam gram (POST)
		  courier* - kode courer:jne, pos, tiki (POST)
		*/ 

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=pos",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);		
		curl_close($curl);
		
		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		  //echo '<pre>' . var_export(json_decode($response,true), true) . '</pre>';
		  //exit;
		}


	}

	// public function getJNE($destination,$weight,$origin=151){
	// 	//echo $weight;exit;
	// 	//die($weight);
	// 	$curl = curl_init();
	// 	/*
	// 	  key* - API key (POST/HEAD)
	// 	  origin* - id kota asal (POST)
	// 	  destination* - id kota tujuan (POST)
	// 	  weight* - berat dalam gram (POST)
	// 	  courier* - kode courer:jne, pos, tiki (POST)
	// 	*/ 

	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 30,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "POST",
	// 	  CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=jne",
	// 	  CURLOPT_HTTPHEADER => array(
	// 	    "content-type: application/x-www-form-urlencoded",
	// 	    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
	// 	  ),
	// 	));

	// 	$response = curl_exec($curl);
	// 	$err = curl_error($curl);
	// 	curl_close($curl);
	// 	//echo $response;exit;
	// 	if ($err) {
	// 	  return "cURL Error #:" . $err;
	// 	} else {
	// 	  return json_decode($response,true);
	// 	}


	// }

	public function save() {
		//var_dump($this->session->data['shipping_methods']);
		
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if shipping is required. If not the customer should not have reached this page.
		// if (!$this->cart->hasShipping()) {
		// 	$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		// }

		// Validate if shipping address has been set.
		if (!isset($this->session->data['shipping_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}


		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}
		$_shipping_methods = array();
		//var_dump($this->session->data['shipping_methods']);
		if (!isset($this->request->post['shipping_method'])) {
			//echo "a";exit;
			$json['error']['warning'] = $this->language->get('error_shipping');
		} else {
			//echo "b";exit;
			$shipping = explode('.', $this->request->post['shipping_method']);
			// if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
			// 	$json['error']['warning'] = $this->language->get('error_shipping');
			// }

			if (!isset($shipping[0]) || !isset($shipping[1])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			}
		}
		//exit;
		
		//var_dump($this->session->data['shipping_methods']);
		if (!$json && isset($shipping[0]) && isset($shipping[1])) {
			// echo $shipping[0]."<br/>";
			// echo $shipping[1]."<br/>";
			// echo '<pre>' . var_export($this->session->data['shipping_methods'], true) . '</pre>';exit;

			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][($shipping[0])]['quote'][$shipping[1]];	
			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		//echo '<pre>' . var_export($this->session->data['shipping_method'], true) . '</pre>';exit;
		// var_dump($json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}