<?php
//http://128.199.70.89/index.php?route=checkout/callbackdoku/identify
//http://128.199.70.89/index.php?route=checkout/callbackdoku/notify
class ControllerCheckoutCallback extends Controller {
	public function index(){
		$json = file_get_contents("php://input");

		//saving callback to db & log
		$this->db->query("INSERT INTO `" . DB_PREFIX . "callback` (callback) values ('".$json."') ");
		$log = new Log('callback.log');
		$log->write(" ");
		//$log->write($json);
		/*
		{
		"data":
		{"id":"5a82987ceef87c0fcd2816fd","timestamp":1518508261,"details":{"app_id":"84a6673c17364fa5bc01","user_id":"33","merchant_transaction_id":"PRD-242","transaction_description":"Transaction Product #PRD-242","payment_channel":"Airtime Testing","currency":"IDR","amount":10000,"status_code":"PAYMENT_COMPLETED","status":"Payment Completed","item_id":"","item_name":"KR House PRD-242","custom":""}}}
		*/

		// echo 'ok';exit;
		$data = json_decode($json,true);
		//$log->write($data);
		$id = $data['data']['id'];
		$timestamp = $data['data']['timestamp'];
		$details = $data['data']['details'];

		$app_id = $data['data']['details']['app_id'];
		$user_id = $data['data']['details']['user_id'];
		$merchant_transaction_id = $data['data']['details']['merchant_transaction_id'];
		$transaction_description = $data['data']['details']['transaction_description'];
		$payment_channel = $data['data']['details']['payment_channel'];
		$currency = $data['data']['details']['currency'];
		$amount = $data['data']['details']['amount'];
		$status_code = $data['data']['details']['status_code'];
		$status = $data['data']['details']['status'];
		$item_id = $data['data']['details']['item_id'];
		$item_name = $data['data']['details']['item_name'];
		$custom = $data['data']['details']['custom'];


		// echo '<pre>' . var_export($data, true) . '</pre>';
		// echo '<br/>';
		// echo $details['status_code'];
		if($status_code=="PAYMENT_COMPLETED"){
			$log->write("payment completed");
			$trans_type = explode('-',$merchant_transaction_id)[0];
			$uniqueID = explode('-',$merchant_transaction_id)[1];
			if ($trans_type == 'PRD') { //product purchase
				$log->write("PRD transaction");
				$this->load->model('checkout/order');
				$order_info = $this->model_checkout_order->getOrderByUniqueID($merchant_transaction_id);

				$complete_processing_status = array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status'));

				if($order_info && !in_array($order_info['order_status_id'], $complete_processing_status)){
					$order_id = $order_info['order_id'];
					$log->write("order found & not complete");
					$this->load->model('checkout/doku');
					$this->load->model('account/customer');


					//update status order
					$_sql_order_status = "SELECT * FROM `" . DB_PREFIX . "order_status` WHERE upper(name) = 'ON PROCESS' limit 1"; //many language but still same id
					$order_status_id = $this->db->query($_sql_order_status)->row['order_status_id'];
					$log->write("order_id:".$order_id);
					$log->write("new order_status_id:".$order_status_id);
					$this->model_checkout_order->addOrderHistory($order_id, $order_status_id,'',true,false,null,$id);


					//if refferal, send reward to upline									
					$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
					if($customer_info && $customer_info['reference_id']!=NULL){
						//find reference
						$reference_info = $this->model_account_customer->getCustomer($customer_info['reference_id']);
						if($reference_info){
							//find if membership or not
							$membership_info = $this->model_account_customer->getCustomerMembership($customer_info['reference_id']);
							if($membership_info){ //IF REFERAL IS MEMBERSHIP					
								$bonus_point = $membership_info['membership_bonus_point'];
								$results = $this->db->query("SELECT * FROM ".DB_PREFIX."order_product WHERE order_id = ".$order_id)->rows;
								$_reward = 0;
								foreach ($results as $result) {
									$_reward  = (int) $_reward + floor($bonus_point*$result['price']*$result['quantity']);
								}

								$sql_reward = "UPDATE ".DB_PREFIX."customer SET reward = reward + ".$_reward." WHERE customer_id  = ".$customer_info['reference_id']."";
								$this->db->query($sql_reward);
							}
							else{ //IF REFERAL IS NOT MEMBERSHIP
								$bonus_point = $this->config->get('config_referral_reward');
								$results = $this->db->query("SELECT * FROM ".DB_PREFIX."order_product WHERE order_id = ".$order_id)->rows;
								$_reward = 0;
								foreach ($results as $result) {
									$_reward  = (int) $_reward + floor($bonus_point*$result['price']*$result['quantity']);
								}

								$sql_reward = "UPDATE ".DB_PREFIX."customer SET reward = reward + ".$_reward." WHERE customer_id  = ".$customer_info['reference_id']."";
								$this->db->query($sql_reward);

							}										
						}
						echo 'ok';exit;
					}
					
				}
				else{
					$log->write("order not found or already complete");
				}
			}
			else if($trans_type == 'MBR'){ //membership purchase
				$log->write("MBR transaction");
				//update customer membership
				$this->load->model('membership/membership');
				$this->load->model('account/customer');

				$order_info = $this->model_membership_membership->getOrderByUniqueID($merchant_transaction_id);
				if($order_info && $order_info['status']=="0"){
					$customer_membership_id = $order_info['customer_membership_id'];
					$log->write("order found");
					$log->write("customer_membership_id : ".$customer_membership_id);
					$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
					if($customer_info){
						$dataMembership['customer'] = $customer_info;
						$dataMembership['status'] = 1;
						$dataMembership['payment_date'] = date('Y-m-d H:i:s');//$this->formatDate($paymentdatetime);
						$dataMembership['amount'] = $amount;
						$dataMembership['membership_period'] = $order_info['membership_period'];
						$dataMembership['membership_name'] = $order_info['membership_name'];
						$dataMembership['membership_discount'] = $order_info['membership_discount'];
						$log->write("membership_name:".$dataMembership['membership_name']);
						$log->write("membership_discount:".$dataMembership['membership_discount']);
						$this->model_membership_membership->editOrder($customer_membership_id, $dataMembership,$order_info['customer_id'],$id);
						$log->write("order updated, email sent");
						echo 'ok';exit;
					}
					
				}
				else{
					$log->write("order not found or already complete");
				}
			}
		}
		else{
			$log->write("payment not completed");
		}
	}


	public function formatDate($_date){
		//20170707105735
		$year = substr($_date,0,4);
		$month = substr($_date,4,2);
		$date = substr($_date,6,2);
		$hour = substr($_date, 8,2);
		$min = substr($_date, 10,2);
		$sec = substr($_date, 12,2);
		return $year."-".$month."-".$date." ".$hour.":".$min.":".$sec;

	}
































	




}
