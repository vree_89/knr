<?php
class ControllerCheckoutCoupon extends Controller {
	public function index() {
			$this->load->language('checkout/checkout');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_coupon'] = $this->language->get('text_coupon');
			$data['text_loading'] = $this->language->get('text_loading');
			$data['text_select'] = $this->language->get('text_select');
			$data['text_modify_cart'] = sprintf($this->language->get('text_modify_cart'), $this->url->link('checkout/cart'));
			$data['entry_coupon'] = $this->language->get('entry_coupon');

			$data['button_apply'] = $this->language->get('button_apply');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_upload'] = $this->language->get('button_upload');

			$data['button_update'] = $this->language->get('button_update');
			$data['button_remove'] = $this->language->get('button_remove');


			$data['column_image'] = $this->language->get('column_image');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_remove'] = $this->language->get('column_remove');

			if ($this->config->get('config_cart_weight')) {
				$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));

				if($this->cart->getWeight()<=0){
					$data['weight'] = $this->weight->format(number_format(1,3), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
				}
				else{
					$data['weight']	 = $this->weight->format(number_format(ceil($this->cart->getWeight()),3), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
				}
			} else {
				$data['weight'] = '';
			}


			$reward_percentage = null;
			if($this->customer->isLogged()){
				//echo "logged<br/>";
				if(isset($this->session->data['membership']['membership_loyalty_point']) && isset($this->session->data['membership']['membership_bonus_point'])){
					$reward_percentage = $this->session->data['membership']['membership_loyalty_point'];
					//echo "pertama</br>";

				}
				else{
					$reward_percentage = $this->config->get('config_global_reward') ;
					//echo "kedua</br>";
				}
			}



			$this->load->model('tool/image');
			$this->load->model('tool/upload');

			$this->load->model('catalog/product');
			$data['products'] = array();
			$products = $this->cart->getProducts();
			//var_dump($products);
			//echo "<br/>";

			foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {					
					$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					if($image==""){
						$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					}
				} else {
					$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}


				$option_data = array();

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
					
					$price = $this->currency->format($unit_price, $this->session->data['currency']);
					$price_num = $unit_price;
					$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
				} else {
					$price = false;
					$price_num = false;
					$total = false;
				}



				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}



				$reward = "";
				if($reward_percentage && $price_num){
					//echo $reward_percentage."<br/>";
					//echo $price_num."<br/>";
					$reward = sprintf($this->language->get('text_points'), floor($reward_percentage*$price_num*$product['quantity']));
				}


				$data['products'][] = array(
					//'options'=>$_options,
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'recurring' => $recurring,
					'quantity'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					//'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					'reward' => $reward,
					'price'     => $price,
					'total'     => $total,
					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'is_preorder'      => $product['is_preorder'],
					'date_available'      => $product['date_available'],
				);
			}



			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			// Display prices
			//if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {		
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);
					
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);
			//var_dump($totals);exit;
			//}

			$data['order_total'] = $total_data['total'];
			$data['currency'] = $this->session->data['currency'];
			$data['logged'] = $this->customer->isLogged();

			$data['totals'] = array();
			
			foreach ($totals as $total) {
				//echo $total['value']."<br/>";
				if($total['title'] == 'Use Wallet' && $this->customer->isLogged()){

					$data['totals'][] = array(
						'currency'=>$this->session->data['currency'],
						'type' => $total['type'],
						'code' => $total['code'],
						'title' => $total['title'].'<br/>(Your Wallet : '.(int) $this->session->data['customer_deposit'].')',
						'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
						'value'=>abs($total['value']),
						'symbol'=> $total['value']>=0 ? '' : '-',
						'button' => $total['button'],
					);

				}
				else {
					$data['totals'][] = array(
						'currency'=>$this->session->data['currency'],
						'type' => $total['type'],
						'code' => $total['code'],
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
						'value'=>abs($total['value']),
						'symbol'=> $total['value']>=0 ? '' : '-',
						'button' => $total['button'],
					);
				}


			}
			// exit;
			//var_dump($data['totals']);exit;

			$data['continue'] = $this->url->link('common/home');

			$data['checkout'] = $this->url->link('checkout/checkout', '', true);

			$this->load->model('extension/extension');

			//exit;
			//var_dump($data['totals']);exit;

		if (isset($this->session->data['coupon'])) {
			$data['coupon'] = $this->session->data['coupon'];
		} else {
			$data['coupon'] = '';
		}

		if (isset($this->session->data['input_payment_wallet'])) {
			$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
		} else {
			$data['input_payment_wallet'] = 0;
		}

		$this->session->data['input_payment_total'] = $data['order_total'];
		$data['input_payment_total'] = $data['order_total'];	


		//echo $data['input_payment_wallet'];exit;

		if($this->customer->isLogged()){
			if(isset($this->session->data['membership'])){
				$data['is_wallet'] = true;	
			}
			else{
				$data['is_wallet'] = false;	
			}
		}
		else{
			$data['is_wallet'] = false;
		}




		//var_dump(@$this->session->data['shipping_method']);exit;
		$data['shipping_method'] = @$this->session->data['shipping_method'];
		$data['cart_url'] = $this->url->link('checkout/cart');
		$data['action'] = $this->url->link('checkout/cart/edit', '', true);



		$data['deposit'] = 0;
		if(isset($this->session->data['customer_deposit'])){
			$data['deposit'] = $this->session->data['customer_deposit'];
		}
		
		if($this->customer->isLogged()){
		if($data['input_payment_total']<0 || $data['order_total']<0 || ($data['input_payment_total']>0 && $data['order_total']<50000 && $data['input_payment_wallet']>0)){
				//set payment wallet to 0
				$this->session->data['input_payment_wallet'] = 0;
				//echo "<p>Error : minus/salah perhitungan</p>";

				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
				
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);		

				// Display prices
				//if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);


				foreach ($results as $result) {		
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
				//}

				$data['order_total'] = $total_data['total'];
				$data['currency'] = $this->session->data['currency'];
				$data['logged'] = $this->customer->isLogged();
								//die('x');


				$data['totals'] = array();

				foreach ($totals as $total) {
					if($total['title'] == 'Use Wallet' && $this->customer->isLogged()){
						$data['totals'][] = array(
							'currency'=>$this->session->data['currency'],
							'type' => $total['type'],
							'code' => $total['code'],
							'title' => $total['title'].'<br/>(Your Wallet : '.(int) $this->session->data['customer_deposit'].')',
							'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
							'value'=>abs($total['value']),
							'symbol'=> $total['value']>=0 ? '' : '-',
							'button' => $total['button'],
						);

					}
					else {
						$data['totals'][] = array(
								'currency'=>$this->session->data['currency'],
								'type' => $total['type'],
								'code' => $total['code'],
								'title' => $total['title'],
								'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
								'value'=>abs($total['value']),
								'symbol'=> $total['value']>=0 ? '' : '-',
								'button' => $total['button'],
						);
					}
				}
				$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
				$this->session->data['input_payment_total'] = $data['order_total'];		
				$data['input_payment_total'] = $this->session->data['input_payment_total'];
		}			
		}

		$data['btn_use'] = HTTP_SERVER.'image/checkout/btn_use_'.$this->session->data['language'].'.png';
		$data['btn_next'] = HTTP_SERVER.'image/checkout/btn_next_'.$this->session->data['language'].'.png';

		$this->response->setOutput($this->load->view('checkout/coupon', $data));
	}


	//apply voucher discount
	public function save() {
		$this->load->language('extension/total/coupon');

		$json = array();

		$this->load->model('extension/total/coupon');

		if (isset($this->request->post['coupon'])) {
			$coupon = $this->request->post['coupon'];
		} else {
			$coupon = '';
		}

		$coupon_info = $this->model_extension_total_coupon->getCoupon($coupon);
		//var_dump($coupon_info);EXIT;
		if (empty($this->request->post['coupon'])) {
			$json['error']['warning'] = $this->language->get('error_empty');

			unset($this->session->data['coupon']);
		} elseif ($coupon_info) {
			if($coupon_info['status']){
				$this->session->data['coupon'] = $this->request->post['coupon'];

				$json['success'] = $this->language->get('text_success');

				//$json['redirect'] = $this->url->link('checkout/cart');
			}
			else{
				//unset the previous coupon used
				unset($this->session->data['coupon']);

				$json['error']['warning'] = $coupon_info['message'];	
			}
		} else {
			$json['error']['warning'] = $this->language->get('error_coupon');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function setwallet(){
		$input_payment_wallet = $this->request->post['input_payment_wallet'];
		$input_payment_total = $this->request->post['input_payment_total'];

		$this->session->data['input_payment_wallet'] = $input_payment_wallet;
		$this->session->data['input_payment_total'] = $input_payment_total;
	}

	public function reload() {
		$this->load->language('checkout/checkout');

		$data['heading_title'] = $this->language->get('heading_title');



		$data['text_coupon'] = $this->language->get('text_coupon');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_select'] = $this->language->get('text_select');
		// $data['action'] = $this->url->link('checkout/cart/ajax_edit', '', true);
		$data['entry_coupon'] = $this->language->get('entry_coupon');

		$data['button_apply'] = $this->language->get('button_apply');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		$data['button_update'] = $this->language->get('button_update');
		$data['button_remove'] = $this->language->get('button_remove');


		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_remove'] = $this->language->get('column_remove');


		if ($this->config->get('config_cart_weight')) {
			$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));

			if($this->cart->getWeight()<=0){
				$data['weight'] = $this->weight->format(number_format(1,3), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			}
			else{
				$data['weight']	 = $this->weight->format(number_format(ceil($this->cart->getWeight()),3), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			}
		} else {
			$data['weight'] = '';
		}


		$reward_percentage = null;
		if($this->customer->isLogged()){
			//echo "logged<br/>";
			if(isset($this->session->data['membership']['membership_loyalty_point']) && isset($this->session->data['membership']['membership_bonus_point'])){
				$reward_percentage = $this->session->data['membership']['membership_loyalty_point'];
				//echo "pertama</br>";

			}
			else{
				$reward_percentage = $this->config->get('config_global_reward') ;
				//echo "kedua</br>";
			}
		}

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$this->load->model('catalog/product');
		$data['products'] = array();
		$products = $this->cart->getProducts();
		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
			}

			// if ($product['image']) {
			// 	$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			// 	//$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
			// } else {
			// 	$image = '';
			// }

			if ($product['image']) {					
				$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				if($image==""){
					$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
			} else {
				$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			}


			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			// // Display prices
			// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			// 	$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
				
			// 	$price = $this->currency->format($unit_price, $this->session->data['currency']);
			// 	$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
			// } else {
			// 	$price = false;
			// 	$total = false;
			// }

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
				
				$price = $this->currency->format($unit_price, $this->session->data['currency']);
				$price_num = $unit_price;
				$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
			} else {
				$price = false;
				$price_num = false;
				$total = false;
			}

			$recurring = '';

			if ($product['recurring']) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($product['recurring']['trial']) {
					$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
				}

				if ($product['recurring']['duration']) {
					$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				} else {
					$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				}
			}


				$reward = "";
				if($reward_percentage && $price_num){
					//echo $reward_percentage."<br/>";
					//echo $price_num."<br/>";
					$reward = sprintf($this->language->get('text_points'), floor($reward_percentage*$price_num*$product['quantity']));
				}

			$data['products'][] = array(
				//'options'=>$_options,
				'cart_id'   => $product['cart_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => $recurring,
				'quantity'  => $product['quantity'],
				'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
				//'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
				'reward'    => $reward,
				'price'     => $price,
				'total'     => $total,
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id']),
				'is_preorder'      => $product['is_preorder'],
				'date_available'      => $product['date_available'],
			);
		}



		// Totals
		$this->load->model('extension/extension');

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;
		
		// Because __call can not keep var references so we put them into an array. 			
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);		

		// Display prices
		//if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
		$sort_order = array();

		$results = $this->model_extension_extension->getExtensions('total');

		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);


		foreach ($results as $result) {		
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('extension/total/' . $result['code']);
				
				// We have to put the totals in an array so that they pass by reference.
				$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
			}
		}

		$sort_order = array();

		foreach ($totals as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $totals);
		//var_dump($totals);exit;
		//}

		$data['order_total'] = $total_data['total'];
		$data['currency'] = $this->session->data['currency'];
		$data['logged'] = $this->customer->isLogged();
						//die('x');


		$data['totals'] = array();

		foreach ($totals as $total) {
			//echo $total['value']."<br/>";
			if($total['title'] == 'Use Wallet'){
				$data['totals'][] = array(
				'currency'=>$this->session->data['currency'],
				'type' => $total['type'],
				'code' => $total['code'],
				'title' => $total['title'].'<br/>(Your Wallet : '.(int) $this->session->data['customer_deposit'].')',
				'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
				'value'=>abs($total['value']),
				'symbol'=> $total['value']>=0 ? '' : '-',
				'button' => $total['button'],
				);

			}
			else {
				$data['totals'][] = array(
				'currency'=>$this->session->data['currency'],
				'type' => $total['type'],
				'code' => $total['code'],
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
				'value'=>abs($total['value']),
				'symbol'=> $total['value']>=0 ? '' : '-',
				'button' => $total['button'],
				);
			}



		}
		// exit;
		//var_dump($data['totals']);exit;

		$data['continue'] = $this->url->link('common/home');

		$data['checkout'] = $this->url->link('checkout/checkout', '', true);

		$this->load->model('extension/extension');
		$data['action'] = $this->url->link('checkout/cart/edit', '', true);
		//echo "sess payment wallet skrg ".$this->session->data['input_payment_wallet']."<br/>";
		//echo "sess payment total skrg ".$this->session->data['input_payment_total']."<br/>";
		if (isset($this->session->data['input_payment_wallet'])) {
			//echo "ada wallet : ".$this->session->data['input_payment_wallet']."<br/>";
			$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
		} else {
			//echo "ga ada wallet<br/>";
			$data['input_payment_wallet'] = 0;
		}

		$this->session->data['input_payment_total'] = $data['order_total'];
		$data['input_payment_total'] = $data['order_total'];	





		if($this->customer->isLogged()){
		if($data['input_payment_total']<0 || $data['order_total']<0 || ($data['input_payment_total']>0 && $data['order_total']<50000 && $data['input_payment_wallet']>0)){

				//set payment wallet to 0
				$this->session->data['input_payment_wallet'] = 0;
				//echo "<p>Error : minus/salah perhitungan</p>";


				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
				
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);		

				// Display prices
				//if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);


				foreach ($results as $result) {		
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
				//var_dump($totals);exit;
				//}

				$data['order_total'] = $total_data['total'];
				$data['currency'] = $this->session->data['currency'];
				$data['logged'] = $this->customer->isLogged();
								//die('x');


				$data['totals'] = array();

				foreach ($totals as $total) {
					//echo $total['value']."<br/>";

					if($total['title'] == 'Use Wallet'){
						$data['totals'][] = array(
							'currency'=>$this->session->data['currency'],
							'type' => $total['type'],
							'code' => $total['code'],
							'title' => $total['title'].'<br/>(Your Wallet : '.(int) $this->session->data['customer_deposit'].')',
							'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
							'value'=>abs($total['value']),
							'symbol'=> $total['value']>=0 ? '' : '-',
							'button' => $total['button'],
						);
					}
					else {
						$data['totals'][] = array(
							'currency'=>$this->session->data['currency'],
							'type' => $total['type'],
							'code' => $total['code'],
							'title' => $total['title'],
							'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
							'value'=>abs($total['value']),
							'symbol'=> $total['value']>=0 ? '' : '-',
							'button' => $total['button'],
						);
					}

				}

				$data['input_payment_wallet'] = $this->session->data['input_payment_wallet'];
				$this->session->data['input_payment_total'] = $data['order_total'];
				$data['input_payment_total'] = $this->session->data['input_payment_total'];
		

		}			
		}

		if (isset($this->session->data['coupon'])) {
			$data['coupon'] = $this->session->data['coupon'];
		} else {
			$data['coupon'] = '';
		}


		$this->response->setOutput($this->load->view('checkout/reload_cart', $data));




	}

}