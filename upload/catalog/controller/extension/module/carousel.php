<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		// $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		// $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['banners'] = array();


		$this->load->model('catalog/manufacturer');
		$results = $this->model_catalog_manufacturer->getManufacturers();

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['name'],
					'link'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']),
					'image' => HTTP_SERVER.'image/'.$result['image']
				);
			}
		}

		// $results = $this->model_design_banner->getBanner($setting['banner_id']);
		// foreach ($results as $result) {
		// 	//echo HTTP_SERVER.'image/'.$result['image']."<br/>";
		// 	if (is_file(DIR_IMAGE . $result['image'])) {
		// 		$data['banners'][] = array(
		// 			'title' => $result['title'],
		// 			'link'  => $result['link'],
		// 			//'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
		// 			'image' => HTTP_SERVER.'image/'.$result['image']
		// 		);
		// 	}
		// }

		$data['module'] = $module++;
		//var_dump($data);
		/*
array(2) { 
	["banners"]=> array(11) { 
	[0]=> array(3) { 
		["title"]=> string(3) "NFL" 
		["link"]=> string(0) "" 
		["image"]=> string(86) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/nfl-130x100.png" 
	} 
	[1]=> array(3) { 
		["title"]=> string(7) "RedBull" 
		["link"]=> string(0) "" 
		["image"]=> string(90) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/redbull-130x100.png" } [2]=> array(3) { ["title"]=> string(4) "Sony" ["link"]=> string(0) "" ["image"]=> string(87) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/sony-130x100.png" } [3]=> array(3) { ["title"]=> string(9) "Coca Cola" ["link"]=> string(0) "" ["image"]=> string(91) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/cocacola-130x100.png" } [4]=> array(3) { ["title"]=> string(11) "Burger King" ["link"]=> string(0) "" ["image"]=> string(93) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/burgerking-130x100.png" } [5]=> array(3) { ["title"]=> string(5) "Canon" ["link"]=> string(0) "" ["image"]=> string(88) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/canon-130x100.png" } [6]=> array(3) { ["title"]=> string(15) "Harley Davidson" ["link"]=> string(0) "" ["image"]=> string(89) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/harley-130x100.png" } [7]=> array(3) { ["title"]=> string(4) "Dell" ["link"]=> string(0) "" ["image"]=> string(87) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/dell-130x100.png" } [8]=> array(3) { ["title"]=> string(6) "Disney" ["link"]=> string(0) "" ["image"]=> string(89) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/disney-130x100.png" } [9]=> array(3) { ["title"]=> string(9) "Starbucks" ["link"]=> string(0) "" ["image"]=> string(92) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/starbucks-130x100.png" } [10]=> array(3) { ["title"]=> string(8) "Nintendo" ["link"]=> string(0) "" ["image"]=> string(91) "http://localhost/opencart/upload/image/cache/catalog/demo/manufacturer/nintendo-130x100.png" 
	} 
		} 
	["module"]=> int(0) 
	}

    my account
    my wishlist


		*/
		return $this->load->view('extension/module/carousel', $data);
	}
}