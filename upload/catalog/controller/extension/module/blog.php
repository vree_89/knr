<?php
class ControllerExtensionModuleBlog extends Controller {
	public function index($setting) {
		// $this->load->language('extension/module/blog');

		// $data['heading_title'] = $this->language->get('heading_title');

		// $data['text_tax'] = $this->language->get('text_tax');

		// $data['button_cart'] = $this->language->get('button_cart');
		// $data['button_wishlist'] = $this->language->get('button_wishlist');
		// $data['button_compare'] = $this->language->get('button_compare');

		// $this->load->model('extension/module/blog');

		// $this->load->model('tool/image');

		// $data['blogs'] = array();
		//var_dump($this->model_extension_module_blog);
		//$results = $this->model_extension_module_blog->getBlogs($setting['limit']);

		// var_dump($setting['blog']);
		// exit;
		//if (!empty($setting['blog'])) {
			// $blogs = array_slice($setting['blog'], 0, (int)10);
			// var_dump($blogs);

			// foreach ($blogs as $blog_id) {
			// 	$blog_info = $this->model_extension_module_blog->getBlog($blog_id);
			// 	if ($blog_info) {
			// 		if ($blog_info['image']) {
			// 			$image = $this->model_tool_image->resize($blog_info['image'], $setting['width'], $setting['height']);
			// 		} else {
			// 			$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
			// 		}



			// 		$data['blogs'][] = array(
			// 			'blog_id'  => $blog_info['blog_id'],
			// 			'title'       => $blog_info['title'],
			// 			'text'        => strip_tags(html_entity_decode($blog_info['text'], ENT_QUOTES, 'UTF-8')),
			// 			'image' => HTTP_SERVER.'image/'.$blog_info['image'],
			// 			'input_date'  => $blog_info['input_date']
			// 		);
			// 	}
			// }




		//}



		$this->load->model('blog/article');
        $this->load->language('blog/blog');

		$filter = '';
		$sort = 'p.sort_order';
		$order = 'ASC';
		$page  = 1;
		//$limit = $this->config->get('easy_blog_global_article_limit');
		$limit = 3;
		
		$status = $this->config->get('easy_blog_global_status');
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['blogs'] = array();
		if ($status) {
			// $this->document->setTitle($this->config->get('easy_blog_home_page_meta_title'));
			// $this->document->setDescription($this->config->get('easy_blog_home_page_meta_description'));
			// $this->document->setKeywords($this->config->get('easy_blog_home_page_meta_keyword'));
			// $this->document->addLink($this->url->link('blog/blog'),'');

			//$data['heading_title'] = $this->config->get('easy_blog_home_page_meta_title');

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $this->config->get('easy_blog_home_page_name'),
				'href' => $this->url->link('blog/blog')
			);
			
			$data['name'] = $this->config->get('easy_blog_home_page_name');
			


			$data['reviews'] = array();

			$data['videos'] = array();

			$filter_data = array(
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$article_total = $this->model_blog_article->getTotalArticles($filter_data);

			$results = $this->model_blog_article->getArticles($filter_data);
			//var_dump($results);exit;
			foreach ($results as $result) {
				$data['reviews'][] = array(
					'article_id'  => $result['article_id'],
					'name'        => $result['name'],
                    'date_modified'  => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
                    'intro_text' => html_entity_decode($result['intro_text'], ENT_QUOTES, 'UTF-8'),
					'href'        => $this->url->link('blog/article', 'article_id=' . $result['article_id'] ),
					'image'        => $result['image']!=NULL ? 'image/'.$result['image'] : 'image/catalog/demo/blog/none.png',	
				);
			}
			// var_dump($data['blogs']);exit;



			$video_total = $this->model_blog_article->getTotalVideos($filter_data);

			$results = $this->model_blog_article->getVideos($filter_data);
			//var_dump($results);exit;
			foreach ($results as $result) {
				$data['videos'][] = array(
					'article_id'  => $result['article_id'],
					'name'        => $result['name'],
                    'date_modified'  => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
                    'intro_text' => html_entity_decode($result['intro_text'], ENT_QUOTES, 'UTF-8'),
					'href'        => $this->url->link('blog/article', 'article_id=' . $result['article_id'] ),
					'image'        => $result['image']!=NULL ? 'image/'.$result['image'] : 'image/catalog/demo/blog/none.png',	
				);
			}





			
		}


		if ($data['reviews'] || $data['videos']) {
			return $this->load->view('extension/module/blog', $data);
		}		

		
		// if ($results) {
		// 	foreach ($results as $result) {
		// 		if ($result['image']) {
		// 			$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
		// 		} else {
		// 			$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
		// 		}





		// 		$data['blogs'][] = array(
		// 			'blog_id'  => $result['blog_id'],
		// 			'title'       => $result['title'],
		// 			'text'        => strip_tags(html_entity_decode($result['text'], ENT_QUOTES, 'UTF-8')),
		// 			'image' => HTTP_SERVER.'image/'.$result['image'],
		// 			'input_date'  => $result['input_date']
		// 		);
		// 	}
			
		// 	return $this->load->view('extension/module/blog', $data);
		// }
		//return $this->load->view('extension/module/blog', $data);
	}
}
