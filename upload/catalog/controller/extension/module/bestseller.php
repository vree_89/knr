<?php
class ControllerExtensionModuleBestSeller extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/bestseller');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);
		$data['special_thumb'] = HTTP_SERVER.'image/special_price.png';

		if ($results) {
			foreach ($results as $result) {
					if ($result['quantity']) {
						$quantity = $result['quantity'];
					} else {
						$quantity = false;
					}


				if ($result['manufacturer']) {
					$manufacturer = $result['manufacturer'];
				} else {
					$manufacturer = false;
				}
				
				// if ($result['image']) {
				// 	$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				// } else {
				// 	$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				// }
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					if($image==""){
						//echo "kosong<br/>";
						$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));


					}
					//echo "Image :".$image."<br/>";
				} else {
					$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					//echo "Image :".$image."<br/>";
				}



				// if (!$this->config->get('config_customer_price')) {
				// 	$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$price = false;
				// }

				// if (!$this->config->get('config_customer_price')) {
				// 	$price_retail = $this->currency->format($this->tax->calculate($result['price_retail'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$price_retail = false;
				// }

				// //calculate price
				// if($price!=false && $price_retail!=false){
				// 	$_price = ($result['price']);
				// 	$_price_retail = ($result['price_retail']);
				// 	if($_price_retail>$_price){
				// 		$potongan =  ($_price_retail - $_price);
				// 		$potongan_retail = floor(($potongan/$_price_retail) * 100)."%";
				// 	}
				// 	else{
				// 		$potongan_retail = false;	
				// 	}
				// }
				// else{
				// 	$potongan_retail = false;
				// }




			if($this->customer->isLogged() ){					
				$_price_retail = $result['price_retail'];
				$_price = $result['price'];
				$_base_price = $result['base_price'];
				//if membership user
				if(isset($this->session->data['membership'])){						
					$discount = $this->session->data['membership']['membership_discount'];
					$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$discount_amount = (($discount/100)*($_price-$_base_price));
					$price = $_price - floor(($discount/100)*($_price-$_base_price));

					$discount_total  = $_price_retail-$price;

					$discount = round(($discount_total/$_price_retail)*100);

					$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);


				}
				else{
					if($_price_retail>0 && $_price>0){
						if($_price_retail>$_price){
							$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
							$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							
							$price = $_price;
							$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}
					}
					else{
						$discount = null;
						$price_before = null;
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					}
				}
			}
			else{
				$_price_retail = $result['price_retail'];
				$_price = $result['price'];

				if($_price_retail>0 && $_price>0){
					if($_price_retail>$_price){
						$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
						$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

						$price = $_price;
						$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					}
				}
				else{
					$discount = null;
					$price_before = null;
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				}
			}


				if ((float)$result['special']) {
					$discount = ceil((($_price_retail-$result['special'])/$_price_retail) * 100);
					$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$specialnum = $result['special'];
				} else {
					$special = false;
					$specialnum = false;
				}	



			// if ((float)$result['special']) {
			// 	$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			// } else {
			// 	$special = false;
			// }




				// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				// 	$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$price = false;
				// }


				// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				// 	$price_retail = $this->currency->format($this->tax->calculate($result['price_retail'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$price_retail = false;
				// }

				// //calculate price
				// if($price!=false && $price_retail!=false){
				// 	$_price = ($result['price']);
				// 	$_price_retail = ($result['price_retail']);
				// 	if($_price_retail>$_price){
						
				// 		$potongan =  ($_price_retail - $_price);
				// 		$potongan_retail = floor(($potongan/$_price_retail) * 100)."%";
				// 	}
				// 	else{
				// 		$potongan_retail = false;	
				// 	}
				// }
				// else{
				// 	$potongan_retail = false;

				// }


				// if ((float)$result['special']) {
				// 	$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$special = false;
				// }

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
				$data['products'][] = array(
					'quantity'=> $quantity,
					'manufacturer'=> $manufacturer,
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'price_before'       => $price_before,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					'discount'	  => $discount,
					'stock_status' => $result['stock_status'],
					'is_preorder' => $result['is_preorder'],
					'date_available' => $result['date_available'],
					'special'=>$special,
				);
				// $data['products'][] = array(
				// 	'quantity' =>  $quantity,
				// 	'manufacturer'=> $manufacturer,
				// 	'product_id'  => $result['product_id'],
				// 	'thumb'       => $image,
				// 	'name'        => $result['name'],
				// 	'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				// 	'price'       => $price,
				// 	'additional_discount'       => $result['additional_discount'],
				// 		'price_retail'       => $price_retail,
				// 		'potongan_retail' => $potongan_retail,
				// 	'special'     => $special,
				// 	'tax'         => $tax,
				// 	'rating'      => $rating,
				// 	'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				// );
			}
			$data['url'] = $this->url->link('product/bestseller', 'path=bestseller');
			
			return $this->load->view('extension/module/bestseller', $data);
		}
		return $this->load->view('extension/module/bestseller', $data);
	}
}
