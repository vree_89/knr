<?php
class ControllerExtensionModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();
		$data['bestseller'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}
		// $setting['limit'] = 10;


		$data['special_thumb'] = HTTP_SERVER.'image/special_price.png';
		
		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)10);
			//var_dump($products);exit;
			
			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);
				//echo $product_info['manufacturer'];
				// var_dump($product_info);
				if ($product_info) {
					// echo $product_info['name'],"<br/>";
					if ($product_info['quantity']) {
						$quantity = $product_info['quantity'];
					} else {
						$quantity = false;
					}

					if ($product_info['manufacturer']) {
						$manufacturer = $product_info['manufacturer'];
					} else {
						$manufacturer = false;
					}

					// if ($product_info['image']) {
					// 	$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					// } else {
					// 	$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					// }
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
						if($image==""){
							//echo "kosong<br/>";
							$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));


						}
						//echo "Image :".$image."<br/>";
					} else {
						$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
						//echo "Image :".$image."<br/>";
					}



					// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					// 	$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					// } else {
					// 	$price = false;
					// }


					// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					// 	$price_retail = $this->currency->format($this->tax->calculate($product_info['price_retail'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					// } else {
					// 	$price_retail = false;
					// }

					// //calculate price
					// if($price!=false && $price_retail!=false){
					// 	$_price = ($product_info['price']);
					// 	$_price_retail = ($product_info['price_retail']);
					// 	if($_price_retail>$_price){							
					// 		$potongan =  ($_price_retail - $_price);
					// 		$potongan_retail = floor(($potongan/$_price_retail) * 100)."%";
					// 	}
					// 	else{
					// 		$potongan_retail = false;	
					// 	}
					// }
					// else{
					// 	$potongan_retail = false;

					// }
					//exit;


				if($this->customer->isLogged() ){					
					$_price_retail = $product_info['price_retail'];
					$_price = $product_info['price'];
					$_base_price = $product_info['base_price'];
					//if membership user
					if(isset($this->session->data['membership'])){						
						$discount = $this->session->data['membership']['membership_discount'];
						$price_before = $this->currency->format($this->tax->calculate($_price_retail, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						$discount_amount = (($discount/100)*($_price-$_base_price));
						$price = $_price - floor(($discount/100)*($_price-$_base_price));

						$discount_total  = $_price_retail-$price;

						$discount = round(($discount_total/$_price_retail)*100);

						$price = $this->currency->format($this->tax->calculate($price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);


					}
					else{
						if($_price_retail>0 && $_price>0){
							if($_price_retail>$_price){
								$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
								$price_before = $this->currency->format($this->tax->calculate($_price_retail, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
								
								$price = $_price;
								$price = $this->currency->format($this->tax->calculate($price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							}
						}
						else{
							$discount = null;
							$price_before = null;
							$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}
					}
				}
				else{
					$_price_retail = $product_info['price_retail'];
					$_price = $product_info['price'];

					if($_price_retail>0 && $_price>0){
						if($_price_retail>$_price){
							$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
							$price_before = $this->currency->format($this->tax->calculate($_price_retail, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

							$price = $_price;
							$price = $this->currency->format($this->tax->calculate($price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}
					}
					else{
						$discount = null;
						$price_before = null;
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					}
				}

				if ((float)$product_info['special']) {
					$discount = ceil((($_price_retail-$product_info['special'])/$_price_retail) * 100);
					$price_before = $this->currency->format($this->tax->calculate($_price_retail, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$specialnum = $product_info['special'];
				} else {
					$special = false;
					$specialnum = false;
				}	



					// if ((float)$product_info['special']) {
					// 	$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					// } else {
					// 	$special = false;
					// }

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

				$data['products'][] = array(
					'quantity'=> $quantity,
					'manufacturer'=> $manufacturer,
					'product_id'  => $product_info['product_id'],
					'thumb'       => $image,
					'name'        => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'price_before'       => $price_before,
					'tax'         => $tax,
					'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
					'rating'      => $product_info['rating'],
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
					'discount'	  => $discount,
					'stock_status' => $product_info['stock_status'],
					'is_preorder' => $product_info['is_preorder'],
					'date_available' => $product_info['date_available'],
					'special'=>$special,
				);
					// $data['products'][] = array(
					// 	'quantity' =>  $quantity,
					// 	'manufacturer'=> $manufacturer,
					// 	'product_id'  => $product_info['product_id'],
					// 	'thumb'       => $image,
					// 	'name'        => $product_info['name'],
					// 	'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					// 	'price'       => $price,
					// 	'additional_discount'       => $product_info['additional_discount'],
					// 	'price_retail'       => $price_retail,
					// 	'potongan_retail' => $potongan_retail,
					// 	'special'     => $special,
					// 	'tax'         => $tax,
					// 	'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
					// 	'rating'      => $rating,
					// 	'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					// );
				}
			}







		}

		// var_dump($data['products']);

		if ($data['products']) {
			return $this->load->view('extension/module/featured', $data);
		}
	}
}