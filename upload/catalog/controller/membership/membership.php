<?php
class ControllerMembershipMembership extends Controller {
	public function index() {
		if (!isset($this->session->data['location'])) {
			if(isset($this->request->get['path'])){
			$this->session->data['redirect'] = $this->url->link('membership/membership', 'path='.$this->request->get['path']);
			}
			else{
			$this->session->data['redirect'] = $this->url->link('membership/membership');
			}
			$this->response->redirect($this->url->link('common/home', '', true));
		}
		$this->load->language('membership/membership');
				$this->load->model('tool/image');


		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_membership'),
			'href'      => $this->url->link('membership/membership', '', true)
		);


		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_membership_title'] = $this->language->get('text_membership_title');
		$data['text_membership_info1'] = $this->language->get('text_membership_info1');
		$data['text_membership_info2'] = $this->language->get('text_membership_info2');


		$this->load->model('membership/membership');

		$data['memberships'] = array();
		$results = $this->model_membership_membership->getMemberships();
		foreach ($results as $result) {
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], null, $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}	

				if ($result['name']) {
					//echo strtolower(str_replace(" ","_",$result['name']));
					$image = $this->model_tool_image->resize(strtolower(str_replace(" ","_",$result['name'])).".png", 54, 86);
				} else {
					$image = "";
				}
				//echo "Image :".$image."<br/>";


			$data['memberships'][] = array(
				"membership_id"=>$result['membership_id'],
				"name"=>$result['name'],
				"price"=>$price,
				"period"=>$result['period'],
				"discount"=>$result['discount'],
				"loyalty_point"=>$result['loyalty_point'],
				"bonus_point"=>$result['bonus_point'],
				"text_color"=>$result['text_color'],
				"featured"=>$result['featured'],
				"sort_order"=>$result['sort_order'],
				"status"=>$result['status'],
				"date_modified"=>$result['date_modified'],
				'href'        => $this->url->link('membership/membership/confirm', 'membership_id=' . $result['membership_id']),
				'image' => $image,



				);
		}

		//var_dump($data['memberships']);

		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		//die($data['column_right']);
		
		$this->response->setOutput($this->load->view('membership/membership', $data));
	}

	public function preview(){
		if(!$this->customer->isLogged()){
			$this->session->data['next'] = $this->url->link('membership/membership/preview', 'membership_id=' . $this->request->get['membership_id']);
			$this->response->redirect($this->url->link('account/login'));
		}


		$this->load->language('membership/membership');
		if (isset($this->request->get['membership_id'])) {
			$membership_id = (int)$this->request->get['membership_id'];
		} else {
			$membership_id = 0;
		}
		$this->load->model('membership/membership');
		$membership_info = $this->model_membership_membership->getMembership($membership_id);		
		if ($membership_info) {
			$url = '';
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_membership'),
				'href'      => $this->url->link('membership/membership', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $membership_info['name'],
				'href' => $this->url->link('membership/preview', $url . '&membership_id=' . $this->request->get['membership_id'])
			);

			$this->document->setTitle($membership_info['name']);


			$data['membership_id'] = $membership_info['membership_id'];
			$data['name'] = $membership_info['name'];
			$data['price'] = $this->currency->format($this->tax->calculate($membership_info['price'], null, $this->config->get('config_tax')), $this->session->data['currency']);
			$data['period'] = $membership_info['period']." days";
			$data['discount'] = $membership_info['discount']." %";
			$data['loyalty_point'] = $membership_info['loyalty_point']." %";
			$data['bonus_point'] = $membership_info['bonus_point']." %";
			$data['featured'] = $membership_info['featured'];
			$data['href'] = $this->url->link('membership/membership/confirm	', 'membership_id=' . $this->request->get['membership_id']);


			$data['heading_title'] = $membership_info['name'];
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			//die($data['column_right']);
			
			$this->response->setOutput($this->load->view('membership/preview', $data));	
		}

		
	
	}





	public function confirm(){
		if(!$this->customer->isLogged()){
			$this->response->redirect($this->url->link('account/login'));
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {


			//add membership order
			$this->load->model('membership/membership');
			$order_data['membership_id'] = $this->request->post['membership_id'];
			$order_data['customer_id'] = $this->customer->getId();
			$order_data['status'] = 'pending';
			$this->session->data['order_id'] = $this->model_membership_membership->addCustomerMembership($order_data);		

		}
	

		if (isset($this->error['agree'])) {
			$data['error_agree'] = $this->error['agree'];
		} else {
			$data['error_agree'] = '';
		}


		$this->load->language('membership/membership');
		if (isset($this->request->get['membership_id'])) {
			$membership_id = (int)$this->request->get['membership_id'];
		} else {
			$membership_id = 0;
		}
		$this->load->model('membership/membership');
		$membership_info = $this->model_membership_membership->getMembership($membership_id);		

		$data['text_membership_confirm_info'] = $this->language->get('text_membership_confirm_info');
		$data['text_payment_method'] = $this->language->get('text_payment_method');


		if ($membership_info) {
			$url = '';
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_membership'),
				'href'      => $this->url->link('membership/membership', '', true)
			);



			$data['breadcrumbs'][] = array(
				'text'=>$membership_info['name'],
				'href'=>$this->url->link('membership/membership/confirm', 'membership_id=' . $this->request->get['membership_id']),
			);


			// $data['payment_methods'] = [
			// 	'bank_transfer' => [
			// 		'id' 			=> '36',
			// 		'code' 			=> 'bank_transfer',
			// 		'title'			=> 'Bank Transfer',
			// 		'image'			=> 'catalog/view/theme/default/assets/img/payment/jaringanatm.jpg',
			// 		'terms'			=> '',
			// 		'sort_order'	=> 1
			// 	],
			// 	'alfa_group' => [
			// 		'id' 			=> '35',
			// 		'code' 			=> 'alfa_group',
			// 		'title'			=> 'Alfa Group',
			// 		'image'			=> 'catalog/view/theme/default/assets/img/payment/alfagroup.jpg',
			// 		'terms'			=> '',
			// 		'sort_order'	=> 2
			// 	],
			// 	'doku_wallet' => [
			// 		'id' 			=> '04',
			// 		'code' 			=> 'doku_wallet',
			// 		'title'			=> 'Doku Wallet',
			// 		'image'			=> 'catalog/view/theme/default/assets/img/payment/dokuwallet.jpg',
			// 		'terms'			=> '',
			// 		'sort_order'	=> 3
			// 	]
			// ];

			// $this->session->data['payment_methods'] = $data['payment_methods'];

			$data['payment_methods'] = array();

			$method_data = array();
			//if (!isset($this->session->data['payment_methods']) && $this->session->data['location']=="id") {

				// $method_data = [
				// 	'bank_transfer' => [
				// 		'id' 			=> '36',
				// 		'code' 			=> 'bank_transfer',
				// 		'title'			=> 'Bank Transfer',
				// 		'image'			=> 'catalog/view/theme/default/assets/img/payment/jaringanatm.jpg',
				// 		'terms'			=> '',
				// 		'sort_order'	=> 1
				// 	],
				// 	'alfa_group' => [
				// 		'id' 			=> '35',
				// 		'code' 			=> 'alfa_group',
				// 		'title'			=> 'Alfa Group',
				// 		'image'			=> 'catalog/view/theme/default/assets/img/payment/alfagroup.jpg',
				// 		'terms'			=> '',
				// 		'sort_order'	=> 2
				// 	],
				// 	'doku_wallet' => [
				// 		'id' 			=> '04',
				// 		'code' 			=> 'doku_wallet',
				// 		'title'			=> 'Doku Wallet',
				// 		'image'			=> 'catalog/view/theme/default/assets/img/payment/dokuwallet.jpg',
				// 		'terms'			=> '',
				// 		'sort_order'	=> 3
				// 	]

				// ];


				$method_data = [
					'bank_transfer_permata' => [
						'code' 			=> 'bank_transfer_permata',
						'title'			=> 'Bank Transfer',
						'sort_order'	=> 1,
						'image'			=> 'catalog/view/theme/default/assets/img/payment/jaringanatm.jpg',
					],
					'wallet_doku' => [
						'code' 			=> 'wallet_doku',
						'title'			=> 'Doku Wallet',
						'sort_order'	=> 2,
						'image'			=> 'catalog/view/theme/default/assets/img/payment/dokuwallet.jpg',
					],
					'otc_alfa' => [
						'code' 			=> 'otc_alfa',
						'title'			=> 'Alfa Group',
						'sort_order'	=> 3,
						'image'			=> 'catalog/view/theme/default/assets/img/payment/alfagroup.jpg',
					]
				];				

				$this->session->data['payment_methods'] = $method_data;
			//}
			$data['payment_methods'] = $this->session->data['payment_methods'];

			// echo $this->session->data['location'];
			// var_dump($this->session->data['payment_methods']) ;

			$this->document->setTitle($membership_info['name']);
			//$this->document->addScript('catalog/view/theme/default/assets/js/jquery.redirect.js');

			$data['membership_id'] = $membership_info['membership_id'];
			$data['name'] = $membership_info['name'];
			$data['price'] = $this->currency->format($this->tax->calculate($membership_info['price'], null, $this->config->get('config_tax')), $this->session->data['currency']);
			$data['period'] = $membership_info['period']." days";
			$data['discount'] = $membership_info['discount']." %";
			$data['loyalty_point'] = $membership_info['loyalty_point']." %";
			$data['bonus_point'] = $membership_info['bonus_point']." %";
			$data['featured'] = $membership_info['featured'];
			$data['href'] = $this->url->link('membership/membership/confirm	', 'membership_id=' . $this->request->get['membership_id']);

			$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_checkout_id'), true), 'Term and Condition', 'Term and Condition');
			if (isset($this->session->data['agree'])) {
				$data['agree'] = $this->session->data['agree'];
			} else {
				$data['agree'] = '';
			}

			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_confirm'] = $this->language->get('button_confirm');
			$data['action'] = $this->url->link('membership/membership/confirm', '', true);
			$data['text_loading'] = $this->language->get('text_loading');
			$data['heading_title'] = $membership_info['name'];
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			$data['btn_next'] = HTTP_SERVER.'image/membership/btn_next_'.$this->session->data['language'].'.png';

			//die($data['column_right']);
			
			$this->response->setOutput($this->load->view('membership/confirm', $data));	
		}

		
	
	}


	public function save() {
		$this->load->language('membership/membership');
		$json = array();

		// Validate if user is logged in
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('membership/membership');
			$json['redirect'] = $this->url->link('account/login', '', true);
		}

		if (!isset($this->request->post['membership_id'])) {
			$json['error'] = sprintf($this->language->get('error_warning'), $this->url->link('information/contact'));
		}

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$json['error'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}


		$this->load->model('membership/membership');
		$membership_info = $this->model_membership_membership->getMembership($this->request->post['membership_id']);		


		if (!$membership_info) {
			$json['error'] = sprintf($this->language->get('error_warning'), $this->url->link('information/contact'));
		}

		if (!isset($this->request->post['payment_method'])) {
			$json['error'] = $this->language->get('error_payment');
		}

		if ($membership_info && !isset($this->request->post['agree'])) {
			$json['error'] = sprintf($this->language->get('error_agree'), 'Term and Condition');
			// $this->response->addHeader('Content-Type: application/json');
			// $this->response->setOutput(json_encode($json));
			// return 0;
		}


		if (empty($json)) {
			$this->load->model('account/customer');
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());


			$payment_method = $this->request->post['payment_method'];

			$order_data['customer_id'] = $this->customer->getId();
			$order_data['customer_group_id'] = $customer_info['customer_group_id'];
			$order_data['membership_id'] = $this->request->post['membership_id'];
			$order_data['membership_name'] = $membership_info['name'];
			$order_data['membership_deposit'] = $membership_info['price'];
			$order_data['membership_period'] = $membership_info['period'];
			$order_data['membership_discount'] = $membership_info['discount'];
			$order_data['membership_loyalty_point'] = $membership_info['loyalty_point'];
			$order_data['membership_bonus_point'] = $membership_info['bonus_point'];
			$order_data['ip'] = $this->request->server['REMOTE_ADDR'];
			$order_data['payment_total'] = $membership_info['price'];
			$order_data['payment_method'] = $this->session->data['payment_methods'][$payment_method]['title'];
			$order_data['payment_code'] = $this->session->data['payment_methods'][$payment_method]['code'];
			$order_data['language_id'] = $this->config->get('config_language_id');
			$order_data['store_name'] = $this->config->get('config_name');


			$this->session->data['membership_id'] = $this->request->post['membership_id'];
			$this->session->data['membership_name'] = $membership_info['name'];

			//add uniqueID
			$uniqueID = "MBR-".md5(time());
			$order_data['uniqueID'] = $uniqueID;
			$this->session->data['membership_order_id'] = $this->model_membership_membership->addOrder($order_data);

			/*old payment method*/
			/*
			$transidmerchant 	= 'MBR-'.$this->session->data['membership_order_id'];
			$total 				= number_format($membership_info['price'], 2, '.', '');
			$payment_channel	= $this->session->data['payment_methods'][$payment_method]['id'];
			$basket 			= $membership_info['name'].','.$total.',1,'.$total.';';
			$mallid 			= 4565;
			$sharedkey 			= 'RzN4pKta12R9';
			$words 				= sha1($total.$mallid.$sharedkey.$transidmerchant);
			$name 				= $this->customer->getFirstName().' '.$this->customer->getLastName();
			$session_id			= session_id();
			//expired date
			$date = strtotime("+1 day");
			$expired_date = date('Y-m-d H:i:s', $date);


			$doku_data = [
				'transidmerchant'	=> $transidmerchant,
				'totalamount'		=> $total,
				'words'				=> $words,
				'payment_channel'	=> $payment_channel,
				'session_id'		=> $session_id,
				//'order_id' 			=> $order_id,
				'expired_date'		=> $expired_date,
			];

			$this->model_membership_membership->addDokuHistory($doku_data);

			$json['doku_data'] = [
				'MALLID'			=> $mallid,
				'CHAINMERCHANT'		=> 'NA',
				'AMOUNT'			=> $total,
				'PURCHASEAMOUNT'	=> $total,
				'TRANSIDMERCHANT'	=> $transidmerchant,
				'WORDS'				=> $words,
				'REQUESTDATETIME'	=> date("YmdHis"),
				'CURRENCY'			=> 360,
				'PURCHASECURRENCY'	=> 360,
				'SESSIONID'			=> $session_id,
				'NAME'				=> $name,
				'EMAIL'				=> $this->customer->getEmail(),
				'BASKET'			=> $basket,
				'PAYMENTCHANNEL'	=> $this->session->data['payment_methods'][$payment_method]['id']
			];

			//var_dump($json['doku_data']);exit;
			//$json['doku_url'] = ($_SERVER['HTTP_HOST'] == 'localhost') ? 'https://staging.doku.com/Suite/Receive' : 'https://pay.doku.com/Suite/Receive';
			$json['doku_url'] = 'https://staging.doku.com/Suite/Receive';
			*/
			
			/*NEW payment method*/
			$merchant_transaction_id 	= 'MBR-'.$this->session->data['membership_order_id'];
			$total 				= $membership_info['price'];
			$payment_channel	= $this->session->data['payment_methods'][$payment_method]['code'];


			$json['data'] = array(
				"timestamp" => time(),
				"user_id" => $order_data['customer_id'],
				"merchant_transaction_id" => $merchant_transaction_id,
				"transaction_description" => "Transaction Membership #".$merchant_transaction_id,
				"payment_channel" => $payment_channel,
				"currency" => "IDR",
				"amount" => $total,
				"item_id" => "",
				"item_name" => "Membership ".$order_data['membership_name'],
				"redirect_url" => "",
				"redirect_target" => "_top",
				"custom" => "",
				"order_id" => $this->session->data['membership_order_id'],
				"uniqueID"=>$uniqueID,
				);
			$json['redirect_payment_url'] = $this->url->link('membership/membership/pay');


			unset($this->session->data['membership_id']);
			unset($this->session->data['membership_name']);
			unset($this->session->data['membership_order_id']);
			unset($this->session->data['payment_methods']);
		}
	

		$json['success'] = 'success';
		$json['continue'] = $this->url->link('membership/payment');


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));





	}



	public function pay(){
		// var_dump($this->request);exit;
		$url = "https://api.simplepayment.solutions/api/v1/create";
		$appkey   = "5a797f06d7fc016849a82e45";
		$appid   = "84a6673c17364fa5bc01";
		$secretkey  = "g33696P326224hE274GTb6T59QJ794jN";

		$timestamp = time();
		$user_id = $this->request->post['user_id'];
		$merchant_transaction_id = $this->request->post['merchant_transaction_id'];
		$transaction_description = $this->request->post['transaction_description'];
		$payment_channel = $this->request->post['payment_channel'];
		$amount = $this->request->post['amount'];
		//$redirect_url = $this->request->post['redirect_url'];
		$order_id = $this->request->post['order_id'];
		$uniqueID = $this->request->post['uniqueID'];

		$data = array(
			'timestamp' => time(),
			'user_id' => $user_id,
			'merchant_transaction_id' => $uniqueID, //$merchant_transaction_id,
			'transaction_description' => $transaction_description,
			'payment_channel' => $payment_channel,
			'currency' => 'IDR',
			'amount' => $amount,
			'item_id' => "",
			'item_name' => 'KR House '.$merchant_transaction_id,
			'redirect_url' => 'http://staging.krhouse.net/index.php?route=checkout/success',
			'redirect_target' => '_top',
			'custom' => "",
			);
		//var_dump($data);exit;
		$json_data = json_encode($data);
		//echo $json_data;exit;

		// echo base64_encode($json_data);exit;
		$signature = hash_hmac('sha256', base64_encode($json_data), $secretkey);
		//$signature = $stringBodySign = strtr(base64_encode(hash_hmac('sha256',  $json_data, $secretkey , true )), '+/', '-_');
		// echo $signature;exit;



		$header = [ 
			'Content-Type: application/json',
			'AppId: '.$appid,
			'Bodysign: '.$signature
		];


		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
		$output = curl_exec($ch);
		//echo $output;exit;
		curl_close($ch);
		$arrayResult = json_decode( $output, true );		
	    if ( isset($arrayResult['data'])){
	    	$this->load->model('checkout/order');

			//add payment record
			$date = strtotime("+5 minute");
			$expired_date = date('Y-m-d H:i:s', $date);

			$payment_data = [
				'totalamount'		=> (float)$amount,
				'payment_channel'	=> $payment_channel,
				'order_id' 			=> $order_id,
				'expired_date'		=> $expired_date,
			];

			$this->model_checkout_order->addSimplepaymentHistory($payment_data);


	    	$redirectUrl = $arrayResult['data']['links']['href'];
	    	$this->response->redirect($redirectUrl);
	    }else{
	    	die('Request failed! Err '.$arrayResult['errors']['id'].' : '.$arrayResult['errors']['title']);
	    }


	}

	public function validate(){
		$this->load->model('membership/membership');
		$membership_info = $this->model_membership_membership->getMembership($this->request->post['membership_id']);		

		if ($membership_info && !isset($this->request->post['agree'])) {
			$this->error['agree'] = $this->language->get('error_agree');
		}

		return !$this->error;		
	}












}
