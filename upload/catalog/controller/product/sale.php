<?php
class ControllerProductSale extends Controller {
	public function index() {
		if (!isset($this->session->data['location'])) {
			if(isset($this->request->get['path'])){
			$this->session->data['redirect'] = $this->url->link('product/sale', 'path='.$this->request->get['path']);
			}
			else{
			$this->session->data['redirect'] = $this->url->link('product/sale');
			}
			$this->response->redirect($this->url->link('common/home', '', true));
		}
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');


		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			//$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
			$limit = 16;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				// if ($category_info) {
				// 	$data['breadcrumbs'][] = array(
				// 		'text' => $category_info['name'],
				// 		'href' => $this->url->link('product/category', 'path=' . $path . $url)
				// 	);
				// }



			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);


			// $this->document->setTitle($category_info['meta_title']);
			// $this->document->setDescription($category_info['meta_description']);
			// $this->document->setKeywords($category_info['meta_keyword']);

			//$data['heading_title'] = $category_info['name'];

		$this->document->setTitle('SALE');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_index'] = $this->language->get('text_index');
		$data['text_empty'] = $this->language->get('text_empty');

			$data['heading_title'] = 'Sale';
			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => 'Sale',
				'href' => $this->url->link('product/sale')
			);

			$data['thumb'] = '';
			$data['description'] = '';
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				$data['categories'][] = array(
					'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalBestSellers($filter_data) . ')' : ''),
					'href' => $this->url->link('product/bestseller')
				);
			}

			$data['products'] = array();

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$product_total = $this->model_catalog_product->getTotalSaleProducts($filter_data);


			$results = $this->model_catalog_product->getProductSales($filter_data);
			//exit;
			//var_dump($results);
			foreach ($results as $result) {
					if ($result['quantity']) {
						$quantity = $result['quantity'];
					} else {
						$quantity = false;
					}

					if ($result['manufacturer']) {
						$manufacturer = $result['manufacturer'];
					} else {
						$manufacturer = false;
					}

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					if($image==""){
						//echo "kosong<br/>";
						$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));


					}
					//echo "Image :".$image."<br/>";
				} else {
					$image = $this->model_tool_image->resize('product-noimage.jpg', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					//echo "Image :".$image."<br/>";
				}




				if($this->customer->isLogged() ){					
					$_price_retail = $result['price_retail'];
					$_price = $result['price'];
					$_base_price = $result['base_price'];
					//if membership user
					if(isset($this->session->data['membership'])){						
						$discount = $this->session->data['membership']['membership_discount'];
						$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						$discount_amount = (($discount/100)*($_price-$_base_price));
						$price = $_price - (($discount/100)*($_price-$_base_price));

						$discount_total  = $_price_retail-$price;

						$discount = round(($discount_total/$_price_retail)*100);

						$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);


					}
					else{
						if($_price_retail>0 && $_price>0){
							if($_price_retail>$_price){
								$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
								$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
								
								$price = $_price;
								$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							}
						}
						else{
							$discount = null;
							$price_before = null;
							$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}
					}
				}
				else{
					$_price_retail = $result['price_retail'];
					$_price = $result['price'];

					if($_price_retail>0 && $_price>0){
						if($_price_retail>$_price){
							$discount = ceil((($_price_retail-$_price)/$_price_retail) * 100);
							$price_before = $this->currency->format($this->tax->calculate($_price_retail, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

							$price = $_price;
							$price = $this->currency->format($this->tax->calculate($price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}
					}
					else{
						$discount = null;
						$price_before = null;
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					}
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$specialnum = $result['special'];
				} else {
					$special = false;
					$specialnum = false;
				}		
				// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				// 	$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$price = false;
				// }

				// if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				// 	$price_retail = $this->currency->format($this->tax->calculate($result['price_retail'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$price_retail = false;
				// }

				// //calculate price
				// if($price!=false && $price_retail!=false){
				// 	$_price = ($result['price']);
				// 	$_price_retail = ($result['price_retail']);
				// 	if($_price_retail>$_price){
						
				// 		$potongan =  ($_price_retail - $_price);
				// 		$potongan_retail = floor(($potongan/$_price_retail) * 100)."%";
				// 	}
				// 	else{
				// 		$potongan_retail = false;	
				// 	}
				// }
				// else{
				// 	$potongan_retail = false;

				// }


				// if ((float)$result['special']) {
				// 	$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				// } else {
				// 	$special = false;
				// }




				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'quantity'=> $quantity,
					'manufacturer'=> $manufacturer,
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'price_before'       => $price_before,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', 'path=sale&product_id=' . $result['product_id'] . $url),
					'discount'	  => $discount,
					'stock_status' => $result['stock_status'],
					'is_preorder' => $result['is_preorder'],
					'date_available' => $result['date_available'],
				);


				// $data['products'][] = array(
				// 		'quantity'=> $quantity,
				// 		'manufacturer'=> $manufacturer,
				// 	'product_id'  => $result['product_id'],
				// 	'thumb'       => $image,
				// 	'name'        => $result['name'],
				// 	'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				// 	'price'       => $price,
				// 	'additional_discount'       => $result['additional_discount'],
				// 		'price_retail'       => $price_retail,
				// 		'potongan_retail' => $potongan_retail,
				// 	'special'     => $special,
				// 	'tax'         => $tax,
				// 	'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				// 	'rating'      => $result['rating'],
				// 	'href'        => $this->url->link('product/product', 'path=best_seller&product_id=' . $result['product_id'] . $url)
				// );
			}

			//var_dump($data['products']);
			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/bestseller', 'path=bestseller&sort=p.model&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			//$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));
			$limits = array(16,40,100);

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/bestseller', 'path=bestseller'  . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/bestseller', 'path=bestseller'  . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
			if ($page == 1) {
			    $this->document->addLink($this->url->link('product/bestseller', 'path=bestseller'  , true), 'canonical');
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('product/bestseller', 'path=bestseller' , true), 'prev');
			} else {
			    $this->document->addLink($this->url->link('product/bestseller', 'path=bestseller&page='. ($page - 1), true), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/bestseller', 'path=&page='. ($page + 1), true), 'next');
			}

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('product/sale', $data));

	}
}
