<?php
class ControllerAccountForgotten extends Controller {
	private $error = array();


	public function verify_token(){
		$status = "";
		$message = "";
		$redirect = "";

		$this->load->language('account/forgotten');
		$this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomerByCode($this->request->post['token']);
		if($customer_info){
			if(strtotime($customer_info['code_expired_date'])>strtotime(date('Y-m-d H:i:s'))){
				$status  = "1";
				$message  = "User found";
				$redirect = $this->url->link('account/reset&code='. $this->request->post['token']);
			}
			else{
				$status  = "0";
				$message  = "Token not match or already expired";
				$redirect = "";
			}
		}
		else{
			$status  = "0";
			$message  = "Token not match or already expired";
			$redirect = "";
		}
		$res = array(
			"status" =>$status,
			"message" =>$message,
			"redirect"=>$redirect,
			);
		echo json_encode($res);

	}

	public function request_token(){

		$status = "";
		$message = "";
		$prenumber = "";

		$this->load->language('account/forgotten');
		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['telephone']);
		if(!$customer_info){
			$res = array(
				"status" =>"2",
				"message" =>$this->language->get('error_phone_not_exists'),
				);
			echo json_encode($res);
			return;
		}

		if((int)$customer_info['count_password_change']>=3 && $customer_info['last_password_change']==date('Y-m-d')){
			$res = array(
				"status" =>"3",
				"message" =>$this->language->get('error_limit'),
				);
			echo json_encode($res);
			return;
		}



			$url = 'http://104.199.196.122/gateway/v1/call';

			$arrayBody = [ 
			'userid' => "GamesPark",
			'password' => "GPark!Intv",
			'msisdn' => $this->beautify_id_number($this->request->post['telephone']),
			'gateway' => 0,
			];

			$json = json_encode( $arrayBody );

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
			$output = curl_exec($ch);
				
			curl_close($ch);
			$arrayResult = json_decode( $output, true );
			// $arrayResult['result'] = "Success";
			// $arrayResult['rc'] = "00";
			// $arrayResult['token'] = "6221304".$this->randomNumber();

			

			if(@$arrayResult['result']=="Success" && @$arrayResult['rc']=="00"){
				
				$this->model_account_customer->editCodeByTelephone($this->request->post['telephone'], substr($arrayResult['token'],  -5));
				// $this->model_account_customer->editLastPasswordChange($this->request->post['telephone']);

				$status = "1";
				$message = sprintf($this->language->get('text_miscall_success'), $this->masking($arrayResult['token']));
				

				$prenumber = substr($arrayResult['token'],0,  (strlen($arrayResult['token'])-5));
				$prenumber = "+".$this->beautify_id_number($prenumber);


			}
			else if(@$arrayResult['rc']!="00"){
				$status = "0";
				if($arrayResult['rc']=="06"){
				$message = "Phone number is invalid.";
				}
				else{
				$message = "Verify failed, please try again later.";
				}
			}
			else{
				$status = "0";
				$message = $this->language->get('text_miscall_failed');
			}


		$res = array(
			"status" =>$status,
			"message" =>$message,
			"prenumber"=>$prenumber,
			);
		echo json_encode($res);

	}

private function randomNumber($length = 4) {
    $result = '';

    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(0, 9);
    }

    return $result;
}	

	public function index() {
		if (!isset($this->session->data['location'])) {
			if(isset($this->request->get['path'])){
			$this->session->data['redirect'] = $this->url->link('account/forgotten', 'path='.$this->request->get['path']);
			}
			else{
			$this->session->data['redirect'] = $this->url->link('account/forgotten');
			}
			$this->response->redirect($this->url->link('common/home', '', true));
		}


		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));


		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['telephone']);

			$this->load->language('mail/forgotten');

			// $code = token(40);

			// $this->model_account_customer->editCodeByTelephone($this->request->post['telephone'], $code);
			// $this->model_account_customer->editLastPasswordChange($this->request->post['telephone']);

			// $subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

			// $message  = sprintf($this->language->get('text_greeting'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";
			// $message .= $this->language->get('text_change') . "\n\n";
			// $message .= $this->url->link('account/reset', 'code=' . $code, true) . "\n\n";
			// $message .= sprintf($this->language->get('text_ip'), $this->request->server['REMOTE_ADDR']) . "\n\n";

			// $mail = new Mail();
			// $mail->protocol = $this->config->get('config_mail_protocol');
			// $mail->parameter = $this->config->get('config_mail_parameter');
			// $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			// $mail->smtp_username = $this->config->get('config_mail_smtp_username');
			// $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			// $mail->smtp_port = $this->config->get('config_mail_smtp_port');
			// $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			// $mail->setTo($customer_info['email']);
			// $mail->setFrom($this->config->get('config_email'));
			// $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			// $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			// $mail->send();

			// $this->session->data['success'] = $this->language->get('text_success');

			// Add to activity log
			// if ($this->config->get('config_customer_activity')) {
				

			// 	if ($customer_info) {
			// 		$this->load->model('account/activity');

			// 		$activity_data = array(
			// 			'customer_id' => $customer_info['customer_id'],
			// 			'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
			// 		);

			// 		$this->model_account_activity->addActivity('forgotten', $activity_data);
			// 	}
			// }

			// $this->response->redirect($this->url->link('account/login', '', true));

			$this->response->setOutput($this->load->view('account/forgotten/verify', $data));
		}
		else{
			$data['success_miscall'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('account/forgotten', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_your_email'] = $this->language->get('text_your_email');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_your_phone'] = $this->language->get('text_your_phone');
		$data['text_phone'] = $this->language->get('text_phone');

		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_phone'] = $this->language->get('entry_phone');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		$data['btn_save'] = HTTP_SERVER.'image/forgotten/btn_save_'.$this->session->data['language'].'.png';		
		$data['btn_continue'] = HTTP_SERVER.'image/forgotten/btn_continue_'.$this->session->data['language'].'.png';		
		//echo $data['btn_continue'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten', '', true);
		$data['action_request'] = $this->url->link('account/forgotten/request_token');
		$data['action_verify'] = $this->url->link('account/forgotten/verify_token');

		$data['back'] = $this->url->link('account/login', '', true);

		// if (isset($this->request->post['email'])) {
		// 	$data['email'] = $this->request->post['email'];
		// } else {
		// 	$data['email'] = '';
		// }
		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/forgotten', $data));
	}

	protected function validate() {
		// if (!isset($this->request->post['email'])) {
		// 	$this->error['warning'] = $this->language->get('error_email');
		// } elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
		// 	$this->error['warning'] = $this->language->get('error_email');
		// }


		// $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		// if ($customer_info && !$customer_info['approved']) {
		// 	$this->error['warning'] = $this->language->get('error_approved');
		// }

		// return !$this->error;


		if($this->request->post['do']=="request"){
			//uncomment later
			if (!isset($this->request->post['telephone'])) {
				$this->error['warning'] = $this->language->get('error_phone');
			} elseif (!$this->model_account_customer->getTotalCustomersByTelephone($this->request->post['telephone'])) {
				$this->error['warning'] = $this->language->get('error_phone');
			}

			$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['telephone']);

			if ($customer_info && !$customer_info['approved']) {
				$this->error['warning'] = $this->language->get('error_approved');
			}
			if ($customer_info['last_password_change'] == date('Y-m-d')) {
				$this->error['warning'] = $this->language->get('error_limit');
			}


			$url = 'http://104.199.196.122/gateway/v1/call';

			$arrayBody = [ 
			'userid' => "GamesPark",
			'password' => "GPark!Intv",
			'msisdn' => $this->beautify_id_number($this->request->post['telephone']),
			'gateway' => 0,
			];

			$json = json_encode( $arrayBody );

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
			$output = curl_exec($ch);
			curl_close($ch);
			$arrayResult = json_decode( $output, true );
			if($arrayResult['result']=="Success"){
				$this->session->data['token'] = $arrayResult['token'];
			}
			else{
				$this->error['warning'] = $this->language->get('text_miscall_failed');
			}

			return !$this->error;

		}
		else if($this->request->post['do']=="verify"){
			$customer_info = $this->model_account_customer->getCustomerByCode($code);

			if ($customer_info) {
				$this->load->language('account/reset');

				$this->document->setTitle($this->language->get('heading_title'));

				if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
					$this->model_account_customer->editPassword($customer_info['email'], $this->request->post['password']);

					if ($this->config->get('config_customer_activity')) {
						$this->load->model('account/activity');

						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
						);

						$this->model_account_activity->addActivity('reset', $activity_data);
					}

					$this->session->data['success'] = $this->language->get('text_success');

					$this->response->redirect($this->url->link('account/login', '', true));
				}

				$data['heading_title'] = $this->language->get('heading_title');

				$data['text_password'] = $this->language->get('text_password');

				$data['entry_password'] = $this->language->get('entry_password');
				$data['entry_confirm'] = $this->language->get('entry_confirm');

				$data['button_continue'] = $this->language->get('button_continue');
				$data['button_back'] = $this->language->get('button_back');

				$data['breadcrumbs'] = array();

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_home'),
					'href' => $this->url->link('common/home')
				);

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_account'),
					'href' => $this->url->link('account/account', '', true)
				);

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('heading_title'),
					'href' => $this->url->link('account/reset', '', true)
				);

				if (isset($this->error['password'])) {
					$data['error_password'] = $this->error['password'];
				} else {
					$data['error_password'] = '';
				}

				if (isset($this->error['confirm'])) {
					$data['error_confirm'] = $this->error['confirm'];
				} else {
					$data['error_confirm'] = '';
				}

				$data['action'] = $this->url->link('account/reset', 'code=' . $code, true);

				$data['back'] = $this->url->link('account/login', '', true);

				if (isset($this->request->post['password'])) {
					$data['password'] = $this->request->post['password'];
				} else {
					$data['password'] = '';
				}

				if (isset($this->request->post['confirm'])) {
					$data['confirm'] = $this->request->post['confirm'];
				} else {
					$data['confirm'] = '';
				}

				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');

				$this->response->setOutput($this->load->view('account/reset', $data));
			} else {
				$this->load->language('account/reset');

				$this->session->data['error'] = $this->language->get('error_code');

				return new Action('account/login');
			}

		}


	}



	private  function beautify_id_number( $mdn , $zero = false ){
		$check = true;
		while ( $check == true ){
			$check = false;
			if( !is_numeric(substr( $mdn , 0 , 1 )) ){
				$mdn = substr( $mdn, 1 );
				$check = true;
			}

			if( substr( $mdn , 0 , 2 ) == '62' ){
				$mdn = substr( $mdn, 2 );
				$check = true;
			}

			while ( substr( $mdn , 0 , 1 ) == '0' ){
				$mdn = substr( $mdn, 1 );
				$check = true;
			}
		}

		if ( $zero ){
			$mdn = '0'.$mdn;
		}else{
			$mdn = '62'.$mdn;
		}

		return $mdn;

    }

function masking($number, $maskingCharacter = 'X') {
	$string = "";
	$arr = str_split($number);
	for($i=0;$i<strlen($number);$i++){
		if($i>=(strlen($number)-5)){ 
			$string .= "x";
		}
		else{
			$string .= $arr[$i];
		}
	}
	return $string;
} 




	public function verify() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {

			$customer_info = $this->model_account_customer->getCustomerByCode($code);

			if ($customer_info) {
				$this->load->language('account/reset');

				$this->document->setTitle($this->language->get('heading_title'));

				if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
					$this->model_account_customer->editPassword($customer_info['email'], $this->request->post['password']);

					if ($this->config->get('config_customer_activity')) {
						$this->load->model('account/activity');

						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
						);

						$this->model_account_activity->addActivity('reset', $activity_data);
					}

					$this->session->data['success'] = $this->language->get('text_success');

					$this->response->redirect($this->url->link('account/login', '', true));
				}

				$data['heading_title'] = $this->language->get('heading_title');

				$data['text_password'] = $this->language->get('text_password');

				$data['entry_password'] = $this->language->get('entry_password');
				$data['entry_confirm'] = $this->language->get('entry_confirm');

				$data['button_continue'] = $this->language->get('button_continue');
				$data['button_back'] = $this->language->get('button_back');

				$data['breadcrumbs'] = array();

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_home'),
					'href' => $this->url->link('common/home')
				);

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_account'),
					'href' => $this->url->link('account/account', '', true)
				);

				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('heading_title'),
					'href' => $this->url->link('account/reset', '', true)
				);

				if (isset($this->error['password'])) {
					$data['error_password'] = $this->error['password'];
				} else {
					$data['error_password'] = '';
				}

				if (isset($this->error['confirm'])) {
					$data['error_confirm'] = $this->error['confirm'];
				} else {
					$data['error_confirm'] = '';
				}

				$data['action'] = $this->url->link('account/reset', 'code=' . $code, true);

				$data['back'] = $this->url->link('account/login', '', true);

				if (isset($this->request->post['password'])) {
					$data['password'] = $this->request->post['password'];
				} else {
					$data['password'] = '';
				}

				if (isset($this->request->post['confirm'])) {
					$data['confirm'] = $this->request->post['confirm'];
				} else {
					$data['confirm'] = '';
				}

				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');

				$this->response->setOutput($this->load->view('account/reset', $data));
			} else {
				$this->load->language('account/reset');

				$this->session->data['error'] = $this->language->get('error_code');

				return new Action('account/login');
			}

		}
		else{
			$data['success_miscall'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('account/forgotten', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten', '', true);

		$data['back'] = $this->url->link('account/login', '', true);

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/verify_forgotten', $data));
	}

}
