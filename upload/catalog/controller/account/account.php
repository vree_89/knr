<?php
class ControllerAccountAccount extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_membership'] = $this->language->get('text_membership');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', true);
		$data['password'] = $this->url->link('account/password', '', true);
		$data['address'] = $this->url->link('account/address', '', true);
		$data['membership'] = $this->url->link('account/membership', '', true);
		
		$data['credit_cards'] = array();
		
		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');
		
		foreach ($files as $file) {
			$code = basename($file, '.php');
			
			if ($this->config->get($code . '_status') && $this->config->get($code . '_card')) {
				$this->load->language('extension/credit_card/' . $code);

				$data['credit_cards'][] = array(
					'name' => $this->language->get('heading_title'),
					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				);
			}
		}
		
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		
		
		$data['return'] = $this->url->link('account/return', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['recurring'] = $this->url->link('account/recurring', '', true);


		$this->load->model('account/customer');
		$this->load->model('account/address');
		$this->load->model('account/reward');
		$this->load->model('membership/membership');


		$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		$address_info = $this->model_account_address->getAddress($customer_info['address_id']);
		$reward_total = $customer_info['reward'];
		if(isset($this->session->data['membership'])){
			$membership_info = $this->session->data['membership'];
		}
		else{
			$membership_info = null;
		}

		$data['customer_info'] = $customer_info;
		$data['address_info'] = $address_info;
		$data['reward_total'] = number_format($reward_total);
		$data['referal_link'] = $this->url->link('account/register', 'reference=' . $customer_info['reference_code']);
		$data['membership_info'] = $membership_info;

		$data['text_category_account'] = $this->language->get('text_category_account');
		$data['text_category_membership'] = $this->language->get('text_category_membership');
		$data['text_category_address'] = $this->language->get('text_category_address');
		$data['text_category_referal'] = $this->language->get('text_category_referal');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		//$data['content_top'] = $this->load->controller('common/content_top');
		//$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		//die($data['content_bottom']);
		//die($data['column_right']);


		
		$this->response->setOutput($this->load->view('account/account', $data));
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				//'id_city'              => $this->getProvince(),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function zone() {
		$json = array();
		$this->load->model('localisation/zone');
		$zone_id = $this->request->get['zone_id'];
		//echo $zone_id;exit;
		$zone = $this->model_localisation_zone->getZone($zone_id);
			$json = array(
				'city'              => '',
			);

		if(isset($zone['name'])){
			$zone_name = $zone['name'];
			$province_id = "";
			$province = $this->getProvince()['rajaongkir']['results'];
			//var_dump($province);
			// exit;
			foreach ($province as $_province) {
				if($_province['province']==$zone_name){
					$province_id = $_province['province_id'];
					break;
				}
			}
			//var_dump($this->getCity($province_id));

			$json = array(
				'city'              => $this->getCity($province_id)['rajaongkir']['results'],
			);
		}
	
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}




	public function city() {
		$json = array();
		$id = $this->request->get['id'];
		$city = $this->getCityName($id);
		//var_dump($city['rajaongkir']['results']);exit;
		$json = array(
			'city_name'              => $city['rajaongkir']['results']['type'].' '.$city['rajaongkir']['results']['city_name'],
		);
		var_dump($json);exit;


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getProvince(){
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  id - id_propinsi (GET)
		*/ 

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key:  a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		//echo $response;exit;
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return  "cURL Error #:" . $err;
		} else {
		   return json_decode($response,true);
		}

	}

	public function getCity($province, $id = null){
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  id - id kota/kabupaten (GET)
		  province - id propinsi (GET)
		  
		*/ 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=$province",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		// var_dump($response);
		// exit;

/*string(3379) "{
	"rajaongkir":{
		"query":{"province":"9"},
		"status":{"code":200,"description":"OK"},
		"results":[
			{"city_id":"22","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Bandung","postal_code":"40311"},{"city_id":"23","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Bandung","postal_code":"40115"},
			{"city_id":"24","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Bandung Barat","postal_code":"40721"},
			{"city_id":"34","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Banjar","postal_code":"46311"},
			{"city_id":"54","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Bekasi","postal_code":"17837"},
			{"city_id":"55","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Bekasi","postal_code":"17121"},
			{"city_id":"78","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Bogor","postal_code":"16911"},
			{"city_id":"79","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Bogor","postal_code":"16119"},
			{"city_id":"103","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Ciamis","postal_code":"46211"},
			{"city_id":"104","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Cianjur","postal_code":"43217"},
			{"city_id":"107","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Cimahi","postal_code":"40512"},

			{"city_id":"108","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Cirebon","postal_code":"45611"},
			{"city_id":"109","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Cirebon","postal_code":"45116"},
			{"city_id":"115","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Depok","postal_code":"16416"},
			
			{"city_id":"126","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Garut","postal_code":"44126"},
			{"city_id":"149","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Indramayu","postal_code":"45214"},
			{"city_id":"171","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Karawang","postal_code":"41311"},
			{"city_id":"211","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Kuningan","postal_code":"45511"},
			{"city_id":"252","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Majalengka","postal_code":"45412"},
			{"city_id":"332","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Pangandaran","postal_code":"46511"},
			{"city_id":"376","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Purwakarta","postal_code":"41119"},
			{"city_id":"428","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Subang","postal_code":"41215"},
			{"city_id":"430","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Sukabumi","postal_code":"43311"},
			{"city_id":"431","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Sukabumi","postal_code":"43114"},
			{"city_id":"440","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Sumedang","postal_code":"45326"},
			{"city_id":"468","province_id":"9","province":"Jawa Barat","type":"Kabupaten","city_name":"Tasikmalaya","postal_code":"46411"},
			{"city_id":"469","province_id":"9","province":"Jawa Barat","type":"Kota","city_name":"Tasikmalaya","postal_code":"46116"}
		]
	}
}"*/

		if ($err) {
		  return  "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		}		
	}

	public function getCityName( $id = null){
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  id - id kota/kabupaten (GET)
		  province - id propinsi (GET)
		  
		*/ 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/city?id=$id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return  "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		}		
	}

}
