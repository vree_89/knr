<?php
class ControllerAccountLogin extends Controller {
	private $error = array();

	public function index() {
		if (!isset($this->session->data['location'])) {
			if(isset($this->request->get['path'])){
			$this->session->data['redirect'] = $this->url->link('account/login', 'path='.$this->request->get['path']);
			}
			else{
			$this->session->data['redirect'] = $this->url->link('account/login');
			}
			$this->response->redirect($this->url->link('common/home', '', true));
		}

		$this->load->model('account/customer');

		// Login override for admin users
		if (!empty($this->request->get['token'])) {
			$this->customer->logout();
			$this->cart->clear();

			unset($this->session->data['order_id']);
			unset($this->session->data['payment_address']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['comment']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);

			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

			if ($customer_info && $this->customer->login($customer_info['email'], '', true)) {
				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}

		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/login');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$next = $this->session->data['next'];

			
			// Unset guest
			unset($this->session->data['guest']);
			unset($this->session->data['next']);









			// Default Shipping Address
			$this->load->model('account/address');

			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			// Wishlist
			if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
				$this->load->model('account/wishlist');

				foreach ($this->session->data['wishlist'] as $key => $product_id) {
					$this->model_account_wishlist->addWishlist($product_id);

					unset($this->session->data['wishlist'][$key]);
				}
			}

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('login', $activity_data);
			}


			// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
			if (isset($this->request->post['redirect']) && $this->request->post['redirect'] != $this->url->link('account/logout', '', true) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
				$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
			} else {
				if($next){
					$this->response->redirect($next);
				}
				else{
					$this->response->redirect($this->url->link('account/account', '', true));	
				}
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_new_customer'] = $this->language->get('text_new_customer');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_register_account'] = $this->language->get('text_register_account');
		$data['text_returning_customer'] = $this->language->get('text_returning_customer');
		$data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
		$data['text_forgotten'] = $this->language->get('text_forgotten');

		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_email_telephone'] = $this->language->get('entry_email_telephone');
		$data['entry_password'] = $this->language->get('entry_password');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_login'] = $this->language->get('button_login');

		if (isset($this->session->data['error'])) {
			//echo "a<br/>";
			//echo $this->session->data['error']."<br/>";
			$data['error_warning'] = $this->session->data['error'];
			$data['error_message'] = @$this->error['message'];
			unset($this->session->data['error']);
		} 
		else if(isset($this->session->data['error_verification'])){
			//echo "b<br/>";
			$data['error_warning'] = $this->session->data['error_verification'];
			$data['error_message'] = @$this->error['message'];
			unset($this->session->data['error_verification']);

		}
		else if (isset($this->error['warning'])) {
			//echo "c<br/>";
			$data['error_warning'] = @$this->error['warning'];
			$data['error_message'] = @$this->error['message'];
		} else {
			//echo "d<br/>";
			$data['error_warning'] = '';
			$data['error_message'] = '';
		}

		//echo "error warning: ".$data['error_warning']."<br/>";
		//echo "error message: ".$data['error_message']."<br/>";

		$data['action'] = $this->url->link('account/login', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['forgotten'] = $this->url->link('account/forgotten', '', true);

		$data['btn_login'] = HTTP_SERVER.'image/login/btn_login.png';
		$data['btn_register'] = HTTP_SERVER.'image/login/btn_register.png';

		//echo $data['btn_register'];
		// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
		if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
			$data['redirect'] = $this->request->post['redirect'];
		} elseif (isset($this->session->data['redirect'])) {
			$data['redirect'] = $this->session->data['redirect'];

			unset($this->session->data['redirect']);
		} else {
			$data['redirect'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/login', $data));
	}

	protected function validate() {
		//die('xa');
		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['message'] = '';
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
		// echo ($customer_info['approved']);
		// echo ($customer_info['verified']);
		// exit;

		if ($customer_info && !$customer_info['approved']) {
			// die('a');
			$this->error['message'] = '';
			$this->error['warning'] = $this->language->get('error_approved');
		}

		// if ($customer_info && !$customer_info['verified']) {
		// 	//update token
		// 	$token 		= token(40);
		// 	$this->db->query("UPDATE " . DB_PREFIX . "customer SET code = '" .$this->db->escape($salt = $token) . "' WHERE customer_id = ".$customer_info['customer_id']);

		// 	$this->error['message'] = sprintf($this->language->get('message_resend_email'), $this->url->link('account/login/resend_email&customer_id='.$customer_info['customer_id'].'&token='.$token));

		// 	;
		// 	$this->error['warning'] = $this->language->get('error_verified');
		// }
		//exit;


		if (!$this->error) {
			if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
				$this->error['warning'] = $this->language->get('error_login');

				$this->model_account_customer->addLoginAttempt($this->request->post['email']);
			} else {
				//logged in check membership
				$membership_info = $this->model_account_customer->getCustomerMembership($this->session->data['customer_id']);
				if($membership_info){
					$this->session->data['membership']['membership_name'] = $membership_info['membership_name'];
					$this->session->data['membership']['membership_deposit'] = $membership_info['membership_deposit'];
					$this->session->data['membership']['membership_expired'] = $membership_info['membership_expired'];
					$this->session->data['membership']['membership_discount'] = $membership_info['membership_discount'];
					$this->session->data['membership']['membership_loyalty_point'] = $membership_info['membership_loyalty_point'];
					$this->session->data['membership']['membership_bonus_point'] = $membership_info['membership_bonus_point'];
				}

				//get customer deposit
				$deposit = $this->model_account_customer->getCustomerDeposit($this->session->data['customer_id']);
				$this->session->data['customer_deposit'] = $deposit;

				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
				// var_dump($this->session->data['membership']);exit;
			}
			// $this->session->data['input_payment_wallet'] = 0;
			// $this->session->data['input_payment_total'] = 0;
		}

		return !$this->error;
	}

	public function resend_email(){
		$this->load->model('account/customer');
		$this->load->model('account/customer_group');

		$this->load->language('mail/customer');	
		$customer_info = $this->model_account_customer->getCustomer($this->request->get['customer_id']);
		$token = $this->request->get['token'];
		$email = $customer_info['email'];

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_info['customer_group_id']);

		
		$subject = sprintf($this->language->get('text_subject_verification'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message = "\n";



		// $message .= $activation_url . "\n\n";

		$message .= $this->language->get('text_verify')."\n";
		$message .= $this->url->link('account/verification&code='.$token, '', true)."\n\n";

		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($email);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		
		$mail->send();


		//send sms code also
		$phone = $customer_info['telephone'];
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
		$base = $this->config->get('config_ssl');
		} else {
		$base = $this->config->get('config_url');
		}

		$smstext = 'Your verification link : '.$base.'index.php?route=account/verification&code='.$token;

		$data = array(
		"no_hp"    =>  $phone,
		"smstext"  =>  urlencode($smstext),
		);

		// create curl resource
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://api.popay.co.id:8182/SMSGW/kirimsms");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		     
		$output = curl_exec($ch);

		curl_close($ch);     

		$smsid = $output ? true : false;
		//if(!$smsid){echo "failed to send sms";exit;}









		$this->document->setTitle($this->language->get('heading_title'));


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/text_account', '', true)
		);
			$data['text_success'] = $this->language->get('text_success');
			$data['text_email_sent'] = $this->language->get('text_email_sent');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');			
			$this->response->setOutput($this->load->view('account/resend_success', $data));
		
	}
}
