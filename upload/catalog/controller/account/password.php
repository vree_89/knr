<?php
class ControllerAccountPassword extends Controller {
	private $error = array();


	public function verify_token(){
		$status = "";
		$message = "";

		$this->load->language('account/register');
		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByCode($this->request->post['token']);
		if($customer_info){
			if(strtotime($customer_info['code_expired_date'])>strtotime(date('Y-m-d H:i:s'))){
				$status  = "1";
				$message  = "User found";
				$redirect = $this->url->link('account/reset&code='. $this->request->post['token']);
			}
			else{
				$status  = "0";
				$message  = "Token not match or already expired";
				$redirect = "";
			}
		}
		else{
			$status  = "0";
			$message  = "Token not match or already expired";
			$redirect = "";
		}

		$res = array(
			"status" =>$status,
			"message" =>$message,
			);
		echo json_encode($res);

	}

	public function request_token(){
		$status = "";
		$message = "";
		$prenumber = "";

		$this->load->language('account/register');
		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['telephone']);
			$url = 'http://104.199.196.122/gateway/v1/call';

			$arrayBody = [ 
			'userid' => "GamesPark",
			'password' => "GPark!Intv",
			'msisdn' => $this->beautify_id_number($this->request->post['telephone']),
			'gateway' => 0,
			];

			$json = json_encode( $arrayBody );

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
			$output = curl_exec($ch);

			curl_close($ch);

			$arrayResult = json_decode( $output, true );
			// $arrayResult['result'] = "Success";
			// $arrayResult['rc'] = "00";
			// $arrayResult['token'] = "6221304".$this->randomNumber(5);
			// echo $arrayResult['token'];exit;
			if(@$arrayResult['result']=="Success" && @$arrayResult['rc']=="00"){

				$this->model_account_customer->editCodeByTelephone($this->request->post['telephone'], substr($arrayResult['token'],  -5));
				$this->model_account_customer->editLastPasswordChange($this->request->post['telephone']);

				$status = "1";
				$message = sprintf($this->language->get('text_miscall_success'), $this->masking($arrayResult['token']));
				$prenumber = substr($arrayResult['token'],0,  (strlen($arrayResult['token'])-5));
				$prenumber = "+".$this->beautify_id_number($prenumber);

				// $this->session->data['token_register'] = substr($arrayResult['token'],  -5);
				// $status = "1";
				// $message = sprintf($this->language->get('text_miscall_success'), $this->masking($arrayResult['token']));


				// $this->model_account_customer->addVerifyNumber($this->request->post['telephone'],substr($arrayResult['token'],  -5));
				// $verify = $this->model_account_customer->getVerifyNumber($this->request->post['telephone']);
				// if(!$verify){
				// 	$this->model_account_customer->addVerifyNumber($this->request->post['telephone'],substr($arrayResult['token'],  -7));
				// 	//$this->session->data['verifiy_counter'][$this->request->post['telephone']] = $this->session->data['verifiy_counter'][$this->request->post['telephone']] + 1;
				// }
				// else{
				// 	$this->model_account_customer->updateVerifyNumber($this->request->post['telephone'],substr($arrayResult['token'],  -7));
				// 	//$this->session->data['verifiy_counter'][$this->request->post['telephone']] = 1;
				// }

			}
			else if(@$arrayResult['rc']!="00"){
				$status = "0";
				if($arrayResult['rc']=="06"){
				$message = "Phone number is invalid.";
				}
				else{
				$message = "Verify failed, please try again later.";
				}

			}
			else{
				$status = "0";
				$message = $this->language->get('text_miscall_failed');
			}


		$res = array(
			"status" =>$status,
			"message" =>$message,
			"prenumber"=>$prenumber,
			);
		echo json_encode($res);

	}

	private function randomNumber($length = 4) {
	    $result = '';

	    for($i = 0; $i < $length; $i++) {
	        $result .= mt_rand(0, 9);
	    }

	    return $result;
	}		

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/password', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/password');

		$this->load->model('account/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/customer');

			$this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);

			$this->session->data['success'] = $this->language->get('text_success');

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('password', $activity_data);
			}

			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/password', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_password'] = $this->language->get('text_password');

		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_back'] = $this->language->get('button_back');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		$data['action'] = $this->url->link('account/password', '', true);
		$data['action_request'] = $this->url->link('account/password/request_token');
		$data['action_verify'] = $this->url->link('account/password/verify_token');


		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} elseif (!empty($customer_info)) {
			$data['telephone'] = $customer_info['telephone'];
		} else {
			$data['telephone'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}

		$data['back'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');


		$data['btn_back'] = HTTP_SERVER.'image/password/btn_back.png';
		$data['btn_continue'] = HTTP_SERVER.'image/password/btn_continue.png';
		$data['btn_save'] = HTTP_SERVER.'image/password/btn_save_'.$this->session->data['language'].'.png';
		//echo $data['btn_save'];

		$this->response->setOutput($this->load->view('account/password', $data));
	}

	private  function beautify_id_number( $mdn , $zero = false ){
		$check = true;
		while ( $check == true ){
			$check = false;
			if( !is_numeric(substr( $mdn , 0 , 1 )) ){
				$mdn = substr( $mdn, 1 );
				$check = true;
			}

			if( substr( $mdn , 0 , 2 ) == '62' ){
				$mdn = substr( $mdn, 2 );
				$check = true;
			}

			while ( substr( $mdn , 0 , 1 ) == '0' ){
				$mdn = substr( $mdn, 1 );
				$check = true;
			}
		}

		if ( $zero ){
			$mdn = '0'.$mdn;
		}else{
			$mdn = '62'.$mdn;
		}

		return $mdn;

    }

function masking($number, $maskingCharacter = 'X') {
	$string = "";
	$arr = str_split($number);
	for($i=0;$i<strlen($number);$i++){
		if($i>=(strlen($number)-5)){ 
			$string .= "x";
		}
		else{
			$string .= $arr[$i];
		}
	}
	return $string;
}

	protected function validate() {


		if ($this->request->post['verified']=="") {
			$this->error['warning'] = $this->language->get('error_verified');
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		return !$this->error;
	}
}