<?php
class ControllerAccountVerification extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/verification');
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		if (isset($this->request->get['code'])) {
			$code = $this->request->get['code'];
		} else {
			$code = '';
		}


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => '#'
		);

		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByCode($code);

		if ($customer_info) {
			$this->load->language('account/verification');

			$this->document->setTitle($this->language->get('heading_title'));

			if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
				$this->model_account_customer->editVerify($customer_info['telephone']);

				if ($this->config->get('config_customer_activity')) {
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $customer_info['customer_id'],
						'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
					);

					$this->model_account_activity->addActivity('verify', $activity_data);
				}

				// $this->session->data['success'] = $this->language->get('text_success');

				// $this->response->redirect($this->url->link('account/login', '', true));
			}

			$data['heading_title'] = $this->language->get('heading_title');

			$data['continue'] = $this->url->link('account/login', '', true);
			$data['text_message'] = $this->language->get('text_message');
			$data['redirect'] = $this->url->link('account/login', '', true);

			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/verification', $data));
		} else {
			$this->load->language('account/verification');
			
			$this->session->data['error_verification'] = $this->language->get('error_code');

			return new Action('account/login');
		}
	}
}
