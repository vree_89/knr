<?php
class ControllerAccountMembership extends Controller {
	public function index() {

		$this->load->language('account/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_membership'),
			'href'      => $this->url->link('account/membership', '', true)
		);


		$data['heading_title'] = $this->language->get('heading_title');

	


		$this->load->model('account/membership');

		$data['memberships'] = array();
		$customer_id = $this->customer->getId();
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$membership_total = $this->model_account_membership->getTotalMemberships($customer_id);
		$results = $this->model_account_membership->getMemberships($customer_id, ($page - 1) * 5, 5);
		foreach ($results as $result) {
			$data['memberships'][] = array(
				"membership_id"=>$result['membership_id'],
				"name"=>$result['name'],
				"status"=>$result['status'],
				"date_added"=>$result['date_added'],
				"payment_method"=>$result['payment_method'],
				"payment_date"=>$result['payment_date'],
				"payment_code"=>$result['payment_code'],
				"payment_total"=>$result['payment_total'],
				'customer_membership_id'=>$result['customer_membership_id'],
			);
		}


		$pagination = new Pagination();
		$pagination->total = $membership_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('account/membership', 'page={page}', true);


		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($membership_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($membership_total - 10)) ? $membership_total : ((($page - 1) * 10) + 10), $membership_total, ceil($membership_total / 10));

		$data['continue'] = $this->url->link('account/account', '', true);

		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		//die($data['column_right']);

		//membership information
		$data['membership_info'] = null;
		if(isset($this->session->data['membership'])){
			$data['membership_info'] = $this->session->data['membership'];
			//var_dump($data['membership_info']);

		}

		//die($membership_total);
		
		$this->response->setOutput($this->load->view('account/membership', $data));
	}

	public function membership(){
		$this->load->language('account/membership');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_membership'),
			'href'      => $this->url->link('account/membership', '', true)
		);


		$data['heading_title'] = $this->language->get('heading_title');

	


		$this->load->model('account/membership');

		$data['memberships'] = array();
		$results = $this->model_account_membership->getMemberships();
		foreach ($results as $result) {
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], null, $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}	

			$data['memberships'][] = array(
				"membership_id"=>$result['membership_id'],
				"name"=>$result['name'],
				"price"=>$price,
				"period"=>$result['period'],
				"discount"=>$result['discount'],
				"loyalty_point"=>$result['loyalty_point'],
				"bonus_point"=>$result['bonus_point'],
				"featured"=>$result['featured'],
				"sort_order"=>$result['sort_order'],
				"status"=>$result['status'],
				"date_modified"=>$result['date_modified'],



				);
		}

		//var_dump($data['memberships']);

		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		//die($data['column_right']);
		
		$this->response->setOutput($this->load->view('account/membership', $data));		
	}


	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				//'id_city'              => $this->getProvince(),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function zone() {
		$json = array();
		$this->load->model('localisation/zone');
		$zone_id = $this->request->get['zone_id'];
		//echo $zone_id;exit;
		$zone = $this->model_localisation_zone->getZone($zone_id);
			$json = array(
				'city'              => '',
			);
		if(isset($zone['name'])){
			$zone_name = $zone['name'];
			$province_id = "";
			$province = $this->getProvince()['rajaongkir']['results'];
			foreach ($province as $_province) {
				if($_province['province']==$zone_name){
					$province_id = $_province['province_id'];
					break;
				}
			}
			//var_dump($this->getCity($province_id));

			$json = array(
				'city'              => $this->getCity($province_id)['rajaongkir']['results'],
			);
		}
	
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}




	public function city() {
		$json = array();
		$id = $this->request->get['id'];
		$city = $this->getCityName($id);
		//var_dump($city);exit;
		$json = array(
			'city_name'              => $city['rajaongkir']['results']['type'].' '.$city['rajaongkir']['results']['city_name'],
		);
		//var_dump($json);exit;


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getProvince(){
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  id - id_propinsi (GET)
		*/ 

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key:  a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return  "cURL Error #:" . $err;
		} else {
		   return json_decode($response,true);
		}

	}

	public function getCity($province, $id = null){
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  id - id kota/kabupaten (GET)
		  province - id propinsi (GET)
		  
		*/ 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=$province",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		// var_dump($response);
		// exit;



		if ($err) {
		  return  "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		}		
	}

	public function getCityName( $id = null){
		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  id - id kota/kabupaten (GET)
		  province - id propinsi (GET)
		  
		*/ 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/city?id=$id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return  "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		}		
	}

}
