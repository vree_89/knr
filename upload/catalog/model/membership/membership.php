<?php
class ModelMembershipMembership extends Model {
	public function getMemberships() {
		$sql = "SELECT * FROM " . DB_PREFIX . "membership";

		
		$sql .= " GROUP BY membership_id";

        $sql .= " ORDER BY sort_order ASC, date_modified DESC";



		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getMembership($membership_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "membership where membership_id = ".$membership_id);

		if ($query->num_rows) {
			return array(
				'membership_id'       => $query->row['membership_id'],
				'name'       => $query->row['name'],
				'price'             => $query->row['price'],
				'period'      => $query->row['period'],
				'discount'       => $query->row['discount'],
				'loyalty_point' => $query->row['loyalty_point'],
				'bonus_point'     => $query->row['bonus_point'],
				'featured'              => $query->row['featured'],
				'sort_order'            => $query->row['sort_order'],
				'status'              => $query->row['status'],
				'date_modified'              => $query->row['date_modified'],
			);
		} else {
			return false;
		}		
	}

	public function addCustomerMembership($data){
		$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_membership` SET customer_id = '" . $this->db->escape($data['customer_id']) . "', membership_id = '" . (int)$data['membership_id'] . "', status = '" . $this->db->escape($data['status']) . "', payment_method = NULL, date_added = NOW(), membership_name = '" . $data['membership_name'] . "', membership_deposit = '" . (int)$data['membership_deposit'] . "', membership_period = '" . (int)$data['membership_period'] . "', payment_total = NULL, payment_date = NULL");
		$order_id = $this->db->getLastId();
		return $order_id;

	}

public function addOrder($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_membership SET customer_id = '" . (int)$data['customer_id'] . "', membership_id = '" . (int)$data['membership_id'] . "', membership_name = '" . $this->db->escape($data['membership_name']) . "', membership_deposit = '" . (int)$data['membership_deposit'] . "', membership_period = '" . (int)$data['membership_period'] . "', membership_discount = '" . (int)$data['membership_discount'] . "', membership_loyalty_point = '" . (float)$data['membership_loyalty_point'] . "', membership_bonus_point = '" . (float)$data['membership_bonus_point'] . "', date_added = NOW(), ip = '" . $this->db->escape($data['ip']) . "', payment_total = '" . (int)$data['payment_total'] . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', language_id = '" . (int)$data['language_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', uniqueID = '".$data['uniqueID']."'");
		return $this->db->getLastId();

	}

	public function editOrder($order_id, $data,$customer_id,$reference_id=null) {
		$log = new Log('model_membership_membership.log');
		$log->write("");
		$log->write("order_id : ".$order_id);
		$log->write("customer_id : ".$customer_id);
		$log->write("reference_id : ".$reference_id);



		//update customer membership
		if($data['status']==1){
			//$log->write("status : 1");
			//check membership
			$membership_info = $this->model_account_customer->getCustomerMembership($data['customer']['customer_id']);
			//user already buy membership before
			if($membership_info){
				$log->write("membership found");
				//echo "1";
				$current_membership_name = $membership_info['membership_name'];
				$current_membership_discount = $membership_info['membership_discount'];

				$log->write("buy membership_name : ".$data['membership_name']);
				$log->write("buy membership_discount : ".$data['membership_discount']);
				$log->write("current membership name : ".$current_membership_name);
				$log->write("current membership_discount : ".$current_membership_discount);

				//buy the same membership just add period to current expired date
				if($data['membership_name']==$current_membership_name && $data['membership_discount']==$current_membership_discount){
					$log->write("buy same membership");
					//echo "1";
					$deposit = $data['customer']['deposit'];
					$membership_expired = $data['customer']['membership_expired'];
					$sqlMembership = "UPDATE `".DB_PREFIX."customer` SET";
					$sqlMembership .= " `deposit` = `deposit` + ".$data['amount'];
					$sqlMembership .= ", `membership_expired` = `membership_expired` + INTERVAL ".$data['membership_period']." DAY";
					$sqlMembership .= " WHERE customer_id = ".$data['customer']['customer_id'];
					$log->write("sql membership".$sqlMembership);
					$this->db->query($sqlMembership);			

				}
				//buy newer membership (higher benefit, reset expired date)
				else{
					$log->write("buy newer membership with higher benefit");
					//echo "2";
					$deposit = $data['customer']['deposit'];
					$membership_expired = $data['customer']['membership_expired'];
					$sqlMembership = "UPDATE `".DB_PREFIX."customer` SET";
					$sqlMembership .= " `deposit` = `deposit` + ".$data['amount'];
					$sqlMembership .= ", `membership_expired` =  '".date('Y-m-d H:i:s')."' + INTERVAL ".$data['membership_period']." DAY";
					$sqlMembership .= " WHERE customer_id = ".$data['customer']['customer_id'];
					$log->write("sql membership".$sqlMembership);
					$this->db->query($sqlMembership);			

				}
				//echo "<br/>";

			}
			//first buy
			else{
				$log->write("first buy membership");

				//echo "2";
				$deposit = $data['customer']['deposit'];
				$membership_expired = $data['customer']['membership_expired'];
				$sqlMembership = "UPDATE `".DB_PREFIX."customer` SET";

				if($deposit==NULL){
					$sqlMembership .= " `deposit` = ".$data['amount'];
	 			}
				else{
					$sqlMembership .= " `deposit` = `deposit` + ".$data['amount'];
				}
				$sqlMembership .= ", `membership_expired` =  '".date('Y-m-d H:i:s')."' + INTERVAL ".$data['membership_period']." DAY";
				// if($membership_expired==NULL){
				// }
				// else{
				// 	$sqlMembership .= ", `membership_expired` = `membership_expired` + INTERVAL ".$data['membership_period']." DAY";
				// }
				$sqlMembership .= " WHERE customer_id = ".$data['customer']['customer_id'];
				$log->write("sql membership".$sqlMembership);
				$this->db->query($sqlMembership);		
				//echo "<br/>";					
			}
			//exit;


			//update membership
			$sqlref = "";
			if(isset($reference_id)){
				$sqlref = ", reference_id = '".$reference_id."' ";
			}
			$this->load->model('account/customer');
			$sql = "UPDATE `" . DB_PREFIX . "customer_membership` SET status = '" . $this->db->escape($data['status']) . "', payment_date = '".$data['payment_date']."' ".$sqlref."	WHERE customer_membership_id = '" . (int)$order_id . "'";
			$log->write($sql);
			$this->db->query($sql);
			




			$log->write("sending email..");
			// If order status is not 0 then send update text email
			$order_info = $this->getOrder($order_id);
			$language = new Language($order_info['language_code']);
			$language->load($order_info['language_code']);
			$language->load('mail/membership');

			$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), "MBR-".$order_id);

			$message  = $language->get('text_update_order') . ' MBR-' . $order_id . "\n";
			$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";


			$message .= $language->get('text_update_order_status') . "\n\n";
			$message .= "PAID\n\n";

			$message .= $language->get('text_update_footer');

			// $log->write("subject message : ".html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			// $log->write("email message : ".$message);
			// $log->write("config_mail_protocol : ".$this->config->get('config_mail_protocol'));
			// $log->write("config_mail_parameter : ".$this->config->get('config_mail_parameter'));
			// $log->write("config_mail_smtp_hostname : ".$this->config->get('config_mail_smtp_hostname'));
			// $log->write("config_mail_smtp_username : ".$this->config->get('config_mail_smtp_username'));
			// $log->write("config_mail_smtp_password : ".html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8'));
			// $log->write("config_mail_smtp_port : ".$this->config->get('config_mail_smtp_port'));
			// $log->write("config_mail_smtp_timeout : ".$this->config->get('config_mail_smtp_timeout'));
			// $log->write("customer email : ".$data['customer']['email']);
			// $log->write("set from : ".$this->config->get('config_email'));
			// $log->write("set sender : ".$order_info['store_name']);


			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($data['customer']['email']);
			$mail->setFrom($this->config->get('config_email'));
			$log->write("store_name : ".$order_info['store_name']);
			$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			$log->write("email sent for order_id :".$order_id);


		}
	}

	public function getOrderByUniqueID($uniqueID){
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_membership` WHERE uniqueID = '" . $uniqueID . "'");

		if ($order_query->num_rows) {

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
			} else {
				$language_code = $this->config->get('config_language');
			}
			return array(
				'customer_membership_id'   	=> $order_query->row['customer_membership_id'],
				'customer_id'              	=> $order_query->row['customer_id'],
				'membership_id'          	=> $order_query->row['membership_id'],
				'membership_name'          	=> $order_query->row['membership_name'],
				'membership_deposit'       	=> $order_query->row['membership_deposit'],
				'membership_period'       	=> $order_query->row['membership_period'],
				'membership_discount'      	=> $order_query->row['membership_discount'],
				'membership_loyalty_point' 	=> $order_query->row['membership_loyalty_point'],
				'membership_bonus_point'   	=> $order_query->row['membership_bonus_point'],
				'status'          			=> $order_query->row['status'],
				'date_added'          		=> $order_query->row['date_added'],
				'ip'          				=> $order_query->row['ip'],
				'payment_total'          	=> $order_query->row['payment_total'],
				'payment_method'          	=> $order_query->row['payment_method'],
				'payment_code'          	=> $order_query->row['payment_code'],
				'payment_date'          	=> $order_query->row['payment_date'],
				'store_name'          	=> $order_query->row['store_name'],
				'language_code'				=> $language_code,
			);
		} else {
			return false;
		}
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_membership` WHERE customer_membership_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
			} else {
				$language_code = $this->config->get('config_language');
			}
			return array(
				'customer_membership_id'   	=> $order_query->row['customer_membership_id'],
				'customer_id'              	=> $order_query->row['customer_id'],
				'membership_id'          	=> $order_query->row['membership_id'],
				'membership_name'          	=> $order_query->row['membership_name'],
				'membership_deposit'       	=> $order_query->row['membership_deposit'],
				'membership_period'       	=> $order_query->row['membership_period'],
				'membership_discount'      	=> $order_query->row['membership_discount'],
				'membership_loyalty_point' 	=> $order_query->row['membership_loyalty_point'],
				'membership_bonus_point'   	=> $order_query->row['membership_bonus_point'],
				'status'          			=> $order_query->row['status'],
				'date_added'          		=> $order_query->row['date_added'],
				'ip'          				=> $order_query->row['ip'],
				'payment_total'          	=> $order_query->row['payment_total'],
				'payment_method'          	=> $order_query->row['payment_method'],
				'payment_code'          	=> $order_query->row['payment_code'],
				'payment_date'          	=> $order_query->row['payment_date'],
				'store_name'          	=> $order_query->row['store_name'],
				'language_code'				=> $language_code,
			);
		} else {
			return false;
		}
	}

	public function getActiveMembership($customer_id) {
		$sql = "SELECT cm.* 
				FROM " . DB_PREFIX . "customer c
				LEFT JOIN " . DB_PREFIX . "customer_membership cm ON (c.customer_id = cm.customer_id)
				WHERE c.customer_id = '" . (int)$customer_id . "' 
				AND c.membership_expired >= NOW()
				AND cm.status = 1
				ORDER BY cm.customer_membership_id DESC";

		$query = $this->db->query($sql);

		return $query->row;
	}	

	public function addOrderHistory($order_id, $comment = '',$expired_date){
		$order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_membership WHERE customer_membership_id = '" . (int)$order_id . "'");		

		if ($order_query->num_rows) {
			$this->load->model('account/customer');
			$customer_info = $this->model_account_customer->getCustomer($order_query->row['customer_id']);
			$language = new Language($this->config->get('config_language'));
			$language->load($this->config->get('config_language'));
			$language->load('mail/membership');
			if ($this->request->server['HTTPS']) {
				$store_url = HTTPS_SERVER;
			} else {
				$store_url = HTTP_SERVER;
			}

			$store_name = $this->config->get('config_name');
			$order_status = 'Pending';

			$subject = sprintf($language->get('text_new_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'), "MBR-".$order_id);


			// HTML Mail
			$data = array();
			$data['title'] = sprintf($language->get('text_new_subject'), $store_name, $order_id);
			$data['text_greeting'] = sprintf($language->get('text_new_greeting'), $store_name);
			$data['text_link'] = $language->get('text_new_link');
			$data['text_order_detail'] = $language->get('text_new_order_detail');
			$data['text_instruction'] = $language->get('text_new_instruction');
			$data['text_order_id'] = $language->get('text_new_order_id');
			$data['text_date_added'] = $language->get('text_new_date_added');
			$data['text_payment_method'] = $language->get('text_new_payment_method');
			$data['text_package_name'] = $language->get('text_new_package_name');
			$data['text_email'] = $language->get('text_new_email');
			$data['text_telephone'] = $language->get('text_new_telephone');
			$data['text_ip'] = $language->get('text_new_ip');
			$data['text_order_status'] = $language->get('text_new_order_status');
			$data['text_total'] = $language->get('text_new_total');
			$data['text_footer'] = $language->get('text_new_footer');
			$data['logo'] = $store_url . 'image/' . $this->config->get('config_logo');
			$data['store_name'] = $store_name;
			$data['store_url'] = $store_url;
			$data['customer_id'] = $order_query->row['customer_id'];
			$data['link'] = $store_url . 'index.php?route=account/membership';
			$data['order_id'] = "MBR-".$order_id;
			$data['date_added'] = date($language->get('date_format_short'), strtotime($order_query->row['date_added']));
			$data['payment_method'] = $order_query->row['payment_method'];
			$data['package_name'] = $order_query->row['membership_name'];
			$data['total'] = 'IDR'.number_format($order_query->row['payment_total']);
			$data['email'] = $customer_info['email'];
			$data['telephone'] = $customer_info['telephone'];
			$data['ip'] = $order_query->row['ip'];
			$data['order_status'] = $order_status;

			if ($comment) {
				$data['comment'] = nl2br($comment);
			} else {
				$data['comment'] = '';
			}

			$data['comment'] .= sprintf($language->get('text_expired_date'), $expired_date);

			// Text Mail
			$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8')) . "\n\n";
			$text .= $language->get('text_new_order_id') . ' MBR-' . $order_id . "\n";
			$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_query->row['date_added'])) . "\n";
			$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

			if ($comment) {
				$text .= $language->get('text_new_instruction') . "\n\n";
				$text .= $comment . "\n\n";
			}

			if ($order_query->row['customer_id']) {
				$text .= $language->get('text_new_link') . "\n";
				$text .= $store_url . 'index.php?route=account/membership';
			}


			$text .= $language->get('text_new_footer') . "\n\n";
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($this->load->view('mail/membership', $data));
			$mail->setText($text);
			$mail->send();

		}

	}	

	public function addDokuHistory($data) {		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "doku` SET transidmerchant = '" . $this->db->escape($data['transidmerchant']) . "', totalamount = '" . (double)$data['totalamount'] . "', words = '" .  $this->db->escape($data['words']) . "', payment_channel = '" . $this->db->escape($data['payment_channel']) . "', session_id = '" . $this->db->escape($data['session_id']) . "', payment_date_time = NOW(), trxstatus = 'Requested', expired_date = '".$this->db->escape($data['expired_date'])."'");
	}	

	public function updateDokuHistory($paymentcode,$transidmerchant){
		$this->db->query("UPDATE `" . DB_PREFIX . "doku` set paymentcode = ".$paymentcode." WHERE transidmerchant = '$transidmerchant'");

	}	

}