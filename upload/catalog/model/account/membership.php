<?php
class ModelAccountMembership extends Model {
	public function getMemberships($customer_id,$start = null, $limit = null) {

		$sql = "SELECT cm.*, m.name FROM " . DB_PREFIX . "customer_membership cm 
		LEFT JOIN " . DB_PREFIX . "membership m on cm.membership_id = m.membership_id
		where 
		cm.customer_id = ".$customer_id." 
		AND cm.status = '1'
		/*and cm.status != 0*/
		order by cm.date_added desc
		";
		if(isset($start) && isset($limit)){
			if ($start < 0) {
				$start = 0;
			}

			if ($limit < 1) {
				$limit = 1;
			}
			$sql = "SELECT cm.*, m.name FROM " . DB_PREFIX . "customer_membership cm 
			LEFT JOIN " . DB_PREFIX . "membership m on cm.membership_id = m.membership_id
			where 
			cm.customer_id = ".$customer_id." 
			AND cm.status = '1'
			/*and cm.status != 0*/
			
			order by cm.date_added desc LIMIT " . (int)$start . "," . (int)$limit;
		}



		//echo $sql;

		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getTotalMemberships($customer_id) {
		
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "customer_membership` 
			where customer_id = ".$customer_id." 
			AND status = '1'
			/*and status != 0*/
			");

		return $query->row['total'];
	}

}