<?php
class ModelExtensionPayment2c2p extends Model {
  public function getMethod($address, $total) {
    $this->load->language('payment/2c2p');
  
    $method_data = array(
      'code'     => '2c2p',
      'title'    => $this->language->get('text_title'),
      'sort_order' => $this->config->get('2c2p_sort_order')
    );
  
    return $method_data;
  }
}