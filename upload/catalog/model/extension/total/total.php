<?php
class ModelExtensionTotalTotal extends Model {
	public function getTotal($total) {		
		$this->load->language('extension/total/total');

		$total['totals'][] = array(
			'button' => false,
			'type'       => 'input-disabled',
			'code'       => 'total',
			'title'      => $this->language->get('text_total'),
			'value'      => max(0, $total['total']),
			'sort_order' => $this->config->get('total_sort_order'),
			'button_cancel' => false,
		);
	}
}