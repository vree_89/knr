<?php
class ModelExtensionShippingPos extends Model {

	function getQuote($address,$id_city){
		$method_data = array();
		$zone_id = $address['zone_id'] ;
		$country_id = $address['country_id'];
		$weight = round($this->cart->getWeight()*1000);			
		if($weight<1000){
			$weight = 1000; //in gr
		}
		
		//$destination = $this->session->data['shipping_address']['id_city'];
		$destination = $id_city;
		$sql = "SELECT `key`, `value` from " . DB_PREFIX . "setting where `code` = 'pos'";
		$pos = $this->db->query($sql);
		$pos_key = "";
		$pos_default_origin = "";
		$pos_services = "";
		foreach ($pos->rows as $result) {
			if($result['key']=="pos_key"){
				$pos_key = $result['value'];
				
			}
			if($result['key']=="pos_default_origin"){
				$pos_default_origin = $result['value'];
				
			}
			if($result['key']=="pos_services"){
				$pos_services = $result['value'];
				
			}

		}


		$curl = curl_init();
		/*
		  key* - API key (POST/HEAD)
		  origin* - id kota asal (POST)
		  destination* - id kota tujuan (POST)
		  weight* - berat dalam gram (POST)
		  courier* - kode courer:pos, pos, tiki (POST)
		*/ 

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "origin=$pos_default_origin&destination=$destination&weight=$weight&courier=pos",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return false;
		} else {
			$pos = json_decode($response,true);
			$_pos_services = explode(",",$pos_services);
			$status = $pos['rajaongkir']['status']['code']; //200 = OK
			if($status=="200"){
				$method_data = array(
					'title' => 'JNE',
					'quote' => array(),
					'sort_order' => '1',
					'error' => false,
					);
				$res = $pos['rajaongkir']['results']; //result of type of costs yes, regular, oke
				foreach ($res as $_res) {
					foreach($_res['costs'] as $costs){
						foreach($costs['cost'] as $_cost){
							if(in_array($costs['service'], $_pos_services) && !isset($method_data['quote'][strtolower($costs['service'])])){
							$method_data['quote'][strtolower($costs['service'])]['code'] = $_res['code'].".".strtolower($costs['service']);
							$method_data['quote'][strtolower($costs['service'])]['title'] = 'POS '.$costs['service'];
							$method_data['quote'][strtolower($costs['service'])]['cost'] = $_cost['value'];
							$method_data['quote'][strtolower($costs['service'])]['tax_class_id'] = $this->config->get('pos_tax_class_id');
							$method_data['quote'][strtolower($costs['service'])]['text'] = $this->currency->format($this->tax->calculate($_cost['value'], $this->config->get('pos_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']);


							}
						}
					}
				}
			}
			die('x');
			return $method_data;
		}



	}


	function getCity(){

		$sql = "SELECT `key`, `value` from " . DB_PREFIX . "setting where `code` = 'pos'";
		$pos = $this->db->query($sql);
		$pos_key = "";
		foreach ($pos->rows as $result) {
			if($result['key']=="pos_key"){
				$pos_key = $result['value'];				
			}
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: a70b524ce5e327cbd44550fe6ad5c9b5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return false;
		} else {
		  return json_decode($response,true);
		}		
	}

}





















